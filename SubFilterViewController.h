//
//  SubFilterViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 7/6/17.
//
//

#import <UIKit/UIKit.h>
#import "FilterButton.h"
#import "cUIButton.h"
#import "cUIScrollView.h"

@class SubFilterViewController;

@protocol SubFilterDelegate

-(void)didSelectThisOption:(SubFilterViewController*)subFilter index:(int)index;

@end

@interface SubFilterViewController : UIViewController <UIScrollViewDelegate, FilterButtonDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) FilterButton *filterButton;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic) NSInteger currentSelectedFilter;

@property (nonatomic, weak) id <SubFilterDelegate> delegate;

@end
