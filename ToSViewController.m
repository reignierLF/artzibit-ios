//
//  ToSViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 26/5/17.
//
//

#import "ToSViewController.h"

@interface ToSViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;

@end

@implementation ToSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initToS];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    self.view.backgroundColor = [UIColor whiteColor];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    backButton.icon = [UIImage imageNamed:@"left-arrow"];
    [_mainNavigationBarView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Terms of Service";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initToS{

    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(20, _mainNavigationBarView.frame.origin.y + _mainNavigationBarView.frame.size.height, _screenWidth - 40, _screenHeight - _mainNavigationBarView.frame.size.height)];
    textView.delegate = self;
    textView.editable = NO;
    textView.showsVerticalScrollIndicator = NO;
    textView.text = [self tosText];
    
    NSString *textViewText = textView.text;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:textViewText];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
                           //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Agreement Between User and Artzibit"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Modification of these terms of use"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Privacy"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Limited License"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Software"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Accounts & Security"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Reporting Intellectual Property Rights Infringement"]];
    
    [attributedText addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14]
     //value:[UIFont fontWithName:@"Courier" size:20]
                           range:[textViewText rangeOfString:@"Purchase and Payment"]];
    
    textView.attributedText = attributedText;
    
    [self.view addSubview:textView];
}

-(NSString*)tosText{

    NSString *text = [NSString stringWithFormat:@"\n\n%@\n\n%@\n\n%@\n\n%@\n%@%@%@%@%@%@%@\n\n%@\n\n%@\n\n%@%@%@%@%@\n\n%@%@%@\n\n%@%@%@%@%@\n\n",
                      @"Agreement Between User and Artzibit\n\nThe Artzibit mobile app is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the Artzibit mobile app constitutes your agreement to all such terms, conditions, and notices. Your use of the Artzibit mobile app may also be subject to additional terms outlined elsewhere on this app (the \"Additional Terms\").",
                      @"PLEASE READ THE FOLLOWING TERMS OF USE AND DISCLAIMERS CAREFULLY BEFORE USING THE ARTZIBIT MOBILE APP (this \"App\"). By accessing or using this App, you agree to these terms of use, conditions and all applicable laws. If you do not agree to these terms you may not use this App.",
                      @"Modification of these terms of use\n\nArtzibit reserves the right to change the terms, conditions, and notices under which the Artzibit mobile app is offered, including but not limited any charges associated with the use of the Artzibit mobile app. You are responsible for regularly reviewing these terms and conditions.",
                      @"Privacy\n\nTo better protect your rights we have provided the Artzibit Privacy Policy to explain our privacy practices in detail for you to understand how we collect and use the information associated with your Account and/or your use of the Services. By using the Services or providing information on the Site, you consent to Artzibit's collection, use, disclosure and/or processing of your Content and personal data as described in the Privacy Policy.",
                      @"\n\u2022 Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.\n",
                      @"\n\u2022 We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.\n",
                      @"\n\u2022 We will only retain personal information as long as necessary for the fulfillment of those purposes.\n",
                      @"\n\u2022 We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.\n",
                      @"\n\u2022 Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.\n",
                      @"\n\u2022 We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.\n",
                      @"\n\u2022 We will make readily available to customers’ information about our policies and practices relating to the management of personal information.",
                      @"Limited License\n\nArtzibit grants you a limited and revocable license to access and use the Services subject to the terms and conditions of these Terms of Service. All proprietary Content, trademarks, service marks, brand names, logos and other intellectual property (“Intellectual Property”) displayed in the Site are the property of Artzibit and where applicable, third party proprietors identified in the Site. No right or licence is granted directly or indirectly to any party accessing the Site to use or reproduce any Intellectual Property, and no party accessing the Site shall claim any right, title or interest therein. By using or accessing the Services you agree to comply with the copyright, trademark, service mark, and all other applicable laws that protect the Services, the App and its Content. You agree not to copy, distribute, republish, transmit, publicly display, publicly perform, modify, adapt, rent, sell, or create derivative works of any portion of the Services, the App or its Content. You also may not, without our prior written consent, mirror or frame any part or whole of the contents of this App on any other server or as part of any other website. In addition, you agree that you will not use any robot, spider or any other automatic device or manual process to monitor or copy our Content, without our prior written consent (such consent is deemed given for standard search engine technology employed by Internet search websites to direct Internet users to this website).",
                      @"Software\n\nAny software provided by us to you as part of the Services is subject to the provisions of these Terms of Service. Artzibit reserves all rights to the software not expressly granted by Artzibit hereunder. Any third-party scripts or code, linked to or referenced from the Services, are licensed to you by the third parties that own such scripts or code, not by Artzibit.",
                      @"Accounts & Security\n\nSome functions of our Services require registration for an Account by selecting a unique user identification (\"User ID\") and password, and by providing certain personal information. If you select a User ID that Artzibit, in its sole discretion, finds offensive or inappropriate, Artzibit has the right to suspend or terminate your Account. You may be able to use your Account to gain access to other products, websites or services to which we have enabled access or with which we have tied up or collaborated. Artzibit has not reviewed, and assumes no responsibility for any third party content, functionality, security, services, privacy policies, or other practices of those products, websites or services. If you do so, the terms of service for those products, websites or services, including their respective privacy policies, if different from these Terms of Service and/or our Privacy Policy, may also apply to your use of those products, websites or services.",
                      @"\n\nYou agree to (a) keep your password confidential and use only your User ID and password when logging in, (b) ensure that you log out from your account at the end of each session on the Site, (c) immediately notify Artzibit of any unauthorised use of your Account, User ID and/or password, and (d) ensure that your Account information is accurate and up-to-date. You are fully responsible for all activities that occur under your User ID and Account even if such activities or uses were not committed by you. Artzibit will not be liable for any loss or damage arising from unauthorised use of your password or your failure to comply with this Section.",
                      @"\n\nYou agree that Artzibit may for any reason, in its sole discretion and with or without notice or liability to you or any third party, immediately terminate your Account and your User ID, and remove or discard from the Site any Content associated with your Account and User ID. Grounds for such termination may include, but are not limited to, (a) extended periods of inactivity, (b) violation of the letter or spirit of these Terms of Service, (c) illegal, fraudulent, harassing, defamatory, threatening or abusive behaviour (d) having multiple user accounts for illegitimate reasons, or (e) behaviour that is harmful to other Users, third parties, or the business interests of Artzibit. Use of an Account for illegal, fraudulent, harassing, defamatory, threatening or abusive purposes may be referred to law enforcement authorities without notice to you. If a legal dispute arises or law enforcement action is commenced relating to your Account or your use of the Services for any reason, Artzibit may terminate your Account immediately with or without notice.",
                      @"\n\nUsers may terminate their Account if they notify Artzibit in writing (including via email at support@artzibit.net) of their desire to do so. Notwithstanding any such termination, Users remain responsible and liable for any incomplete transaction (whether commenced prior to or after such termination), shipment of the product, payment for the product, or the like, and Users must contact Artzibit after he or she has promptly and effectively carried out and completed all incomplete transactions according to the Terms of Service. Artzibit shall have no liability, and shall not be liable for any damages incurred due to the actions taken in accordance with this section. Users waive any and all claims based on any such action taken by Artzibit.",
                      @"\n\nYou may only use the Services and/or open an Account if you are located in one of our approved countries, as updated from time to time.",
                      @"Reporting Intellectual Property Rights Infringement\n\nThe Users are independent individuals or businesses and they are not associated with Artzibit in any way. Artzibit is neither the agent nor representative of the Users and does not hold and/or own any of the merchandises listed on the Site.",
                      @"\n\nIf you are an intellectual property right owner (“IPR Owner”) or an agent duly authorised by an IPR Owner (“IPR Agent”) and you believe that your right or your principal’s right has been infringed, please notify us in writing by email to support@artzibit.net and provide us the documents requested below to support your claim. Do allow us time to process the information provided. Artzibit will respond to your complaint as soon as practicable.",
                      @"\n\nComplaints under this section must be provided in the form prescribed by Artzibit which may be updated from time to time, and must include at least the following: (a) a physical or electronic signature of an IPR Owner or IPR Agent (collectively, “Informant”); (b) a description of the type and nature of intellectual property right that is allegedly infringed and proof of rights; (c) details of the listing which contains the alleged infringement; (d) sufficient information to allow Artzibit to contact the Informant, such as Informant’s physical address, telephone number and e-mail address; (e) a statement by Informant that the complaint is filed on good faith belief and that the use of the intellectual property as identified by the Informant is not authorised by the IPR Owner or the law; (f) a statement by the Informant that the information in the notification is accurate, indemnify us for any damages we may suffer as a result of the information provided by and that the Informant has the appropriate right or is authorised to act on IPR Owner’s behalf to the complaint.",
                      @"Purchase and Payment\n\nArtzibit supports one or more of the following payment methods in each country it operates in:",
                      @"\n\u2022 Credit Card\no\tCard payments are processed through third-party payment channels and the type of credit cards accepted by these payment channels may vary depending on the jurisdiction you are in.",
                      @"\n\u2022 Cash on Delivery (COD)\no\tArtzibit provides COD services in selected countries. Buyers may pay cash directly to the deliver agent upon their receipt of the purchased item.",
                      @"\n\nBuyer may only change their preferred mode of payment for their purchase prior to making payment.",
                      @"\n\nArtzibit takes no responsibility and assume no liability for any loss or damages to Buyer arising from shipping information and/or payment information entered by Buyer or wrong remittance by Buyer in connection with the payment for the items purchased. We reserve the right to check whether Buyer is duly authorised to use certain payment method, and may suspend the transaction until such authorisation is confirmed or cancel the relevant transaction where such confirmation is not available."
                      ];
    return text;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    NSLog(@"scrolling offset = %f",scrollView.contentOffset.y);
    
    if(scrollView.contentOffset.y > 50){
    
        NSLog(@"disable scrolling to center");
    }else{
    
        NSLog(@"enable scrolling to center");
    }
}

-(void)back{

    [self.navigationController popViewControllerAnimated:YES];
}
@end
