//
//  FilterButton.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 6/6/17.
//
//

#import "FilterButton.h"

@implementation FilterButton

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, (self.frame.size.width / 2) - 30, self.frame.size.height)];
        //_leftLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
        _leftLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_leftLabel];
        
        _rightLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width / 2), 0, (self.frame.size.width / 2) - 30, self.frame.size.height)];
        //_rightLabel.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.3];
        _rightLabel.textAlignment = NSTextAlignmentRight;
        _rightLabel.numberOfLines = 0;
        _rightLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:_rightLabel];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        //button.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
        button.tag = self.tag;
        [self addSubview:button];
        
        [button addTarget:self action:@selector(didClick) forControlEvents:UIControlEventTouchUpInside];
        
        float checkWidth = self.frame.size.height / 4;
        float checkHeight = checkWidth;
        
        _check = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check-mark"]];
        _check.frame = CGRectMake(self.frame.size.width - (checkWidth + 30), (self.frame.size.height / 2) - (checkHeight / 2), checkWidth, checkHeight);
        _check.userInteractionEnabled = NO;
        _check.hidden = YES;
        [self addSubview:_check];
        
        _seperator = [[UIView alloc] initWithFrame:CGRectMake(20, self.frame.size.height - 1, self.frame.size.width - 40, 1)];
        _seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
        [self addSubview:_seperator];
    }
    
    return self;
}

-(void)didClick{

    [_delegate didClick:self];
    //NSLog(@"tag = %ld",(long)self.tag);
}

@end
