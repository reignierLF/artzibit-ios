//
//  CustomDetailViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 8/6/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIScrollView.h"
#import "cUIButton.h"
#import "cUITextField.h"

#import "Api.h"
#import "ArtDetails.h"

@class CustomDetailViewController;

@protocol CustomDetailDelegate
-(void)showUnityAR:(CustomDetailViewController*)customDetail;
@end

@interface CustomDetailViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) NSString *artId;
@property (nonatomic, strong) NSString *artName;
@property (nonatomic, strong) NSString *artArtistName;
@property (nonatomic, strong) NSString *artImage;
@property (nonatomic, strong) NSString *artPrice;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) ArtDetails *artDetails;

@property (nonatomic, weak) id <CustomDetailDelegate> delegate;

@end
