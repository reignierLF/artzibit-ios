//
//  FilterButton.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 6/6/17.
//
//

#import <UIKit/UIKit.h>

@class FilterButton;

@protocol FilterButtonDelegate

-(void)didClick:(FilterButton*)filterButton;

@end

@interface FilterButton : UIView

@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, strong) UIImageView *check;
@property (nonatomic, strong) UIView *seperator;

@property (nonatomic, weak) id <FilterButtonDelegate> delegate;

@end
