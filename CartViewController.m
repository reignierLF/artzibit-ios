//
//  CartViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import "CartViewController.h"

@interface CartViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;
@property (nonatomic, strong) UIView *checkOutView;
@property (nonatomic, strong) cUIScrollView *listScrollView;
@property (nonatomic, strong) NSMutableArray *itemCellArray;

@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initCheckout];
    [self initListItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = _backgroundColor;
    
    _itemCellArray = [[NSMutableArray alloc] init];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *mainButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    mainButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    mainButton.icon = [UIImage imageNamed:@"left-arrow"];
    [_mainNavigationBarView addSubview:mainButton];
    
    [mainButton addTarget:self action:@selector(navigateToMain) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Cart";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initCheckout{
    
    float height = (_screenHeight - _mainNavigationBarView.frame.size.height) / 3;
    
    _checkOutView = [[UIView alloc] initWithFrame:CGRectMake(0, _screenHeight - height, _screenWidth, height)];
    //_checkOutView.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:_checkOutView];
    
    UILabel *shippingLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 5, (_checkOutView.frame.size.width - 60) / 2, 25)];
    //shippingLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    shippingLabel.text = @"Shipping";
    shippingLabel.textAlignment = NSTextAlignmentLeft;
    shippingLabel.textColor = [UIColor blackColor];
    shippingLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_checkOutView addSubview:shippingLabel];
    
    UILabel *shippingTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth - (shippingLabel.frame.size.width + 25), shippingLabel.frame.origin.y, shippingLabel.frame.size.width, shippingLabel.frame.size.height)];
    //shippingTotalLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    shippingTotalLabel.text = @"$9999.00";
    shippingTotalLabel.textAlignment = NSTextAlignmentRight;
    shippingTotalLabel.textColor = [UIColor blackColor];
    shippingTotalLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_checkOutView addSubview:shippingTotalLabel];
    
    UILabel *installationLabel = [[UILabel alloc] initWithFrame:CGRectMake(shippingLabel.frame.origin.x, shippingLabel.frame.origin.y + shippingLabel.frame.size.height, shippingLabel.frame.size.width, shippingLabel.frame.size.height)];
    //installationLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    installationLabel.text = @"Installation fees";
    installationLabel.textAlignment = NSTextAlignmentLeft;
    installationLabel.textColor = [UIColor blackColor];
    installationLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_checkOutView addSubview:installationLabel];
    
    UILabel *installationTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(shippingTotalLabel.frame.origin.x, installationLabel.frame.origin.y, shippingTotalLabel.frame.size.width, shippingTotalLabel.frame.size.height)];
    //installationTotalLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    installationTotalLabel.text = @"$9999.00";
    installationTotalLabel.textAlignment = NSTextAlignmentRight;
    installationTotalLabel.textColor = [UIColor blackColor];
    installationTotalLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    [_checkOutView addSubview:installationTotalLabel];
    
    UILabel *discountLabel = [[UILabel alloc] initWithFrame:CGRectMake(installationLabel.frame.origin.x, installationLabel.frame.origin.y + installationLabel.frame.size.height, installationLabel.frame.size.width, installationLabel.frame.size.height)];
    //installationLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    discountLabel.text = @"Discount";
    discountLabel.textAlignment = NSTextAlignmentLeft;
    discountLabel.textColor = [UIColor blackColor];
    discountLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    //[_checkOutView addSubview:discountLabel];
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(discountLabel.frame.origin.x, discountLabel.frame.origin.y + discountLabel.frame.size.height, _checkOutView.frame.size.width - 60, 1)];
    seperatorView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
    [_checkOutView addSubview:seperatorView];
    
    UILabel *subTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(seperatorView.frame.origin.x, seperatorView.frame.origin.y + seperatorView.frame.size.height, shippingTotalLabel.frame.size.width, shippingTotalLabel.frame.size.height + 10)];
    //subTotalLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    subTotalLabel.text = @"SUBTOTAL";
    subTotalLabel.textAlignment = NSTextAlignmentLeft;
    subTotalLabel.textColor = [UIColor blackColor];
    subTotalLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_checkOutView addSubview:subTotalLabel];
    
    UILabel *totalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(installationTotalLabel.frame.origin.x, subTotalLabel.frame.origin.y, installationTotalLabel.frame.size.width, subTotalLabel.frame.size.height)];
    //totalPriceLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    totalPriceLabel.text = @"$9999.00";
    totalPriceLabel.textAlignment = NSTextAlignmentRight;
    totalPriceLabel.textColor = [UIColor blackColor];
    totalPriceLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    [_checkOutView addSubview:totalPriceLabel];
    
    UIButton *promoCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    promoCodeButton.frame = CGRectMake(30, _checkOutView.frame.size.height - 80, ((_checkOutView.frame.size.width - 60) / 2) - 5, 60);
    promoCodeButton.backgroundColor = [UIColor whiteColor];
    promoCodeButton.layer.cornerRadius = 60 / 3;
    promoCodeButton.layer.borderColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0].CGColor;
    promoCodeButton.layer.borderWidth = 1;
    [promoCodeButton setTitle:@"Apply Code" forState:UIControlStateNormal];
    [promoCodeButton setTitleColor:[UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0] forState:UIControlStateNormal];
    //promoCodeButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:14];
    promoCodeButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    //button.titleLabel.textColor = [UIColor blackColor];
    [_checkOutView addSubview:promoCodeButton];
    
    //[promoCodeButton addTarget:self action:@selector(pressedButton:) forControlEvents:UIControlEventTouchUpInside];

    UIButton *checkOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkOutButton.frame = CGRectMake(_screenWidth - (promoCodeButton.frame.size.width + promoCodeButton.frame.origin.x), promoCodeButton.frame.origin.y, promoCodeButton.frame.size.width, promoCodeButton.frame.size.height);
    checkOutButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    checkOutButton.layer.cornerRadius = 60 / 3;
    [checkOutButton setTitle:@"Checkout" forState:UIControlStateNormal];
    [checkOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //checkOutButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:14];
    checkOutButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:18];
    //button.titleLabel.textColor = [UIColor blackColor];
    [_checkOutView addSubview:checkOutButton];
    
    //[checkOutButton addTarget:self action:@selector(pressedButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initListItem{
    
    int size = 4;

    _listScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _mainNavigationBarView.frame.origin.y + _mainNavigationBarView.frame.size.height, _screenWidth, (_screenHeight - _mainNavigationBarView.frame.size.height) - _checkOutView.frame.size.height)];
    //_listScrollView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.8];
    _listScrollView.delegate = self;
    _listScrollView.contentSize = CGSizeMake(_listScrollView.frame.size.width, (_listScrollView.frame.size.height / 3) * size);
    _listScrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_listScrollView];
    
    float cellHeight = _listScrollView.contentSize.height / size;
    
    for (int i = 0; i < size; i++) {
        
        ItemCell *itemCell = [[ItemCell alloc] initWithFrame:CGRectMake(0, cellHeight * i, _listScrollView.frame.size.width, cellHeight)];
        //itemCell.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.6];
        itemCell.delegate = self;
        itemCell.maxSize = size;
        itemCell.parentView = _listScrollView;
        itemCell.tag = i;
        itemCell.price = @"$999.00";
        itemCell.title = @"title art here";
        itemCell.size = @"33\"x44\"";
        [_listScrollView addSubview:itemCell];
        
        [_itemCellArray addObject:itemCell];
    }
}

#pragma mark Delegates

-(void)navigateToMain{
    
    [_delegate goToMain];
}

-(void)removeItem:(ItemCell *)itemCell{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"This action will delete this item from your cart." message:@"Do you still wish to proceed?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancel];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             itemCell.alpha = 0.0;
                             itemCell.frame = CGRectMake(-itemCell.frame.size.width, itemCell.frame.origin.y, itemCell.frame.size.width, itemCell.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                                 //[self removeFromSuperview];
                             }
                         }];
        
        if(_itemCellArray.count != 1){
            
            for (int i = (int)itemCell.tag + 1; i < _itemCellArray.count; i++) {
                
                ItemCell *newItemCellRef = [_itemCellArray objectAtIndex:i];
                
                [UIView animateWithDuration:0.3
                                      delay:0.0
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^{
                                     
                                     newItemCellRef.frame = CGRectMake(newItemCellRef.frame.origin.x, newItemCellRef.frame.origin.y - itemCell.frame.size.height, newItemCellRef.frame.size.width, newItemCellRef.frame.size.height);
                                 }
                                 completion:^(BOOL finished1){
                                     if(finished1){
                                         
                                     }
                                 }];
            }
        }
        
        [_itemCellArray removeObjectAtIndex:itemCell.tag];
        
        for (int i = 0; i < _itemCellArray.count; i++) {
            [[_itemCellArray objectAtIndex:i] setTag:i];
        }
        
        NSLog(@"%@",_itemCellArray);
    }];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
