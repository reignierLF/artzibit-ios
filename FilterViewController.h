//
//  FIlterViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 6/6/17.
//
//

#import <UIKit/UIKit.h>
#import "FilterButton.h"
#import "SubFilterViewController.h"

#import "Api.h"
#import "Filters.h"

@interface FilterViewController : UIViewController <FilterButtonDelegate, SubFilterDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) Filters *filters;

@property (nonatomic, strong) UINavigationController *filterNavigationController;
@property (nonatomic, strong) FilterButton *filterButton;
@property (nonatomic, strong) SubFilterViewController *subFilterVC;

@end
