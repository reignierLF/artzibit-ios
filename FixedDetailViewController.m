//
//  FixedDetailViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 8/6/17.
//
//

#import "FixedDetailViewController.h"

#import "ImageLoader.h"

@interface FixedDetailViewController ()

@property (nonatomic, strong) cUIScrollView *scrollView;

@property(nonatomic, strong) UIImageView *artBlurImageView;
@property(nonatomic, strong) UIImageView *artImageView;

@property (nonatomic, strong) UIView *buttonvView;
@property (nonatomic, strong) UIView *detailView;

@end

@implementation FixedDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initScrollView];
    [self initArtImage];
    [self initButtons];
    [self initBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _api = [[Api alloc] init];
}

-(void)initScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = _backgroundColor;
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height * 2);
    //_scrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_scrollView];
}

-(void)initArtImage{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    _artBlurImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight / 2)];
    //_artBlurImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    _artBlurImageView.contentMode = UIViewContentModeScaleAspectFill;
    //_artBlurImageView.image = [UIImage imageNamed:@"checker"];
    _artBlurImageView.clipsToBounds = YES;
    [_scrollView addSubview:_artBlurImageView];
    
    [il parseImage:_artBlurImageView url:_artImage errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = _artBlurImageView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [_scrollView addSubview:blurEffectView];
    } else {
        _artBlurImageView.backgroundColor = [UIColor blackColor];
    }
    
    float width = _artBlurImageView.frame.size.width - ((_artBlurImageView.frame.size.width / 10) * 2);
    float height = _artBlurImageView.frame.size.height - ((_artBlurImageView.frame.size.height / 10) * 2);
    
    _artImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_artBlurImageView.frame.size.width / 10, _artBlurImageView.frame.size.height / 10, width, height)];
    //_artBlurImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    _artImageView.contentMode = UIViewContentModeScaleAspectFill;
    //_artImageView.image = [UIImage imageNamed:@"checker"];
    _artImageView.clipsToBounds = YES;
    _artImageView.layer.shadowColor =    [UIColor blackColor].CGColor;
    _artImageView.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    _artImageView.layer.shadowOpacity =  0.2;
    _artImageView.layer.shadowRadius =   5.0;
    [_scrollView addSubview:_artImageView];
    
    [il parseImage:_artImageView url:_artImage errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
}

-(void)initButtons{
    
    _buttonvView = [[UIView alloc] initWithFrame:CGRectMake(0, _artBlurImageView.frame.origin.y + _artBlurImageView.frame.size.height, _screenWidth, 100)];
    //view.backgroundColor = [UIColor greenColor];
    [_scrollView addSubview:_buttonvView];
    
    float height = _buttonvView.frame.size.height - (_buttonvView.frame.size.height / 4);
    float columnWidth = _buttonvView.frame.size.width / 3;
    int divider = 8;
    
    cUIButton *faveButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    faveButton.frame = CGRectMake(((columnWidth / 2) - (height / 2)) + (columnWidth / divider), ((_buttonvView.frame.size.height + 50) / 2) - (height / 2), height, height);
    faveButton.backgroundColor = [UIColor colorWithRed:80.0f/255.0f green:193.0f/255.0f blue:233.0f/255.0f alpha:1.0];
    faveButton.layer.cornerRadius = faveButton.frame.size.width / 2;
    faveButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    faveButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    faveButton.layer.shadowOpacity =  0.2;
    faveButton.layer.shadowRadius =   5.0;
    faveButton.icon = [UIImage imageNamed:@"star"];
    [_buttonvView addSubview:faveButton];
    
    //[faveButton addTarget:self action:@selector(addToFaveEvent) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *arButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    arButton.frame = CGRectMake((faveButton.frame.origin.x + columnWidth) - (columnWidth / divider), faveButton.frame.origin.y, faveButton.frame.size.width, faveButton.frame.size.height);
    arButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    arButton.layer.cornerRadius = arButton.frame.size.width / 2;
    arButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    arButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    arButton.layer.shadowOpacity =  0.2;
    arButton.layer.shadowRadius =   5.0;
    [_buttonvView addSubview:arButton];
    
    arButton.label.text = @"AR";
    arButton.label.textColor = [UIColor whiteColor];
    arButton.label.font = [UIFont systemFontOfSize:26];
    
    [arButton addTarget:self action:@selector(showUnityAR) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *addToCartButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    addToCartButton.frame = CGRectMake((faveButton.frame.origin.x + (columnWidth * 2)) - ((columnWidth / divider) * 2), arButton.frame.origin.y, arButton.frame.size.width, arButton.frame.size.height);
    addToCartButton.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:206.0f/255.0f blue:85.0f/255.0f alpha:1.0];
    addToCartButton.layer.cornerRadius = addToCartButton.frame.size.width / 2;
    addToCartButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    addToCartButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    addToCartButton.layer.shadowOpacity =  0.2;
    addToCartButton.layer.shadowRadius =   5.0;
    addToCartButton.icon = [UIImage imageNamed:@"cart"];
    [_buttonvView addSubview:addToCartButton];
    
    //[addToCartButton addTarget:self action:@selector(addToCartEvent) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonvView.frame = CGRectMake(_buttonvView.frame.origin.x, _buttonvView.frame.origin.y, _buttonvView.frame.size.width, _buttonvView.frame.size.height + 50);
    
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, _buttonvView.frame.size.height - 5, _screenWidth, 5)];
    seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
    [_buttonvView addSubview:seperator];
}

-(void)initBackButton{
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 80, 40)];
    backView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    [_scrollView addSubview:backView];
    
    UIBezierPath *cornerMaskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(backView.frame.size.height / 2, backView.frame.size.height / 2)];
    
    CAShapeLayer *cornerMaskLayer = [[CAShapeLayer alloc] init];
    cornerMaskLayer.frame = backView.bounds;
    cornerMaskLayer.path  = cornerMaskPath.CGPath;
    backView.layer.mask = cornerMaskLayer;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(10, -10, backView.frame.size.width - 20, backView.frame.size.height + 20);
    backButton.icon = [UIImage imageNamed:@"left-arrow"];
    backButton.clipsToBounds = YES;
    [backView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
}

-(void)showUnityAR{
    
    [_delegate showUnityAR:self];
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
