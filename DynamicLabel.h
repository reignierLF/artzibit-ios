//
//  DynamicLabel.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 24/5/17.
//
//

#import <UIKit/UIKit.h>

@class DynamicLabel;

@protocol DynamicLabelDelegate

-(void)didSelectAtIndex:(NSInteger)index dynamicLabel:(DynamicLabel*)dynamicLabel;

@end

@interface DynamicLabel : UILabel

@property (nonatomic, strong) NSArray *linkArray;
@property (nonatomic, weak) id <DynamicLabelDelegate> delegate;

@end
