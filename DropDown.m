//
//  DropDown.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 17/5/17.
//
//

#import "DropDown.h"

@interface DropDown ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) NSMutableArray *buttonArray;
@property (nonatomic, strong) UIView *arrowUp;
@property (nonatomic) CGPoint touchButtonPoint;

@property (nonatomic, strong) UIImageView *downDirection;
@property (nonatomic) float titleWidth;

@property (nonatomic, strong) CAShapeLayer *mask;

@end

@implementation DropDown

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    _buttonArray = [[NSMutableArray alloc] init];
    
    _window = [UIApplication sharedApplication].keyWindow;
    
    _viewWidth = _window.frame.size.width;
    _viewHeight = _window.frame.size.height;
    
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _backgroundView.alpha = 0.0;
    _backgroundView.userInteractionEnabled = NO;
    [self addSubview:_backgroundView];
    
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){10, 0}];
    [path addLineToPoint:(CGPoint){20, 20}];
    [path addLineToPoint:(CGPoint){0, 20}];
    [path addLineToPoint:(CGPoint){10, 0}];
    
    _mask = [CAShapeLayer new];
    _mask.path = path.CGPath;
    
    for (int i = 0; i < _titleArray.count; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, 0, 0);
        button.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
        button.layer.cornerRadius = self.frame.size.height / 8;
        [button setTitle:[_titleArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:14];
        button.tag = i;
        //button.titleLabel.textColor = [UIColor blackColor];
        [_backgroundView addSubview:button];
        
        [button addTarget:self action:@selector(pressedButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_buttonArray addObject:button];
    }
    
    float directionWidth = 10;
    float directionHeight = directionWidth;
    _titleWidth = [self widthOfString:@"Fixed"];
    
    _downDirection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down-Direction-w"]];
    _downDirection.frame = CGRectMake((self.frame.size.width / 2) + (_titleWidth / 2) + 5, (self.frame.size.height / 2) - (directionHeight / 2), directionWidth, directionHeight);
    _downDirection.userInteractionEnabled = NO;
    [self addSubview:_downDirection];
    
    //_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    //_label.backgroundColor = [UIColor orangeColor];
    //_label.textAlignment = NSTextAlignmentCenter;
    //[self addSubview:_label];
    
    UITapGestureRecognizer *oneFingerTwoTaps = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedButton:)];
    oneFingerTwoTaps.delegate=self;
    oneFingerTwoTaps.delaysTouchesEnded = YES;
    oneFingerTwoTaps.delaysTouchesBegan = YES;
    // Set required taps and number of touches
    [oneFingerTwoTaps setNumberOfTapsRequired:1];
    //[oneFingerTwoTaps setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:oneFingerTwoTaps];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    _backgroundView.userInteractionEnabled = YES;
    
    [self startAnimating];
}

-(void)startAnimating{
    
    for (int i = 0; i < _buttonArray.count; i++) {
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             _backgroundView.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                             }
                         }];
     
        UIButton *newButtonRef = [_buttonArray objectAtIndex:i];
        
        newButtonRef.frame = CGRectMake(_touchButtonPoint.x + (self.frame.size.width / 2), _touchButtonPoint.y + (self.frame.size.height / 2), newButtonRef.frame.size.width, newButtonRef.frame.size.height);
        
        [UIView animateWithDuration:0.25
                              delay:(float)i * 0.1
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             //newButtonRef.frame = CGRectMake(newButtonRef.frame.origin.x, self.frame.origin.y + 10 + self.frame.size.height + ((self.frame.size.height - 5) * i), newButtonRef.frame.size.width, newButtonRef.frame.size.height);
                             
                             NSLog(@"%f = %f",_touchButtonPoint.y,(_backgroundView.frame.size.height / 2));
                             
                             if(_touchButtonPoint.y > (_backgroundView.frame.size.height / 2)){
                                 
                                newButtonRef.frame = CGRectMake(newButtonRef.frame.origin.x, (_touchButtonPoint.y - (self.frame.size.height / 1.15)) - ((self.frame.size.height - 5) * i), newButtonRef.frame.size.width, newButtonRef.frame.size.height);
                             }else{
                             
                                newButtonRef.frame = CGRectMake(newButtonRef.frame.origin.x, _touchButtonPoint.y + (self.frame.size.height / 1.15) + ((self.frame.size.height - 5) * i), newButtonRef.frame.size.width, newButtonRef.frame.size.height);
                             }
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                             }
                         }];
        
        [UIView animateWithDuration:0.25
                              delay:(float)i * 0.2
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             newButtonRef.frame = CGRectMake(self.frame.origin.x + (self.frame.size.width / 10), newButtonRef.frame.origin.y, self.frame.size.width - (self.frame.size.width / 5), self.frame.size.height - 10);
                         }
                         completion:^(BOOL finished1){
                             if(finished1){
                                 
                             }
                         }];
        
        if(i == 0){
            
            _arrowUp = [[UIView alloc] initWithFrame:CGRectMake(newButtonRef.frame.origin.x + ((newButtonRef.frame.size.width / 2) - 10), newButtonRef.frame.origin.y, 20, 20)];
            _arrowUp.backgroundColor = [UIColor whiteColor];
            _arrowUp.layer.mask = _mask;
            _arrowUp.alpha = 0.0;
            [_backgroundView addSubview:_arrowUp];
            
            [UIView animateWithDuration:0.15
                                  delay:0.1
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 
                                 _arrowUp.alpha = 1;
                                 
                                 if(_touchButtonPoint.y > (_backgroundView.frame.size.height / 2)){
                                     
                                     _arrowUp.transform = CGAffineTransformMakeRotation(M_PI);
                                     _arrowUp.frame = CGRectMake(_arrowUp.frame.origin.x, (newButtonRef.frame.origin.y + newButtonRef.frame.size.height) - (_arrowUp.frame.size.height / 2), _arrowUp.frame.size.width, _arrowUp.frame.size.height);
                                 }else{
                                     
                                     _arrowUp.frame = CGRectMake(_arrowUp.frame.origin.x, newButtonRef.frame.origin.y - (_arrowUp.frame.size.height / 2), _arrowUp.frame.size.width, _arrowUp.frame.size.height);
                                 }
                             }
                             completion:^(BOOL finished1){
                                 if(finished1){
                                     
                                 }
                             }];
        }
    }
}

-(void)pressedButton:(UIButton*)sender{
    
    //[self setTitle:[_titleArray objectAtIndex:sender.tag] forState:UIControlStateNormal];

    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         _backgroundView.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                             _backgroundView.userInteractionEnabled = NO;
                             
                             //_arrowUp.hidden = YES;
                             //_arrowUp.alpha = 0.0;
                             //_arrowUp.frame = CGRectMake(_arrowUp.frame.origin.x, self.frame.origin.y + self.frame.size.height + 20, _arrowUp.frame.size.width, _arrowUp.frame.size.height);
                             
                             [_arrowUp removeFromSuperview];
                             _arrowUp = nil;
                             
                             for (int i = 0; i < _buttonArray.count; i++) {
                                 
                                 UIButton *newButtonRef = [_buttonArray objectAtIndex:i];
                                 newButtonRef.frame = CGRectMake(self.frame.size.width / 2, self.frame.size.height / 2, 0, 0);
                                 newButtonRef.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
                                 newButtonRef.layer.cornerRadius = self.frame.size.height / 8;
                             }
                         }
                     }];
    
    [_delegate didSelectItem:self index:sender.tag];
    
    _titleWidth = [self widthOfString:self.titleLabel.text];
    _downDirection.frame = CGRectMake((self.frame.size.width / 2) + (_titleWidth / 2) + 5, _downDirection.frame.origin.y, _downDirection.frame.size.width, _downDirection.frame.size.height);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    [[[[UIApplication sharedApplication] windows] lastObject] addSubview:_backgroundView];
    
    CGPoint point = [touch locationInView:touch.view];
    CGPoint pointOnScreen = [touch.view convertPoint:point toView:_backgroundView];
    _touchButtonPoint = CGPointMake(pointOnScreen.x - point.x, pointOnScreen.y - point.y);
    NSLog(@"Point: %f, %f",_touchButtonPoint.x, _touchButtonPoint.y);
    NSLog(@"Touch");
    
    return NO;
}

- (CGFloat)widthOfString:(NSString *)string{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}
@end
