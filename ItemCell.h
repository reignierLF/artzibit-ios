//
//  ItemCell.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 18/5/17.
//
//

#import <UIKit/UIKit.h>
#import "DropDown.h"
#import "DropDownV2.h"

@class ItemCell;

@protocol ItemCellDelegate

-(void)removeItem:(ItemCell*)itemCell;

@end

@interface ItemCell : UIView <DropDownDelegate, DropDownV2Delegate>

@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *size;

@property (nonatomic) NSInteger maxSize;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, weak) id <ItemCellDelegate> delegate;

@end
