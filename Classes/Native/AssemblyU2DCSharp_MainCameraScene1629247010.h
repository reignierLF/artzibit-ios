﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainCameraScene
struct  MainCameraScene_t1629247010  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.WebCamTexture MainCameraScene::mCamera
	WebCamTexture_t1079476942 * ___mCamera_2;

public:
	inline static int32_t get_offset_of_mCamera_2() { return static_cast<int32_t>(offsetof(MainCameraScene_t1629247010, ___mCamera_2)); }
	inline WebCamTexture_t1079476942 * get_mCamera_2() const { return ___mCamera_2; }
	inline WebCamTexture_t1079476942 ** get_address_of_mCamera_2() { return &___mCamera_2; }
	inline void set_mCamera_2(WebCamTexture_t1079476942 * value)
	{
		___mCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCamera_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
