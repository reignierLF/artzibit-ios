﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController/UIItems
struct  UIItems_t3088753794  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button MenuController/UIItems::closeButton
	Button_t2872111280 * ___closeButton_0;
	// UnityEngine.UI.Button MenuController/UIItems::screenShotButton
	Button_t2872111280 * ___screenShotButton_1;
	// UnityEngine.UI.Button MenuController/UIItems::addCartButton
	Button_t2872111280 * ___addCartButton_2;
	// UnityEngine.UI.Text MenuController/UIItems::sizeText
	Text_t356221433 * ___sizeText_3;
	// UnityEngine.UI.Text MenuController/UIItems::totalPriceText
	Text_t356221433 * ___totalPriceText_4;
	// UnityEngine.GameObject MenuController/UIItems::detailPanel
	GameObject_t1756533147 * ___detailPanel_5;
	// UnityEngine.GameObject MenuController/UIItems::detectorPanel
	GameObject_t1756533147 * ___detectorPanel_6;

public:
	inline static int32_t get_offset_of_closeButton_0() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___closeButton_0)); }
	inline Button_t2872111280 * get_closeButton_0() const { return ___closeButton_0; }
	inline Button_t2872111280 ** get_address_of_closeButton_0() { return &___closeButton_0; }
	inline void set_closeButton_0(Button_t2872111280 * value)
	{
		___closeButton_0 = value;
		Il2CppCodeGenWriteBarrier(&___closeButton_0, value);
	}

	inline static int32_t get_offset_of_screenShotButton_1() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___screenShotButton_1)); }
	inline Button_t2872111280 * get_screenShotButton_1() const { return ___screenShotButton_1; }
	inline Button_t2872111280 ** get_address_of_screenShotButton_1() { return &___screenShotButton_1; }
	inline void set_screenShotButton_1(Button_t2872111280 * value)
	{
		___screenShotButton_1 = value;
		Il2CppCodeGenWriteBarrier(&___screenShotButton_1, value);
	}

	inline static int32_t get_offset_of_addCartButton_2() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___addCartButton_2)); }
	inline Button_t2872111280 * get_addCartButton_2() const { return ___addCartButton_2; }
	inline Button_t2872111280 ** get_address_of_addCartButton_2() { return &___addCartButton_2; }
	inline void set_addCartButton_2(Button_t2872111280 * value)
	{
		___addCartButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___addCartButton_2, value);
	}

	inline static int32_t get_offset_of_sizeText_3() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___sizeText_3)); }
	inline Text_t356221433 * get_sizeText_3() const { return ___sizeText_3; }
	inline Text_t356221433 ** get_address_of_sizeText_3() { return &___sizeText_3; }
	inline void set_sizeText_3(Text_t356221433 * value)
	{
		___sizeText_3 = value;
		Il2CppCodeGenWriteBarrier(&___sizeText_3, value);
	}

	inline static int32_t get_offset_of_totalPriceText_4() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___totalPriceText_4)); }
	inline Text_t356221433 * get_totalPriceText_4() const { return ___totalPriceText_4; }
	inline Text_t356221433 ** get_address_of_totalPriceText_4() { return &___totalPriceText_4; }
	inline void set_totalPriceText_4(Text_t356221433 * value)
	{
		___totalPriceText_4 = value;
		Il2CppCodeGenWriteBarrier(&___totalPriceText_4, value);
	}

	inline static int32_t get_offset_of_detailPanel_5() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___detailPanel_5)); }
	inline GameObject_t1756533147 * get_detailPanel_5() const { return ___detailPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_detailPanel_5() { return &___detailPanel_5; }
	inline void set_detailPanel_5(GameObject_t1756533147 * value)
	{
		___detailPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___detailPanel_5, value);
	}

	inline static int32_t get_offset_of_detectorPanel_6() { return static_cast<int32_t>(offsetof(UIItems_t3088753794, ___detectorPanel_6)); }
	inline GameObject_t1756533147 * get_detectorPanel_6() const { return ___detectorPanel_6; }
	inline GameObject_t1756533147 ** get_address_of_detectorPanel_6() { return &___detectorPanel_6; }
	inline void set_detectorPanel_6(GameObject_t1756533147 * value)
	{
		___detectorPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___detectorPanel_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
