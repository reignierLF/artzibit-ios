﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerlessMenuController/UIItems
struct UIItems_t3002935515;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerlessMenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m2992831972 (UIItems_t3002935515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
