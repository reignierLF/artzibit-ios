﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// System.String
struct String_t;
// PersistentScript
struct PersistentScript_t2409504826;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainSceneController
struct  MainSceneController_t1039965335  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button MainSceneController::closeButton
	Button_t2872111280 * ___closeButton_2;
	// UnityEngine.UI.Button MainSceneController::markerButton
	Button_t2872111280 * ___markerButton_3;
	// UnityEngine.UI.Button MainSceneController::markerlessButton
	Button_t2872111280 * ___markerlessButton_4;
	// UnityEngine.UI.Button MainSceneController::emailMarkerButton
	Button_t2872111280 * ___emailMarkerButton_5;
	// System.String MainSceneController::artImageUrl
	String_t* ___artImageUrl_6;
	// System.String MainSceneController::selectedWidth
	String_t* ___selectedWidth_7;
	// System.String MainSceneController::selectedHeight
	String_t* ___selectedHeight_8;
	// System.String MainSceneController::selectedPrice
	String_t* ___selectedPrice_9;
	// System.String MainSceneController::selectedWidthAmountPerCm
	String_t* ___selectedWidthAmountPerCm_10;
	// System.String MainSceneController::selectedHeightAmountPerCm
	String_t* ___selectedHeightAmountPerCm_11;
	// PersistentScript MainSceneController::persistentScript
	PersistentScript_t2409504826 * ___persistentScript_12;

public:
	inline static int32_t get_offset_of_closeButton_2() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___closeButton_2)); }
	inline Button_t2872111280 * get_closeButton_2() const { return ___closeButton_2; }
	inline Button_t2872111280 ** get_address_of_closeButton_2() { return &___closeButton_2; }
	inline void set_closeButton_2(Button_t2872111280 * value)
	{
		___closeButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___closeButton_2, value);
	}

	inline static int32_t get_offset_of_markerButton_3() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___markerButton_3)); }
	inline Button_t2872111280 * get_markerButton_3() const { return ___markerButton_3; }
	inline Button_t2872111280 ** get_address_of_markerButton_3() { return &___markerButton_3; }
	inline void set_markerButton_3(Button_t2872111280 * value)
	{
		___markerButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___markerButton_3, value);
	}

	inline static int32_t get_offset_of_markerlessButton_4() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___markerlessButton_4)); }
	inline Button_t2872111280 * get_markerlessButton_4() const { return ___markerlessButton_4; }
	inline Button_t2872111280 ** get_address_of_markerlessButton_4() { return &___markerlessButton_4; }
	inline void set_markerlessButton_4(Button_t2872111280 * value)
	{
		___markerlessButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___markerlessButton_4, value);
	}

	inline static int32_t get_offset_of_emailMarkerButton_5() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___emailMarkerButton_5)); }
	inline Button_t2872111280 * get_emailMarkerButton_5() const { return ___emailMarkerButton_5; }
	inline Button_t2872111280 ** get_address_of_emailMarkerButton_5() { return &___emailMarkerButton_5; }
	inline void set_emailMarkerButton_5(Button_t2872111280 * value)
	{
		___emailMarkerButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___emailMarkerButton_5, value);
	}

	inline static int32_t get_offset_of_artImageUrl_6() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___artImageUrl_6)); }
	inline String_t* get_artImageUrl_6() const { return ___artImageUrl_6; }
	inline String_t** get_address_of_artImageUrl_6() { return &___artImageUrl_6; }
	inline void set_artImageUrl_6(String_t* value)
	{
		___artImageUrl_6 = value;
		Il2CppCodeGenWriteBarrier(&___artImageUrl_6, value);
	}

	inline static int32_t get_offset_of_selectedWidth_7() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___selectedWidth_7)); }
	inline String_t* get_selectedWidth_7() const { return ___selectedWidth_7; }
	inline String_t** get_address_of_selectedWidth_7() { return &___selectedWidth_7; }
	inline void set_selectedWidth_7(String_t* value)
	{
		___selectedWidth_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectedWidth_7, value);
	}

	inline static int32_t get_offset_of_selectedHeight_8() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___selectedHeight_8)); }
	inline String_t* get_selectedHeight_8() const { return ___selectedHeight_8; }
	inline String_t** get_address_of_selectedHeight_8() { return &___selectedHeight_8; }
	inline void set_selectedHeight_8(String_t* value)
	{
		___selectedHeight_8 = value;
		Il2CppCodeGenWriteBarrier(&___selectedHeight_8, value);
	}

	inline static int32_t get_offset_of_selectedPrice_9() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___selectedPrice_9)); }
	inline String_t* get_selectedPrice_9() const { return ___selectedPrice_9; }
	inline String_t** get_address_of_selectedPrice_9() { return &___selectedPrice_9; }
	inline void set_selectedPrice_9(String_t* value)
	{
		___selectedPrice_9 = value;
		Il2CppCodeGenWriteBarrier(&___selectedPrice_9, value);
	}

	inline static int32_t get_offset_of_selectedWidthAmountPerCm_10() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___selectedWidthAmountPerCm_10)); }
	inline String_t* get_selectedWidthAmountPerCm_10() const { return ___selectedWidthAmountPerCm_10; }
	inline String_t** get_address_of_selectedWidthAmountPerCm_10() { return &___selectedWidthAmountPerCm_10; }
	inline void set_selectedWidthAmountPerCm_10(String_t* value)
	{
		___selectedWidthAmountPerCm_10 = value;
		Il2CppCodeGenWriteBarrier(&___selectedWidthAmountPerCm_10, value);
	}

	inline static int32_t get_offset_of_selectedHeightAmountPerCm_11() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___selectedHeightAmountPerCm_11)); }
	inline String_t* get_selectedHeightAmountPerCm_11() const { return ___selectedHeightAmountPerCm_11; }
	inline String_t** get_address_of_selectedHeightAmountPerCm_11() { return &___selectedHeightAmountPerCm_11; }
	inline void set_selectedHeightAmountPerCm_11(String_t* value)
	{
		___selectedHeightAmountPerCm_11 = value;
		Il2CppCodeGenWriteBarrier(&___selectedHeightAmountPerCm_11, value);
	}

	inline static int32_t get_offset_of_persistentScript_12() { return static_cast<int32_t>(offsetof(MainSceneController_t1039965335, ___persistentScript_12)); }
	inline PersistentScript_t2409504826 * get_persistentScript_12() const { return ___persistentScript_12; }
	inline PersistentScript_t2409504826 ** get_address_of_persistentScript_12() { return &___persistentScript_12; }
	inline void set_persistentScript_12(PersistentScript_t2409504826 * value)
	{
		___persistentScript_12 = value;
		Il2CppCodeGenWriteBarrier(&___persistentScript_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
