﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas
struct Canvas_t209405766;
// MenuController
struct MenuController_t848154101;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DetectHandler
struct  DetectHandler_t4061016557  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean DetectHandler::isDetected
	bool ___isDetected_2;
	// UnityEngine.Canvas DetectHandler::canvas
	Canvas_t209405766 * ___canvas_3;
	// MenuController DetectHandler::menuController
	MenuController_t848154101 * ___menuController_4;
	// Vuforia.TrackableBehaviour DetectHandler::mTrackableBehaviour
	TrackableBehaviour_t1779888572 * ___mTrackableBehaviour_5;
	// UnityEngine.Animator DetectHandler::detectorPanelAnimator
	Animator_t69676727 * ___detectorPanelAnimator_6;
	// UnityEngine.Animator DetectHandler::detailPanelAnimator
	Animator_t69676727 * ___detailPanelAnimator_7;
	// System.Boolean DetectHandler::skipAnimation
	bool ___skipAnimation_8;

public:
	inline static int32_t get_offset_of_isDetected_2() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___isDetected_2)); }
	inline bool get_isDetected_2() const { return ___isDetected_2; }
	inline bool* get_address_of_isDetected_2() { return &___isDetected_2; }
	inline void set_isDetected_2(bool value)
	{
		___isDetected_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___canvas_3)); }
	inline Canvas_t209405766 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t209405766 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t209405766 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_3, value);
	}

	inline static int32_t get_offset_of_menuController_4() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___menuController_4)); }
	inline MenuController_t848154101 * get_menuController_4() const { return ___menuController_4; }
	inline MenuController_t848154101 ** get_address_of_menuController_4() { return &___menuController_4; }
	inline void set_menuController_4(MenuController_t848154101 * value)
	{
		___menuController_4 = value;
		Il2CppCodeGenWriteBarrier(&___menuController_4, value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_5() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___mTrackableBehaviour_5)); }
	inline TrackableBehaviour_t1779888572 * get_mTrackableBehaviour_5() const { return ___mTrackableBehaviour_5; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_mTrackableBehaviour_5() { return &___mTrackableBehaviour_5; }
	inline void set_mTrackableBehaviour_5(TrackableBehaviour_t1779888572 * value)
	{
		___mTrackableBehaviour_5 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_5, value);
	}

	inline static int32_t get_offset_of_detectorPanelAnimator_6() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___detectorPanelAnimator_6)); }
	inline Animator_t69676727 * get_detectorPanelAnimator_6() const { return ___detectorPanelAnimator_6; }
	inline Animator_t69676727 ** get_address_of_detectorPanelAnimator_6() { return &___detectorPanelAnimator_6; }
	inline void set_detectorPanelAnimator_6(Animator_t69676727 * value)
	{
		___detectorPanelAnimator_6 = value;
		Il2CppCodeGenWriteBarrier(&___detectorPanelAnimator_6, value);
	}

	inline static int32_t get_offset_of_detailPanelAnimator_7() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___detailPanelAnimator_7)); }
	inline Animator_t69676727 * get_detailPanelAnimator_7() const { return ___detailPanelAnimator_7; }
	inline Animator_t69676727 ** get_address_of_detailPanelAnimator_7() { return &___detailPanelAnimator_7; }
	inline void set_detailPanelAnimator_7(Animator_t69676727 * value)
	{
		___detailPanelAnimator_7 = value;
		Il2CppCodeGenWriteBarrier(&___detailPanelAnimator_7, value);
	}

	inline static int32_t get_offset_of_skipAnimation_8() { return static_cast<int32_t>(offsetof(DetectHandler_t4061016557, ___skipAnimation_8)); }
	inline bool get_skipAnimation_8() const { return ___skipAnimation_8; }
	inline bool* get_address_of_skipAnimation_8() { return &___skipAnimation_8; }
	inline void set_skipAnimation_8(bool value)
	{
		___skipAnimation_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
