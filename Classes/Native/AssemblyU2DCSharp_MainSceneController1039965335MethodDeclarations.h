﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainSceneController
struct MainSceneController_t1039965335;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MainSceneController::.ctor()
extern "C"  void MainSceneController__ctor_m3083733182 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::_CloseUnity3dARView()
extern "C"  void MainSceneController__CloseUnity3dARView_m695634793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::_EmailRequestForMarker()
extern "C"  void MainSceneController__EmailRequestForMarker_m1326298853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::Start()
extern "C"  void MainSceneController_Start_m3927469810 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::Update()
extern "C"  void MainSceneController_Update_m2786355995 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::ArtImageUrlFromXcode(System.String)
extern "C"  void MainSceneController_ArtImageUrlFromXcode_m3604364818 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::SelectedWidthFromXcode(System.String)
extern "C"  void MainSceneController_SelectedWidthFromXcode_m824207294 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::SelectedHeightFromXcode(System.String)
extern "C"  void MainSceneController_SelectedHeightFromXcode_m1471352809 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::SelectedPriceFromXcode(System.String)
extern "C"  void MainSceneController_SelectedPriceFromXcode_m2321260611 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::SelectedWidthAmountPerCmFromXcode(System.String)
extern "C"  void MainSceneController_SelectedWidthAmountPerCmFromXcode_m2451641693 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::SelectedHeightAmountPerCmFromXcode(System.String)
extern "C"  void MainSceneController_SelectedHeightAmountPerCmFromXcode_m3839430516 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::closeEvent()
extern "C"  void MainSceneController_closeEvent_m2102478408 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::emailRequestForMarker()
extern "C"  void MainSceneController_emailRequestForMarker_m2739022044 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::loadSceneEvent(System.Int32)
extern "C"  void MainSceneController_loadSceneEvent_m3130337703 (MainSceneController_t1039965335 * __this, int32_t ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::<Start>m__0()
extern "C"  void MainSceneController_U3CStartU3Em__0_m1972807941 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSceneController::<Start>m__1()
extern "C"  void MainSceneController_U3CStartU3Em__1_m426926976 (MainSceneController_t1039965335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
