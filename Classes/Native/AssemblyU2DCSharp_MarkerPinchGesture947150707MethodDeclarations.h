﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerPinchGesture
struct MarkerPinchGesture_t947150707;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerPinchGesture::.ctor()
extern "C"  void MarkerPinchGesture__ctor_m1003233036 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerPinchGesture::Start()
extern "C"  void MarkerPinchGesture_Start_m678196352 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerPinchGesture::Update()
extern "C"  void MarkerPinchGesture_Update_m3899703603 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
