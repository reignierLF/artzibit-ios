﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PinchGesture/AccessableVariable
struct  AccessableVariable_t686036674  : public Il2CppObject
{
public:
	// System.Single PinchGesture/AccessableVariable::artFrameLossyScaleX
	float ___artFrameLossyScaleX_0;
	// System.Single PinchGesture/AccessableVariable::artFrameLossyScaleY
	float ___artFrameLossyScaleY_1;
	// System.Single PinchGesture/AccessableVariable::artFrameLossyScaleZ
	float ___artFrameLossyScaleZ_2;

public:
	inline static int32_t get_offset_of_artFrameLossyScaleX_0() { return static_cast<int32_t>(offsetof(AccessableVariable_t686036674, ___artFrameLossyScaleX_0)); }
	inline float get_artFrameLossyScaleX_0() const { return ___artFrameLossyScaleX_0; }
	inline float* get_address_of_artFrameLossyScaleX_0() { return &___artFrameLossyScaleX_0; }
	inline void set_artFrameLossyScaleX_0(float value)
	{
		___artFrameLossyScaleX_0 = value;
	}

	inline static int32_t get_offset_of_artFrameLossyScaleY_1() { return static_cast<int32_t>(offsetof(AccessableVariable_t686036674, ___artFrameLossyScaleY_1)); }
	inline float get_artFrameLossyScaleY_1() const { return ___artFrameLossyScaleY_1; }
	inline float* get_address_of_artFrameLossyScaleY_1() { return &___artFrameLossyScaleY_1; }
	inline void set_artFrameLossyScaleY_1(float value)
	{
		___artFrameLossyScaleY_1 = value;
	}

	inline static int32_t get_offset_of_artFrameLossyScaleZ_2() { return static_cast<int32_t>(offsetof(AccessableVariable_t686036674, ___artFrameLossyScaleZ_2)); }
	inline float get_artFrameLossyScaleZ_2() const { return ___artFrameLossyScaleZ_2; }
	inline float* get_address_of_artFrameLossyScaleZ_2() { return &___artFrameLossyScaleZ_2; }
	inline void set_artFrameLossyScaleZ_2(float value)
	{
		___artFrameLossyScaleZ_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
