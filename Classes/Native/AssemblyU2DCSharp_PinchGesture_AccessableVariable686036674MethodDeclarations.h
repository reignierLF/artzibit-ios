﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PinchGesture/AccessableVariable
struct AccessableVariable_t686036674;

#include "codegen/il2cpp-codegen.h"

// System.Void PinchGesture/AccessableVariable::.ctor()
extern "C"  void AccessableVariable__ctor_m3710508327 (AccessableVariable_t686036674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
