﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ArtFrame
struct ArtFrame_t69822562;
// PersistentScript
struct PersistentScript_t2409504826;
// System.Object
struct Il2CppObject;
// DetectHandler
struct DetectHandler_t4061016557;
// MenuController
struct MenuController_t848154101;
// UnityEngine.Animator
struct Animator_t69676727;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;
// DidFinishSplashScreenLoading
struct DidFinishSplashScreenLoading_t2427584111;
// ExtendTrackingController
struct ExtendTrackingController_t1670312339;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2654589389;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// FadeInOut
struct FadeInOut_t611841665;
// GetDistance
struct GetDistance_t254146547;
// LockAndUnlock
struct LockAndUnlock_t2349896792;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// MainCameraScene
struct MainCameraScene_t1629247010;
// UnityEngine.Renderer
struct Renderer_t257310565;
// MainSceneController
struct MainSceneController_t1039965335;
// Markerless
struct Markerless_t1009278913;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.UI.Text
struct Text_t356221433;
// Markerless/<GetCoordinates>c__Iterator0
struct U3CGetCoordinatesU3Ec__Iterator0_t2481679252;
// MarkerlessMenuController
struct MarkerlessMenuController_t3874082612;
// MarkerlessPinchGesture
struct MarkerlessPinchGesture_t1797694232;
// MarkerlessMenuController/UIItems
struct UIItems_t3002935515;
// MarkerlessPinchGesture/AccessableVariable
struct AccessableVariable_t3397361249;
// MarkerMenuController
struct MarkerMenuController_t1479643319;
// MarkerPinchGesture
struct MarkerPinchGesture_t947150707;
// MarkerMenuController/UIItems
struct UIItems_t2800976608;
// MarkerPinchGesture/AccessableVariable
struct AccessableVariable_t4249881888;
// MenuController/UIItems
struct UIItems_t3088753794;
// PinchGesture
struct PinchGesture_t1024774609;
// PinchGesture/AccessableVariable
struct AccessableVariable_t686036674;
// URLImage
struct URLImage_t3675617922;
// URLImage/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3672507052;
// VRIntegrationHelper
struct VRIntegrationHelper_t556656694;
// UnityEngine.Camera
struct Camera_t189460977;
// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t2687577327;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t2431285219;
// Vuforia.AndroidUnityPlayer
struct AndroidUnityPlayer_t852788525;
// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t3077176941;
// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t3249343815;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t2699667469;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t3622673382;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t2091399712;
// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t965510117;
// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t870608571;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t4009935945;
// Vuforia.Prop
struct Prop_t444071959;
// Vuforia.Surface
struct Surface_t2221641095;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t1082256726;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// Vuforia.GLErrorHandler
struct GLErrorHandler_t3809113141;
// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t3495034315;
// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t3656371703;
// Vuforia.MaskOutBehaviour
struct MaskOutBehaviour_t2994129365;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t3504654311;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t3836044259;
// Vuforia.PropBehaviour
struct PropBehaviour_t966064926;
// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t2111803406;
// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t2405314212;
// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t3400239837;
// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t3058161409;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t584991835;
// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t4184040062;
// Vuforia.VideoBackgroundBehaviour
struct VideoBackgroundBehaviour_t3161817952;
// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t2515041812;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t359035403;
// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t1383853028;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t3489038957;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t2478279366;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t4084926705;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t3327552701;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t3616801211;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t665872082;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2878458725;
// Vuforia.WordBehaviour
struct WordBehaviour_t3366478421;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t2386081773;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t2805337095;
// Vuforia.VuMarkAbstractBehaviour
struct VuMarkAbstractBehaviour_t1830666997;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t2060629989;
// Vuforia.VuforiaAbstractConfiguration
struct VuforiaAbstractConfiguration_t1891710424;
// Vuforia.VuforiaConfiguration
struct VuforiaConfiguration_t3823746026;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t2720985375;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t2494532455;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// Vuforia.WireframeTrackableEventHandler
struct WireframeTrackableEventHandler_t1535150527;
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494;
// Vuforia.WSAUnityPlayer
struct WSAUnityPlayer_t425981959;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_ArtFrame69822562.h"
#include "AssemblyU2DCSharp_ArtFrame69822562MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_PersistentScript2409504826.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "AssemblyU2DCSharp_DetectHandler4061016557.h"
#include "AssemblyU2DCSharp_DetectHandler4061016557MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "AssemblyU2DCSharp_MenuController848154101.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_MenuController_UIItems3088753794.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_DidFinishSplashScreenLoading2427584111.h"
#include "AssemblyU2DCSharp_DidFinishSplashScreenLoading2427584111MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "AssemblyU2DCSharp_ExtendTrackingController1670312339.h"
#include "AssemblyU2DCSharp_ExtendTrackingController1670312339MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharp_FadeInOut611841665.h"
#include "AssemblyU2DCSharp_FadeInOut611841665MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "AssemblyU2DCSharp_GetDistance254146547.h"
#include "AssemblyU2DCSharp_GetDistance254146547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576MethodDeclarations.h"
#include "AssemblyU2DCSharp_LockAndUnlock2349896792.h"
#include "AssemblyU2DCSharp_LockAndUnlock2349896792MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "AssemblyU2DCSharp_MainCameraScene1629247010.h"
#include "AssemblyU2DCSharp_MainCameraScene1629247010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType2454390065.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_WebCamDevice3983871389.h"
#include "UnityEngine_UnityEngine_WebCamDevice3983871389MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharp_MainSceneController1039965335.h"
#include "AssemblyU2DCSharp_MainSceneController1039965335MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "AssemblyU2DCSharp_Markerless1009278913.h"
#include "AssemblyU2DCSharp_Markerless1009278913MethodDeclarations.h"
#include "AssemblyU2DCSharp_Markerless_U3CGetCoordinatesU3Ec2481679252MethodDeclarations.h"
#include "AssemblyU2DCSharp_Markerless_U3CGetCoordinatesU3Ec2481679252.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService1617852714MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "UnityEngine_UnityEngine_LocationService1617852714.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus2482073234.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController3874082612.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController3874082612MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture1797694232.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController_UIItems3002935515.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture_Accessabl3397361249.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController_UIItems3002935515MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture1797694232MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture_Accessabl3397361249MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerMenuController1479643319.h"
#include "AssemblyU2DCSharp_MarkerMenuController1479643319MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture947150707.h"
#include "AssemblyU2DCSharp_MarkerMenuController_UIItems2800976608.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture_AccessableVar4249881888.h"
#include "AssemblyU2DCSharp_MarkerMenuController_UIItems2800976608MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture947150707MethodDeclarations.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture_AccessableVar4249881888MethodDeclarations.h"
#include "AssemblyU2DCSharp_MenuController848154101MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "AssemblyU2DCSharp_MenuController_UIItems3088753794MethodDeclarations.h"
#include "AssemblyU2DCSharp_PersistentScript2409504826MethodDeclarations.h"
#include "AssemblyU2DCSharp_PinchGesture1024774609.h"
#include "AssemblyU2DCSharp_PinchGesture1024774609MethodDeclarations.h"
#include "AssemblyU2DCSharp_PinchGesture_AccessableVariable686036674.h"
#include "AssemblyU2DCSharp_PinchGesture_AccessableVariable686036674MethodDeclarations.h"
#include "AssemblyU2DCSharp_URLImage3675617922.h"
#include "AssemblyU2DCSharp_URLImage3675617922MethodDeclarations.h"
#include "AssemblyU2DCSharp_URLImage_U3CStartU3Ec__Iterator03672507052MethodDeclarations.h"
#include "AssemblyU2DCSharp_URLImage_U3CStartU3Ec__Iterator03672507052.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485MethodDeclarations.h"
#include "System_Core_System_Action3226471752MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485.h"
#include "System_Core_System_Action3226471752.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1398758191MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1398758191.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2687577327.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs3732945727MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2687577327MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractB2070832277MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2699667469MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2699667469.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2234397143.h"
#include "mscorlib_System_Attribute542643598.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2234397143MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter648583075.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3267823770MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr665872082MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntime2075282796MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1951195598MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntime2075282796.h"
#include "mscorlib_System_Action_1_gen1951195598.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen245871341MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbst3509595417MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2023440477MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "mscorlib_System_Action_1_gen245871341.h"
#include "mscorlib_System_Action_1_gen2023440477.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeh2669615494.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703MethodDeclarations.h"
#include "Vuforia_UnityExtensions_iOS_Vuforia_VuforiaNativeI1210651633MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617MethodDeclarations.h"
#include "Vuforia_UnityExtensions_iOS_Vuforia_VuforiaNativeI1210651633.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom4122236588MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeh2669615494MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr665872082.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstra2805337095.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBeha1830666997.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1891710424.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon1891710424MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon3866211740MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractCon3866211740.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_NullUnityPlayer754446093.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer918240325.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkAbstractBeha1830666997MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573MethodDeclarations.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m4211544902(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PersistentScript>()
#define GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(__this, method) ((  PersistentScript_t2409504826 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<MenuController>()
#define Component_GetComponent_TisMenuController_t848154101_m1169873364(__this, method) ((  MenuController_t848154101 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t69676727_m2717502299(__this, method) ((  Animator_t69676727 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, method) ((  TrackableBehaviour_t1779888572 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_GetComponent_TisImageTargetBehaviour_t2654589389_m2018094066(__this, method) ((  ImageTargetBehaviour_t2654589389 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ArtFrame>()
#define GameObject_GetComponent_TisArtFrame_t69822562_m457944649(__this, method) ((  ArtFrame_t69822562 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ExtendTrackingController>()
#define Component_GetComponent_TisExtendTrackingController_t1670312339_m1331458250(__this, method) ((  ExtendTrackingController_t1670312339 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2539673265_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2539673265(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2539673265_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
#define Resources_Load_TisSprite_t309593783_m1838468444(__this /* static, unused */, p0, method) ((  Sprite_t309593783 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2539673265_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m697787402(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<GetDistance>()
#define GameObject_GetComponent_TisGetDistance_t254146547_m3541233510(__this, method) ((  GetDistance_t254146547 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MarkerlessPinchGesture>()
#define GameObject_GetComponent_TisMarkerlessPinchGesture_t1797694232_m1222991259(__this, method) ((  MarkerlessPinchGesture_t1797694232 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MarkerlessMenuController>()
#define GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599(__this, method) ((  MarkerlessMenuController_t3874082612 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<DetectHandler>()
#define GameObject_GetComponent_TisDetectHandler_t4061016557_m4178546280(__this, method) ((  DetectHandler_t4061016557 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MarkerPinchGesture>()
#define GameObject_GetComponent_TisMarkerPinchGesture_t947150707_m2758117480(__this, method) ((  MarkerPinchGesture_t947150707 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<MarkerMenuController>()
#define GameObject_GetComponent_TisMarkerMenuController_t1479643319_m3421409556(__this, method) ((  MarkerMenuController_t1479643319 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m4211544902_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m2803939486(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m1978993906(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.HideExcessAreaAbstractBehaviour>()
#define Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011(__this, method) ((  HideExcessAreaAbstractBehaviour_t2687577327 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2641795278_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m2641795278(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2641795278_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t2431285219_m4080560834(__this, method) ((  BackgroundPlaneBehaviour_t2431285219 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2641795278_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m2472981332_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m2472981332(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m2472981332_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m2736443864(__this /* static, unused */, p0, method) ((  List_1_t2699667469 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m2472981332_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t4009935945_m3509900328(__this, method) ((  ReconstructionBehaviour_t4009935945 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared (Component_t3819376471 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m4164830438(__this, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, p0, method) ((  RendererU5BU5D_t2810717544* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, p0, method) ((  ColliderU5BU5D_t462843629* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477(__this, method) ((  MeshRenderer_t1268241104 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m2231772097(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(__this, method) ((  ComponentFactoryStarterBehaviour_t3249343815 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m758847274(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<Vuforia.VuforiaBehaviour>()
#define Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(__this /* static, unused */, method) ((  VuforiaBehaviour_t359035403 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m758847274_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069(__this, method) ((  MaskOutBehaviour_t2994129365 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184(__this, method) ((  VirtualButtonBehaviour_t2515041812 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313(__this, method) ((  TurnOffBehaviour_t3058161409 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077(__this, method) ((  ImageTargetBehaviour_t2654589389 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387(__this, method) ((  MultiTargetBehaviour_t3504654311 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918(__this, method) ((  CylinderTargetBehaviour_t2091399712 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061(__this, method) ((  WordBehaviour_t3366478421 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645(__this, method) ((  TextRecoBehaviour_t3400239837 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915(__this, method) ((  ObjectTargetBehaviour_t3836044259 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VuMarkBehaviour>()
#define GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781(__this, method) ((  VuMarkBehaviour_t2060629989 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2231772097_gshared)(__this, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m658541722_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m658541722(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m658541722_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.ScriptableObject::CreateInstance<Vuforia.VuforiaConfiguration>()
#define ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354(__this /* static, unused */, method) ((  VuforiaConfiguration_t3823746026 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m658541722_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(__this, method) ((  CameraU5BU5D_t3079764780* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1163966231_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, p0, method) ((  WireframeBehaviourU5BU5D_t2935582494* (*) (Component_t3819376471 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m4164830438_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArtFrame::.ctor()
extern "C"  void ArtFrame__ctor_m474197265 (ArtFrame_t69822562 * __this, const MethodInfo* method)
{
	{
		__this->set_unitToCM_9((0.01f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArtFrame::Start()
extern const MethodInfo* GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3970507932;
extern const uint32_t ArtFrame_Start_m1229393505_MetadataUsageId;
extern "C"  void ArtFrame_Start_m1229393505 (ArtFrame_t69822562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArtFrame_Start_m1229393505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3970507932, /*hidden argument*/NULL);
		NullCheck(L_0);
		PersistentScript_t2409504826 * L_1 = GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(L_0, /*hidden argument*/GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var);
		__this->set_persistentScript_8(L_1);
		GameObject_t1756533147 * L_2 = __this->get_art_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		PersistentScript_t2409504826 * L_4 = __this->get_persistentScript_8();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_selectedWidth_3();
		float L_6 = Single_Parse_m1861732734(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_unitToCM_9();
		PersistentScript_t2409504826 * L_8 = __this->get_persistentScript_8();
		NullCheck(L_8);
		String_t* L_9 = L_8->get_selectedHeight_4();
		float L_10 = Single_Parse_m1861732734(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_unitToCM_9();
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, ((float)((float)L_6*(float)L_7)), (1.0f), ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m2325460848(L_3, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_unitToCM_9();
		Vector3__ctor_m2638739322((&V_0), (0.0f), (1.0f), ((float)((float)(500.0f)*(float)L_13)), /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = V_0;
		NullCheck(L_14);
		Transform_set_localPosition_m1026930133(L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArtFrame::Update()
extern "C"  void ArtFrame_Update_m3516943232 (ArtFrame_t69822562 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TextMesh_t1641806576 * L_0 = __this->get_distance3DText_7();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_art_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_localScale_m3074381503(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_z_3();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, (0.0f), (0.3f), (((float)((float)L_5))), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localPosition_m1026930133(L_1, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DetectHandler::.ctor()
extern "C"  void DetectHandler__ctor_m3751729232 (DetectHandler_t4061016557 * __this, const MethodInfo* method)
{
	{
		__this->set_skipAnimation_8((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DetectHandler::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMenuController_t848154101_m1169873364_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var;
extern const uint32_t DetectHandler_Start_m1978693000_MetadataUsageId;
extern "C"  void DetectHandler_Start_m1978693000 (DetectHandler_t4061016557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DetectHandler_Start_m1978693000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Canvas_t209405766 * L_0 = __this->get_canvas_3();
		NullCheck(L_0);
		MenuController_t848154101 * L_1 = Component_GetComponent_TisMenuController_t848154101_m1169873364(L_0, /*hidden argument*/Component_GetComponent_TisMenuController_t848154101_m1169873364_MethodInfo_var);
		__this->set_menuController_4(L_1);
		MenuController_t848154101 * L_2 = __this->get_menuController_4();
		NullCheck(L_2);
		UIItems_t3088753794 * L_3 = L_2->get_uiItems_2();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = L_3->get_detectorPanel_6();
		NullCheck(L_4);
		Animator_t69676727 * L_5 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_detectorPanelAnimator_6(L_5);
		MenuController_t848154101 * L_6 = __this->get_menuController_4();
		NullCheck(L_6);
		UIItems_t3088753794 * L_7 = L_6->get_uiItems_2();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_detailPanel_5();
		NullCheck(L_8);
		Animator_t69676727 * L_9 = GameObject_GetComponent_TisAnimator_t69676727_m2717502299(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimator_t69676727_m2717502299_MethodInfo_var);
		__this->set_detailPanelAnimator_7(L_9);
		TrackableBehaviour_t1779888572 * L_10 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_mTrackableBehaviour_5(L_10);
		TrackableBehaviour_t1779888572 * L_11 = __this->get_mTrackableBehaviour_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006f;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_13 = __this->get_mTrackableBehaviour_5();
		NullCheck(L_13);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_13, __this, /*hidden argument*/NULL);
	}

IL_006f:
	{
		__this->set_isDetected_2((bool)0);
		return;
	}
}
// System.Void DetectHandler::Update()
extern "C"  void DetectHandler_Update_m95363405 (DetectHandler_t4061016557 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DetectHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2436156478;
extern Il2CppCodeGenString* _stringLiteral819340226;
extern Il2CppCodeGenString* _stringLiteral1406010350;
extern Il2CppCodeGenString* _stringLiteral2774347671;
extern Il2CppCodeGenString* _stringLiteral420890801;
extern Il2CppCodeGenString* _stringLiteral3753421356;
extern Il2CppCodeGenString* _stringLiteral2015806312;
extern const uint32_t DetectHandler_OnTrackableStateChanged_m2541740733_MetadataUsageId;
extern "C"  void DetectHandler_OnTrackableStateChanged_m2541740733 (DetectHandler_t4061016557 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DetectHandler_OnTrackableStateChanged_m2541740733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0052;
		}
	}

IL_0015:
	{
		__this->set_skipAnimation_8((bool)0);
		__this->set_isDetected_2((bool)1);
		Animator_t69676727 * L_3 = __this->get_detectorPanelAnimator_6();
		NullCheck(L_3);
		Animator_Play_m1123842248(L_3, _stringLiteral2436156478, /*hidden argument*/NULL);
		Animator_t69676727 * L_4 = __this->get_detailPanelAnimator_7();
		NullCheck(L_4);
		Animator_Play_m1123842248(L_4, _stringLiteral819340226, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1406010350, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_0052:
	{
		__this->set_isDetected_2((bool)0);
		bool L_5 = __this->get_skipAnimation_8();
		if (L_5)
		{
			goto IL_008e;
		}
	}
	{
		Animator_t69676727 * L_6 = __this->get_detectorPanelAnimator_6();
		NullCheck(L_6);
		Animator_Play_m1123842248(L_6, _stringLiteral2774347671, /*hidden argument*/NULL);
		Animator_t69676727 * L_7 = __this->get_detailPanelAnimator_7();
		NullCheck(L_7);
		Animator_Play_m1123842248(L_7, _stringLiteral420890801, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3753421356, /*hidden argument*/NULL);
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2015806312, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void DidFinishSplashScreenLoading::.ctor()
extern "C"  void DidFinishSplashScreenLoading__ctor_m4194989786 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _DidFinishUnitySplashScreenLoading();
// System.Void DidFinishSplashScreenLoading::_DidFinishUnitySplashScreenLoading()
extern "C"  void DidFinishSplashScreenLoading__DidFinishUnitySplashScreenLoading_m4151621763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_DidFinishUnitySplashScreenLoading)();

}
// System.Void DidFinishSplashScreenLoading::Start()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1406626904;
extern Il2CppCodeGenString* _stringLiteral3538073363;
extern Il2CppCodeGenString* _stringLiteral2556141943;
extern const uint32_t DidFinishSplashScreenLoading_Start_m1705684910_MetadataUsageId;
extern "C"  void DidFinishSplashScreenLoading_Start_m1705684910 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DidFinishSplashScreenLoading_Start_m1705684910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DidFinishSplashScreenLoading_DidFinishUnitySplashScreenLoading_m3572516558(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1406626904, /*hidden argument*/NULL);
		SceneManager_LoadSceneAsync_m4130852156(NULL /*static, unused*/, _stringLiteral3538073363, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2556141943, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DidFinishSplashScreenLoading::Update()
extern "C"  void DidFinishSplashScreenLoading_Update_m3558713339 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DidFinishSplashScreenLoading::DidFinishUnitySplashScreenLoading()
extern "C"  void DidFinishSplashScreenLoading_DidFinishUnitySplashScreenLoading_m3572516558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		DidFinishSplashScreenLoading__DidFinishUnitySplashScreenLoading_m4151621763(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void ExtendTrackingController::.ctor()
extern "C"  void ExtendTrackingController__ctor_m111577496 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExtendTrackingController::Start()
extern Il2CppClass* UnityAction_1_t897193173_il2cpp_TypeInfo_var;
extern const MethodInfo* ExtendTrackingController_U3CStartU3Em__0_m55630388_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m1968084291_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m1708363187_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImageTargetBehaviour_t2654589389_m2018094066_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern const uint32_t ExtendTrackingController_Start_m4291075508_MetadataUsageId;
extern "C"  void ExtendTrackingController_Start_m4291075508 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendTrackingController_Start_m4291075508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Toggle_t3976754468 * L_0 = __this->get_lockToggle_2();
		NullCheck(L_0);
		ToggleEvent_t1896830814 * L_1 = L_0->get_onValueChanged_19();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ExtendTrackingController_U3CStartU3Em__0_m55630388_MethodInfo_var);
		UnityAction_1_t897193173 * L_3 = (UnityAction_1_t897193173 *)il2cpp_codegen_object_new(UnityAction_1_t897193173_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m1968084291(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m1968084291_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m1708363187(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m1708363187_MethodInfo_var);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		NullCheck(L_4);
		ImageTargetBehaviour_t2654589389 * L_5 = GameObject_GetComponent_TisImageTargetBehaviour_t2654589389_m2018094066(L_4, /*hidden argument*/GameObject_GetComponent_TisImageTargetBehaviour_t2654589389_m2018094066_MethodInfo_var);
		__this->set_itb_3(L_5);
		return;
	}
}
// System.Void ExtendTrackingController::Update()
extern "C"  void ExtendTrackingController_Update_m3705178103 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ExtendTrackingController::lockToggleButtonValueChangeHandler(UnityEngine.UI.Toggle)
extern Il2CppClass* ExtendedTrackable_t1730600702_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2013390931;
extern const uint32_t ExtendTrackingController_lockToggleButtonValueChangeHandler_m1729102927_MetadataUsageId;
extern "C"  void ExtendTrackingController_lockToggleButtonValueChangeHandler_m1729102927 (ExtendTrackingController_t1670312339 * __this, Toggle_t3976754468 * ___target0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExtendTrackingController_lockToggleButtonValueChangeHandler_m1729102927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Toggle_t3976754468 * L_0 = ___target0;
		NullCheck(L_0);
		bool L_1 = Toggle_get_isOn_m366838229(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		ImageTargetBehaviour_t2654589389 * L_2 = __this->get_itb_3();
		NullCheck(L_2);
		Il2CppObject * L_3 = ImageTargetAbstractBehaviour_get_ImageTarget_m3261756177(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ExtendedTrackable::StartExtendedTracking() */, ExtendedTrackable_t1730600702_il2cpp_TypeInfo_var, L_3);
		goto IL_0032;
	}

IL_0021:
	{
		ImageTargetBehaviour_t2654589389 * L_4 = __this->get_itb_3();
		NullCheck(L_4);
		Il2CppObject * L_5 = ImageTargetAbstractBehaviour_get_ImageTarget_m3261756177(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ExtendedTrackable::StopExtendedTracking() */, ExtendedTrackable_t1730600702_il2cpp_TypeInfo_var, L_5);
	}

IL_0032:
	{
		Toggle_t3976754468 * L_6 = ___target0;
		NullCheck(L_6);
		bool L_7 = Toggle_get_isOn_m366838229(L_6, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2013390931, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExtendTrackingController::<Start>m__0(System.Boolean)
extern "C"  void ExtendTrackingController_U3CStartU3Em__0_m55630388 (ExtendTrackingController_t1670312339 * __this, bool p0, const MethodInfo* method)
{
	{
		Toggle_t3976754468 * L_0 = __this->get_lockToggle_2();
		ExtendTrackingController_lockToggleButtonValueChangeHandler_m1729102927(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeInOut::.ctor()
extern "C"  void FadeInOut__ctor_m3835509650 (FadeInOut_t611841665 * __this, const MethodInfo* method)
{
	{
		__this->set_fadeSpeed_3((0.225f));
		__this->set_drawDepth_4(((int32_t)-1000));
		__this->set_alpha_5((1.0f));
		__this->set_fadeDir_6((-1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeInOut::Start()
extern "C"  void FadeInOut_Start_m4254002546 (FadeInOut_t611841665 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FadeInOut::Update()
extern "C"  void FadeInOut_Update_m3292029453 (FadeInOut_t611841665 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FadeInOut::OnGUI()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t FadeInOut_OnGUI_m798385406_MetadataUsageId;
extern "C"  void FadeInOut_OnGUI_m798385406 (FadeInOut_t611841665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FadeInOut_OnGUI_m798385406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_alpha_5();
		float L_1 = __this->get_fadeDir_6();
		float L_2 = __this->get_fadeSpeed_3();
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_alpha_5(((float)((float)L_0+(float)((float)((float)((float)((float)L_1*(float)L_2))*(float)L_3)))));
		float L_4 = __this->get_alpha_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_alpha_5(L_5);
		Initobj (Color_t2020392075_il2cpp_TypeInfo_var, (&V_0));
		float L_6 = __this->get_alpha_5();
		(&V_0)->set_a_3(L_6);
		Color_t2020392075  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_drawDepth_4();
		GUI_set_depth_m3824351935(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, (0.0f), (0.0f), (((float)((float)L_9))), (((float)((float)L_10))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_12 = __this->get_fadeTexture_2();
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GetDistance::.ctor()
extern "C"  void GetDistance__ctor_m782192962 (GetDistance_t254146547 * __this, const MethodInfo* method)
{
	{
		__this->set_distanceUnitToCM_2((0.01f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GetDistance::Start()
extern const MethodInfo* GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern const uint32_t GetDistance_Start_m2748077262_MetadataUsageId;
extern "C"  void GetDistance_Start_m2748077262 (GetDistance_t254146547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetDistance_Start_m2748077262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArtFrame_t69822562 * L_1 = GameObject_GetComponent_TisArtFrame_t69822562_m457944649(L_0, /*hidden argument*/GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var);
		__this->set_artFrame_3(L_1);
		return;
	}
}
// System.Void GetDistance::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2659714478;
extern const uint32_t GetDistance_Update_m3767149079_MetadataUsageId;
extern "C"  void GetDistance_Update_m3767149079 (GetDistance_t254146547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GetDistance_Update_m3767149079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		ArtFrame_t69822562 * L_0 = __this->get_artFrame_3();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = L_0->get_artFrame_2();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		float L_6 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ArtFrame_t69822562 * L_7 = __this->get_artFrame_3();
		NullCheck(L_7);
		TextMesh_t1641806576 * L_8 = L_7->get_distance3DText_7();
		float L_9 = V_0;
		float L_10 = __this->get_distanceUnitToCM_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_11 = Mathf_CeilToInt_m2672598779(NULL /*static, unused*/, ((float)((float)L_9/(float)L_10)), /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m56707527(NULL /*static, unused*/, L_13, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, L_14, /*hidden argument*/NULL);
		float L_15 = V_0;
		__this->set_distanceInUnit_4(L_15);
		float L_16 = V_0;
		float L_17 = __this->get_distanceUnitToCM_2();
		__this->set_distanceInCM_5(((float)((float)L_16/(float)L_17)));
		return;
	}
}
// System.Void LockAndUnlock::.ctor()
extern "C"  void LockAndUnlock__ctor_m1094286887 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LockAndUnlock::Start()
extern const MethodInfo* Component_GetComponent_TisExtendTrackingController_t1670312339_m1331458250_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral332801758;
extern const uint32_t LockAndUnlock_Start_m761137011_MetadataUsageId;
extern "C"  void LockAndUnlock_Start_m761137011 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LockAndUnlock_Start_m761137011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		ExtendTrackingController_t1670312339 * L_1 = Component_GetComponent_TisExtendTrackingController_t1670312339_m1331458250(L_0, /*hidden argument*/Component_GetComponent_TisExtendTrackingController_t1670312339_m1331458250_MethodInfo_var);
		__this->set_script_2(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_FindChild_m2677714886(L_2, _stringLiteral332801758, /*hidden argument*/NULL);
		__this->set_background_3(L_3);
		Transform_t3275118058 * L_4 = __this->get_background_3();
		NullCheck(L_4);
		Image_t2042527209 * L_5 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_4, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		__this->set_imageScript_4(L_5);
		return;
	}
}
// System.Void LockAndUnlock::Update()
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1904326903;
extern Il2CppCodeGenString* _stringLiteral2529233344;
extern const uint32_t LockAndUnlock_Update_m587801242_MetadataUsageId;
extern "C"  void LockAndUnlock_Update_m587801242 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LockAndUnlock_Update_m587801242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtendTrackingController_t1670312339 * L_0 = __this->get_script_2();
		NullCheck(L_0);
		Toggle_t3976754468 * L_1 = L_0->get_lockToggle_2();
		NullCheck(L_1);
		bool L_2 = Toggle_get_isOn_m366838229(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Image_t2042527209 * L_3 = __this->get_imageScript_4();
		Sprite_t309593783 * L_4 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1904326903, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		NullCheck(L_3);
		Image_set_sprite_m1800056820(L_3, L_4, /*hidden argument*/NULL);
		goto IL_0044;
	}

IL_002f:
	{
		Image_t2042527209 * L_5 = __this->get_imageScript_4();
		Sprite_t309593783 * L_6 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2529233344, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		NullCheck(L_5);
		Image_set_sprite_m1800056820(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void MainCameraScene::.ctor()
extern "C"  void MainCameraScene__ctor_m3800890799 (MainCameraScene_t1629247010 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCameraScene::Start()
extern Il2CppClass* WebCamTexture_t1079476942_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m697787402_MethodInfo_var;
extern const uint32_t MainCameraScene_Start_m3008645075_MetadataUsageId;
extern "C"  void MainCameraScene_Start_m3008645075 (MainCameraScene_t1629247010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainCameraScene_Start_m3008645075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_CreatePrimitive_m973880764(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (90.0f), (0.0f), (180.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m3411284563(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (150.0f), (100.0f), (100.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localScale_m2325460848(L_8, L_9, /*hidden argument*/NULL);
		WebCamDeviceU5BU5D_t2903637840* L_10 = WebCamTexture_get_devices_m4137524804(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = WebCamDevice_get_name_m1117076425(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/NULL);
		V_1 = L_11;
		String_t* L_12 = V_1;
		WebCamTexture_t1079476942 * L_13 = (WebCamTexture_t1079476942 *)il2cpp_codegen_object_new(WebCamTexture_t1079476942_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m2490275306(L_13, L_12, ((int32_t)1280), ((int32_t)720), ((int32_t)30), /*hidden argument*/NULL);
		__this->set_mCamera_2(L_13);
		WebCamTexture_t1079476942 * L_14 = __this->get_mCamera_2();
		NullCheck(L_14);
		WebCamTexture_Play_m1997372813(L_14, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = V_0;
		NullCheck(L_15);
		Renderer_t257310565 * L_16 = GameObject_GetComponent_TisRenderer_t257310565_m697787402(L_15, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m697787402_MethodInfo_var);
		NullCheck(L_16);
		Material_t193706927 * L_17 = Renderer_get_material_m2553789785(L_16, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_18 = __this->get_mCamera_2();
		NullCheck(L_17);
		Material_set_mainTexture_m3584203343(L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainCameraScene::Update()
extern "C"  void MainCameraScene_Update_m2667073068 (MainCameraScene_t1629247010 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MainSceneController::.ctor()
extern "C"  void MainSceneController__ctor_m3083733182 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _CloseUnity3dARView();
// System.Void MainSceneController::_CloseUnity3dARView()
extern "C"  void MainSceneController__CloseUnity3dARView_m695634793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CloseUnity3dARView)();

}
extern "C" void DEFAULT_CALL _EmailRequestForMarker();
// System.Void MainSceneController::_EmailRequestForMarker()
extern "C"  void MainSceneController__EmailRequestForMarker_m1326298853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_EmailRequestForMarker)();

}
// System.Void MainSceneController::Start()
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var;
extern const MethodInfo* MainSceneController_closeEvent_m2102478408_MethodInfo_var;
extern const MethodInfo* MainSceneController_emailRequestForMarker_m2739022044_MethodInfo_var;
extern const MethodInfo* MainSceneController_U3CStartU3Em__0_m1972807941_MethodInfo_var;
extern const MethodInfo* MainSceneController_U3CStartU3Em__1_m426926976_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3970507932;
extern Il2CppCodeGenString* _stringLiteral3954205922;
extern const uint32_t MainSceneController_Start_m3927469810_MetadataUsageId;
extern "C"  void MainSceneController_Start_m3927469810 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_Start_m3927469810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3970507932, /*hidden argument*/NULL);
		NullCheck(L_0);
		PersistentScript_t2409504826 * L_1 = GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(L_0, /*hidden argument*/GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var);
		__this->set_persistentScript_12(L_1);
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Scene_get_name_m745914591((&V_0), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral3954205922, L_3, /*hidden argument*/NULL);
		Button_t2872111280 * L_4 = __this->get_closeButton_2();
		NullCheck(L_4);
		ButtonClickedEvent_t2455055323 * L_5 = Button_get_onClick_m1595880935(L_4, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)MainSceneController_closeEvent_m2102478408_MethodInfo_var);
		UnityAction_t4025899511 * L_7 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m1596810379(L_5, L_7, /*hidden argument*/NULL);
		Button_t2872111280 * L_8 = __this->get_emailMarkerButton_5();
		NullCheck(L_8);
		ButtonClickedEvent_t2455055323 * L_9 = Button_get_onClick_m1595880935(L_8, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)MainSceneController_emailRequestForMarker_m2739022044_MethodInfo_var);
		UnityAction_t4025899511 * L_11 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_11, __this, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEvent_AddListener_m1596810379(L_9, L_11, /*hidden argument*/NULL);
		Button_t2872111280 * L_12 = __this->get_markerButton_3();
		NullCheck(L_12);
		ButtonClickedEvent_t2455055323 * L_13 = Button_get_onClick_m1595880935(L_12, /*hidden argument*/NULL);
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)MainSceneController_U3CStartU3Em__0_m1972807941_MethodInfo_var);
		UnityAction_t4025899511 * L_15 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_15, __this, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		UnityEvent_AddListener_m1596810379(L_13, L_15, /*hidden argument*/NULL);
		Button_t2872111280 * L_16 = __this->get_markerlessButton_4();
		NullCheck(L_16);
		ButtonClickedEvent_t2455055323 * L_17 = Button_get_onClick_m1595880935(L_16, /*hidden argument*/NULL);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)MainSceneController_U3CStartU3Em__1_m426926976_MethodInfo_var);
		UnityAction_t4025899511 * L_19 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_19, __this, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		UnityEvent_AddListener_m1596810379(L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::Update()
extern "C"  void MainSceneController_Update_m2786355995 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MainSceneController::ArtImageUrlFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2275548956;
extern Il2CppCodeGenString* _stringLiteral386989284;
extern const uint32_t MainSceneController_ArtImageUrlFromXcode_m3604364818_MetadataUsageId;
extern "C"  void MainSceneController_ArtImageUrlFromXcode_m3604364818 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_ArtImageUrlFromXcode_m3604364818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral2275548956;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_artImageUrl_6(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_artImageUrl_2(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral386989284, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::SelectedWidthFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral1167476352;
extern const uint32_t MainSceneController_SelectedWidthFromXcode_m824207294_MetadataUsageId;
extern "C"  void MainSceneController_SelectedWidthFromXcode_m824207294 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_SelectedWidthFromXcode_m824207294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral372029326;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_selectedWidth_7(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_selectedWidth_3(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1167476352, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::SelectedHeightFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral1866202019;
extern const uint32_t MainSceneController_SelectedHeightFromXcode_m1471352809_MetadataUsageId;
extern "C"  void MainSceneController_SelectedHeightFromXcode_m1471352809 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_SelectedHeightFromXcode_m1471352809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral372029326;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_selectedHeight_8(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_selectedHeight_4(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1866202019, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::SelectedPriceFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral1059487825;
extern const uint32_t MainSceneController_SelectedPriceFromXcode_m2321260611_MetadataUsageId;
extern "C"  void MainSceneController_SelectedPriceFromXcode_m2321260611 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_SelectedPriceFromXcode_m2321260611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral372029326;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_selectedPrice_9(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_selectedPrice_5(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1059487825, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::SelectedWidthAmountPerCmFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral422458823;
extern const uint32_t MainSceneController_SelectedWidthAmountPerCmFromXcode_m2451641693_MetadataUsageId;
extern "C"  void MainSceneController_SelectedWidthAmountPerCmFromXcode_m2451641693 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_SelectedWidthAmountPerCmFromXcode_m2451641693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral372029326;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_selectedWidthAmountPerCm_10(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_selectedWidthAmountPerCm_6(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral422458823, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::SelectedHeightAmountPerCmFromXcode(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral573401222;
extern const uint32_t MainSceneController_SelectedHeightAmountPerCmFromXcode_m3839430516_MetadataUsageId;
extern "C"  void MainSceneController_SelectedHeightAmountPerCmFromXcode_m3839430516 (MainSceneController_t1039965335 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_SelectedHeightAmountPerCmFromXcode_m3839430516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___text0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___text0 = _stringLiteral372029326;
	}

IL_000d:
	{
		String_t* L_1 = ___text0;
		__this->set_selectedHeightAmountPerCm_11(L_1);
		PersistentScript_t2409504826 * L_2 = __this->get_persistentScript_12();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		L_2->set_selectedHeightAmountPerCm_7(L_3);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral573401222, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::closeEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2704120344;
extern const uint32_t MainSceneController_closeEvent_m2102478408_MetadataUsageId;
extern "C"  void MainSceneController_closeEvent_m2102478408 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_closeEvent_m2102478408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MainSceneController__CloseUnity3dARView_m695634793(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2704120344, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::emailRequestForMarker()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3494434938;
extern const uint32_t MainSceneController_emailRequestForMarker_m2739022044_MetadataUsageId;
extern "C"  void MainSceneController_emailRequestForMarker_m2739022044 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_emailRequestForMarker_m2739022044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MainSceneController__EmailRequestForMarker_m1326298853(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3494434938, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::loadSceneEvent(System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1968658960;
extern Il2CppCodeGenString* _stringLiteral2600517442;
extern Il2CppCodeGenString* _stringLiteral1737357431;
extern Il2CppCodeGenString* _stringLiteral414490486;
extern const uint32_t MainSceneController_loadSceneEvent_m3130337703_MetadataUsageId;
extern "C"  void MainSceneController_loadSceneEvent_m3130337703 (MainSceneController_t1039965335 * __this, int32_t ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainSceneController_loadSceneEvent_m3130337703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___sender0;
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		SceneManager_LoadSceneAsync_m4130852156(NULL /*static, unused*/, _stringLiteral1968658960, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2600517442, /*hidden argument*/NULL);
		goto IL_003b;
	}

IL_0020:
	{
		int32_t L_1 = ___sender0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_003b;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1737357431, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral414490486, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void MainSceneController::<Start>m__0()
extern "C"  void MainSceneController_U3CStartU3Em__0_m1972807941 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	{
		MainSceneController_loadSceneEvent_m3130337703(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainSceneController::<Start>m__1()
extern "C"  void MainSceneController_U3CStartU3Em__1_m426926976 (MainSceneController_t1039965335 * __this, const MethodInfo* method)
{
	{
		MainSceneController_loadSceneEvent_m3130337703(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Markerless::.ctor()
extern "C"  void Markerless__ctor_m832057758 (Markerless_t1009278913 * __this, const MethodInfo* method)
{
	{
		__this->set_setOriginalValues_8((bool)1);
		__this->set_speed_11((0.1f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Markerless::GetCoordinates()
extern Il2CppClass* U3CGetCoordinatesU3Ec__Iterator0_t2481679252_il2cpp_TypeInfo_var;
extern const uint32_t Markerless_GetCoordinates_m2944219429_MetadataUsageId;
extern "C"  Il2CppObject * Markerless_GetCoordinates_m2944219429 (Markerless_t1009278913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Markerless_GetCoordinates_m2944219429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * V_0 = NULL;
	{
		U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * L_0 = (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 *)il2cpp_codegen_object_new(U3CGetCoordinatesU3Ec__Iterator0_t2481679252_il2cpp_TypeInfo_var);
		U3CGetCoordinatesU3Ec__Iterator0__ctor_m2828517887(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Markerless::Calc(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral106545481;
extern const uint32_t Markerless_Calc_m997497509_MetadataUsageId;
extern "C"  void Markerless_Calc_m997497509 (Markerless_t1009278913 * __this, float ___lat10, float ___lon11, float ___lat22, float ___lon23, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Markerless_Calc_m997497509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		V_0 = (6378.137);
		float L_0 = ___lat22;
		float L_1 = ___lat10;
		V_1 = ((float)((float)((float)((float)((float)((float)L_0*(float)(3.14159274f)))/(float)(180.0f)))-(float)((float)((float)((float)((float)L_1*(float)(3.14159274f)))/(float)(180.0f)))));
		float L_2 = ___lon23;
		float L_3 = ___lon11;
		V_2 = ((float)((float)((float)((float)((float)((float)L_2*(float)(3.14159274f)))/(float)(180.0f)))-(float)((float)((float)((float)((float)L_3*(float)(3.14159274f)))/(float)(180.0f)))));
		float L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = sinf(((float)((float)L_4/(float)(2.0f))));
		float L_6 = V_1;
		float L_7 = sinf(((float)((float)L_6/(float)(2.0f))));
		float L_8 = ___lat10;
		float L_9 = cosf(((float)((float)((float)((float)L_8*(float)(3.14159274f)))/(float)(180.0f))));
		float L_10 = ___lat22;
		float L_11 = cosf(((float)((float)((float)((float)L_10*(float)(3.14159274f)))/(float)(180.0f))));
		float L_12 = V_2;
		float L_13 = sinf(((float)((float)L_12/(float)(2.0f))));
		float L_14 = V_2;
		float L_15 = sinf(((float)((float)L_14/(float)(2.0f))));
		V_3 = ((float)((float)((float)((float)L_5*(float)L_7))+(float)((float)((float)((float)((float)((float)((float)L_9*(float)L_11))*(float)L_13))*(float)L_15))));
		float L_16 = V_3;
		float L_17 = sqrtf(L_16);
		float L_18 = V_3;
		float L_19 = sqrtf(((float)((float)(1.0f)-(float)L_18)));
		float L_20 = atan2f(L_17, L_19);
		V_4 = ((float)((float)(2.0f)*(float)L_20));
		double L_21 = V_0;
		float L_22 = V_4;
		__this->set_distance_7(((double)((double)L_21*(double)(((double)((double)L_22))))));
		double L_23 = __this->get_distance_7();
		__this->set_distance_7(((double)((double)L_23*(double)(1000.0))));
		GameObject_t1756533147 * L_24 = __this->get_distanceTextObject_6();
		NullCheck(L_24);
		Text_t356221433 * L_25 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_24, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		double L_26 = __this->get_distance_7();
		double L_27 = L_26;
		Il2CppObject * L_28 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral106545481, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_29);
		double L_30 = __this->get_distance_7();
		V_5 = (((float)((float)L_30)));
		Vector3_t2243707580  L_31 = __this->get_originalPosition_10();
		float L_32 = V_5;
		Vector3_t2243707580  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2638739322(&L_33, (0.0f), (0.0f), ((float)((float)L_32*(float)(12.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		__this->set_targetPosition_9(L_34);
		return;
	}
}
// System.Void Markerless::Start()
extern Il2CppCodeGenString* _stringLiteral735649190;
extern Il2CppCodeGenString* _stringLiteral3277727211;
extern const uint32_t Markerless_Start_m3555613838_MetadataUsageId;
extern "C"  void Markerless_Start_m3555613838 (Markerless_t1009278913 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Markerless_Start_m3555613838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral735649190, /*hidden argument*/NULL);
		__this->set_distanceTextObject_6(L_0);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral3277727211, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		__this->set_targetPosition_9(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		__this->set_originalPosition_10(L_4);
		return;
	}
}
// System.Void Markerless::Update()
extern "C"  void Markerless_Update_m3633286693 (Markerless_t1009278913 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = __this->get_targetPosition_9();
		float L_4 = __this->get_speed_11();
		Vector3_t2243707580  L_5 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = L_6;
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_eulerAngles_m4066505159(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_eulerAngles_m2881310872(L_7, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Markerless/<GetCoordinates>c__Iterator0::.ctor()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0__ctor_m2828517887 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Markerless/<GetCoordinates>c__Iterator0::MoveNext()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral920211983;
extern Il2CppCodeGenString* _stringLiteral2754308450;
extern Il2CppCodeGenString* _stringLiteral2294766215;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m1709006085_MetadataUsageId;
extern "C"  bool U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m1709006085 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCoordinatesU3Ec__Iterator0_MoveNext_m1709006085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	LocationInfo_t1364725149  V_1;
	memset(&V_1, 0, sizeof(V_1));
	LocationInfo_t1364725149  V_2;
	memset(&V_2, 0, sizeof(V_2));
	LocationInfo_t1364725149  V_3;
	memset(&V_3, 0, sizeof(V_3));
	LocationInfo_t1364725149  V_4;
	memset(&V_4, 0, sizeof(V_4));
	LocationInfo_t1364725149  V_5;
	memset(&V_5, 0, sizeof(V_5));
	LocationInfo_t1364725149  V_6;
	memset(&V_6, 0, sizeof(V_6));
	LocationInfo_t1364725149  V_7;
	memset(&V_7, 0, sizeof(V_7));
	LocationInfo_t1364725149  V_8;
	memset(&V_8, 0, sizeof(V_8));
	LocationInfo_t1364725149  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_027d;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_2 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = LocationService_get_isEnabledByUser_m840009485(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_027d;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_4 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		LocationService_Start_m1102337569(L_4, (1.0f), (0.1f), /*hidden argument*/NULL);
		__this->set_U3CmaxWaitU3E__0_0(((int32_t)20));
		goto IL_0088;
	}

IL_0056:
	{
		WaitForSeconds_t3839502067 * L_5 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_5, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_5);
		bool L_6 = __this->get_U24disposing_3();
		if (L_6)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0075:
	{
		goto IL_027f;
	}

IL_007a:
	{
		int32_t L_7 = __this->get_U3CmaxWaitU3E__0_0();
		__this->set_U3CmaxWaitU3E__0_0(((int32_t)((int32_t)L_7-(int32_t)1)));
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_8 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = LocationService_get_status_m1865246926(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_10 = __this->get_U3CmaxWaitU3E__0_0();
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0056;
		}
	}

IL_00a4:
	{
		int32_t L_11 = __this->get_U3CmaxWaitU3E__0_0();
		if ((((int32_t)L_11) >= ((int32_t)1)))
		{
			goto IL_00bf;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral920211983, /*hidden argument*/NULL);
		goto IL_027d;
	}

IL_00bf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_12 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = LocationService_get_status_m1865246926(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)3))))
		{
			goto IL_00de;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral2754308450, /*hidden argument*/NULL);
		goto IL_027d;
	}

IL_00de:
	{
		ObjectU5BU5D_t3614634134* L_14 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, _stringLiteral2294766215);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2294766215);
		ObjectU5BU5D_t3614634134* L_15 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_16 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		LocationInfo_t1364725149  L_17 = LocationService_get_lastData_m2521124837(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = LocationInfo_get_latitude_m2482205269((&V_1), /*hidden argument*/NULL);
		float L_19 = L_18;
		Il2CppObject * L_20 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_20);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_20);
		ObjectU5BU5D_t3614634134* L_21 = L_15;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral372029310);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_22 = L_21;
		LocationService_t1617852714 * L_23 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		LocationInfo_t1364725149  L_24 = LocationService_get_lastData_m2521124837(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		float L_25 = LocationInfo_get_longitude_m306881672((&V_2), /*hidden argument*/NULL);
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_27);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_22;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral372029310);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		LocationService_t1617852714 * L_30 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		LocationInfo_t1364725149  L_31 = LocationService_get_lastData_m2521124837(L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		float L_32 = LocationInfo_get_altitude_m523209073((&V_3), /*hidden argument*/NULL);
		float L_33 = L_32;
		Il2CppObject * L_34 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_34);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_34);
		ObjectU5BU5D_t3614634134* L_35 = L_29;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral372029310);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_36 = L_35;
		LocationService_t1617852714 * L_37 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		LocationInfo_t1364725149  L_38 = LocationService_get_lastData_m2521124837(L_37, /*hidden argument*/NULL);
		V_4 = L_38;
		float L_39 = LocationInfo_get_horizontalAccuracy_m753214408((&V_4), /*hidden argument*/NULL);
		float L_40 = L_39;
		Il2CppObject * L_41 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_41);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_41);
		ObjectU5BU5D_t3614634134* L_42 = L_36;
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, _stringLiteral372029310);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_43 = L_42;
		LocationService_t1617852714 * L_44 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		LocationInfo_t1364725149  L_45 = LocationService_get_lastData_m2521124837(L_44, /*hidden argument*/NULL);
		V_5 = L_45;
		double L_46 = LocationInfo_get_timestamp_m1207779140((&V_5), /*hidden argument*/NULL);
		double L_47 = L_46;
		Il2CppObject * L_48 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_48);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m3881798623(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Markerless_t1009278913 * L_50 = __this->get_U24this_1();
		NullCheck(L_50);
		bool L_51 = L_50->get_setOriginalValues_8();
		if (!L_51)
		{
			goto IL_01f4;
		}
	}
	{
		Markerless_t1009278913 * L_52 = __this->get_U24this_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_53 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_53);
		LocationInfo_t1364725149  L_54 = LocationService_get_lastData_m2521124837(L_53, /*hidden argument*/NULL);
		V_6 = L_54;
		float L_55 = LocationInfo_get_latitude_m2482205269((&V_6), /*hidden argument*/NULL);
		NullCheck(L_52);
		L_52->set_originalLatitude_2(L_55);
		Markerless_t1009278913 * L_56 = __this->get_U24this_1();
		LocationService_t1617852714 * L_57 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		LocationInfo_t1364725149  L_58 = LocationService_get_lastData_m2521124837(L_57, /*hidden argument*/NULL);
		V_7 = L_58;
		float L_59 = LocationInfo_get_longitude_m306881672((&V_7), /*hidden argument*/NULL);
		NullCheck(L_56);
		L_56->set_originalLongitude_3(L_59);
		Markerless_t1009278913 * L_60 = __this->get_U24this_1();
		NullCheck(L_60);
		L_60->set_setOriginalValues_8((bool)0);
	}

IL_01f4:
	{
		Markerless_t1009278913 * L_61 = __this->get_U24this_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_62 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_62);
		LocationInfo_t1364725149  L_63 = LocationService_get_lastData_m2521124837(L_62, /*hidden argument*/NULL);
		V_8 = L_63;
		float L_64 = LocationInfo_get_latitude_m2482205269((&V_8), /*hidden argument*/NULL);
		NullCheck(L_61);
		L_61->set_currentLatitude_5(L_64);
		Markerless_t1009278913 * L_65 = __this->get_U24this_1();
		LocationService_t1617852714 * L_66 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		LocationInfo_t1364725149  L_67 = LocationService_get_lastData_m2521124837(L_66, /*hidden argument*/NULL);
		V_9 = L_67;
		float L_68 = LocationInfo_get_longitude_m306881672((&V_9), /*hidden argument*/NULL);
		NullCheck(L_65);
		L_65->set_currentLongitude_4(L_68);
		Markerless_t1009278913 * L_69 = __this->get_U24this_1();
		Markerless_t1009278913 * L_70 = __this->get_U24this_1();
		NullCheck(L_70);
		float L_71 = L_70->get_originalLatitude_2();
		Markerless_t1009278913 * L_72 = __this->get_U24this_1();
		NullCheck(L_72);
		float L_73 = L_72->get_originalLongitude_3();
		Markerless_t1009278913 * L_74 = __this->get_U24this_1();
		NullCheck(L_74);
		float L_75 = L_74->get_currentLatitude_5();
		Markerless_t1009278913 * L_76 = __this->get_U24this_1();
		NullCheck(L_76);
		float L_77 = L_76->get_currentLongitude_4();
		NullCheck(L_69);
		Markerless_Calc_m997497509(L_69, L_71, L_73, L_75, L_77, /*hidden argument*/NULL);
		LocationService_t1617852714 * L_78 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_78);
		LocationService_Stop_m583234201(L_78, /*hidden argument*/NULL);
		goto IL_0021;
	}
	// Dead block : IL_0276: ldarg.0

IL_027d:
	{
		return (bool)0;
	}

IL_027f:
	{
		return (bool)1;
	}
}
// System.Object Markerless/<GetCoordinates>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2335715909 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Markerless/<GetCoordinates>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetCoordinatesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m282950557 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Markerless/<GetCoordinates>c__Iterator0::Dispose()
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Dispose_m2713162118 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Markerless/<GetCoordinates>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCoordinatesU3Ec__Iterator0_Reset_m966037908_MetadataUsageId;
extern "C"  void U3CGetCoordinatesU3Ec__Iterator0_Reset_m966037908 (U3CGetCoordinatesU3Ec__Iterator0_t2481679252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCoordinatesU3Ec__Iterator0_Reset_m966037908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MarkerlessMenuController::.ctor()
extern "C"  void MarkerlessMenuController__ctor_m1293276087 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController::_CloseUnity3dARView()
extern "C"  void MarkerlessMenuController__CloseUnity3dARView_m2219918690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CloseUnity3dARView)();

}
extern "C" void DEFAULT_CALL _ScreenShot();
// System.Void MarkerlessMenuController::_ScreenShot()
extern "C"  void MarkerlessMenuController__ScreenShot_m1921154874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ScreenShot)();

}
extern "C" void DEFAULT_CALL _ConfirmWidth(char*);
// System.Void MarkerlessMenuController::_ConfirmWidth(System.String)
extern "C"  void MarkerlessMenuController__ConfirmWidth_m1963284504 (Il2CppObject * __this /* static, unused */, String_t* ___confirmWidth0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmWidth0' to native representation
	char* ____confirmWidth0_marshaled = NULL;
	____confirmWidth0_marshaled = il2cpp_codegen_marshal_string(___confirmWidth0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmWidth)(____confirmWidth0_marshaled);

	// Marshaling cleanup of parameter '___confirmWidth0' native representation
	il2cpp_codegen_marshal_free(____confirmWidth0_marshaled);
	____confirmWidth0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _ConfirmHeight(char*);
// System.Void MarkerlessMenuController::_ConfirmHeight(System.String)
extern "C"  void MarkerlessMenuController__ConfirmHeight_m3076499927 (Il2CppObject * __this /* static, unused */, String_t* ___confirmHeight0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmHeight0' to native representation
	char* ____confirmHeight0_marshaled = NULL;
	____confirmHeight0_marshaled = il2cpp_codegen_marshal_string(___confirmHeight0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmHeight)(____confirmHeight0_marshaled);

	// Marshaling cleanup of parameter '___confirmHeight0' native representation
	il2cpp_codegen_marshal_free(____confirmHeight0_marshaled);
	____confirmHeight0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL _ConfirmPrice(char*);
// System.Void MarkerlessMenuController::_ConfirmPrice(System.String)
extern "C"  void MarkerlessMenuController__ConfirmPrice_m1664757449 (Il2CppObject * __this /* static, unused */, String_t* ___confirmPrice0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmPrice0' to native representation
	char* ____confirmPrice0_marshaled = NULL;
	____confirmPrice0_marshaled = il2cpp_codegen_marshal_string(___confirmPrice0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmPrice)(____confirmPrice0_marshaled);

	// Marshaling cleanup of parameter '___confirmPrice0' native representation
	il2cpp_codegen_marshal_free(____confirmPrice0_marshaled);
	____confirmPrice0_marshaled = NULL;

}
// System.Void MarkerlessMenuController::Start()
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGetDistance_t254146547_m3541233510_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarkerlessPinchGesture_t1797694232_m1222991259_MethodInfo_var;
extern const MethodInfo* MarkerlessMenuController_closeEvent_m1344608029_MethodInfo_var;
extern const MethodInfo* MarkerlessMenuController_screenShotEvent_m260816955_MethodInfo_var;
extern const MethodInfo* MarkerlessMenuController_confirmSizeEvent_m4127143624_MethodInfo_var;
extern const MethodInfo* MarkerlessMenuController_decreaseDistanceEvent_m225527146_MethodInfo_var;
extern const MethodInfo* MarkerlessMenuController_increaseDistanceEvent_m1525724254_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern Il2CppCodeGenString* _stringLiteral1159688946;
extern const uint32_t MarkerlessMenuController_Start_m642934771_MetadataUsageId;
extern "C"  void MarkerlessMenuController_Start_m642934771 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_Start_m642934771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		__this->set_imageTarget_5(L_0);
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1159688946, /*hidden argument*/NULL);
		NullCheck(L_1);
		GetDistance_t254146547 * L_2 = GameObject_GetComponent_TisGetDistance_t254146547_m3541233510(L_1, /*hidden argument*/GameObject_GetComponent_TisGetDistance_t254146547_m3541233510_MethodInfo_var);
		__this->set_getDistance_4(L_2);
		GameObject_t1756533147 * L_3 = __this->get_imageTarget_5();
		NullCheck(L_3);
		MarkerlessPinchGesture_t1797694232 * L_4 = GameObject_GetComponent_TisMarkerlessPinchGesture_t1797694232_m1222991259(L_3, /*hidden argument*/GameObject_GetComponent_TisMarkerlessPinchGesture_t1797694232_m1222991259_MethodInfo_var);
		__this->set_markerlessPinchGesture_3(L_4);
		UIItems_t3002935515 * L_5 = __this->get_uiItems_2();
		NullCheck(L_5);
		Button_t2872111280 * L_6 = L_5->get_closeButton_2();
		NullCheck(L_6);
		ButtonClickedEvent_t2455055323 * L_7 = Button_get_onClick_m1595880935(L_6, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)MarkerlessMenuController_closeEvent_m1344608029_MethodInfo_var);
		UnityAction_t4025899511 * L_9 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		UnityEvent_AddListener_m1596810379(L_7, L_9, /*hidden argument*/NULL);
		UIItems_t3002935515 * L_10 = __this->get_uiItems_2();
		NullCheck(L_10);
		Button_t2872111280 * L_11 = L_10->get_screenShotButton_3();
		NullCheck(L_11);
		ButtonClickedEvent_t2455055323 * L_12 = Button_get_onClick_m1595880935(L_11, /*hidden argument*/NULL);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)MarkerlessMenuController_screenShotEvent_m260816955_MethodInfo_var);
		UnityAction_t4025899511 * L_14 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityEvent_AddListener_m1596810379(L_12, L_14, /*hidden argument*/NULL);
		UIItems_t3002935515 * L_15 = __this->get_uiItems_2();
		NullCheck(L_15);
		Button_t2872111280 * L_16 = L_15->get_confirmSizeButton_4();
		NullCheck(L_16);
		ButtonClickedEvent_t2455055323 * L_17 = Button_get_onClick_m1595880935(L_16, /*hidden argument*/NULL);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)MarkerlessMenuController_confirmSizeEvent_m4127143624_MethodInfo_var);
		UnityAction_t4025899511 * L_19 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_19, __this, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		UnityEvent_AddListener_m1596810379(L_17, L_19, /*hidden argument*/NULL);
		UIItems_t3002935515 * L_20 = __this->get_uiItems_2();
		NullCheck(L_20);
		Button_t2872111280 * L_21 = L_20->get_minusButton_0();
		NullCheck(L_21);
		ButtonClickedEvent_t2455055323 * L_22 = Button_get_onClick_m1595880935(L_21, /*hidden argument*/NULL);
		IntPtr_t L_23;
		L_23.set_m_value_0((void*)(void*)MarkerlessMenuController_decreaseDistanceEvent_m225527146_MethodInfo_var);
		UnityAction_t4025899511 * L_24 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_24, __this, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		UnityEvent_AddListener_m1596810379(L_22, L_24, /*hidden argument*/NULL);
		UIItems_t3002935515 * L_25 = __this->get_uiItems_2();
		NullCheck(L_25);
		Button_t2872111280 * L_26 = L_25->get_plusButton_1();
		NullCheck(L_26);
		ButtonClickedEvent_t2455055323 * L_27 = Button_get_onClick_m1595880935(L_26, /*hidden argument*/NULL);
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)MarkerlessMenuController_increaseDistanceEvent_m1525724254_MethodInfo_var);
		UnityAction_t4025899511 * L_29 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_29, __this, L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		UnityEvent_AddListener_m1596810379(L_27, L_29, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_30 = __this->get_imageTarget_5();
		NullCheck(L_30);
		Transform_t3275118058 * L_31 = GameObject_get_transform_m909382139(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_localPosition_m2533925116(L_31, /*hidden argument*/NULL);
		V_0 = L_32;
		float L_33 = (&V_0)->get_z_3();
		GetDistance_t254146547 * L_34 = __this->get_getDistance_4();
		NullCheck(L_34);
		float L_35 = L_34->get_distanceUnitToCM_2();
		__this->set_currentDistance_6(((float)((float)L_33/(float)L_35)));
		return;
	}
}
// System.Void MarkerlessMenuController::Update()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral242422407;
extern Il2CppCodeGenString* _stringLiteral1789800790;
extern Il2CppCodeGenString* _stringLiteral106545481;
extern Il2CppCodeGenString* _stringLiteral372029377;
extern const uint32_t MarkerlessMenuController_Update_m1687670614_MetadataUsageId;
extern "C"  void MarkerlessMenuController_Update_m1687670614 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_Update_m1687670614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GetDistance_t254146547 * L_0 = __this->get_getDistance_4();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localPosition_m2533925116(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_z_3();
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral242422407, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_7 = __this->get_currentDistance_6();
		float L_8 = L_7;
		Il2CppObject * L_9 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_8);
		String_t* L_10 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1789800790, L_9, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		UIItems_t3002935515 * L_11 = __this->get_uiItems_2();
		NullCheck(L_11);
		Text_t356221433 * L_12 = L_11->get_distanceText_5();
		float L_13 = __this->get_currentDistance_6();
		double L_14 = Math_Round_m3846462091(NULL /*static, unused*/, (((double)((double)((float)((float)L_13/(float)(1000.0f)))))), 1, /*hidden argument*/NULL);
		double L_15 = L_14;
		Il2CppObject * L_16 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral106545481, L_16, _stringLiteral372029377, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_17);
		return;
	}
}
// System.Void MarkerlessMenuController::decreaseDistanceEvent()
extern "C"  void MarkerlessMenuController_decreaseDistanceEvent_m225527146 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_currentDistance_6();
		if ((!(((float)L_0) >= ((float)(1000.0f)))))
		{
			goto IL_0056;
		}
	}
	{
		float L_1 = __this->get_currentDistance_6();
		__this->set_currentDistance_6(((float)((float)L_1-(float)(500.0f))));
		float L_2 = __this->get_currentDistance_6();
		GetDistance_t254146547 * L_3 = __this->get_getDistance_4();
		NullCheck(L_3);
		float L_4 = L_3->get_distanceUnitToCM_2();
		Vector3__ctor_m2638739322((&V_0), (0.0f), (1.0f), ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_imageTarget_5();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = V_0;
		NullCheck(L_6);
		Transform_set_localPosition_m1026930133(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void MarkerlessMenuController::increaseDistanceEvent()
extern "C"  void MarkerlessMenuController_increaseDistanceEvent_m1525724254 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_currentDistance_6();
		if ((!(((float)L_0) <= ((float)(19500.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = __this->get_currentDistance_6();
		__this->set_currentDistance_6(((float)((float)L_1+(float)(500.0f))));
	}

IL_0022:
	{
		float L_2 = __this->get_currentDistance_6();
		GetDistance_t254146547 * L_3 = __this->get_getDistance_4();
		NullCheck(L_3);
		float L_4 = L_3->get_distanceUnitToCM_2();
		Vector3__ctor_m2638739322((&V_0), (0.0f), (1.0f), ((float)((float)L_2*(float)L_4)), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_imageTarget_5();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = V_0;
		NullCheck(L_6);
		Transform_set_localPosition_m1026930133(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController::closeEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2704120344;
extern const uint32_t MarkerlessMenuController_closeEvent_m1344608029_MetadataUsageId;
extern "C"  void MarkerlessMenuController_closeEvent_m1344608029 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_closeEvent_m1344608029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MarkerlessMenuController__CloseUnity3dARView_m2219918690(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2704120344, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController::screenShotEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3958257611;
extern const uint32_t MarkerlessMenuController_screenShotEvent_m260816955_MetadataUsageId;
extern "C"  void MarkerlessMenuController_screenShotEvent_m260816955 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_screenShotEvent_m260816955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MarkerlessMenuController__ScreenShot_m1921154874(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3958257611, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController::confirmSizeEvent()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2891694928;
extern const uint32_t MarkerlessMenuController_confirmSizeEvent_m4127143624_MetadataUsageId;
extern "C"  void MarkerlessMenuController_confirmSizeEvent_m4127143624 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_confirmSizeEvent_m4127143624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerlessPinchGesture_t1797694232 * L_1 = __this->get_markerlessPinchGesture_3();
		NullCheck(L_1);
		AccessableVariable_t3397361249 * L_2 = L_1->get_accessableVariable_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_currentWidth_3();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		MarkerlessMenuController__ConfirmWidth_m1963284504(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerlessPinchGesture_t1797694232 * L_8 = __this->get_markerlessPinchGesture_3();
		NullCheck(L_8);
		AccessableVariable_t3397361249 * L_9 = L_8->get_accessableVariable_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_currentHeight_4();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, L_7, L_12, /*hidden argument*/NULL);
		MarkerlessMenuController__ConfirmHeight_m3076499927(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerlessPinchGesture_t1797694232 * L_15 = __this->get_markerlessPinchGesture_3();
		NullCheck(L_15);
		AccessableVariable_t3397361249 * L_16 = L_15->get_accessableVariable_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_currentPrice_5();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20 = String_Concat_m56707527(NULL /*static, unused*/, L_14, L_19, /*hidden argument*/NULL);
		MarkerlessMenuController__ConfirmPrice_m1664757449(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2891694928, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController::ReturnToMain(System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3954205922;
extern Il2CppCodeGenString* _stringLiteral1530352176;
extern const uint32_t MarkerlessMenuController_ReturnToMain_m157268523_MetadataUsageId;
extern "C"  void MarkerlessMenuController_ReturnToMain_m157268523 (MarkerlessMenuController_t3874082612 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessMenuController_ReturnToMain_m157268523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		String_t* L_0 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral3954205922, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1530352176, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = V_0;
		SceneManager_LoadSceneAsync_m4130852156(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessMenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m2992831972 (UIItems_t3002935515 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessPinchGesture::.ctor()
extern "C"  void MarkerlessPinchGesture__ctor_m1782302171 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method)
{
	{
		__this->set_unitToCM_5((0.01f));
		__this->set_orthoZoomSpeed_6((0.025f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerlessPinchGesture::Start()
extern const MethodInfo* GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern Il2CppCodeGenString* _stringLiteral3970507932;
extern const uint32_t MarkerlessPinchGesture_Start_m1053862679_MetadataUsageId;
extern "C"  void MarkerlessPinchGesture_Start_m1053862679 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessPinchGesture_Start_m1053862679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		NullCheck(L_0);
		MarkerlessMenuController_t3874082612 * L_1 = GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599(L_0, /*hidden argument*/GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599_MethodInfo_var);
		__this->set_markerlessMenuController_3(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArtFrame_t69822562 * L_3 = GameObject_GetComponent_TisArtFrame_t69822562_m457944649(L_2, /*hidden argument*/GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var);
		__this->set_artFrame_4(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3970507932, /*hidden argument*/NULL);
		NullCheck(L_4);
		PersistentScript_t2409504826 * L_5 = GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(L_4, /*hidden argument*/GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var);
		__this->set_persistentScript_9(L_5);
		return;
	}
}
// System.Void MarkerlessPinchGesture::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2659714478;
extern Il2CppCodeGenString* _stringLiteral1004101347;
extern Il2CppCodeGenString* _stringLiteral3127475894;
extern Il2CppCodeGenString* _stringLiteral834479146;
extern Il2CppCodeGenString* _stringLiteral3796781159;
extern Il2CppCodeGenString* _stringLiteral496016797;
extern Il2CppCodeGenString* _stringLiteral4153429643;
extern Il2CppCodeGenString* _stringLiteral443309609;
extern Il2CppCodeGenString* _stringLiteral1322503673;
extern Il2CppCodeGenString* _stringLiteral1199420369;
extern const uint32_t MarkerlessPinchGesture_Update_m4203074650_MetadataUsageId;
extern "C"  void MarkerlessPinchGesture_Update_m4203074650 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerlessPinchGesture_Update_m4203074650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	Touch_t407273883  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Touch_t407273883  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector2_t2243707579  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	{
		bool L_0 = __this->get_isGetSize_12();
		if (L_0)
		{
			goto IL_0070;
		}
	}
	{
		ArtFrame_t69822562 * L_1 = __this->get_artFrame_4();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = L_1->get_art_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_lossyScale_m1638545862(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		float L_6 = __this->get_unitToCM_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		__this->set_widthPriceRatioPerCm_10(L_7);
		ArtFrame_t69822562 * L_8 = __this->get_artFrame_4();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_art_3();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_lossyScale_m1638545862(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_z_3();
		float L_13 = __this->get_unitToCM_5();
		int32_t L_14 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_12/(float)L_13)), /*hidden argument*/NULL);
		__this->set_heightPriceRatioPerCm_11(L_14);
		__this->set_isGetSize_12((bool)1);
	}

IL_0070:
	{
		ArtFrame_t69822562 * L_15 = __this->get_artFrame_4();
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_art_3();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_lossyScale_m1638545862(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_x_1();
		float L_20 = __this->get_unitToCM_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_21 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_19/(float)L_20)), /*hidden argument*/NULL);
		__this->set_widthInCM_7(L_21);
		ArtFrame_t69822562 * L_22 = __this->get_artFrame_4();
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = L_22->get_art_3();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_lossyScale_m1638545862(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_z_3();
		float L_27 = __this->get_unitToCM_5();
		int32_t L_28 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_26/(float)L_27)), /*hidden argument*/NULL);
		__this->set_heightInCM_8(L_28);
		ArtFrame_t69822562 * L_29 = __this->get_artFrame_4();
		NullCheck(L_29);
		TextMesh_t1641806576 * L_30 = L_29->get_width3DText_5();
		int32_t L_31 = __this->get_widthInCM_7();
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m56707527(NULL /*static, unused*/, L_33, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_30);
		TextMesh_set_text_m3390063817(L_30, L_34, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_35 = __this->get_artFrame_4();
		NullCheck(L_35);
		TextMesh_t1641806576 * L_36 = L_35->get_height3DText_6();
		int32_t L_37 = __this->get_heightInCM_8();
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_38);
		String_t* L_40 = String_Concat_m56707527(NULL /*static, unused*/, L_39, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_36);
		TextMesh_set_text_m3390063817(L_36, L_40, /*hidden argument*/NULL);
		MarkerlessMenuController_t3874082612 * L_41 = __this->get_markerlessMenuController_3();
		NullCheck(L_41);
		UIItems_t3002935515 * L_42 = L_41->get_uiItems_2();
		NullCheck(L_42);
		Text_t356221433 * L_43 = L_42->get_sizeText_6();
		ObjectU5BU5D_t3614634134* L_44 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, _stringLiteral1004101347);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1004101347);
		ObjectU5BU5D_t3614634134* L_45 = L_44;
		int32_t L_46 = __this->get_widthInCM_7();
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_48);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_48);
		ObjectU5BU5D_t3614634134* L_49 = L_45;
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, _stringLiteral3127475894);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3127475894);
		ObjectU5BU5D_t3614634134* L_50 = L_49;
		int32_t L_51 = __this->get_heightInCM_8();
		int32_t L_52 = L_51;
		Il2CppObject * L_53 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_53);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_53);
		ObjectU5BU5D_t3614634134* L_54 = L_50;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, _stringLiteral2659714478);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2659714478);
		String_t* L_55 = String_Concat_m3881798623(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_55);
		int32_t L_56 = __this->get_widthInCM_7();
		int32_t L_57 = __this->get_widthPriceRatioPerCm_10();
		V_4 = (((float)((float)((int32_t)((int32_t)L_56-(int32_t)L_57)))));
		int32_t L_58 = __this->get_heightInCM_8();
		int32_t L_59 = __this->get_heightPriceRatioPerCm_11();
		V_5 = (((float)((float)((int32_t)((int32_t)L_58-(int32_t)L_59)))));
		PersistentScript_t2409504826 * L_60 = __this->get_persistentScript_9();
		NullCheck(L_60);
		String_t* L_61 = L_60->get_selectedPrice_5();
		float L_62 = Single_Parse_m1861732734(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		int32_t L_63 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		float L_64 = V_4;
		PersistentScript_t2409504826 * L_65 = __this->get_persistentScript_9();
		NullCheck(L_65);
		String_t* L_66 = L_65->get_selectedWidthAmountPerCm_6();
		float L_67 = Single_Parse_m1861732734(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		int32_t L_68 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_64*(float)L_67)), /*hidden argument*/NULL);
		float L_69 = V_5;
		PersistentScript_t2409504826 * L_70 = __this->get_persistentScript_9();
		NullCheck(L_70);
		String_t* L_71 = L_70->get_selectedHeightAmountPerCm_7();
		float L_72 = Single_Parse_m1861732734(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		int32_t L_73 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_69*(float)L_72)), /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63+(int32_t)L_68))+(int32_t)L_73));
		float L_74 = V_4;
		float L_75 = L_74;
		Il2CppObject * L_76 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_75);
		String_t* L_77 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral834479146, L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		float L_78 = V_5;
		float L_79 = L_78;
		Il2CppObject * L_80 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_79);
		String_t* L_81 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3796781159, L_80, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		PersistentScript_t2409504826 * L_82 = __this->get_persistentScript_9();
		NullCheck(L_82);
		String_t* L_83 = L_82->get_selectedPrice_5();
		String_t* L_84 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral496016797, L_83, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		int32_t L_85 = V_6;
		int32_t L_86 = L_85;
		Il2CppObject * L_87 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_86);
		String_t* L_88 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4153429643, L_87, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
		MarkerlessMenuController_t3874082612 * L_89 = __this->get_markerlessMenuController_3();
		NullCheck(L_89);
		UIItems_t3002935515 * L_90 = L_89->get_uiItems_2();
		NullCheck(L_90);
		Text_t356221433 * L_91 = L_90->get_totalPriceText_7();
		int32_t L_92 = V_6;
		int32_t L_93 = L_92;
		Il2CppObject * L_94 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_93);
		String_t* L_95 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral443309609, L_94, /*hidden argument*/NULL);
		NullCheck(L_91);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_91, L_95);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_96 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_96) == ((uint32_t)2))))
		{
			goto IL_03ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_97 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_7 = L_97;
		Touch_t407273883  L_98 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_8 = L_98;
		Vector2_t2243707579  L_99 = Touch_get_position_m2079703643((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_100 = Touch_get_deltaPosition_m97688791((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_101 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_9 = L_101;
		Vector2_t2243707579  L_102 = Touch_get_position_m2079703643((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_103 = Touch_get_deltaPosition_m97688791((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_104 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_102, L_103, /*hidden argument*/NULL);
		V_10 = L_104;
		Vector2_t2243707579  L_105 = V_9;
		Vector2_t2243707579  L_106 = V_10;
		Vector2_t2243707579  L_107 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_105, L_106, /*hidden argument*/NULL);
		V_12 = L_107;
		float L_108 = Vector2_get_magnitude_m33802565((&V_12), /*hidden argument*/NULL);
		V_11 = L_108;
		Vector2_t2243707579  L_109 = Touch_get_position_m2079703643((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_110 = Touch_get_position_m2079703643((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_111 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		V_14 = L_111;
		float L_112 = Vector2_get_magnitude_m33802565((&V_14), /*hidden argument*/NULL);
		V_13 = L_112;
		float L_113 = V_11;
		float L_114 = V_13;
		V_15 = ((float)((float)L_113-(float)L_114));
		float L_115 = V_15;
		float L_116 = L_115;
		Il2CppObject * L_117 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_116);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_118 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1322503673, L_117, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		float L_119 = V_15;
		float L_120 = __this->get_orthoZoomSpeed_6();
		float L_121 = ((float)((float)L_119*(float)L_120));
		Il2CppObject * L_122 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_121);
		String_t* L_123 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1199420369, L_122, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_123, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_124 = __this->get_artFrame_4();
		NullCheck(L_124);
		GameObject_t1756533147 * L_125 = L_124->get_artFrame_2();
		NullCheck(L_125);
		Transform_t3275118058 * L_126 = GameObject_get_transform_m909382139(L_125, /*hidden argument*/NULL);
		Transform_t3275118058 * L_127 = L_126;
		NullCheck(L_127);
		Vector3_t2243707580  L_128 = Transform_get_localScale_m3074381503(L_127, /*hidden argument*/NULL);
		float L_129 = V_15;
		float L_130 = __this->get_orthoZoomSpeed_6();
		float L_131 = V_15;
		float L_132 = __this->get_orthoZoomSpeed_6();
		float L_133 = V_15;
		float L_134 = __this->get_orthoZoomSpeed_6();
		Vector3_t2243707580  L_135;
		memset(&L_135, 0, sizeof(L_135));
		Vector3__ctor_m2638739322(&L_135, ((float)((float)((float)((float)L_129*(float)L_130))/(float)(35.0f))), ((float)((float)((float)((float)L_131*(float)L_132))/(float)(35.0f))), ((float)((float)((float)((float)L_133*(float)L_134))/(float)(35.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_136 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_128, L_135, /*hidden argument*/NULL);
		NullCheck(L_127);
		Transform_set_localScale_m2325460848(L_127, L_136, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_137 = __this->get_artFrame_4();
		NullCheck(L_137);
		GameObject_t1756533147 * L_138 = L_137->get_artFrame_2();
		NullCheck(L_138);
		Transform_t3275118058 * L_139 = GameObject_get_transform_m909382139(L_138, /*hidden argument*/NULL);
		NullCheck(L_139);
		Vector3_t2243707580  L_140 = Transform_get_localScale_m3074381503(L_139, /*hidden argument*/NULL);
		V_16 = L_140;
		float L_141 = (&V_16)->get_x_1();
		if ((!(((float)L_141) <= ((float)(0.0f)))))
		{
			goto IL_039c;
		}
	}
	{
		ArtFrame_t69822562 * L_142 = __this->get_artFrame_4();
		NullCheck(L_142);
		GameObject_t1756533147 * L_143 = L_142->get_artFrame_2();
		NullCheck(L_143);
		Transform_t3275118058 * L_144 = GameObject_get_transform_m909382139(L_143, /*hidden argument*/NULL);
		Vector3_t2243707580  L_145 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_set_localScale_m2325460848(L_144, L_145, /*hidden argument*/NULL);
	}

IL_039c:
	{
		float L_146 = V_4;
		if ((!(((float)L_146) < ((float)(0.0f)))))
		{
			goto IL_03ce;
		}
	}
	{
		float L_147 = V_5;
		if ((!(((float)L_147) < ((float)(0.0f)))))
		{
			goto IL_03ce;
		}
	}
	{
		ArtFrame_t69822562 * L_148 = __this->get_artFrame_4();
		NullCheck(L_148);
		GameObject_t1756533147 * L_149 = L_148->get_artFrame_2();
		NullCheck(L_149);
		Transform_t3275118058 * L_150 = GameObject_get_transform_m909382139(L_149, /*hidden argument*/NULL);
		Vector3_t2243707580  L_151 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_150);
		Transform_set_localScale_m2325460848(L_150, L_151, /*hidden argument*/NULL);
	}

IL_03ce:
	{
		AccessableVariable_t3397361249 * L_152 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_153 = __this->get_artFrame_4();
		NullCheck(L_153);
		GameObject_t1756533147 * L_154 = L_153->get_artFrame_2();
		NullCheck(L_154);
		Transform_t3275118058 * L_155 = GameObject_get_transform_m909382139(L_154, /*hidden argument*/NULL);
		NullCheck(L_155);
		Vector3_t2243707580  L_156 = Transform_get_localScale_m3074381503(L_155, /*hidden argument*/NULL);
		V_17 = L_156;
		float L_157 = (&V_17)->get_x_1();
		NullCheck(L_152);
		L_152->set_artFrameLossyScaleX_0(L_157);
		AccessableVariable_t3397361249 * L_158 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_159 = __this->get_artFrame_4();
		NullCheck(L_159);
		GameObject_t1756533147 * L_160 = L_159->get_artFrame_2();
		NullCheck(L_160);
		Transform_t3275118058 * L_161 = GameObject_get_transform_m909382139(L_160, /*hidden argument*/NULL);
		NullCheck(L_161);
		Vector3_t2243707580  L_162 = Transform_get_localScale_m3074381503(L_161, /*hidden argument*/NULL);
		V_18 = L_162;
		float L_163 = (&V_18)->get_y_2();
		NullCheck(L_158);
		L_158->set_artFrameLossyScaleY_1(L_163);
		AccessableVariable_t3397361249 * L_164 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_165 = __this->get_artFrame_4();
		NullCheck(L_165);
		GameObject_t1756533147 * L_166 = L_165->get_artFrame_2();
		NullCheck(L_166);
		Transform_t3275118058 * L_167 = GameObject_get_transform_m909382139(L_166, /*hidden argument*/NULL);
		NullCheck(L_167);
		Vector3_t2243707580  L_168 = Transform_get_localScale_m3074381503(L_167, /*hidden argument*/NULL);
		V_19 = L_168;
		float L_169 = (&V_19)->get_z_3();
		NullCheck(L_164);
		L_164->set_artFrameLossyScaleZ_2(L_169);
		AccessableVariable_t3397361249 * L_170 = __this->get_accessableVariable_2();
		int32_t L_171 = __this->get_widthInCM_7();
		NullCheck(L_170);
		L_170->set_currentWidth_3(L_171);
		AccessableVariable_t3397361249 * L_172 = __this->get_accessableVariable_2();
		int32_t L_173 = __this->get_heightInCM_8();
		NullCheck(L_172);
		L_172->set_currentHeight_4(L_173);
		AccessableVariable_t3397361249 * L_174 = __this->get_accessableVariable_2();
		int32_t L_175 = V_6;
		NullCheck(L_174);
		L_174->set_currentPrice_5(L_175);
		return;
	}
}
// System.Void MarkerlessPinchGesture/AccessableVariable::.ctor()
extern "C"  void AccessableVariable__ctor_m3410707070 (AccessableVariable_t3397361249 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController::.ctor()
extern "C"  void MarkerMenuController__ctor_m115110224 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController::_CloseUnity3dARView()
extern "C"  void MarkerMenuController__CloseUnity3dARView_m1469234805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CloseUnity3dARView)();

}
// System.Void MarkerMenuController::_ScreenShot()
extern "C"  void MarkerMenuController__ScreenShot_m4181111469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ScreenShot)();

}
// System.Void MarkerMenuController::_ConfirmWidth(System.String)
extern "C"  void MarkerMenuController__ConfirmWidth_m2587999647 (Il2CppObject * __this /* static, unused */, String_t* ___confirmWidth0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmWidth0' to native representation
	char* ____confirmWidth0_marshaled = NULL;
	____confirmWidth0_marshaled = il2cpp_codegen_marshal_string(___confirmWidth0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmWidth)(____confirmWidth0_marshaled);

	// Marshaling cleanup of parameter '___confirmWidth0' native representation
	il2cpp_codegen_marshal_free(____confirmWidth0_marshaled);
	____confirmWidth0_marshaled = NULL;

}
// System.Void MarkerMenuController::_ConfirmHeight(System.String)
extern "C"  void MarkerMenuController__ConfirmHeight_m4154330880 (Il2CppObject * __this /* static, unused */, String_t* ___confirmHeight0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmHeight0' to native representation
	char* ____confirmHeight0_marshaled = NULL;
	____confirmHeight0_marshaled = il2cpp_codegen_marshal_string(___confirmHeight0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmHeight)(____confirmHeight0_marshaled);

	// Marshaling cleanup of parameter '___confirmHeight0' native representation
	il2cpp_codegen_marshal_free(____confirmHeight0_marshaled);
	____confirmHeight0_marshaled = NULL;

}
// System.Void MarkerMenuController::_ConfirmPrice(System.String)
extern "C"  void MarkerMenuController__ConfirmPrice_m2206267518 (Il2CppObject * __this /* static, unused */, String_t* ___confirmPrice0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___confirmPrice0' to native representation
	char* ____confirmPrice0_marshaled = NULL;
	____confirmPrice0_marshaled = il2cpp_codegen_marshal_string(___confirmPrice0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ConfirmPrice)(____confirmPrice0_marshaled);

	// Marshaling cleanup of parameter '___confirmPrice0' native representation
	il2cpp_codegen_marshal_free(____confirmPrice0_marshaled);
	____confirmPrice0_marshaled = NULL;

}
// System.Void MarkerMenuController::Start()
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGetDistance_t254146547_m3541233510_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisDetectHandler_t4061016557_m4178546280_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMarkerPinchGesture_t947150707_m2758117480_MethodInfo_var;
extern const MethodInfo* MarkerMenuController_closeEvent_m1957353194_MethodInfo_var;
extern const MethodInfo* MarkerMenuController_screenShotEvent_m538650796_MethodInfo_var;
extern const MethodInfo* MarkerMenuController_confirmSizeEvent_m269386821_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern Il2CppCodeGenString* _stringLiteral1159688946;
extern const uint32_t MarkerMenuController_Start_m4009115908_MetadataUsageId;
extern "C"  void MarkerMenuController_Start_m4009115908 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_Start_m4009115908_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		__this->set_imageTarget_6(L_0);
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1159688946, /*hidden argument*/NULL);
		NullCheck(L_1);
		GetDistance_t254146547 * L_2 = GameObject_GetComponent_TisGetDistance_t254146547_m3541233510(L_1, /*hidden argument*/GameObject_GetComponent_TisGetDistance_t254146547_m3541233510_MethodInfo_var);
		__this->set_getDistance_4(L_2);
		GameObject_t1756533147 * L_3 = __this->get_imageTarget_6();
		NullCheck(L_3);
		DetectHandler_t4061016557 * L_4 = GameObject_GetComponent_TisDetectHandler_t4061016557_m4178546280(L_3, /*hidden argument*/GameObject_GetComponent_TisDetectHandler_t4061016557_m4178546280_MethodInfo_var);
		__this->set_detectHandler_5(L_4);
		GameObject_t1756533147 * L_5 = __this->get_imageTarget_6();
		NullCheck(L_5);
		MarkerPinchGesture_t947150707 * L_6 = GameObject_GetComponent_TisMarkerPinchGesture_t947150707_m2758117480(L_5, /*hidden argument*/GameObject_GetComponent_TisMarkerPinchGesture_t947150707_m2758117480_MethodInfo_var);
		__this->set_markerPinchGesture_3(L_6);
		UIItems_t2800976608 * L_7 = __this->get_uiItems_2();
		NullCheck(L_7);
		Button_t2872111280 * L_8 = L_7->get_closeButton_0();
		NullCheck(L_8);
		ButtonClickedEvent_t2455055323 * L_9 = Button_get_onClick_m1595880935(L_8, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)MarkerMenuController_closeEvent_m1957353194_MethodInfo_var);
		UnityAction_t4025899511 * L_11 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_11, __this, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEvent_AddListener_m1596810379(L_9, L_11, /*hidden argument*/NULL);
		UIItems_t2800976608 * L_12 = __this->get_uiItems_2();
		NullCheck(L_12);
		Button_t2872111280 * L_13 = L_12->get_screenShotButton_1();
		NullCheck(L_13);
		ButtonClickedEvent_t2455055323 * L_14 = Button_get_onClick_m1595880935(L_13, /*hidden argument*/NULL);
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)MarkerMenuController_screenShotEvent_m538650796_MethodInfo_var);
		UnityAction_t4025899511 * L_16 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_16, __this, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		UnityEvent_AddListener_m1596810379(L_14, L_16, /*hidden argument*/NULL);
		UIItems_t2800976608 * L_17 = __this->get_uiItems_2();
		NullCheck(L_17);
		Button_t2872111280 * L_18 = L_17->get_confirmSizeButton_2();
		NullCheck(L_18);
		ButtonClickedEvent_t2455055323 * L_19 = Button_get_onClick_m1595880935(L_18, /*hidden argument*/NULL);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)MarkerMenuController_confirmSizeEvent_m269386821_MethodInfo_var);
		UnityAction_t4025899511 * L_21 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_21, __this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		UnityEvent_AddListener_m1596810379(L_19, L_21, /*hidden argument*/NULL);
		GetDistance_t254146547 * L_22 = __this->get_getDistance_4();
		NullCheck(L_22);
		float L_23 = L_22->get_distanceInUnit_4();
		GetDistance_t254146547 * L_24 = __this->get_getDistance_4();
		NullCheck(L_24);
		float L_25 = L_24->get_distanceUnitToCM_2();
		__this->set_currentDistance_7(((float)((float)L_23/(float)L_25)));
		return;
	}
}
// System.Void MarkerMenuController::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral106545481;
extern Il2CppCodeGenString* _stringLiteral2659714478;
extern const uint32_t MarkerMenuController_Update_m3378171543_MetadataUsageId;
extern "C"  void MarkerMenuController_Update_m3378171543 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_Update_m3378171543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DetectHandler_t4061016557 * L_0 = __this->get_detectHandler_5();
		NullCheck(L_0);
		bool L_1 = L_0->get_isDetected_2();
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GetDistance_t254146547 * L_2 = __this->get_getDistance_4();
		NullCheck(L_2);
		float L_3 = L_2->get_distanceInUnit_4();
		GetDistance_t254146547 * L_4 = __this->get_getDistance_4();
		NullCheck(L_4);
		float L_5 = L_4->get_distanceUnitToCM_2();
		__this->set_currentDistance_7(((float)((float)L_3/(float)L_5)));
		goto IL_003d;
	}

IL_0032:
	{
		__this->set_currentDistance_7((0.0f));
	}

IL_003d:
	{
		UIItems_t2800976608 * L_6 = __this->get_uiItems_2();
		NullCheck(L_6);
		Text_t356221433 * L_7 = L_6->get_distanceText_3();
		float L_8 = __this->get_currentDistance_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_8/(float)(10.0f))), /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral106545481, L_11, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_12);
		return;
	}
}
// System.Void MarkerMenuController::closeEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2704120344;
extern const uint32_t MarkerMenuController_closeEvent_m1957353194_MetadataUsageId;
extern "C"  void MarkerMenuController_closeEvent_m1957353194 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_closeEvent_m1957353194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MarkerMenuController__CloseUnity3dARView_m1469234805(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2704120344, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController::screenShotEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3958257611;
extern const uint32_t MarkerMenuController_screenShotEvent_m538650796_MetadataUsageId;
extern "C"  void MarkerMenuController_screenShotEvent_m538650796 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_screenShotEvent_m538650796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MarkerMenuController__ScreenShot_m4181111469(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3958257611, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController::confirmSizeEvent()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2891694928;
extern const uint32_t MarkerMenuController_confirmSizeEvent_m269386821_MetadataUsageId;
extern "C"  void MarkerMenuController_confirmSizeEvent_m269386821 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_confirmSizeEvent_m269386821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerPinchGesture_t947150707 * L_1 = __this->get_markerPinchGesture_3();
		NullCheck(L_1);
		AccessableVariable_t4249881888 * L_2 = L_1->get_accessableVariable_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_currentWidth_3();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		MarkerMenuController__ConfirmWidth_m2587999647(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerPinchGesture_t947150707 * L_8 = __this->get_markerPinchGesture_3();
		NullCheck(L_8);
		AccessableVariable_t4249881888 * L_9 = L_8->get_accessableVariable_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_currentHeight_4();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, L_7, L_12, /*hidden argument*/NULL);
		MarkerMenuController__ConfirmHeight_m4154330880(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		MarkerPinchGesture_t947150707 * L_15 = __this->get_markerPinchGesture_3();
		NullCheck(L_15);
		AccessableVariable_t4249881888 * L_16 = L_15->get_accessableVariable_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_currentPrice_5();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20 = String_Concat_m56707527(NULL /*static, unused*/, L_14, L_19, /*hidden argument*/NULL);
		MarkerMenuController__ConfirmPrice_m2206267518(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2891694928, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController::ReturnToMain(System.String)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3954205922;
extern Il2CppCodeGenString* _stringLiteral1530352176;
extern const uint32_t MarkerMenuController_ReturnToMain_m4254932418_MetadataUsageId;
extern "C"  void MarkerMenuController_ReturnToMain_m4254932418 (MarkerMenuController_t1479643319 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerMenuController_ReturnToMain_m4254932418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		String_t* L_0 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral3954205922, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1530352176, /*hidden argument*/NULL);
		V_1 = L_1;
		String_t* L_2 = V_0;
		SceneManager_LoadSceneAsync_m4130852156(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerMenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m3373216083 (UIItems_t2800976608 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerPinchGesture::.ctor()
extern "C"  void MarkerPinchGesture__ctor_m1003233036 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method)
{
	{
		__this->set_unitToCM_5((0.01f));
		__this->set_orthoZoomSpeed_6((0.025f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MarkerPinchGesture::Start()
extern const MethodInfo* GameObject_GetComponent_TisMarkerMenuController_t1479643319_m3421409556_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern Il2CppCodeGenString* _stringLiteral3970507932;
extern const uint32_t MarkerPinchGesture_Start_m678196352_MetadataUsageId;
extern "C"  void MarkerPinchGesture_Start_m678196352 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerPinchGesture_Start_m678196352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		NullCheck(L_0);
		MarkerMenuController_t1479643319 * L_1 = GameObject_GetComponent_TisMarkerMenuController_t1479643319_m3421409556(L_0, /*hidden argument*/GameObject_GetComponent_TisMarkerMenuController_t1479643319_m3421409556_MethodInfo_var);
		__this->set_markerMenuController_3(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArtFrame_t69822562 * L_3 = GameObject_GetComponent_TisArtFrame_t69822562_m457944649(L_2, /*hidden argument*/GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var);
		__this->set_artFrame_4(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3970507932, /*hidden argument*/NULL);
		NullCheck(L_4);
		PersistentScript_t2409504826 * L_5 = GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(L_4, /*hidden argument*/GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var);
		__this->set_persistentScript_9(L_5);
		return;
	}
}
// System.Void MarkerPinchGesture::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2659714478;
extern Il2CppCodeGenString* _stringLiteral1004101347;
extern Il2CppCodeGenString* _stringLiteral3127475894;
extern Il2CppCodeGenString* _stringLiteral1850001558;
extern Il2CppCodeGenString* _stringLiteral3248983806;
extern Il2CppCodeGenString* _stringLiteral2420175083;
extern Il2CppCodeGenString* _stringLiteral2095712431;
extern Il2CppCodeGenString* _stringLiteral834479146;
extern Il2CppCodeGenString* _stringLiteral3796781159;
extern Il2CppCodeGenString* _stringLiteral496016797;
extern Il2CppCodeGenString* _stringLiteral4153429643;
extern Il2CppCodeGenString* _stringLiteral443309609;
extern Il2CppCodeGenString* _stringLiteral1322503673;
extern Il2CppCodeGenString* _stringLiteral1199420369;
extern const uint32_t MarkerPinchGesture_Update_m3899703603_MetadataUsageId;
extern "C"  void MarkerPinchGesture_Update_m3899703603 (MarkerPinchGesture_t947150707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MarkerPinchGesture_Update_m3899703603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	Touch_t407273883  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Touch_t407273883  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Vector2_t2243707579  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Vector2_t2243707579  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	{
		bool L_0 = __this->get_isGetSize_12();
		if (L_0)
		{
			goto IL_0070;
		}
	}
	{
		ArtFrame_t69822562 * L_1 = __this->get_artFrame_4();
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = L_1->get_art_3();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_lossyScale_m1638545862(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		float L_6 = __this->get_unitToCM_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		__this->set_widthPriceRatioPerCm_10(L_7);
		ArtFrame_t69822562 * L_8 = __this->get_artFrame_4();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_art_3();
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_lossyScale_m1638545862(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_z_3();
		float L_13 = __this->get_unitToCM_5();
		int32_t L_14 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_12/(float)L_13)), /*hidden argument*/NULL);
		__this->set_heightPriceRatioPerCm_11(L_14);
		__this->set_isGetSize_12((bool)1);
	}

IL_0070:
	{
		ArtFrame_t69822562 * L_15 = __this->get_artFrame_4();
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = L_15->get_art_3();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_lossyScale_m1638545862(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_x_1();
		float L_20 = __this->get_unitToCM_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_21 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_19/(float)L_20)), /*hidden argument*/NULL);
		__this->set_widthInCM_7(L_21);
		ArtFrame_t69822562 * L_22 = __this->get_artFrame_4();
		NullCheck(L_22);
		GameObject_t1756533147 * L_23 = L_22->get_art_3();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_lossyScale_m1638545862(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_z_3();
		float L_27 = __this->get_unitToCM_5();
		int32_t L_28 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_26/(float)L_27)), /*hidden argument*/NULL);
		__this->set_heightInCM_8(L_28);
		ArtFrame_t69822562 * L_29 = __this->get_artFrame_4();
		NullCheck(L_29);
		TextMesh_t1641806576 * L_30 = L_29->get_width3DText_5();
		int32_t L_31 = __this->get_widthInCM_7();
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m56707527(NULL /*static, unused*/, L_33, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_30);
		TextMesh_set_text_m3390063817(L_30, L_34, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_35 = __this->get_artFrame_4();
		NullCheck(L_35);
		TextMesh_t1641806576 * L_36 = L_35->get_height3DText_6();
		int32_t L_37 = __this->get_heightInCM_8();
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_38);
		String_t* L_40 = String_Concat_m56707527(NULL /*static, unused*/, L_39, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_36);
		TextMesh_set_text_m3390063817(L_36, L_40, /*hidden argument*/NULL);
		MarkerMenuController_t1479643319 * L_41 = __this->get_markerMenuController_3();
		NullCheck(L_41);
		UIItems_t2800976608 * L_42 = L_41->get_uiItems_2();
		NullCheck(L_42);
		Text_t356221433 * L_43 = L_42->get_sizeText_4();
		ObjectU5BU5D_t3614634134* L_44 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, _stringLiteral1004101347);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1004101347);
		ObjectU5BU5D_t3614634134* L_45 = L_44;
		int32_t L_46 = __this->get_widthInCM_7();
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_48);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_48);
		ObjectU5BU5D_t3614634134* L_49 = L_45;
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, _stringLiteral3127475894);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3127475894);
		ObjectU5BU5D_t3614634134* L_50 = L_49;
		int32_t L_51 = __this->get_heightInCM_8();
		int32_t L_52 = L_51;
		Il2CppObject * L_53 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_53);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_53);
		ObjectU5BU5D_t3614634134* L_54 = L_50;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, _stringLiteral2659714478);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2659714478);
		String_t* L_55 = String_Concat_m3881798623(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_55);
		int32_t L_56 = __this->get_widthInCM_7();
		int32_t L_57 = __this->get_widthPriceRatioPerCm_10();
		V_4 = (((float)((float)((int32_t)((int32_t)L_56-(int32_t)L_57)))));
		int32_t L_58 = __this->get_heightInCM_8();
		int32_t L_59 = __this->get_heightPriceRatioPerCm_11();
		V_5 = (((float)((float)((int32_t)((int32_t)L_58-(int32_t)L_59)))));
		PersistentScript_t2409504826 * L_60 = __this->get_persistentScript_9();
		NullCheck(L_60);
		String_t* L_61 = L_60->get_selectedPrice_5();
		float L_62 = Single_Parse_m1861732734(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		int32_t L_63 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		float L_64 = V_4;
		PersistentScript_t2409504826 * L_65 = __this->get_persistentScript_9();
		NullCheck(L_65);
		String_t* L_66 = L_65->get_selectedWidthAmountPerCm_6();
		float L_67 = Single_Parse_m1861732734(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		int32_t L_68 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_64*(float)L_67)), /*hidden argument*/NULL);
		float L_69 = V_5;
		PersistentScript_t2409504826 * L_70 = __this->get_persistentScript_9();
		NullCheck(L_70);
		String_t* L_71 = L_70->get_selectedHeightAmountPerCm_7();
		float L_72 = Single_Parse_m1861732734(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		int32_t L_73 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_69*(float)L_72)), /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63+(int32_t)L_68))+(int32_t)L_73));
		ObjectU5BU5D_t3614634134* L_74 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, _stringLiteral1850001558);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1850001558);
		ObjectU5BU5D_t3614634134* L_75 = L_74;
		int32_t L_76 = __this->get_widthPriceRatioPerCm_10();
		int32_t L_77 = L_76;
		Il2CppObject * L_78 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_77);
		NullCheck(L_75);
		ArrayElementTypeCheck (L_75, L_78);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_78);
		ObjectU5BU5D_t3614634134* L_79 = L_75;
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, _stringLiteral3248983806);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3248983806);
		ObjectU5BU5D_t3614634134* L_80 = L_79;
		int32_t L_81 = __this->get_widthInCM_7();
		int32_t L_82 = L_81;
		Il2CppObject * L_83 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_80);
		ArrayElementTypeCheck (L_80, L_83);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_83);
		String_t* L_84 = String_Concat_m3881798623(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_85 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, _stringLiteral2420175083);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2420175083);
		ObjectU5BU5D_t3614634134* L_86 = L_85;
		int32_t L_87 = __this->get_heightPriceRatioPerCm_11();
		int32_t L_88 = L_87;
		Il2CppObject * L_89 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_88);
		NullCheck(L_86);
		ArrayElementTypeCheck (L_86, L_89);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_89);
		ObjectU5BU5D_t3614634134* L_90 = L_86;
		NullCheck(L_90);
		ArrayElementTypeCheck (L_90, _stringLiteral2095712431);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2095712431);
		ObjectU5BU5D_t3614634134* L_91 = L_90;
		int32_t L_92 = __this->get_heightInCM_8();
		int32_t L_93 = L_92;
		Il2CppObject * L_94 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_93);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_94);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_94);
		String_t* L_95 = String_Concat_m3881798623(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		float L_96 = V_4;
		float L_97 = L_96;
		Il2CppObject * L_98 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_97);
		String_t* L_99 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral834479146, L_98, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		float L_100 = V_5;
		float L_101 = L_100;
		Il2CppObject * L_102 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_101);
		String_t* L_103 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3796781159, L_102, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		PersistentScript_t2409504826 * L_104 = __this->get_persistentScript_9();
		NullCheck(L_104);
		String_t* L_105 = L_104->get_selectedPrice_5();
		String_t* L_106 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral496016797, L_105, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		int32_t L_107 = V_6;
		int32_t L_108 = L_107;
		Il2CppObject * L_109 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_108);
		String_t* L_110 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral4153429643, L_109, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		MarkerMenuController_t1479643319 * L_111 = __this->get_markerMenuController_3();
		NullCheck(L_111);
		UIItems_t2800976608 * L_112 = L_111->get_uiItems_2();
		NullCheck(L_112);
		Text_t356221433 * L_113 = L_112->get_totalPriceText_5();
		int32_t L_114 = V_6;
		int32_t L_115 = L_114;
		Il2CppObject * L_116 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_115);
		String_t* L_117 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral443309609, L_116, /*hidden argument*/NULL);
		NullCheck(L_113);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_113, L_117);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_118 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_118) == ((uint32_t)2))))
		{
			goto IL_0446;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_119 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_7 = L_119;
		Touch_t407273883  L_120 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_8 = L_120;
		Vector2_t2243707579  L_121 = Touch_get_position_m2079703643((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_122 = Touch_get_deltaPosition_m97688791((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_123 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_9 = L_123;
		Vector2_t2243707579  L_124 = Touch_get_position_m2079703643((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_125 = Touch_get_deltaPosition_m97688791((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_126 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_124, L_125, /*hidden argument*/NULL);
		V_10 = L_126;
		Vector2_t2243707579  L_127 = V_9;
		Vector2_t2243707579  L_128 = V_10;
		Vector2_t2243707579  L_129 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_127, L_128, /*hidden argument*/NULL);
		V_12 = L_129;
		float L_130 = Vector2_get_magnitude_m33802565((&V_12), /*hidden argument*/NULL);
		V_11 = L_130;
		Vector2_t2243707579  L_131 = Touch_get_position_m2079703643((&V_7), /*hidden argument*/NULL);
		Vector2_t2243707579  L_132 = Touch_get_position_m2079703643((&V_8), /*hidden argument*/NULL);
		Vector2_t2243707579  L_133 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_131, L_132, /*hidden argument*/NULL);
		V_14 = L_133;
		float L_134 = Vector2_get_magnitude_m33802565((&V_14), /*hidden argument*/NULL);
		V_13 = L_134;
		float L_135 = V_11;
		float L_136 = V_13;
		V_15 = ((float)((float)L_135-(float)L_136));
		float L_137 = V_15;
		float L_138 = L_137;
		Il2CppObject * L_139 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_138);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_140 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1322503673, L_139, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		float L_141 = V_15;
		float L_142 = __this->get_orthoZoomSpeed_6();
		float L_143 = ((float)((float)L_141*(float)L_142));
		Il2CppObject * L_144 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_143);
		String_t* L_145 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1199420369, L_144, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_145, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_146 = __this->get_artFrame_4();
		NullCheck(L_146);
		GameObject_t1756533147 * L_147 = L_146->get_artFrame_2();
		NullCheck(L_147);
		Transform_t3275118058 * L_148 = GameObject_get_transform_m909382139(L_147, /*hidden argument*/NULL);
		Transform_t3275118058 * L_149 = L_148;
		NullCheck(L_149);
		Vector3_t2243707580  L_150 = Transform_get_localScale_m3074381503(L_149, /*hidden argument*/NULL);
		float L_151 = V_15;
		float L_152 = __this->get_orthoZoomSpeed_6();
		float L_153 = V_15;
		float L_154 = __this->get_orthoZoomSpeed_6();
		float L_155 = V_15;
		float L_156 = __this->get_orthoZoomSpeed_6();
		Vector3_t2243707580  L_157;
		memset(&L_157, 0, sizeof(L_157));
		Vector3__ctor_m2638739322(&L_157, ((float)((float)((float)((float)L_151*(float)L_152))/(float)(35.0f))), ((float)((float)((float)((float)L_153*(float)L_154))/(float)(35.0f))), ((float)((float)((float)((float)L_155*(float)L_156))/(float)(35.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_158 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_150, L_157, /*hidden argument*/NULL);
		NullCheck(L_149);
		Transform_set_localScale_m2325460848(L_149, L_158, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_159 = __this->get_artFrame_4();
		NullCheck(L_159);
		GameObject_t1756533147 * L_160 = L_159->get_artFrame_2();
		NullCheck(L_160);
		Transform_t3275118058 * L_161 = GameObject_get_transform_m909382139(L_160, /*hidden argument*/NULL);
		NullCheck(L_161);
		Vector3_t2243707580  L_162 = Transform_get_localScale_m3074381503(L_161, /*hidden argument*/NULL);
		V_16 = L_162;
		float L_163 = (&V_16)->get_x_1();
		if ((!(((float)L_163) <= ((float)(0.0f)))))
		{
			goto IL_0414;
		}
	}
	{
		ArtFrame_t69822562 * L_164 = __this->get_artFrame_4();
		NullCheck(L_164);
		GameObject_t1756533147 * L_165 = L_164->get_artFrame_2();
		NullCheck(L_165);
		Transform_t3275118058 * L_166 = GameObject_get_transform_m909382139(L_165, /*hidden argument*/NULL);
		Vector3_t2243707580  L_167 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_166);
		Transform_set_localScale_m2325460848(L_166, L_167, /*hidden argument*/NULL);
	}

IL_0414:
	{
		float L_168 = V_4;
		if ((!(((float)L_168) < ((float)(0.0f)))))
		{
			goto IL_0446;
		}
	}
	{
		float L_169 = V_5;
		if ((!(((float)L_169) < ((float)(0.0f)))))
		{
			goto IL_0446;
		}
	}
	{
		ArtFrame_t69822562 * L_170 = __this->get_artFrame_4();
		NullCheck(L_170);
		GameObject_t1756533147 * L_171 = L_170->get_artFrame_2();
		NullCheck(L_171);
		Transform_t3275118058 * L_172 = GameObject_get_transform_m909382139(L_171, /*hidden argument*/NULL);
		Vector3_t2243707580  L_173 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_172);
		Transform_set_localScale_m2325460848(L_172, L_173, /*hidden argument*/NULL);
	}

IL_0446:
	{
		AccessableVariable_t4249881888 * L_174 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_175 = __this->get_artFrame_4();
		NullCheck(L_175);
		GameObject_t1756533147 * L_176 = L_175->get_artFrame_2();
		NullCheck(L_176);
		Transform_t3275118058 * L_177 = GameObject_get_transform_m909382139(L_176, /*hidden argument*/NULL);
		NullCheck(L_177);
		Vector3_t2243707580  L_178 = Transform_get_localScale_m3074381503(L_177, /*hidden argument*/NULL);
		V_17 = L_178;
		float L_179 = (&V_17)->get_x_1();
		NullCheck(L_174);
		L_174->set_artFrameLossyScaleX_0(L_179);
		AccessableVariable_t4249881888 * L_180 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_181 = __this->get_artFrame_4();
		NullCheck(L_181);
		GameObject_t1756533147 * L_182 = L_181->get_artFrame_2();
		NullCheck(L_182);
		Transform_t3275118058 * L_183 = GameObject_get_transform_m909382139(L_182, /*hidden argument*/NULL);
		NullCheck(L_183);
		Vector3_t2243707580  L_184 = Transform_get_localScale_m3074381503(L_183, /*hidden argument*/NULL);
		V_18 = L_184;
		float L_185 = (&V_18)->get_y_2();
		NullCheck(L_180);
		L_180->set_artFrameLossyScaleY_1(L_185);
		AccessableVariable_t4249881888 * L_186 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_187 = __this->get_artFrame_4();
		NullCheck(L_187);
		GameObject_t1756533147 * L_188 = L_187->get_artFrame_2();
		NullCheck(L_188);
		Transform_t3275118058 * L_189 = GameObject_get_transform_m909382139(L_188, /*hidden argument*/NULL);
		NullCheck(L_189);
		Vector3_t2243707580  L_190 = Transform_get_localScale_m3074381503(L_189, /*hidden argument*/NULL);
		V_19 = L_190;
		float L_191 = (&V_19)->get_z_3();
		NullCheck(L_186);
		L_186->set_artFrameLossyScaleZ_2(L_191);
		AccessableVariable_t4249881888 * L_192 = __this->get_accessableVariable_2();
		int32_t L_193 = __this->get_widthInCM_7();
		NullCheck(L_192);
		L_192->set_currentWidth_3(L_193);
		AccessableVariable_t4249881888 * L_194 = __this->get_accessableVariable_2();
		int32_t L_195 = __this->get_heightInCM_8();
		NullCheck(L_194);
		L_194->set_currentHeight_4(L_195);
		AccessableVariable_t4249881888 * L_196 = __this->get_accessableVariable_2();
		int32_t L_197 = V_6;
		NullCheck(L_196);
		L_196->set_currentPrice_5(L_197);
		return;
	}
}
// System.Void MarkerPinchGesture/AccessableVariable::.ctor()
extern "C"  void AccessableVariable__ctor_m770339913 (AccessableVariable_t4249881888 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::.ctor()
extern "C"  void MenuController__ctor_m1077658770 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::_CloseUnity3dARView()
extern "C"  void MenuController__CloseUnity3dARView_m518168999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_CloseUnity3dARView)();

}
// System.Void MenuController::_ScreenShot()
extern "C"  void MenuController__ScreenShot_m3215517515 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_ScreenShot)();

}
// System.Void MenuController::Start()
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* MenuController_closeEvent_m638198012_MethodInfo_var;
extern const MethodInfo* MenuController_screenShotEvent_m3813898030_MethodInfo_var;
extern const MethodInfo* MenuController_addCartEvent_m402579493_MethodInfo_var;
extern const uint32_t MenuController_Start_m3141389506_MetadataUsageId;
extern "C"  void MenuController_Start_m3141389506 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_Start_m3141389506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UIItems_t3088753794 * L_0 = __this->get_uiItems_2();
		NullCheck(L_0);
		Button_t2872111280 * L_1 = L_0->get_closeButton_0();
		NullCheck(L_1);
		ButtonClickedEvent_t2455055323 * L_2 = Button_get_onClick_m1595880935(L_1, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)MenuController_closeEvent_m638198012_MethodInfo_var);
		UnityAction_t4025899511 * L_4 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_4, __this, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityEvent_AddListener_m1596810379(L_2, L_4, /*hidden argument*/NULL);
		UIItems_t3088753794 * L_5 = __this->get_uiItems_2();
		NullCheck(L_5);
		Button_t2872111280 * L_6 = L_5->get_screenShotButton_1();
		NullCheck(L_6);
		ButtonClickedEvent_t2455055323 * L_7 = Button_get_onClick_m1595880935(L_6, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)MenuController_screenShotEvent_m3813898030_MethodInfo_var);
		UnityAction_t4025899511 * L_9 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		UnityEvent_AddListener_m1596810379(L_7, L_9, /*hidden argument*/NULL);
		UIItems_t3088753794 * L_10 = __this->get_uiItems_2();
		NullCheck(L_10);
		Button_t2872111280 * L_11 = L_10->get_addCartButton_2();
		NullCheck(L_11);
		ButtonClickedEvent_t2455055323 * L_12 = Button_get_onClick_m1595880935(L_11, /*hidden argument*/NULL);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)MenuController_addCartEvent_m402579493_MethodInfo_var);
		UnityAction_t4025899511 * L_14 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_14, __this, L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityEvent_AddListener_m1596810379(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::Update()
extern "C"  void MenuController_Update_m843216889 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MenuController::closeEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2704120344;
extern const uint32_t MenuController_closeEvent_m638198012_MetadataUsageId;
extern "C"  void MenuController_closeEvent_m638198012 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_closeEvent_m638198012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MenuController__CloseUnity3dARView_m518168999(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2704120344, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::screenShotEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3958257611;
extern const uint32_t MenuController_screenShotEvent_m3813898030_MetadataUsageId;
extern "C"  void MenuController_screenShotEvent_m3813898030 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_screenShotEvent_m3813898030_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MenuController__ScreenShot_m3215517515(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3958257611, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::addCartEvent()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral153988604;
extern const uint32_t MenuController_addCartEvent_m402579493_MetadataUsageId;
extern "C"  void MenuController_addCartEvent_m402579493 (MenuController_t848154101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_addCartEvent_m402579493_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral153988604, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController::ReturnToMain(System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538073363;
extern const uint32_t MenuController_ReturnToMain_m2516101376_MetadataUsageId;
extern "C"  void MenuController_ReturnToMain_m2516101376 (MenuController_t848154101 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuController_ReturnToMain_m2516101376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadSceneAsync_m2648120039(NULL /*static, unused*/, _stringLiteral3538073363, 0, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m4092615153 (UIItems_t3088753794 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersistentScript::.ctor()
extern "C"  void PersistentScript__ctor_m1235758893 (PersistentScript_t2409504826 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersistentScript::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t PersistentScript_Awake_m3640625178_MetadataUsageId;
extern "C"  void PersistentScript_Awake_m3640625178 (PersistentScript_t2409504826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentScript_Awake_m3640625178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PersistentScript::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral858359652;
extern Il2CppCodeGenString* _stringLiteral104526573;
extern Il2CppCodeGenString* _stringLiteral372029325;
extern const uint32_t PersistentScript_Start_m1814869605_MetadataUsageId;
extern "C"  void PersistentScript_Start_m1814869605 (PersistentScript_t2409504826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentScript_Start_m1814869605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_selectedWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		__this->set_selectedWidth_3(_stringLiteral858359652);
	}

IL_0020:
	{
		String_t* L_3 = __this->get_selectedHeight_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_selectedHeight_4(_stringLiteral858359652);
	}

IL_0040:
	{
		String_t* L_6 = __this->get_selectedPrice_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_8 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0060;
		}
	}
	{
		__this->set_selectedPrice_5(_stringLiteral104526573);
	}

IL_0060:
	{
		String_t* L_9 = __this->get_selectedWidthAmountPerCm_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0080;
		}
	}
	{
		__this->set_selectedWidthAmountPerCm_6(_stringLiteral372029325);
	}

IL_0080:
	{
		String_t* L_12 = __this->get_selectedHeightAmountPerCm_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a0;
		}
	}
	{
		__this->set_selectedHeightAmountPerCm_7(_stringLiteral372029325);
	}

IL_00a0:
	{
		return;
	}
}
// System.Void PersistentScript::Update()
extern "C"  void PersistentScript_Update_m3867800380 (PersistentScript_t2409504826 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PinchGesture::.ctor()
extern "C"  void PinchGesture__ctor_m1146794606 (PinchGesture_t1024774609 * __this, const MethodInfo* method)
{
	{
		__this->set_unitToCM_5((0.0095f));
		__this->set_orthoZoomSpeed_6((0.025f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PinchGesture::Start()
extern const MethodInfo* GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern Il2CppCodeGenString* _stringLiteral3541102038;
extern const uint32_t PinchGesture_Start_m3873565150_MetadataUsageId;
extern "C"  void PinchGesture_Start_m3873565150 (PinchGesture_t1024774609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PinchGesture_Start_m3873565150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		NullCheck(L_0);
		MarkerlessMenuController_t3874082612 * L_1 = GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599(L_0, /*hidden argument*/GameObject_GetComponent_TisMarkerlessMenuController_t3874082612_m4008747599_MethodInfo_var);
		__this->set_markerLessMenuController_3(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3541102038, /*hidden argument*/NULL);
		NullCheck(L_2);
		ArtFrame_t69822562 * L_3 = GameObject_GetComponent_TisArtFrame_t69822562_m457944649(L_2, /*hidden argument*/GameObject_GetComponent_TisArtFrame_t69822562_m457944649_MethodInfo_var);
		__this->set_artFrame_4(L_3);
		return;
	}
}
// System.Void PinchGesture::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2659714478;
extern Il2CppCodeGenString* _stringLiteral1004101347;
extern Il2CppCodeGenString* _stringLiteral2827728199;
extern Il2CppCodeGenString* _stringLiteral2428198168;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral443309609;
extern Il2CppCodeGenString* _stringLiteral1322503673;
extern Il2CppCodeGenString* _stringLiteral1199420369;
extern const uint32_t PinchGesture_Update_m644332661_MetadataUsageId;
extern "C"  void PinchGesture_Update_m644332661 (PinchGesture_t1024774609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PinchGesture_Update_m644332661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t407273883  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t2243707580  V_14;
	memset(&V_14, 0, sizeof(V_14));
	{
		ArtFrame_t69822562 * L_0 = __this->get_artFrame_4();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = L_0->get_art_3();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_lossyScale_m1638545862(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_x_1();
		float L_5 = __this->get_unitToCM_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_6 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		__this->set_widthInCM_7(L_6);
		ArtFrame_t69822562 * L_7 = __this->get_artFrame_4();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_art_3();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_lossyScale_m1638545862(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_z_3();
		float L_12 = __this->get_unitToCM_5();
		int32_t L_13 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)L_11/(float)L_12)), /*hidden argument*/NULL);
		__this->set_heightInCM_8(L_13);
		int32_t L_14 = __this->get_widthInCM_7();
		__this->set_widthInInches_9(((float)((float)(((float)((float)L_14)))*(float)(0.3937f))));
		int32_t L_15 = __this->get_heightInCM_8();
		__this->set_heightInInches_10(((float)((float)(((float)((float)L_15)))*(float)(0.3937f))));
		ArtFrame_t69822562 * L_16 = __this->get_artFrame_4();
		NullCheck(L_16);
		TextMesh_t1641806576 * L_17 = L_16->get_width3DText_5();
		int32_t L_18 = __this->get_widthInCM_7();
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m56707527(NULL /*static, unused*/, L_20, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_17);
		TextMesh_set_text_m3390063817(L_17, L_21, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_22 = __this->get_artFrame_4();
		NullCheck(L_22);
		TextMesh_t1641806576 * L_23 = L_22->get_height3DText_6();
		int32_t L_24 = __this->get_heightInCM_8();
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_25);
		String_t* L_27 = String_Concat_m56707527(NULL /*static, unused*/, L_26, _stringLiteral2659714478, /*hidden argument*/NULL);
		NullCheck(L_23);
		TextMesh_set_text_m3390063817(L_23, L_27, /*hidden argument*/NULL);
		MarkerlessMenuController_t3874082612 * L_28 = __this->get_markerLessMenuController_3();
		NullCheck(L_28);
		UIItems_t3002935515 * L_29 = L_28->get_uiItems_2();
		NullCheck(L_29);
		Text_t356221433 * L_30 = L_29->get_sizeText_6();
		StringU5BU5D_t1642385972* L_31 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral1004101347);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1004101347);
		StringU5BU5D_t1642385972* L_32 = L_31;
		float* L_33 = __this->get_address_of_widthInInches_9();
		String_t* L_34 = Single_ToString_m2359963436(L_33, _stringLiteral2827728199, /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_34);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_34);
		StringU5BU5D_t1642385972* L_35 = L_32;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral2428198168);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2428198168);
		StringU5BU5D_t1642385972* L_36 = L_35;
		float* L_37 = __this->get_address_of_heightInInches_10();
		String_t* L_38 = Single_ToString_m2359963436(L_37, _stringLiteral2827728199, /*hidden argument*/NULL);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_38);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_38);
		StringU5BU5D_t1642385972* L_39 = L_36;
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, _stringLiteral372029312);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029312);
		String_t* L_40 = String_Concat_m626692867(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_30, L_40);
		MarkerlessMenuController_t3874082612 * L_41 = __this->get_markerLessMenuController_3();
		NullCheck(L_41);
		UIItems_t3002935515 * L_42 = L_41->get_uiItems_2();
		NullCheck(L_42);
		Text_t356221433 * L_43 = L_42->get_totalPriceText_7();
		float L_44 = __this->get_widthInInches_9();
		float L_45 = __this->get_heightInInches_10();
		int32_t L_46 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)((float)((float)L_44*(float)(3.0f)))+(float)((float)((float)L_45*(float)(3.0f))))), /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_47);
		String_t* L_49 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral443309609, L_48, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_49);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_50 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_50) == ((uint32_t)2))))
		{
			goto IL_02b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_51 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_51;
		Touch_t407273883  L_52 = Input_GetTouch_m1463942798(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_3 = L_52;
		Vector2_t2243707579  L_53 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		Vector2_t2243707579  L_54 = Touch_get_deltaPosition_m97688791((&V_2), /*hidden argument*/NULL);
		Vector2_t2243707579  L_55 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		V_4 = L_55;
		Vector2_t2243707579  L_56 = Touch_get_position_m2079703643((&V_3), /*hidden argument*/NULL);
		Vector2_t2243707579  L_57 = Touch_get_deltaPosition_m97688791((&V_3), /*hidden argument*/NULL);
		Vector2_t2243707579  L_58 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_56, L_57, /*hidden argument*/NULL);
		V_5 = L_58;
		Vector2_t2243707579  L_59 = V_4;
		Vector2_t2243707579  L_60 = V_5;
		Vector2_t2243707579  L_61 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		V_7 = L_61;
		float L_62 = Vector2_get_magnitude_m33802565((&V_7), /*hidden argument*/NULL);
		V_6 = L_62;
		Vector2_t2243707579  L_63 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		Vector2_t2243707579  L_64 = Touch_get_position_m2079703643((&V_3), /*hidden argument*/NULL);
		Vector2_t2243707579  L_65 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		V_9 = L_65;
		float L_66 = Vector2_get_magnitude_m33802565((&V_9), /*hidden argument*/NULL);
		V_8 = L_66;
		float L_67 = V_6;
		float L_68 = V_8;
		V_10 = ((float)((float)L_67-(float)L_68));
		float L_69 = V_10;
		float L_70 = L_69;
		Il2CppObject * L_71 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_70);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_72 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1322503673, L_71, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		float L_73 = V_10;
		float L_74 = __this->get_orthoZoomSpeed_6();
		float L_75 = ((float)((float)L_73*(float)L_74));
		Il2CppObject * L_76 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_75);
		String_t* L_77 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1199420369, L_76, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_78 = __this->get_artFrame_4();
		NullCheck(L_78);
		GameObject_t1756533147 * L_79 = L_78->get_artFrame_2();
		NullCheck(L_79);
		Transform_t3275118058 * L_80 = GameObject_get_transform_m909382139(L_79, /*hidden argument*/NULL);
		Transform_t3275118058 * L_81 = L_80;
		NullCheck(L_81);
		Vector3_t2243707580  L_82 = Transform_get_localScale_m3074381503(L_81, /*hidden argument*/NULL);
		float L_83 = V_10;
		float L_84 = __this->get_orthoZoomSpeed_6();
		float L_85 = V_10;
		float L_86 = __this->get_orthoZoomSpeed_6();
		float L_87 = V_10;
		float L_88 = __this->get_orthoZoomSpeed_6();
		Vector3_t2243707580  L_89;
		memset(&L_89, 0, sizeof(L_89));
		Vector3__ctor_m2638739322(&L_89, ((float)((float)((float)((float)L_83*(float)L_84))/(float)(35.0f))), ((float)((float)((float)((float)L_85*(float)L_86))/(float)(35.0f))), ((float)((float)((float)((float)L_87*(float)L_88))/(float)(35.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_90 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_82, L_89, /*hidden argument*/NULL);
		NullCheck(L_81);
		Transform_set_localScale_m2325460848(L_81, L_90, /*hidden argument*/NULL);
		ArtFrame_t69822562 * L_91 = __this->get_artFrame_4();
		NullCheck(L_91);
		GameObject_t1756533147 * L_92 = L_91->get_artFrame_2();
		NullCheck(L_92);
		Transform_t3275118058 * L_93 = GameObject_get_transform_m909382139(L_92, /*hidden argument*/NULL);
		NullCheck(L_93);
		Vector3_t2243707580  L_94 = Transform_get_localScale_m3074381503(L_93, /*hidden argument*/NULL);
		V_11 = L_94;
		float L_95 = (&V_11)->get_x_1();
		if ((!(((float)L_95) <= ((float)(0.0f)))))
		{
			goto IL_02b1;
		}
	}
	{
		ArtFrame_t69822562 * L_96 = __this->get_artFrame_4();
		NullCheck(L_96);
		GameObject_t1756533147 * L_97 = L_96->get_artFrame_2();
		NullCheck(L_97);
		Transform_t3275118058 * L_98 = GameObject_get_transform_m909382139(L_97, /*hidden argument*/NULL);
		Vector3_t2243707580  L_99 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_98);
		Transform_set_localScale_m2325460848(L_98, L_99, /*hidden argument*/NULL);
	}

IL_02b1:
	{
		AccessableVariable_t686036674 * L_100 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_101 = __this->get_artFrame_4();
		NullCheck(L_101);
		GameObject_t1756533147 * L_102 = L_101->get_artFrame_2();
		NullCheck(L_102);
		Transform_t3275118058 * L_103 = GameObject_get_transform_m909382139(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		Vector3_t2243707580  L_104 = Transform_get_localScale_m3074381503(L_103, /*hidden argument*/NULL);
		V_12 = L_104;
		float L_105 = (&V_12)->get_x_1();
		NullCheck(L_100);
		L_100->set_artFrameLossyScaleX_0(L_105);
		AccessableVariable_t686036674 * L_106 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_107 = __this->get_artFrame_4();
		NullCheck(L_107);
		GameObject_t1756533147 * L_108 = L_107->get_artFrame_2();
		NullCheck(L_108);
		Transform_t3275118058 * L_109 = GameObject_get_transform_m909382139(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		Vector3_t2243707580  L_110 = Transform_get_localScale_m3074381503(L_109, /*hidden argument*/NULL);
		V_13 = L_110;
		float L_111 = (&V_13)->get_y_2();
		NullCheck(L_106);
		L_106->set_artFrameLossyScaleY_1(L_111);
		AccessableVariable_t686036674 * L_112 = __this->get_accessableVariable_2();
		ArtFrame_t69822562 * L_113 = __this->get_artFrame_4();
		NullCheck(L_113);
		GameObject_t1756533147 * L_114 = L_113->get_artFrame_2();
		NullCheck(L_114);
		Transform_t3275118058 * L_115 = GameObject_get_transform_m909382139(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		Vector3_t2243707580  L_116 = Transform_get_localScale_m3074381503(L_115, /*hidden argument*/NULL);
		V_14 = L_116;
		float L_117 = (&V_14)->get_z_3();
		NullCheck(L_112);
		L_112->set_artFrameLossyScaleZ_2(L_117);
		return;
	}
}
// System.Void PinchGesture/AccessableVariable::.ctor()
extern "C"  void AccessableVariable__ctor_m3710508327 (AccessableVariable_t686036674 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void URLImage::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t URLImage__ctor_m1246517459_MetadataUsageId;
extern "C"  void URLImage__ctor_m1246517459 (URLImage_t3675617922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (URLImage__ctor_m1246517459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_url_2(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator URLImage::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t3672507052_il2cpp_TypeInfo_var;
extern const uint32_t URLImage_Start_m3471863665_MetadataUsageId;
extern "C"  Il2CppObject * URLImage_Start_m3471863665 (URLImage_t3675617922 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (URLImage_Start_m3471863665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t3672507052 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t3672507052 * L_0 = (U3CStartU3Ec__Iterator0_t3672507052 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t3672507052_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3826209459(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t3672507052 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CStartU3Ec__Iterator0_t3672507052 * L_2 = V_0;
		return L_2;
	}
}
// System.Void URLImage::Update()
extern "C"  void URLImage_Update_m3363539884 (URLImage_t3675617922 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void URLImage/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3826209459 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean URLImage/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m2803939486_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3970507932;
extern Il2CppCodeGenString* _stringLiteral318171252;
extern Il2CppCodeGenString* _stringLiteral4167692538;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m1189823173_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1189823173 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m1189823173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00e0;
		}
	}
	{
		goto IL_0129;
	}

IL_0021:
	{
		URLImage_t3675617922 * L_2 = __this->get_U24this_2();
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3970507932, /*hidden argument*/NULL);
		NullCheck(L_3);
		PersistentScript_t2409504826 * L_4 = GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765(L_3, /*hidden argument*/GameObject_GetComponent_TisPersistentScript_t2409504826_m402025765_MethodInfo_var);
		NullCheck(L_2);
		L_2->set_persistentScript_3(L_4);
		URLImage_t3675617922 * L_5 = __this->get_U24this_2();
		URLImage_t3675617922 * L_6 = __this->get_U24this_2();
		NullCheck(L_6);
		PersistentScript_t2409504826 * L_7 = L_6->get_persistentScript_3();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_artImageUrl_2();
		NullCheck(L_5);
		L_5->set_url_2(L_8);
		URLImage_t3675617922 * L_9 = __this->get_U24this_2();
		NullCheck(L_9);
		String_t* L_10 = L_9->get_url_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral318171252, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		URLImage_t3675617922 * L_12 = __this->get_U24this_2();
		NullCheck(L_12);
		String_t* L_13 = L_12->get_url_2();
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_009a;
		}
	}
	{
		URLImage_t3675617922 * L_16 = __this->get_U24this_2();
		NullCheck(L_16);
		String_t* L_17 = L_16->get_url_2();
		if (L_17)
		{
			goto IL_00aa;
		}
	}

IL_009a:
	{
		URLImage_t3675617922 * L_18 = __this->get_U24this_2();
		NullCheck(L_18);
		L_18->set_url_2(_stringLiteral4167692538);
	}

IL_00aa:
	{
		URLImage_t3675617922 * L_19 = __this->get_U24this_2();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_url_2();
		WWW_t2919945039 * L_21 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_21, L_20, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_21);
		WWW_t2919945039 * L_22 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_3(L_22);
		bool L_23 = __this->get_U24disposing_4();
		if (L_23)
		{
			goto IL_00db;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00db:
	{
		goto IL_012b;
	}

IL_00e0:
	{
		URLImage_t3675617922 * L_24 = __this->get_U24this_2();
		NullCheck(L_24);
		Renderer_t257310565 * L_25 = Component_GetComponent_TisRenderer_t257310565_m2803939486(L_24, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m2803939486_MethodInfo_var);
		__this->set_U3CimageRendererU3E__1_1(L_25);
		Renderer_t257310565 * L_26 = __this->get_U3CimageRendererU3E__1_1();
		NullCheck(L_26);
		Material_t193706927 * L_27 = Renderer_get_material_m2553789785(L_26, /*hidden argument*/NULL);
		WWW_t2919945039 * L_28 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_28);
		Texture2D_t3542995729 * L_29 = WWW_get_texture_m1121178301(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		Material_set_mainTexture_m3584203343(L_27, L_29, /*hidden argument*/NULL);
		Renderer_t257310565 * L_30 = __this->get_U3CimageRendererU3E__1_1();
		NullCheck(L_30);
		Material_t193706927 * L_31 = Renderer_get_material_m2553789785(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Texture_t2243626319 * L_32 = Material_get_mainTexture_m432794412(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Texture_set_wrapMode_m333956747(L_32, 1, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_0129:
	{
		return (bool)0;
	}

IL_012b:
	{
		return (bool)1;
	}
}
// System.Object URLImage/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m847187761 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object URLImage/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2587673849 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void URLImage/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2022703690 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void URLImage/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m601701508_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m601701508 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m601701508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void VRIntegrationHelper::.ctor()
extern "C"  void VRIntegrationHelper__ctor_m4069773343 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m1978993906_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_Awake_m681899862_MetadataUsageId;
extern "C"  void VRIntegrationHelper_Awake_m681899862 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Awake_m681899862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Component_GetComponent_TisCamera_t189460977_m1978993906(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m1978993906_MethodInfo_var);
		NullCheck(L_0);
		Camera_set_fieldOfView_m3974156396(L_0, (90.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::Start()
extern Il2CppClass* VuforiaARController_t4061728485_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* VRIntegrationHelper_OnVuforiaStarted_m1154042475_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_Start_m536586683_MetadataUsageId;
extern "C"  void VRIntegrationHelper_Start_m536586683 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_Start_m536586683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t4061728485_il2cpp_TypeInfo_var);
		VuforiaARController_t4061728485 * L_0 = VuforiaARController_get_Instance_m3759800119(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)VRIntegrationHelper_OnVuforiaStarted_m1154042475_MethodInfo_var);
		Action_t3226471752 * L_2 = (Action_t3226471752 *)il2cpp_codegen_object_new(Action_t3226471752_il2cpp_TypeInfo_var);
		Action__ctor_m2606471964(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaARController_RegisterVuforiaStartedCallback_m2843633801(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VRIntegrationHelper::OnVuforiaStarted()
extern Il2CppClass* DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var;
extern Il2CppClass* VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_OnVuforiaStarted_m1154042475_MetadataUsageId;
extern "C"  void VRIntegrationHelper_OnVuforiaStarted_m1154042475 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnVuforiaStarted_m1154042475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var);
		DigitalEyewearARController_t1398758191 * L_0 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_t189460977 * L_1 = DigitalEyewearARController_get_PrimaryCamera_m334515774(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mLeftCamera_4(L_1);
		DigitalEyewearARController_t1398758191 * L_2 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_t189460977 * L_3 = DigitalEyewearARController_get_SecondaryCamera_m1083705270(L_2, /*hidden argument*/NULL);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mRightCamera_5(L_3);
		Camera_t189460977 * L_4 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_4);
		HideExcessAreaAbstractBehaviour_t2687577327 * L_5 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011(L_4, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011_MethodInfo_var);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mLeftExcessAreaBehaviour_6(L_5);
		Camera_t189460977 * L_6 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_6);
		HideExcessAreaAbstractBehaviour_t2687577327 * L_7 = Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011(L_6, /*hidden argument*/Component_GetComponent_TisHideExcessAreaAbstractBehaviour_t2687577327_m1183342011_MethodInfo_var);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mRightExcessAreaBehaviour_7(L_7);
		return;
	}
}
// System.Void VRIntegrationHelper::LateUpdate()
extern Il2CppClass* VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var;
extern Il2CppClass* DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaARController_t4061728485_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t2431285219_m4080560834_MethodInfo_var;
extern const uint32_t VRIntegrationHelper_LateUpdate_m4039758880_MetadataUsageId;
extern "C"  void VRIntegrationHelper_LateUpdate_m4039758880 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_LateUpdate_m4039758880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	BackgroundPlaneBehaviour_t2431285219 * V_8 = NULL;
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraDataAcquired_10();
		if (!L_1)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		bool L_2 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraDataAcquired_11();
		if (!L_2)
		{
			goto IL_02dd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DigitalEyewearARController_t1398758191_il2cpp_TypeInfo_var);
		DigitalEyewearARController_t1398758191 * L_3 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = DigitalEyewearARController_get_CentralAnchorPoint_m1690577486(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_5 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_localRotation_m4001487205(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localRotation_m2055111962(L_4, L_7, /*hidden argument*/NULL);
		DigitalEyewearARController_t1398758191 * L_8 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = DigitalEyewearARController_get_CentralAnchorPoint_m1690577486(L_8, /*hidden argument*/NULL);
		Camera_t189460977 * L_10 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_localPosition_m2533925116(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m1026930133(L_9, L_12, /*hidden argument*/NULL);
		Camera_t189460977 * L_13 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_localPosition_m2533925116(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		Camera_t189460977 * L_16 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_16);
		Rect_t3681755626  L_17 = Camera_get_pixelRect_m2084185953(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Camera_t189460977 * L_18 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = Component_get_transform_m2697483695(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = Transform_get_right_m440863970(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Vector3_t2243707580  L_21 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		Camera_t189460977 * L_22 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_22);
		float L_23 = Camera_get_stereoSeparation_m287594473(L_22, /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_24, (-0.5f), /*hidden argument*/NULL);
		V_2 = L_25;
		Camera_t189460977 * L_26 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
		Camera_t189460977 * L_28 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = V_2;
		Vector3_t2243707580  L_32 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_position_m2469242620(L_27, L_32, /*hidden argument*/NULL);
		Camera_t189460977 * L_33 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		Rect_t3681755626  L_34 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraPixelRect_8();
		NullCheck(L_33);
		Camera_set_pixelRect_m1366013782(L_33, L_34, /*hidden argument*/NULL);
		Camera_t189460977 * L_35 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_localPosition_m2533925116(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		Camera_t189460977 * L_38 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_38);
		Rect_t3681755626  L_39 = Camera_get_pixelRect_m2084185953(L_38, /*hidden argument*/NULL);
		V_5 = L_39;
		Camera_t189460977 * L_40 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_40);
		Transform_t3275118058 * L_41 = Component_get_transform_m2697483695(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_right_m440863970(L_41, /*hidden argument*/NULL);
		V_7 = L_42;
		Vector3_t2243707580  L_43 = Vector3_get_normalized_m936072361((&V_7), /*hidden argument*/NULL);
		Camera_t189460977 * L_44 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_44);
		float L_45 = Camera_get_stereoSeparation_m287594473(L_44, /*hidden argument*/NULL);
		Vector3_t2243707580  L_46 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_46, (0.5f), /*hidden argument*/NULL);
		V_6 = L_47;
		Camera_t189460977 * L_48 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_48);
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(L_48, /*hidden argument*/NULL);
		Camera_t189460977 * L_50 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = Component_get_transform_m2697483695(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t2243707580  L_52 = Transform_get_position_m1104419803(L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_6;
		Vector3_t2243707580  L_54 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_position_m2469242620(L_49, L_54, /*hidden argument*/NULL);
		Camera_t189460977 * L_55 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		Rect_t3681755626  L_56 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraPixelRect_9();
		NullCheck(L_55);
		Camera_set_pixelRect_m1366013782(L_55, L_56, /*hidden argument*/NULL);
		Camera_t189460977 * L_57 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_57);
		BackgroundPlaneBehaviour_t2431285219 * L_58 = Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t2431285219_m4080560834(L_57, /*hidden argument*/Component_GetComponentInChildren_TisBackgroundPlaneBehaviour_t2431285219_m4080560834_MethodInfo_var);
		V_8 = L_58;
		BackgroundPlaneBehaviour_t2431285219 * L_59 = V_8;
		Camera_t189460977 * L_60 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_60);
		Transform_t3275118058 * L_61 = Component_get_transform_m2697483695(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t2243707580  L_62 = Transform_get_position_m1104419803(L_61, /*hidden argument*/NULL);
		DigitalEyewearARController_t1398758191 * L_63 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_63);
		Transform_t3275118058 * L_64 = DigitalEyewearARController_get_CentralAnchorPoint_m1690577486(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Vector3_t2243707580  L_65 = Transform_get_position_m1104419803(L_64, /*hidden argument*/NULL);
		Vector3_t2243707580  L_66 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_62, L_65, /*hidden argument*/NULL);
		NullCheck(L_59);
		BackgroundPlaneAbstractBehaviour_set_BackgroundOffset_m48119280(L_59, L_66, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t2687577327 * L_67 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftExcessAreaBehaviour_6();
		Camera_t189460977 * L_68 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = Component_get_transform_m2697483695(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		Vector3_t2243707580  L_70 = Transform_get_position_m1104419803(L_69, /*hidden argument*/NULL);
		DigitalEyewearARController_t1398758191 * L_71 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = DigitalEyewearARController_get_CentralAnchorPoint_m1690577486(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t2243707580  L_73 = Transform_get_position_m1104419803(L_72, /*hidden argument*/NULL);
		Vector3_t2243707580  L_74 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		NullCheck(L_67);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m3820567390(L_67, L_74, /*hidden argument*/NULL);
		HideExcessAreaAbstractBehaviour_t2687577327 * L_75 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightExcessAreaBehaviour_7();
		Camera_t189460977 * L_76 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_76);
		Transform_t3275118058 * L_77 = Component_get_transform_m2697483695(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t2243707580  L_78 = Transform_get_position_m1104419803(L_77, /*hidden argument*/NULL);
		DigitalEyewearARController_t1398758191 * L_79 = DigitalEyewearARController_get_Instance_m277595763(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_t3275118058 * L_80 = DigitalEyewearARController_get_CentralAnchorPoint_m1690577486(L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t2243707580  L_81 = Transform_get_position_m1104419803(L_80, /*hidden argument*/NULL);
		Vector3_t2243707580  L_82 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_78, L_81, /*hidden argument*/NULL);
		NullCheck(L_75);
		HideExcessAreaAbstractBehaviour_set_PlaneOffset_m3820567390(L_75, L_82, /*hidden argument*/NULL);
		Transform_t3275118058 * L_83 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_83, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_020f;
		}
	}
	{
		Transform_t3275118058 * L_85 = __this->get_TrackableParent_13();
		Vector3_t2243707580  L_86 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_85);
		Transform_set_localPosition_m1026930133(L_85, L_86, /*hidden argument*/NULL);
	}

IL_020f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t4061728485_il2cpp_TypeInfo_var);
		VuforiaARController_t4061728485 * L_87 = VuforiaARController_get_Instance_m3759800119(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_87);
		VuforiaARController_UpdateState_m2272296819(L_87, (bool)0, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_88 = __this->get_TrackableParent_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_88, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0249;
		}
	}
	{
		Transform_t3275118058 * L_90 = __this->get_TrackableParent_13();
		Transform_t3275118058 * L_91 = L_90;
		NullCheck(L_91);
		Vector3_t2243707580  L_92 = Transform_get_position_m1104419803(L_91, /*hidden argument*/NULL);
		BackgroundPlaneBehaviour_t2431285219 * L_93 = V_8;
		NullCheck(L_93);
		Vector3_t2243707580  L_94 = BackgroundPlaneAbstractBehaviour_get_BackgroundOffset_m599181539(L_93, /*hidden argument*/NULL);
		Vector3_t2243707580  L_95 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_92, L_94, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_position_m2469242620(L_91, L_95, /*hidden argument*/NULL);
	}

IL_0249:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t4061728485_il2cpp_TypeInfo_var);
		VuforiaARController_t4061728485 * L_96 = VuforiaARController_get_Instance_m3759800119(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_97 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraMatrixOriginal_2();
		NullCheck(L_96);
		VuforiaARController_ApplyCorrectedProjectionMatrix_m3388896403(L_96, L_97, (bool)1, /*hidden argument*/NULL);
		VuforiaARController_t4061728485 * L_98 = VuforiaARController_get_Instance_m3759800119(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_99 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraMatrixOriginal_3();
		NullCheck(L_98);
		VuforiaARController_ApplyCorrectedProjectionMatrix_m3388896403(L_98, L_99, (bool)0, /*hidden argument*/NULL);
		Camera_t189460977 * L_100 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		Camera_t189460977 * L_101 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_101);
		Matrix4x4_t2933234003  L_102 = Camera_get_projectionMatrix_m2365994324(L_101, /*hidden argument*/NULL);
		Camera_t189460977 * L_103 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_103);
		Matrix4x4_t2933234003  L_104 = Camera_get_projectionMatrix_m2365994324(L_103, /*hidden argument*/NULL);
		NullCheck(L_100);
		Camera_SetStereoProjectionMatrices_m1833429783(L_100, L_102, L_104, /*hidden argument*/NULL);
		Camera_t189460977 * L_105 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		Camera_t189460977 * L_106 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_106);
		Matrix4x4_t2933234003  L_107 = Camera_get_projectionMatrix_m2365994324(L_106, /*hidden argument*/NULL);
		Camera_t189460977 * L_108 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_108);
		Matrix4x4_t2933234003  L_109 = Camera_get_projectionMatrix_m2365994324(L_108, /*hidden argument*/NULL);
		NullCheck(L_105);
		Camera_SetStereoProjectionMatrices_m1833429783(L_105, L_107, L_109, /*hidden argument*/NULL);
		Camera_t189460977 * L_110 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_110);
		Transform_t3275118058 * L_111 = Component_get_transform_m2697483695(L_110, /*hidden argument*/NULL);
		Vector3_t2243707580  L_112 = V_0;
		NullCheck(L_111);
		Transform_set_localPosition_m1026930133(L_111, L_112, /*hidden argument*/NULL);
		Camera_t189460977 * L_113 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		Rect_t3681755626  L_114 = V_1;
		NullCheck(L_113);
		Camera_set_pixelRect_m1366013782(L_113, L_114, /*hidden argument*/NULL);
		Camera_t189460977 * L_115 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_115);
		Transform_t3275118058 * L_116 = Component_get_transform_m2697483695(L_115, /*hidden argument*/NULL);
		Vector3_t2243707580  L_117 = V_4;
		NullCheck(L_116);
		Transform_set_localPosition_m1026930133(L_116, L_117, /*hidden argument*/NULL);
		Camera_t189460977 * L_118 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		Rect_t3681755626  L_119 = V_5;
		NullCheck(L_118);
		Camera_set_pixelRect_m1366013782(L_118, L_119, /*hidden argument*/NULL);
	}

IL_02dd:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::OnPreRender()
extern Il2CppClass* VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern const uint32_t VRIntegrationHelper_OnPreRender_m1211949763_MetadataUsageId;
extern "C"  void VRIntegrationHelper_OnPreRender_m1211949763 (VRIntegrationHelper_t556656694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VRIntegrationHelper_OnPreRender_m1211949763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_IsLeft_12();
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		bool L_1 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCameraDataAcquired_10();
		if (L_1)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_2 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_2);
		Matrix4x4_t2933234003  L_3 = Camera_get_projectionMatrix_m2365994324(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_4 = VuforiaRuntimeUtilities_MatrixIsNaN_m431823786(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_5 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_5);
		Matrix4x4_t2933234003  L_6 = Camera_get_projectionMatrix_m2365994324(L_5, /*hidden argument*/NULL);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraMatrixOriginal_2(L_6);
		Camera_t189460977 * L_7 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mLeftCamera_4();
		NullCheck(L_7);
		Rect_t3681755626  L_8 = Camera_get_pixelRect_m2084185953(L_7, /*hidden argument*/NULL);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraPixelRect_8(L_8);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mLeftCameraDataAcquired_10((bool)1);
	}

IL_004d:
	{
		goto IL_0094;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		bool L_9 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCameraDataAcquired_11();
		if (L_9)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_10 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_10);
		Matrix4x4_t2933234003  L_11 = Camera_get_projectionMatrix_m2365994324(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_12 = VuforiaRuntimeUtilities_MatrixIsNaN_m431823786(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var);
		Camera_t189460977 * L_13 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_13);
		Matrix4x4_t2933234003  L_14 = Camera_get_projectionMatrix_m2365994324(L_13, /*hidden argument*/NULL);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraMatrixOriginal_3(L_14);
		Camera_t189460977 * L_15 = ((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->get_mRightCamera_5();
		NullCheck(L_15);
		Rect_t3681755626  L_16 = Camera_get_pixelRect_m2084185953(L_15, /*hidden argument*/NULL);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraPixelRect_9(L_16);
		((VRIntegrationHelper_t556656694_StaticFields*)VRIntegrationHelper_t556656694_il2cpp_TypeInfo_var->static_fields)->set_mRightCameraDataAcquired_11((bool)1);
	}

IL_0094:
	{
		return;
	}
}
// System.Void VRIntegrationHelper::.cctor()
extern "C"  void VRIntegrationHelper__cctor_m708693476 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C"  void AndroidUnityPlayer__ctor_m2233000524 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C"  void AndroidUnityPlayer_LoadNativeLibraries_m694869242 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m2955245326(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C"  void AndroidUnityPlayer_InitializePlatform_m2952017853 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m1583405850(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.AndroidUnityPlayer::InitializeVuforia(System.String)
extern Il2CppClass* VuforiaRenderer_t2933102835_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_InitializeVuforia_m3815589415_MetadataUsageId;
extern "C"  int32_t AndroidUnityPlayer_InitializeVuforia_m3815589415 (AndroidUnityPlayer_t852788525 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_InitializeVuforia_m3815589415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRenderer_t2933102835_il2cpp_TypeInfo_var);
		VuforiaRenderer_t2933102835 * L_0 = VuforiaRenderer_get_Instance_m1621768183(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* Vuforia.VuforiaRenderer/RendererAPI Vuforia.VuforiaRenderer::GetRendererAPI() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		String_t* L_3 = ___licenseKey0;
		int32_t L_4 = AndroidUnityPlayer_InitVuforia_m2858754529(__this, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m1417263607(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		int32_t L_6 = V_1;
		return (int32_t)(L_6);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::StartScene()
extern "C"  void AndroidUnityPlayer_StartScene_m1635484230 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_Update_m4198269979_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_Update_m4198269979 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_Update_m4198269979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2740261893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m1417263607(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2917504922(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m2044239264(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = __this->get_mFramesSinceLastOrientationReset_4();
		__this->set_mFramesSinceLastOrientationReset_4(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_OnPause_m2222496097_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_OnPause_m2222496097 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnPause_m2222496097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m2422224752(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_OnResume_m3489453528_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_OnResume_m3489453528 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnResume_m3489453528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2186520633(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_OnDestroy_m3206719289_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_OnDestroy_m3206719289 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_OnDestroy_m3206719289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m4072609744(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C"  void AndroidUnityPlayer_Dispose_m2709691617 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C"  void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m2955245326 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C"  void AndroidUnityPlayer_InitAndroidPlatform_m1583405850 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitVuforia(System.Int32,System.String)
extern "C"  int32_t AndroidUnityPlayer_InitVuforia_m2858754529 (AndroidUnityPlayer_t852788525 * __this, int32_t ___rendererAPI0, String_t* ___licenseKey1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_InitializeSurface_m1417263607_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_InitializeSurface_m1417263607 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_InitializeSurface_m1417263607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m3675640541(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m2917504922(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m2044239264(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C"  void AndroidUnityPlayer_ResetUnityScreenOrientation_m2917504922 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_2(L_0);
		__this->set_mFramesSinceLastOrientationReset_4(0);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t AndroidUnityPlayer_CheckOrientation_m2044239264_MetadataUsageId;
extern "C"  void AndroidUnityPlayer_CheckOrientation_m2044239264 (AndroidUnityPlayer_t852788525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AndroidUnityPlayer_CheckOrientation_m2044239264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_mFramesSinceLastOrientationReset_4();
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		V_0 = (bool)((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = __this->get_mScreenOrientation_2();
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = __this->get_mJavaScreenOrientation_3();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->set_mJavaScreenOrientation_3(L_8);
		int32_t L_9 = __this->get_mJavaScreenOrientation_3();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m3106547277(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->set_mFramesSinceLastJavaOrientationCheck_5(0);
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = __this->get_mFramesSinceLastJavaOrientationCheck_5();
		__this->set_mFramesSinceLastJavaOrientationCheck_5(((int32_t)((int32_t)L_10+(int32_t)1)));
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern Il2CppClass* BackgroundPlaneAbstractBehaviour_t3732945727_il2cpp_TypeInfo_var;
extern const uint32_t BackgroundPlaneBehaviour__ctor_m2808006244_MetadataUsageId;
extern "C"  void BackgroundPlaneBehaviour__ctor_m2808006244 (BackgroundPlaneBehaviour_t2431285219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackgroundPlaneBehaviour__ctor_m2808006244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t3732945727_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m1193949827(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C"  void CloudRecoBehaviour__ctor_m2555627024 (CloudRecoBehaviour_t3077176941 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m3604390285(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C"  void ComponentFactoryStarterBehaviour__ctor_m1215525256 (ComponentFactoryStarterBehaviour_t3249343815 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t3226471752_0_0_0_var;
extern Il2CppClass* Attribute_t542643598_il2cpp_TypeInfo_var;
extern Il2CppClass* FactorySetter_t648583075_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3226471752_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m2736443864_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1093450066_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2150560895_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1945415651_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2294671071_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m186564181_MethodInfo_var;
extern const uint32_t ComponentFactoryStarterBehaviour_Awake_m583572645_MetadataUsageId;
extern "C"  void ComponentFactoryStarterBehaviour_Awake_m583572645 (ComponentFactoryStarterBehaviour_t3249343815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_Awake_m583572645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2699667469 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Enumerator_t2234397143  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Attribute_t542643598 * V_3 = NULL;
	ObjectU5BU5D_t3614634134* V_4 = NULL;
	int32_t V_5 = 0;
	Action_t3226471752 * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t152480188* L_1 = VirtFuncInvoker1< MethodInfoU5BU5D_t152480188*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t2699667469 * L_2 = Enumerable_ToList_TisMethodInfo_t_m2736443864(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m2736443864_MethodInfo_var);
		V_0 = L_2;
		List_1_t2699667469 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t152480188* L_5 = VirtFuncInvoker1< MethodInfoU5BU5D_t152480188*, int32_t >::Invoke(53 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m1093450066(L_3, (Il2CppObject*)(Il2CppObject*)L_5, /*hidden argument*/List_1_AddRange_m1093450066_MethodInfo_var);
		List_1_t2699667469 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t2234397143  L_7 = List_1_GetEnumerator_m2150560895(L_6, /*hidden argument*/List_1_GetEnumerator_m2150560895_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m1945415651((&V_2), /*hidden argument*/Enumerator_get_Current_m1945415651_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t3614634134* L_10 = VirtFuncInvoker1< ObjectU5BU5D_t3614634134*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, (bool)1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t3614634134* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			int32_t L_13 = L_12;
			Il2CppObject * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
			V_3 = ((Attribute_t542643598 *)CastclassClass(L_14, Attribute_t542643598_il2cpp_TypeInfo_var));
			Attribute_t542643598 * L_15 = V_3;
			if (!((FactorySetter_t648583075 *)IsInstClass(L_15, FactorySetter_t648583075_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Action_t3226471752_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_17 = V_1;
			Delegate_t3022476291 * L_18 = Delegate_CreateDelegate_m2101460062(NULL /*static, unused*/, L_16, __this, L_17, /*hidden argument*/NULL);
			V_6 = ((Action_t3226471752 *)IsInstSealed(L_18, Action_t3226471752_il2cpp_TypeInfo_var));
			Action_t3226471752 * L_19 = V_6;
			if (!L_19)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t3226471752 * L_20 = V_6;
			NullCheck(L_20);
			Action_Invoke_m3801112262(L_20, /*hidden argument*/NULL);
		}

IL_0087:
		{
			int32_t L_21 = V_5;
			V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_22 = V_5;
			ObjectU5BU5D_t3614634134* L_23 = V_4;
			NullCheck(L_23);
			if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_24 = Enumerator_MoveNext_m2294671071((&V_2), /*hidden argument*/Enumerator_MoveNext_m2294671071_MethodInfo_var);
			if (L_24)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m186564181((&V_2), /*hidden argument*/Enumerator_Dispose_m186564181_MethodInfo_var);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaBehaviourComponentFactory_t1383853028_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1055265386;
extern const uint32_t ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3496159002_MetadataUsageId;
extern "C"  void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3496159002 (ComponentFactoryStarterBehaviour_t3249343815 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m3496159002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1055265386, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t1383853028 * L_0 = (VuforiaBehaviourComponentFactory_t1383853028 *)il2cpp_codegen_object_new(VuforiaBehaviourComponentFactory_t1383853028_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m4023364043(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m3406469245(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C"  void CylinderTargetBehaviour__ctor_m2197210535 (CylinderTargetBehaviour_t2091399712 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m3025238902(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DefaultInitializationErrorHandler__ctor_m2781804906_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler__ctor_m2781804906 (DefaultInitializationErrorHandler_t965510117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler__ctor_m2781804906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mErrorText_2(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern Il2CppClass* VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1951195598_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1319028211_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3262798437_MethodInfo_var;
extern const uint32_t DefaultInitializationErrorHandler_Awake_m2567656739_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_Awake_m2567656739 (DefaultInitializationErrorHandler_t965510117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_Awake_m2567656739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_0 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1319028211_MethodInfo_var);
		Action_1_t1951195598 * L_2 = (Action_1_t1951195598 *)il2cpp_codegen_object_new(Action_1_t1951195598_il2cpp_TypeInfo_var);
		Action_1__ctor_m3262798437(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m3262798437_MethodInfo_var);
		NullCheck(L_0);
		VuforiaRuntime_RegisterVuforiaInitErrorCallback_m1800574616(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m1741663736_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3233100706;
extern const uint32_t DefaultInitializationErrorHandler_OnGUI_m2796165714_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_OnGUI_m2796165714 (DefaultInitializationErrorHandler_t965510117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnGUI_m2796165714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_mErrorOccurred_3();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_DrawWindowContent_m1741663736_MethodInfo_var);
		WindowFunction_t3486805455 * L_5 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral3233100706, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern Il2CppClass* VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1951195598_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1319028211_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3262798437_MethodInfo_var;
extern const uint32_t DefaultInitializationErrorHandler_OnDestroy_m3502240837_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_OnDestroy_m3502240837 (DefaultInitializationErrorHandler_t965510117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnDestroy_m3502240837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_0 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1319028211_MethodInfo_var);
		Action_1_t1951195598 * L_2 = (Action_1_t1951195598 *)il2cpp_codegen_object_new(Action_1_t1951195598_il2cpp_TypeInfo_var);
		Action_1__ctor_m3262798437(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m3262798437_MethodInfo_var);
		NullCheck(L_0);
		VuforiaRuntime_UnregisterVuforiaInitErrorCallback_m2870938817(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033402446;
extern const uint32_t DefaultInitializationErrorHandler_DrawWindowContent_m1741663736_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_DrawWindowContent_m1741663736 (DefaultInitializationErrorHandler_t965510117 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_DrawWindowContent_m1741663736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m1220545469(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		String_t* L_3 = __this->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m3054448581(NULL /*static, unused*/, L_6, _stringLiteral3033402446, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral649327173;
extern Il2CppCodeGenString* _stringLiteral819182375;
extern Il2CppCodeGenString* _stringLiteral2499172356;
extern Il2CppCodeGenString* _stringLiteral2968979042;
extern Il2CppCodeGenString* _stringLiteral3603496816;
extern Il2CppCodeGenString* _stringLiteral3442848064;
extern Il2CppCodeGenString* _stringLiteral1812102461;
extern Il2CppCodeGenString* _stringLiteral2487108583;
extern Il2CppCodeGenString* _stringLiteral2986049441;
extern Il2CppCodeGenString* _stringLiteral557470275;
extern Il2CppCodeGenString* _stringLiteral2104855582;
extern const uint32_t DefaultInitializationErrorHandler_SetErrorCode_m617037449_MetadataUsageId;
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m617037449 (DefaultInitializationErrorHandler_t965510117 * __this, int32_t ___errorCode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_SetErrorCode_m617037449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral649327173, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode0;
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ab;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005b;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bb;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cb;
		}
		if (((int32_t)((int32_t)L_2+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00eb;
	}

IL_004b:
	{
		__this->set_mErrorText_2(_stringLiteral819182375);
		goto IL_00eb;
	}

IL_005b:
	{
		__this->set_mErrorText_2(_stringLiteral2499172356);
		goto IL_00eb;
	}

IL_006b:
	{
		__this->set_mErrorText_2(_stringLiteral2968979042);
		goto IL_00eb;
	}

IL_007b:
	{
		__this->set_mErrorText_2(_stringLiteral3603496816);
		goto IL_00eb;
	}

IL_008b:
	{
		__this->set_mErrorText_2(_stringLiteral3442848064);
		goto IL_00eb;
	}

IL_009b:
	{
		__this->set_mErrorText_2(_stringLiteral1812102461);
		goto IL_00eb;
	}

IL_00ab:
	{
		__this->set_mErrorText_2(_stringLiteral2487108583);
		goto IL_00eb;
	}

IL_00bb:
	{
		__this->set_mErrorText_2(_stringLiteral2986049441);
		goto IL_00eb;
	}

IL_00cb:
	{
		__this->set_mErrorText_2(_stringLiteral557470275);
		goto IL_00eb;
	}

IL_00db:
	{
		__this->set_mErrorText_2(_stringLiteral2104855582);
		goto IL_00eb;
	}

IL_00eb:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m3043577076 (DefaultInitializationErrorHandler_t965510117 * __this, bool ___errorOccurred0, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred0;
		__this->set_mErrorOccurred_3(L_0);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m1319028211 (DefaultInitializationErrorHandler_t965510117 * __this, int32_t ___initError0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError0;
		DefaultInitializationErrorHandler_SetErrorCode_m617037449(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m3043577076(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C"  void DefaultSmartTerrainEventHandler__ctor_m2500188526 (DefaultSmartTerrainEventHandler_t870608571 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t245871341_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2023440477_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t4009935945_m3509900328_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m206058748_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1475023678_MethodInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_Start_m2088335438_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_Start_m2088335438 (DefaultSmartTerrainEventHandler_t870608571 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_Start_m2088335438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t4009935945 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t4009935945_m3509900328(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t4009935945_m3509900328_MethodInfo_var);
		__this->set_mReconstructionBehaviour_2(L_0);
		ReconstructionBehaviour_t4009935945 * L_1 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t4009935945 * L_3 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MethodInfo_var);
		Action_1_t245871341 * L_5 = (Action_1_t245871341 *)il2cpp_codegen_object_new(Action_1_t245871341_il2cpp_TypeInfo_var);
		Action_1__ctor_m206058748(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m206058748_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m4192941617(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t4009935945 * L_6 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MethodInfo_var);
		Action_1_t2023440477 * L_8 = (Action_1_t2023440477 *)il2cpp_codegen_object_new(Action_1_t2023440477_il2cpp_TypeInfo_var);
		Action_1__ctor_m1475023678(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m1475023678_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m3624331525(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t245871341_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2023440477_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m206058748_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1475023678_MethodInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnDestroy_m904284311_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnDestroy_m904284311 (DefaultSmartTerrainEventHandler_t870608571 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnDestroy_m904284311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t4009935945 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t4009935945 * L_2 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MethodInfo_var);
		Action_1_t245871341 * L_4 = (Action_1_t245871341 *)il2cpp_codegen_object_new(Action_1_t245871341_il2cpp_TypeInfo_var);
		Action_1__ctor_m206058748(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m206058748_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m2048741868(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t4009935945 * L_5 = __this->get_mReconstructionBehaviour_2();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MethodInfo_var);
		Action_1_t2023440477 * L_7 = (Action_1_t2023440477 *)il2cpp_codegen_object_new(Action_1_t2023440477_il2cpp_TypeInfo_var);
		Action_1__ctor_m1475023678(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m1475023678_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m1039740100(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839 (DefaultSmartTerrainEventHandler_t870608571 * __this, Il2CppObject * ___prop0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnPropCreated_m2699063839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t4009935945 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t4009935945 * L_2 = __this->get_mReconstructionBehaviour_2();
		PropBehaviour_t966064926 * L_3 = __this->get_PropTemplate_3();
		Il2CppObject * L_4 = ___prop0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m920258964(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MetadataUsageId;
extern "C"  void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307 (DefaultSmartTerrainEventHandler_t870608571 * __this, Il2CppObject * ___surface0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultSmartTerrainEventHandler_OnSurfaceCreated_m3013324307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReconstructionBehaviour_t4009935945 * L_0 = __this->get_mReconstructionBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t4009935945 * L_2 = __this->get_mReconstructionBehaviour_2();
		SurfaceBehaviour_t2405314212 * L_3 = __this->get_SurfaceTemplate_4();
		Il2CppObject * L_4 = ___surface0;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m4192971254(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C"  void DefaultTrackableEventHandler__ctor_m205667975 (DefaultTrackableEventHandler_t1082256726 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var;
extern const uint32_t DefaultTrackableEventHandler_Start_m93544099_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_Start_m93544099 (DefaultTrackableEventHandler_t1082256726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_Start_m93544099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void DefaultTrackableEventHandler_OnTrackableStateChanged_m2460643418 (DefaultTrackableEventHandler_t1082256726 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m1963976755(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m3813626281(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral759218142;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingFound_m1963976755_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_OnTrackingFound_m1963976755 (DefaultTrackableEventHandler_t1082256726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingFound_m1963976755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	Renderer_t257310565 * V_2 = NULL;
	RendererU5BU5D_t2810717544* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t3497673348 * V_5 = NULL;
	ColliderU5BU5D_t462843629* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t2810717544* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t2810717544* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t257310565 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t257310565 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m142717579(L_7, (bool)1, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t2810717544* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t462843629* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Collider_t3497673348 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t3497673348 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m3489100454(L_16, (bool)1, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t462843629* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m3173853042(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_21, _stringLiteral759218142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral3033297088;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingLost_m3813626281_MetadataUsageId;
extern "C"  void DefaultTrackableEventHandler_OnTrackingLost_m3813626281 (DefaultTrackableEventHandler_t1082256726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingLost_m3813626281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	Renderer_t257310565 * V_2 = NULL;
	RendererU5BU5D_t2810717544* V_3 = NULL;
	int32_t V_4 = 0;
	Collider_t3497673348 * V_5 = NULL;
	ColliderU5BU5D_t462843629* V_6 = NULL;
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		RendererU5BU5D_t2810717544* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		RendererU5BU5D_t2810717544* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t257310565 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Renderer_t257310565 * L_7 = V_2;
		NullCheck(L_7);
		Renderer_set_enabled_m142717579(L_7, (bool)0, /*hidden argument*/NULL);
		int32_t L_8 = V_4;
		V_4 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002c:
	{
		int32_t L_9 = V_4;
		RendererU5BU5D_t2810717544* L_10 = V_3;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_11 = V_1;
		V_6 = L_11;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		ColliderU5BU5D_t462843629* L_12 = V_6;
		int32_t L_13 = V_7;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Collider_t3497673348 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		V_5 = L_15;
		Collider_t3497673348 * L_16 = V_5;
		NullCheck(L_16);
		Collider_set_enabled_m3489100454(L_16, (bool)0, /*hidden argument*/NULL);
		int32_t L_17 = V_7;
		V_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_18 = V_7;
		ColliderU5BU5D_t462843629* L_19 = V_6;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_20 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_20);
		String_t* L_21 = TrackableBehaviour_get_TrackableName_m3173853042(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_21, _stringLiteral3033297088, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C"  void GLErrorHandler__ctor_m2689292648 (GLErrorHandler_t3809113141 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern Il2CppClass* GLErrorHandler_t3809113141_il2cpp_TypeInfo_var;
extern const uint32_t GLErrorHandler_SetError_m381241178_MetadataUsageId;
extern "C"  void GLErrorHandler_SetError_m381241178 (Il2CppObject * __this /* static, unused */, String_t* ___errorText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_SetError_m381241178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___errorText0;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t3809113141_il2cpp_TypeInfo_var);
		((GLErrorHandler_t3809113141_StaticFields*)GLErrorHandler_t3809113141_il2cpp_TypeInfo_var->static_fields)->set_mErrorText_2(L_0);
		((GLErrorHandler_t3809113141_StaticFields*)GLErrorHandler_t3809113141_il2cpp_TypeInfo_var->static_fields)->set_mErrorOccurred_3((bool)1);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern Il2CppClass* GLErrorHandler_t3809113141_il2cpp_TypeInfo_var;
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m161374698_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2015436302;
extern const uint32_t GLErrorHandler_OnGUI_m3766403336_MetadataUsageId;
extern "C"  void GLErrorHandler_OnGUI_m3766403336 (GLErrorHandler_t3809113141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_OnGUI_m3766403336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t3809113141_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t3809113141_StaticFields*)GLErrorHandler_t3809113141_il2cpp_TypeInfo_var->static_fields)->get_mErrorOccurred_3();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GLErrorHandler_DrawWindowContent_m161374698_MethodInfo_var);
		WindowFunction_t3486805455 * L_5 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Window_m2988139263(NULL /*static, unused*/, 0, L_3, L_5, _stringLiteral2015436302, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern Il2CppClass* GLErrorHandler_t3809113141_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033402446;
extern const uint32_t GLErrorHandler_DrawWindowContent_m161374698_MetadataUsageId;
extern "C"  void GLErrorHandler_DrawWindowContent_m161374698 (GLErrorHandler_t3809113141 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler_DrawWindowContent_m161374698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m1220545469(&L_2, (10.0f), (25.0f), (((float)((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t3809113141_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t3809113141_StaticFields*)GLErrorHandler_t3809113141_il2cpp_TypeInfo_var->static_fields)->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_Label_m2412846501(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75)))))), (((float)((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60)))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m3054448581(NULL /*static, unused*/, L_6, _stringLiteral3033402446, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GLErrorHandler_t3809113141_il2cpp_TypeInfo_var;
extern const uint32_t GLErrorHandler__cctor_m454494933_MetadataUsageId;
extern "C"  void GLErrorHandler__cctor_m454494933 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GLErrorHandler__cctor_m454494933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((GLErrorHandler_t3809113141_StaticFields*)GLErrorHandler_t3809113141_il2cpp_TypeInfo_var->static_fields)->set_mErrorText_2(L_0);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C"  void HideExcessAreaBehaviour__ctor_m1434979708 (HideExcessAreaBehaviour_t3495034315 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m283987487(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C"  void ImageTargetBehaviour__ctor_m2964661606 (ImageTargetBehaviour_t2654589389 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m3584284597(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C"  void IOSUnityPlayer__ctor_m676692974 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern Il2CppClass* VuforiaNativeIosWrapper_t1210651633_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_LoadNativeLibraries_m1172109064_MetadataUsageId;
extern "C"  void IOSUnityPlayer_LoadNativeLibraries_m1172109064 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_LoadNativeLibraries_m1172109064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaNativeIosWrapper_t1210651633 * L_0 = (VuforiaNativeIosWrapper_t1210651633 *)il2cpp_codegen_object_new(VuforiaNativeIosWrapper_t1210651633_il2cpp_TypeInfo_var);
		VuforiaNativeIosWrapper__ctor_m1946048060(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		VuforiaWrapper_SetImplementation_m1019610825(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C"  void IOSUnityPlayer_InitializePlatform_m286541247 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m2804175766(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.IOSUnityPlayer::InitializeVuforia(System.String)
extern Il2CppClass* VuforiaRenderer_t2933102835_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_InitializeVuforia_m260052685_MetadataUsageId;
extern "C"  int32_t IOSUnityPlayer_InitializeVuforia_m260052685 (IOSUnityPlayer_t3656371703 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_InitializeVuforia_m260052685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRenderer_t2933102835_il2cpp_TypeInfo_var);
		VuforiaRenderer_t2933102835 * L_0 = VuforiaRenderer_get_Instance_m1621768183(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* Vuforia.VuforiaRenderer/RendererAPI Vuforia.VuforiaRenderer::GetRendererAPI() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = ___licenseKey0;
		int32_t L_5 = IOSUnityPlayer_initQCARiOS_m4109121394(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m4219932605(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_7 = V_1;
		return (int32_t)(L_7);
	}
}
// System.Void Vuforia.IOSUnityPlayer::StartScene()
extern "C"  void IOSUnityPlayer_StartScene_m2357985464 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_Update_m3611099589_MetadataUsageId;
extern "C"  void IOSUnityPlayer_Update_m3611099589 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_Update_m3611099589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2740261893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m4219932605(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m3793088327(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C"  void IOSUnityPlayer_Dispose_m444239215 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_OnPause_m2518700507_MetadataUsageId;
extern "C"  void IOSUnityPlayer_OnPause_m2518700507 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnPause_m2518700507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m2422224752(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_OnResume_m921094382_MetadataUsageId;
extern "C"  void IOSUnityPlayer_OnResume_m921094382 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnResume_m921094382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2186520633(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_OnDestroy_m3703758411_MetadataUsageId;
extern "C"  void IOSUnityPlayer_OnDestroy_m3703758411 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_OnDestroy_m3703758411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m4072609744(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_InitializeSurface_m4219932605_MetadataUsageId;
extern "C"  void IOSUnityPlayer_InitializeSurface_m4219932605 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_InitializeSurface_m4219932605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m3675640541(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m3793088327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t IOSUnityPlayer_SetUnityScreenOrientation_m3793088327_MetadataUsageId;
extern "C"  void IOSUnityPlayer_SetUnityScreenOrientation_m3793088327 (IOSUnityPlayer_t3656371703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IOSUnityPlayer_SetUnityScreenOrientation_m3793088327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m3106547277(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		IOSUnityPlayer_setSurfaceOrientationiOS_m3165018273(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL setPlatFormNative();
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C"  void IOSUnityPlayer_setPlatFormNative_m2804175766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setPlatFormNative)();

}
extern "C" int32_t DEFAULT_CALL initQCARiOS(int32_t, int32_t, char*);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.Int32,System.String)
extern "C"  int32_t IOSUnityPlayer_initQCARiOS_m4109121394 (Il2CppObject * __this /* static, unused */, int32_t ___rendererAPI0, int32_t ___screenOrientation1, String_t* ___licenseKey2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, char*);

	// Marshaling of parameter '___licenseKey2' to native representation
	char* ____licenseKey2_marshaled = NULL;
	____licenseKey2_marshaled = il2cpp_codegen_marshal_string(___licenseKey2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(initQCARiOS)(___rendererAPI0, ___screenOrientation1, ____licenseKey2_marshaled);

	// Marshaling cleanup of parameter '___licenseKey2' native representation
	il2cpp_codegen_marshal_free(____licenseKey2_marshaled);
	____licenseKey2_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C"  void IOSUnityPlayer_setSurfaceOrientationiOS_m3165018273 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setSurfaceOrientationiOS)(___screenOrientation0);

}
// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C"  void MaskOutBehaviour__ctor_m3872076058 (MaskOutBehaviour_t2994129365 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m3743365693(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* MaterialU5BU5D_t3123989686_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m2803939486_MethodInfo_var;
extern const uint32_t MaskOutBehaviour_Start_m3914251470_MetadataUsageId;
extern "C"  void MaskOutBehaviour_Start_m3914251470 (MaskOutBehaviour_t2994129365 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MaskOutBehaviour_Start_m3914251470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t257310565 * V_0 = NULL;
	int32_t V_1 = 0;
	MaterialU5BU5D_t3123989686* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1774515559(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t257310565 * L_1 = Component_GetComponent_TisRenderer_t257310565_m2803939486(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m2803939486_MethodInfo_var);
		V_0 = L_1;
		Renderer_t257310565 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t3123989686* L_3 = Renderer_get_materials_m810004692(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t257310565 * L_5 = V_0;
		Material_t193706927 * L_6 = ((MaskOutAbstractBehaviour_t3489038957 *)__this)->get_maskMaterial_2();
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m391095487(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t3123989686*)SZArrayNew(MaterialU5BU5D_t3123989686_il2cpp_TypeInfo_var, (uint32_t)L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t3123989686* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t193706927 * L_10 = ((MaskOutAbstractBehaviour_t3489038957 *)__this)->get_maskMaterial_2();
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Material_t193706927 *)L_10);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t257310565 * L_14 = V_0;
		MaterialU5BU5D_t3123989686* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m2669445156(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C"  void MultiTargetBehaviour__ctor_m3457059082 (MultiTargetBehaviour_t3504654311 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m3602545575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C"  void ObjectTargetBehaviour__ctor_m26156260 (ObjectTargetBehaviour_t3836044259 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m4149910295(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PropBehaviour::.ctor()
extern "C"  void PropBehaviour__ctor_m2238867581 (PropBehaviour_t966064926 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m1260771068(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C"  void ReconstructionBehaviour__ctor_m2616285522 (ReconstructionBehaviour_t4009935945 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m2037226869(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C"  void ReconstructionFromTargetBehaviour__ctor_m2764930605 (ReconstructionFromTargetBehaviour_t2111803406 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m2704257964(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C"  void SurfaceBehaviour__ctor_m1448804633 (SurfaceBehaviour_t2405314212 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m4051989004(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C"  void TextRecoBehaviour__ctor_m2446679806 (TextRecoBehaviour_t3400239837 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m3986867921(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C"  void TurnOffBehaviour__ctor_m1873134682 (TurnOffBehaviour_t3058161409 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m1043676009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern const uint32_t TurnOffBehaviour_Awake_m4285356927_MetadataUsageId;
extern "C"  void TurnOffBehaviour_Awake_m4285356927 (TurnOffBehaviour_t3058161409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnOffBehaviour_Awake_m4285356927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1268241104 * V_0 = NULL;
	MeshFilter_t3026937449 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1774515559(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t1268241104 * L_1 = Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t1268241104 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_3 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t3026937449 * L_4 = V_1;
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C"  void TurnOffWordBehaviour__ctor_m2227245698 (TurnOffWordBehaviour_t584991835 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423762541;
extern const uint32_t TurnOffWordBehaviour_Awake_m2854048885_MetadataUsageId;
extern "C"  void TurnOffWordBehaviour_Awake_m2854048885 (TurnOffWordBehaviour_t584991835 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurnOffWordBehaviour_Awake_m2854048885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1268241104 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1774515559(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t1268241104 * L_1 = Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t1268241104_m3385851477_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t1268241104 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_FindChild_m2677714886(L_3, _stringLiteral3423762541, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t3275118058 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t3275118058 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C"  void UserDefinedTargetBuildingBehaviour__ctor_m2304938643 (UserDefinedTargetBuildingBehaviour_t4184040062 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m3581213318(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern Il2CppClass* VideoBackgroundAbstractBehaviour_t395384314_il2cpp_TypeInfo_var;
extern const uint32_t VideoBackgroundBehaviour__ctor_m2133855431_MetadataUsageId;
extern "C"  void VideoBackgroundBehaviour__ctor_m2133855431 (VideoBackgroundBehaviour_t3161817952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VideoBackgroundBehaviour__ctor_m2133855431_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VideoBackgroundAbstractBehaviour_t395384314_il2cpp_TypeInfo_var);
		VideoBackgroundAbstractBehaviour__ctor_m397100456(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C"  void VirtualButtonBehaviour__ctor_m3984899111 (VirtualButtonBehaviour_t2515041812 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m855896756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.ctor()
extern "C"  void VuforiaBehaviour__ctor_m3143982076 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	{
		VuforiaAbstractBehaviour__ctor_m3900338923(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::Awake()
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_Awake_m3772880569_MetadataUsageId;
extern "C"  void VuforiaBehaviour_Awake_m3772880569 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_Awake_m3772880569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4168849520(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107(L_0, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t3249343815_m658142107_MethodInfo_var);
		VuforiaAbstractBehaviour_Awake_m232471254(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::get_Instance()
extern Il2CppClass* VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var;
extern const uint32_t VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId;
extern "C"  VuforiaBehaviour_t359035403 * VuforiaBehaviour_get_Instance_m790681192 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviour_get_Instance_m790681192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_0 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_2 = Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisVuforiaBehaviour_t359035403_m318208984_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->set_mVuforiaBehaviour_19(L_2);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var);
		VuforiaBehaviour_t359035403 * L_3 = ((VuforiaBehaviour_t359035403_StaticFields*)VuforiaBehaviour_t359035403_il2cpp_TypeInfo_var->static_fields)->get_mVuforiaBehaviour_19();
		return L_3;
	}
}
// System.Void Vuforia.VuforiaBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void VuforiaBehaviour_AddOSSpecificExternalDatasetSearchDirs_m4168849520 (VuforiaBehaviour_t359035403 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviour::.cctor()
extern "C"  void VuforiaBehaviour__cctor_m1552098219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C"  void VuforiaBehaviourComponentFactory__ctor_m4023364043 (VuforiaBehaviourComponentFactory_t1383853028 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915_MetadataUsageId;
extern "C"  MaskOutAbstractBehaviour_t3489038957 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m3602623915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MaskOutBehaviour_t2994129365 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t2994129365_m1425322069_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123_MetadataUsageId;
extern "C"  VirtualButtonAbstractBehaviour_t2478279366 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m273150123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VirtualButtonBehaviour_t2515041812 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t2515041812_m3118105184_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091_MetadataUsageId;
extern "C"  TurnOffAbstractBehaviour_t4084926705 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m3691970091_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TurnOffBehaviour_t3058161409 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t3058161409_m2052882313_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307_MetadataUsageId;
extern "C"  ImageTargetAbstractBehaviour_t3327552701 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m110195307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ImageTargetBehaviour_t2654589389 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t2654589389_m3351511077_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955_MetadataUsageId;
extern "C"  MultiTargetAbstractBehaviour_t3616801211 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m1008182955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		MultiTargetBehaviour_t3504654311 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t3504654311_m1135184387_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991_MetadataUsageId;
extern "C"  CylinderTargetAbstractBehaviour_t665872082 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m2573942991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		CylinderTargetBehaviour_t2091399712 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t2091399712_m912348918_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t2878458725 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddWordBehaviour_m3806508403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		WordBehaviour_t3366478421 * L_1 = GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t3366478421_m80916061_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667_MetadataUsageId;
extern "C"  TextRecoAbstractBehaviour_t2386081773 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m2915043667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		TextRecoBehaviour_t3400239837 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t3400239837_m3447592645_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395_MetadataUsageId;
extern "C"  ObjectTargetAbstractBehaviour_t2805337095 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m3475568395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		ObjectTargetBehaviour_t3836044259 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t3836044259_m2560584915_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VuMarkAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVuMarkBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483_MetadataUsageId;
extern "C"  VuMarkAbstractBehaviour_t1830666997 * VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483 (VuforiaBehaviourComponentFactory_t1383853028 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_AddVuMarkBehaviour_m2639490483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___gameObject0;
		NullCheck(L_0);
		VuMarkBehaviour_t2060629989 * L_1 = GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781(L_0, /*hidden argument*/GameObject_AddComponent_TisVuMarkBehaviour_t2060629989_m1435287781_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaBehaviourComponentFactory::CreateVuforiaConfiguration()
extern const MethodInfo* ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354_MethodInfo_var;
extern const uint32_t VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600_MetadataUsageId;
extern "C"  VuforiaAbstractConfiguration_t1891710424 * VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600 (VuforiaBehaviourComponentFactory_t1383853028 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaBehaviourComponentFactory_CreateVuforiaConfiguration_m931474600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VuforiaConfiguration_t3823746026 * L_0 = ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisVuforiaConfiguration_t3823746026_m559723354_MethodInfo_var);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaConfiguration::.ctor()
extern Il2CppClass* VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaConfiguration__ctor_m2808042321_MetadataUsageId;
extern "C"  void VuforiaConfiguration__ctor_m2808042321 (VuforiaConfiguration_t3823746026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaConfiguration__ctor_m2808042321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration__ctor_m1625969650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitPlatform()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_InitPlatform_m3197388960_MetadataUsageId;
extern "C"  void VuforiaRuntimeInitialization_InitPlatform_m3197388960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitPlatform_m3197388960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_SetStandardInitializationParameters_m3220511984(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_0 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaRuntime_InitPlatform_m3306160978(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaRuntimeInitialization::InitVuforia()
extern Il2CppClass* VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_InitVuforia_m2546903059_MetadataUsageId;
extern "C"  void VuforiaRuntimeInitialization_InitVuforia_m2546903059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_InitVuforia_m2546903059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaAbstractConfiguration_t1891710424_il2cpp_TypeInfo_var);
		VuforiaAbstractConfiguration_t1891710424 * L_0 = VuforiaAbstractConfiguration_get_Instance_m3543652287(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GenericVuforiaConfiguration_t3866211740 * L_1 = VuforiaAbstractConfiguration_get_Vuforia_m3334047132(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GenericVuforiaConfiguration_get_DelayedInitialization_m282560601(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t2075282796_il2cpp_TypeInfo_var);
		VuforiaRuntime_t2075282796 * L_3 = VuforiaRuntime_get_Instance_m4069915631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VuforiaRuntime_InitVuforia_m2353933398(L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// Vuforia.IUnityPlayer Vuforia.VuforiaRuntimeInitialization::CreateUnityPlayer()
extern Il2CppClass* NullUnityPlayer_t754446093_il2cpp_TypeInfo_var;
extern Il2CppClass* AndroidUnityPlayer_t852788525_il2cpp_TypeInfo_var;
extern Il2CppClass* IOSUnityPlayer_t3656371703_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var;
extern Il2CppClass* WSAUnityPlayer_t425981959_il2cpp_TypeInfo_var;
extern const uint32_t VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109_MetadataUsageId;
extern "C"  Il2CppObject * VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VuforiaRuntimeInitialization_CreateUnityPlayer_m1650732109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		NullUnityPlayer_t754446093 * L_0 = (NullUnityPlayer_t754446093 *)il2cpp_codegen_object_new(NullUnityPlayer_t754446093_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m483624113(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t852788525 * L_2 = (AndroidUnityPlayer_t852788525 *)il2cpp_codegen_object_new(AndroidUnityPlayer_t852788525_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m2233000524(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0058;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t3656371703 * L_4 = (IOSUnityPlayer_t3656371703 *)il2cpp_codegen_object_new(IOSUnityPlayer_t3656371703_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m676692974(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0058;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_5 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		PlayModeUnityPlayer_t918240325 * L_6 = (PlayModeUnityPlayer_t918240325 *)il2cpp_codegen_object_new(PlayModeUnityPlayer_t918240325_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m2126346857(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0058;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_7 = VuforiaRuntimeUtilities_IsWSARuntime_m3848252715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0058;
		}
	}
	{
		WSAUnityPlayer_t425981959 * L_8 = (WSAUnityPlayer_t425981959 *)il2cpp_codegen_object_new(WSAUnityPlayer_t425981959_il2cpp_TypeInfo_var);
		WSAUnityPlayer__ctor_m1279021334(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0058:
	{
		Il2CppObject * L_9 = V_0;
		return L_9;
	}
}
// System.Void Vuforia.VuMarkBehaviour::.ctor()
extern "C"  void VuMarkBehaviour__ctor_m1415860126 (VuMarkBehaviour_t2060629989 * __this, const MethodInfo* method)
{
	{
		VuMarkAbstractBehaviour__ctor_m326197713(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C"  void WireframeBehaviour__ctor_m420914080 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	{
		__this->set_ShowLines_3((bool)1);
		Color_t2020392075  L_0 = Color_get_green_m2671273823(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_LineColor_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral599011397;
extern const uint32_t WireframeBehaviour_Start_m2184757344_MetadataUsageId;
extern "C"  void WireframeBehaviour_Start_m2184757344 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_Start_m2184757344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_lineMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Material_t193706927 * L_2 = __this->get_lineMaterial_2();
		Material_t193706927 * L_3 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1440882780(L_3, L_2, /*hidden argument*/NULL);
		__this->set_mLineMaterial_5(L_3);
		goto IL_0031;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern Il2CppClass* VuforiaManager_t2424874861_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral599011397;
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId;
extern "C"  void WireframeBehaviour_OnRenderObject_m2411339956 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnRenderObject_m2411339956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	CameraU5BU5D_t3079764780* V_1 = NULL;
	bool V_2 = false;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	MeshFilter_t3026937449 * V_6 = NULL;
	Mesh_t1356156583 * V_7 = NULL;
	Vector3U5BU5D_t1172311765* V_8 = NULL;
	Int32U5BU5D_t3030399641* V_9 = NULL;
	int32_t V_10 = 0;
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaManager_t2424874861_il2cpp_TypeInfo_var);
		VuforiaManager_t2424874861 * L_0 = VuforiaManager_get_Instance_m425433003(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = VirtFuncInvoker0< Transform_t3275118058 * >::Invoke(10 /* UnityEngine.Transform Vuforia.VuforiaManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t3079764780* L_4 = GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t189460977_m3116941434_MethodInfo_var);
		V_1 = L_4;
		V_2 = (bool)0;
		CameraU5BU5D_t3079764780* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t3079764780* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Camera_t189460977 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		Camera_t189460977 * L_10 = Camera_get_current_m2639890517(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = (bool)1;
	}

IL_003c:
	{
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		CameraU5BU5D_t3079764780* L_15 = V_4;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_17 = __this->get_ShowLines_3();
		if (L_17)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t3026937449 * L_18 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_6 = L_18;
		MeshFilter_t3026937449 * L_19 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t193706927 * L_21 = __this->get_mLineMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral599011397, /*hidden argument*/NULL);
		return;
	}

IL_0091:
	{
		MeshFilter_t3026937449 * L_23 = V_6;
		NullCheck(L_23);
		Mesh_t1356156583 * L_24 = MeshFilter_get_sharedMesh_m1310789932(L_23, /*hidden argument*/NULL);
		V_7 = L_24;
		Mesh_t1356156583 * L_25 = V_7;
		NullCheck(L_25);
		Vector3U5BU5D_t1172311765* L_26 = Mesh_get_vertices_m626989480(L_25, /*hidden argument*/NULL);
		V_8 = L_26;
		Mesh_t1356156583 * L_27 = V_7;
		NullCheck(L_27);
		Int32U5BU5D_t3030399641* L_28 = Mesh_get_triangles_m3988715512(L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Matrix4x4_t2933234003  L_30 = Transform_get_localToWorldMatrix_m2868579006(L_29, /*hidden argument*/NULL);
		GL_MultMatrix_m767401141(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Material_t193706927 * L_31 = __this->get_mLineMaterial_5();
		NullCheck(L_31);
		Material_SetPass_m2448940266(L_31, 0, /*hidden argument*/NULL);
		Material_t193706927 * L_32 = __this->get_mLineMaterial_5();
		Color_t2020392075  L_33 = __this->get_LineColor_4();
		NullCheck(L_32);
		Material_SetColor_m650857509(L_32, _stringLiteral895546098, L_33, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_015f;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_34 = V_8;
		Int32U5BU5D_t3030399641* L_35 = V_9;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_11 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3U5BU5D_t1172311765* L_39 = V_8;
		Int32U5BU5D_t3030399641* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)1));
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		V_12 = (*(Vector3_t2243707580 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))));
		Vector3U5BU5D_t1172311765* L_44 = V_8;
		Int32U5BU5D_t3030399641* L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)2));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		V_13 = (*(Vector3_t2243707580 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48))));
		Vector3_t2243707580  L_49 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_12;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_53 = V_13;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = V_11;
		GL_Vertex_m4110027235(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		int32_t L_55 = V_10;
		V_10 = ((int32_t)((int32_t)L_55+(int32_t)3));
	}

IL_015f:
	{
		int32_t L_56 = V_10;
		Int32U5BU5D_t3030399641* L_57 = V_9;
		NullCheck(L_57);
		if ((((int32_t)L_56) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_57)->max_length)))))))
		{
			goto IL_00f2;
		}
	}
	{
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern const uint32_t WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId;
extern "C"  void WireframeBehaviour_OnDrawGizmos_m4179942854 (WireframeBehaviour_t2494532455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeBehaviour_OnDrawGizmos_m4179942854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3026937449 * V_0 = NULL;
	Mesh_t1356156583 * V_1 = NULL;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		bool L_0 = __this->get_ShowLines_3();
		if (!L_0)
		{
			goto IL_00f9;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00f9;
		}
	}
	{
		MeshFilter_t3026937449 * L_2 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t3026937449 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m1033555130(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_lossyScale_m1638545862(L_12, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_14 = Matrix4x4_TRS_m1913765359(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = __this->get_LineColor_4();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t1356156583 * L_17 = MeshFilter_get_sharedMesh_m1310789932(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t1356156583 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f9;
		}
	}
	{
		Mesh_t1356156583 * L_20 = V_1;
		NullCheck(L_20);
		Vector3U5BU5D_t1172311765* L_21 = Mesh_get_vertices_m626989480(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		Mesh_t1356156583 * L_22 = V_1;
		NullCheck(L_22);
		Int32U5BU5D_t3030399641* L_23 = Mesh_get_triangles_m3988715512(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		V_4 = 0;
		goto IL_00ef;
	}

IL_0097:
	{
		Vector3U5BU5D_t1172311765* L_24 = V_2;
		Int32U5BU5D_t3030399641* L_25 = V_3;
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_24);
		V_5 = (*(Vector3_t2243707580 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		Vector3U5BU5D_t1172311765* L_29 = V_2;
		Int32U5BU5D_t3030399641* L_30 = V_3;
		int32_t L_31 = V_4;
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_29);
		V_6 = (*(Vector3_t2243707580 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_33))));
		Vector3U5BU5D_t1172311765* L_34 = V_2;
		Int32U5BU5D_t3030399641* L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)2));
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_34);
		V_7 = (*(Vector3_t2243707580 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))));
		Vector3_t2243707580  L_39 = V_5;
		Vector3_t2243707580  L_40 = V_6;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = V_6;
		Vector3_t2243707580  L_42 = V_7;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = V_7;
		Vector3_t2243707580  L_44 = V_5;
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)3));
	}

IL_00ef:
	{
		int32_t L_46 = V_4;
		Int32U5BU5D_t3030399641* L_47 = V_3;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_0097;
		}
	}

IL_00f9:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C"  void WireframeTrackableEventHandler__ctor_m4253736968 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var;
extern const uint32_t WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_Start_m1475947192 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_Start_m1475947192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1779888572 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1779888572_m2013987957_MethodInfo_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t1779888572 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m1156666476(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void WireframeTrackableEventHandler_OnTrackableStateChanged_m106630617 (WireframeTrackableEventHandler_t1535150527 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m563781220(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m3180126272(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral759218142;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingFound_m563781220_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_OnTrackingFound_m563781220 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingFound_m563781220_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)1, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral759218142, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1040926105;
extern Il2CppCodeGenString* _stringLiteral3033297088;
extern const uint32_t WireframeTrackableEventHandler_OnTrackingLost_m3180126272_MetadataUsageId;
extern "C"  void WireframeTrackableEventHandler_OnTrackingLost_m3180126272 (WireframeTrackableEventHandler_t1535150527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WireframeTrackableEventHandler_OnTrackingLost_m3180126272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t2810717544* V_0 = NULL;
	ColliderU5BU5D_t462843629* V_1 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_2 = NULL;
	Renderer_t257310565 * V_3 = NULL;
	RendererU5BU5D_t2810717544* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t3497673348 * V_6 = NULL;
	ColliderU5BU5D_t462843629* V_7 = NULL;
	int32_t V_8 = 0;
	WireframeBehaviour_t2494532455 * V_9 = NULL;
	WireframeBehaviourU5BU5D_t2935582494* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t2810717544* L_0 = Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t257310565_m1076536047_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t462843629* L_1 = Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t3497673348_m496323568_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t2935582494* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t2494532455_m1655243706_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t2810717544* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t2810717544* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t257310565 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m142717579(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t2810717544* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t462843629* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t462843629* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t3497673348 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t3497673348 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m3489100454(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t462843629* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t2935582494* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t2935582494* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		WireframeBehaviour_t2494532455 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		WireframeBehaviour_t2494532455 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m1796096907(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		WireframeBehaviourU5BU5D_t2935582494* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t1779888572 * L_30 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_30);
		String_t* L_31 = TrackableBehaviour_get_TrackableName_m3173853042(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1040926105, L_31, _stringLiteral3033297088, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordBehaviour::.ctor()
extern "C"  void WordBehaviour__ctor_m581909702 (WordBehaviour_t3366478421 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m1415816009(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::.ctor()
extern "C"  void WSAUnityPlayer__ctor_m1279021334 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::LoadNativeLibraries()
extern "C"  void WSAUnityPlayer_LoadNativeLibraries_m2310474608 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializePlatform()
extern "C"  void WSAUnityPlayer_InitializePlatform_m2450540007 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		WSAUnityPlayer_setPlatFormNative_m413995854(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VuforiaUnity/InitError Vuforia.WSAUnityPlayer::InitializeVuforia(System.String)
extern "C"  int32_t WSAUnityPlayer_InitializeVuforia_m1467576933 (WSAUnityPlayer_t425981959 * __this, String_t* ___licenseKey0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey0;
		int32_t L_1 = WSAUnityPlayer_initVuforiaWSA_m2461582347(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m2485130165(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.WSAUnityPlayer::StartScene()
extern "C"  void WSAUnityPlayer_StartScene_m2884648656 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Update()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_Update_m3453039605_MetadataUsageId;
extern "C"  void WSAUnityPlayer_Update_m3453039605 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_Update_m3453039605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m2740261893(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		WSAUnityPlayer_InitializeSurface_m2485130165(__this, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0015:
	{
		int32_t L_1 = WSAUnityPlayer_GetActualScreenOrientation_m2906081196(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_mScreenOrientation_0();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		WSAUnityPlayer_SetUnityScreenOrientation_m3964512799(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::Dispose()
extern "C"  void WSAUnityPlayer_Dispose_m459169223 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnPause()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_OnPause_m2815166635_MetadataUsageId;
extern "C"  void WSAUnityPlayer_OnPause_m2815166635 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnPause_m2815166635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnPause_m2422224752(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnResume()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_OnResume_m2572251502_MetadataUsageId;
extern "C"  void WSAUnityPlayer_OnResume_m2572251502 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnResume_m2572251502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_OnResume_m2186520633(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::OnDestroy()
extern Il2CppClass* VuforiaUnity_t657456673_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_OnDestroy_m3845019931_MetadataUsageId;
extern "C"  void WSAUnityPlayer_OnDestroy_m3845019931 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_OnDestroy_m3845019931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaUnity_t657456673_il2cpp_TypeInfo_var);
		VuforiaUnity_Deinit_m4072609744(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::InitializeSurface()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_InitializeSurface_m2485130165_MetadataUsageId;
extern "C"  void WSAUnityPlayer_InitializeSurface_m2485130165 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_InitializeSurface_m2485130165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m3675640541(NULL /*static, unused*/, /*hidden argument*/NULL);
		WSAUnityPlayer_SetUnityScreenOrientation_m3964512799(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WSAUnityPlayer::SetUnityScreenOrientation()
extern Il2CppClass* SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_SetUnityScreenOrientation_m3964512799_MetadataUsageId;
extern "C"  void WSAUnityPlayer_SetUnityScreenOrientation_m3964512799 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_SetUnityScreenOrientation_m3964512799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = WSAUnityPlayer_GetActualScreenOrientation_m2906081196(__this, /*hidden argument*/NULL);
		__this->set_mScreenOrientation_0(L_0);
		int32_t L_1 = __this->get_mScreenOrientation_0();
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t4096327849_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m3106547277(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_mScreenOrientation_0();
		WSAUnityPlayer_setSurfaceOrientationWSA_m2082052129(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ScreenOrientation Vuforia.WSAUnityPlayer::GetActualScreenOrientation()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t WSAUnityPlayer_GetActualScreenOrientation_m2906081196_MetadataUsageId;
extern "C"  int32_t WSAUnityPlayer_GetActualScreenOrientation_m2906081196 (WSAUnityPlayer_t425981959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WSAUnityPlayer_GetActualScreenOrientation_m2906081196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m879255848(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_003e;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_0030;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_004c;
	}

IL_0030:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0037:
	{
		V_0 = 4;
		goto IL_0053;
	}

IL_003e:
	{
		V_0 = 1;
		goto IL_0053;
	}

IL_0045:
	{
		V_0 = 2;
		goto IL_0053;
	}

IL_004c:
	{
		V_0 = 3;
		goto IL_0053;
	}

IL_0053:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Void Vuforia.WSAUnityPlayer::setPlatFormNative()
extern "C"  void WSAUnityPlayer_setPlatFormNative_m413995854 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setPlatFormNative", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Int32 Vuforia.WSAUnityPlayer::initVuforiaWSA(System.String)
extern "C"  int32_t WSAUnityPlayer_initVuforiaWSA_m2461582347 (Il2CppObject * __this /* static, unused */, String_t* ___licenseKey0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(char*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "initVuforiaWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initVuforiaWSA'"));
		}
	}

	// Marshaling of parameter '___licenseKey0' to native representation
	char* ____licenseKey0_marshaled = NULL;
	____licenseKey0_marshaled = il2cpp_codegen_marshal_string(___licenseKey0);

	// Native function invocation
	int32_t returnValue = il2cppPInvokeFunc(____licenseKey0_marshaled);

	// Marshaling cleanup of parameter '___licenseKey0' native representation
	il2cpp_codegen_marshal_free(____licenseKey0_marshaled);
	____licenseKey0_marshaled = NULL;

	return returnValue;
}
// System.Void Vuforia.WSAUnityPlayer::setSurfaceOrientationWSA(System.Int32)
extern "C"  void WSAUnityPlayer_setSurfaceOrientationWSA_m2082052129 (Il2CppObject * __this /* static, unused */, int32_t ___screenOrientation0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("VuforiaWrapper"), "setSurfaceOrientationWSA", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (il2cppPInvokeFunc == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationWSA'"));
		}
	}

	// Native function invocation
	il2cppPInvokeFunc(___screenOrientation0);

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
