﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// URLImage
struct URLImage_t3675617922;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void URLImage::.ctor()
extern "C"  void URLImage__ctor_m1246517459 (URLImage_t3675617922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator URLImage::Start()
extern "C"  Il2CppObject * URLImage_Start_m3471863665 (URLImage_t3675617922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void URLImage::Update()
extern "C"  void URLImage_Update_m3363539884 (URLImage_t3675617922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
