﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerMenuController/UIItems
struct  UIItems_t2800976608  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button MarkerMenuController/UIItems::closeButton
	Button_t2872111280 * ___closeButton_0;
	// UnityEngine.UI.Button MarkerMenuController/UIItems::screenShotButton
	Button_t2872111280 * ___screenShotButton_1;
	// UnityEngine.UI.Button MarkerMenuController/UIItems::confirmSizeButton
	Button_t2872111280 * ___confirmSizeButton_2;
	// UnityEngine.UI.Text MarkerMenuController/UIItems::distanceText
	Text_t356221433 * ___distanceText_3;
	// UnityEngine.UI.Text MarkerMenuController/UIItems::sizeText
	Text_t356221433 * ___sizeText_4;
	// UnityEngine.UI.Text MarkerMenuController/UIItems::totalPriceText
	Text_t356221433 * ___totalPriceText_5;
	// UnityEngine.GameObject MarkerMenuController/UIItems::detailPanel
	GameObject_t1756533147 * ___detailPanel_6;
	// UnityEngine.GameObject MarkerMenuController/UIItems::detectorPanel
	GameObject_t1756533147 * ___detectorPanel_7;

public:
	inline static int32_t get_offset_of_closeButton_0() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___closeButton_0)); }
	inline Button_t2872111280 * get_closeButton_0() const { return ___closeButton_0; }
	inline Button_t2872111280 ** get_address_of_closeButton_0() { return &___closeButton_0; }
	inline void set_closeButton_0(Button_t2872111280 * value)
	{
		___closeButton_0 = value;
		Il2CppCodeGenWriteBarrier(&___closeButton_0, value);
	}

	inline static int32_t get_offset_of_screenShotButton_1() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___screenShotButton_1)); }
	inline Button_t2872111280 * get_screenShotButton_1() const { return ___screenShotButton_1; }
	inline Button_t2872111280 ** get_address_of_screenShotButton_1() { return &___screenShotButton_1; }
	inline void set_screenShotButton_1(Button_t2872111280 * value)
	{
		___screenShotButton_1 = value;
		Il2CppCodeGenWriteBarrier(&___screenShotButton_1, value);
	}

	inline static int32_t get_offset_of_confirmSizeButton_2() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___confirmSizeButton_2)); }
	inline Button_t2872111280 * get_confirmSizeButton_2() const { return ___confirmSizeButton_2; }
	inline Button_t2872111280 ** get_address_of_confirmSizeButton_2() { return &___confirmSizeButton_2; }
	inline void set_confirmSizeButton_2(Button_t2872111280 * value)
	{
		___confirmSizeButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___confirmSizeButton_2, value);
	}

	inline static int32_t get_offset_of_distanceText_3() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___distanceText_3)); }
	inline Text_t356221433 * get_distanceText_3() const { return ___distanceText_3; }
	inline Text_t356221433 ** get_address_of_distanceText_3() { return &___distanceText_3; }
	inline void set_distanceText_3(Text_t356221433 * value)
	{
		___distanceText_3 = value;
		Il2CppCodeGenWriteBarrier(&___distanceText_3, value);
	}

	inline static int32_t get_offset_of_sizeText_4() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___sizeText_4)); }
	inline Text_t356221433 * get_sizeText_4() const { return ___sizeText_4; }
	inline Text_t356221433 ** get_address_of_sizeText_4() { return &___sizeText_4; }
	inline void set_sizeText_4(Text_t356221433 * value)
	{
		___sizeText_4 = value;
		Il2CppCodeGenWriteBarrier(&___sizeText_4, value);
	}

	inline static int32_t get_offset_of_totalPriceText_5() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___totalPriceText_5)); }
	inline Text_t356221433 * get_totalPriceText_5() const { return ___totalPriceText_5; }
	inline Text_t356221433 ** get_address_of_totalPriceText_5() { return &___totalPriceText_5; }
	inline void set_totalPriceText_5(Text_t356221433 * value)
	{
		___totalPriceText_5 = value;
		Il2CppCodeGenWriteBarrier(&___totalPriceText_5, value);
	}

	inline static int32_t get_offset_of_detailPanel_6() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___detailPanel_6)); }
	inline GameObject_t1756533147 * get_detailPanel_6() const { return ___detailPanel_6; }
	inline GameObject_t1756533147 ** get_address_of_detailPanel_6() { return &___detailPanel_6; }
	inline void set_detailPanel_6(GameObject_t1756533147 * value)
	{
		___detailPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___detailPanel_6, value);
	}

	inline static int32_t get_offset_of_detectorPanel_7() { return static_cast<int32_t>(offsetof(UIItems_t2800976608, ___detectorPanel_7)); }
	inline GameObject_t1756533147 * get_detectorPanel_7() const { return ___detectorPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_detectorPanel_7() { return &___detectorPanel_7; }
	inline void set_detectorPanel_7(GameObject_t1756533147 * value)
	{
		___detectorPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___detectorPanel_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
