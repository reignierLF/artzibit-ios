﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MenuController/UIItems
struct UIItems_t3088753794;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController
struct  MenuController_t848154101  : public MonoBehaviour_t1158329972
{
public:
	// MenuController/UIItems MenuController::uiItems
	UIItems_t3088753794 * ___uiItems_2;

public:
	inline static int32_t get_offset_of_uiItems_2() { return static_cast<int32_t>(offsetof(MenuController_t848154101, ___uiItems_2)); }
	inline UIItems_t3088753794 * get_uiItems_2() const { return ___uiItems_2; }
	inline UIItems_t3088753794 ** get_address_of_uiItems_2() { return &___uiItems_2; }
	inline void set_uiItems_2(UIItems_t3088753794 * value)
	{
		___uiItems_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiItems_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
