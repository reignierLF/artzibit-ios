﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2508627033.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Vuforia_UnityExtensions_U3CModuleU3E3783534214.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController2638793709.h"
#include "Vuforia_UnityExtensions_Vuforia_ARController_U3CU32604000414.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo1398758191.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2121820252.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo3746630162.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCon342269456.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearARCo2750347603.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice1202635122.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_EyeID642957731.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew1521251591.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHoloLensApiAbs1386933393.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker2183873360.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackerARCon3939888793.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin3766399464.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin2945034146.h"
#include "Vuforia_UnityExtensions_Vuforia_DelegateHelper1202011487.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearUser117253723.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearCal3632467967.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearDev2977282393.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearDevi22891981.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraConfiguratio3904398347.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseCameraConfigurat38459502.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseStereoViewerCa1102239676.h"
#include "Vuforia_UnityExtensions_Vuforia_StereoViewerCamera3365023487.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr3502001541.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr1161658011.h"
#include "Vuforia_UnityExtensions_Vuforia_HoloLensExtendedTr3432166560.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaExtendedTra2074328369.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkManagerImpl1660847547.h"
#include "Vuforia_UnityExtensions_Vuforia_InstanceIdImpl3955455590.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTargetImpl2700679413.h"
#include "Vuforia_UnityExtensions_Vuforia_VuMarkTemplateImpl199901830.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityContro1276557833.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityControll38013191.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr3644694819.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra111727860.h"
#include "Vuforia_UnityExtensions_Vuforia_CustomViewerParamet779886969.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackingMana2097550852.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter648583075.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2025108506.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs3732945727.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra1518014586.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode3894463544.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra832065887.h"
#include "Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel979448318.h"
#include "Vuforia_UnityExtensions_Vuforia_MeshUtils2110180948.h"
#include "Vuforia_UnityExtensions_Vuforia_ExternalStereoCame4187656756.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHideExcessArea2290611987.h"
#include "Vuforia_UnityExtensions_Vuforia_StencilHideExcessA3377289090.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (UnityAPICompatibilityVersionAttribute_t2508627033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1500[1] = 
{
	UnityAPICompatibilityVersionAttribute_t2508627033::get_offset_of__version_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (PersistentListenerMode_t857969000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1501[8] = 
{
	PersistentListenerMode_t857969000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (ArgumentCache_t4810721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1502[6] = 
{
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgument_0(),
	ArgumentCache_t4810721::get_offset_of_m_ObjectArgumentAssemblyTypeName_1(),
	ArgumentCache_t4810721::get_offset_of_m_IntArgument_2(),
	ArgumentCache_t4810721::get_offset_of_m_FloatArgument_3(),
	ArgumentCache_t4810721::get_offset_of_m_StringArgument_4(),
	ArgumentCache_t4810721::get_offset_of_m_BoolArgument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (BaseInvokableCall_t2229564840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (InvokableCall_t2183506063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[1] = 
{
	InvokableCall_t2183506063::get_offset_of_Delegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1505[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1506[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1507[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1508[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (UnityEventCallState_t3420894182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1510[4] = 
{
	UnityEventCallState_t3420894182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (PersistentCall_t3793436469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[5] = 
{
	PersistentCall_t3793436469::get_offset_of_m_Target_0(),
	PersistentCall_t3793436469::get_offset_of_m_MethodName_1(),
	PersistentCall_t3793436469::get_offset_of_m_Mode_2(),
	PersistentCall_t3793436469::get_offset_of_m_Arguments_3(),
	PersistentCall_t3793436469::get_offset_of_m_CallState_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (PersistentCallGroup_t339478082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[1] = 
{
	PersistentCallGroup_t339478082::get_offset_of_m_Calls_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (InvokableCallList_t2295673753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[4] = 
{
	InvokableCallList_t2295673753::get_offset_of_m_PersistentCalls_0(),
	InvokableCallList_t2295673753::get_offset_of_m_RuntimeCalls_1(),
	InvokableCallList_t2295673753::get_offset_of_m_ExecutingCalls_2(),
	InvokableCallList_t2295673753::get_offset_of_m_NeedsUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (UnityEventBase_t828812576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[4] = 
{
	UnityEventBase_t828812576::get_offset_of_m_Calls_0(),
	UnityEventBase_t828812576::get_offset_of_m_PersistentCalls_1(),
	UnityEventBase_t828812576::get_offset_of_m_TypeName_2(),
	UnityEventBase_t828812576::get_offset_of_m_CallsDirty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (UnityAction_t4025899511), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (UnityEvent_t408735097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[1] = 
{
	UnityEvent_t408735097::get_offset_of_m_InvokeArray_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1522[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (UnityString_t276356480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1526[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1527[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1530[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1531[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1535[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (PreserveAttribute_t4182602970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1540[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (ARController_t2638793709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[1] = 
{
	ARController_t2638793709::get_offset_of_mVuforiaBehaviour_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (U3CU3Ec__DisplayClass11_0_t2604000414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2604000414::get_offset_of_controller_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (DigitalEyewearARController_t1398758191), -1, sizeof(DigitalEyewearARController_t1398758191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1547[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t1398758191::get_offset_of_mCameraOffset_7(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingMode_8(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingLayer_9(),
	DigitalEyewearARController_t1398758191::get_offset_of_mEyewearType_10(),
	DigitalEyewearARController_t1398758191::get_offset_of_mStereoFramework_11(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSeeThroughConfiguration_12(),
	DigitalEyewearARController_t1398758191::get_offset_of_mViewerName_13(),
	DigitalEyewearARController_t1398758191::get_offset_of_mViewerManufacturer_14(),
	DigitalEyewearARController_t1398758191::get_offset_of_mUseCustomViewer_15(),
	DigitalEyewearARController_t1398758191::get_offset_of_mCustomViewer_16(),
	DigitalEyewearARController_t1398758191::get_offset_of_mCentralAnchorPoint_17(),
	DigitalEyewearARController_t1398758191::get_offset_of_mParentAnchorPoint_18(),
	DigitalEyewearARController_t1398758191::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearARController_t1398758191::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearARController_t1398758191::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearARController_t1398758191::get_offset_of_mDistortionRenderingBhvr_25(),
	DigitalEyewearARController_t1398758191::get_offset_of_mSetFocusPlaneAutomatically_26(),
	DigitalEyewearARController_t1398758191_StaticFields::get_offset_of_mInstance_27(),
	DigitalEyewearARController_t1398758191_StaticFields::get_offset_of_mPadlock_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (EyewearType_t2121820252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1548[4] = 
{
	EyewearType_t2121820252::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (StereoFramework_t3746630162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1549[4] = 
{
	StereoFramework_t3746630162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (SeeThroughConfiguration_t342269456)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1550[3] = 
{
	SeeThroughConfiguration_t342269456::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (SerializableViewerParameters_t2750347603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1551[11] = 
{
	SerializableViewerParameters_t2750347603::get_offset_of_Version_0(),
	SerializableViewerParameters_t2750347603::get_offset_of_Name_1(),
	SerializableViewerParameters_t2750347603::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t2750347603::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t2750347603::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t2750347603::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t2750347603::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t2750347603::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t2750347603::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t2750347603::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t2750347603::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (EyewearDevice_t1202635122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (EyeID_t642957731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1553[4] = 
{
	EyeID_t642957731::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (EyewearCalibrationReading_t1521251591)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t1521251591_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1554[5] = 
{
	EyewearCalibrationReading_t1521251591::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (NullHoloLensApiAbstraction_t1386933393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (DeviceTracker_t2183873360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (DeviceTrackerARController_t3939888793), -1, sizeof(DeviceTrackerARController_t3939888793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1559[13] = 
{
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t3939888793::get_offset_of_mAutoInitTracker_3(),
	DeviceTrackerARController_t3939888793::get_offset_of_mAutoStartTracker_4(),
	DeviceTrackerARController_t3939888793::get_offset_of_mPosePrediction_5(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelCorrectionMode_6(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelTransformEnabled_7(),
	DeviceTrackerARController_t3939888793::get_offset_of_mModelTransform_8(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerStarted_9(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerWasActiveBeforePause_10(),
	DeviceTrackerARController_t3939888793::get_offset_of_mTrackerWasActiveBeforeDisabling_11(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_mInstance_12(),
	DeviceTrackerARController_t3939888793_StaticFields::get_offset_of_mPadlock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (DistortionRenderingMode_t3766399464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1560[4] = 
{
	DistortionRenderingMode_t3766399464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (DistortionRenderingBehaviour_t2945034146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[12] = 
{
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (DelegateHelper_t1202011487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (PlayModeEyewearUserCalibratorImpl_t117253723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (PlayModeEyewearCalibrationProfileManagerImpl_t3632467967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (PlayModeEyewearDevice_t2977282393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1566[3] = 
{
	PlayModeEyewearDevice_t2977282393::get_offset_of_mProfileManager_1(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mCalibrator_2(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mDummyPredictiveTracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (DedicatedEyewearDevice_t22891981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1567[2] = 
{
	DedicatedEyewearDevice_t22891981::get_offset_of_mProfileManager_1(),
	DedicatedEyewearDevice_t22891981::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (CameraConfigurationUtility_t3904398347), -1, sizeof(CameraConfigurationUtility_t3904398347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1568[6] = 
{
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (BaseCameraConfiguration_t38459502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[10] = 
{
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t38459502::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t38459502::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t38459502::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t38459502::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t38459502::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t38459502::get_offset_of_mBackgroundPlaneBehaviour_8(),
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraParameterChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (BaseStereoViewerCameraConfiguration_t1102239676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[5] = 
{
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mPrimaryCamera_10(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSecondaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSkewFrustum_12(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenWidth_13(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (StereoViewerCameraConfiguration_t3365023487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[7] = 
{
	0,
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mCameraOffset_20(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mIsDistorted_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (HoloLensExtendedTrackingManager_t3502001541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[15] = 
{
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mNumFramesStablePose_0(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxPoseRelDistance_1(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxPoseAngleDiff_2(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxCamPoseAbsDistance_3(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxCamPoseAngleDiff_4(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinNumFramesPoseOff_5(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinPoseUpdateRelDistance_6(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMinPoseUpdateAngleDiff_7(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackableSizeInViewThreshold_8(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mSetWorldAnchors_10(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackingList_11(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackablesExtendedTrackingEnabled_12(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mTrackablesCurrentlyExtendedTracked_13(),
	HoloLensExtendedTrackingManager_t3502001541::get_offset_of_mExtendedTrackablesState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (PoseInfo_t1161658011)+ sizeof (Il2CppObject), sizeof(PoseInfo_t1161658011 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1574[3] = 
{
	PoseInfo_t1161658011::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1161658011::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseInfo_t1161658011::get_offset_of_NumFramesPoseWasOff_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (PoseAgeEntry_t3432166560)+ sizeof (Il2CppObject), sizeof(PoseAgeEntry_t3432166560 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1575[3] = 
{
	PoseAgeEntry_t3432166560::get_offset_of_Pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t3432166560::get_offset_of_CameraPose_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PoseAgeEntry_t3432166560::get_offset_of_Age_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (VuforiaExtendedTrackingManager_t2074328369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (VuMarkManagerImpl_t1660847547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[6] = 
{
	VuMarkManagerImpl_t1660847547::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t1660847547::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (InstanceIdImpl_t3955455590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[5] = 
{
	InstanceIdImpl_t3955455590::get_offset_of_mDataType_0(),
	InstanceIdImpl_t3955455590::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t3955455590::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t3955455590::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t3955455590::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (VuMarkTargetImpl_t2700679413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[5] = 
{
	VuMarkTargetImpl_t2700679413::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mTargetId_2(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImage_3(),
	VuMarkTargetImpl_t2700679413::get_offset_of_mInstanceImageHeader_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (VuMarkTemplateImpl_t199901830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[3] = 
{
	VuMarkTemplateImpl_t199901830::get_offset_of_mUserData_4(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mOrigin_5(),
	VuMarkTemplateImpl_t199901830::get_offset_of_mTrackingFromRuntimeAppearance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (MixedRealityController_t1276557833), -1, sizeof(MixedRealityController_t1276557833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1582[13] = 
{
	MixedRealityController_t1276557833_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t1276557833::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t1276557833::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t1276557833::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t1276557833::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t1276557833::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t1276557833::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t1276557833::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t1276557833::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t1276557833::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t1276557833::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t1276557833::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t1276557833::get_offset_of_mAutoStopCameraIfNotRequired_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (Mode_t38013191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1583[7] = 
{
	Mode_t38013191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (RotationalDeviceTracker_t3644694819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (MODEL_CORRECTION_MODE_t111727860)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1585[4] = 
{
	MODEL_CORRECTION_MODE_t111727860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (CustomViewerParameters_t779886969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[7] = 
{
	CustomViewerParameters_t779886969::get_offset_of_mVersion_1(),
	CustomViewerParameters_t779886969::get_offset_of_mName_2(),
	CustomViewerParameters_t779886969::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t779886969::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t779886969::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t779886969::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t779886969::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (DeviceTrackingManager_t2097550852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[4] = 
{
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t2097550852::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t2097550852::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (FactorySetter_t648583075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (EyewearCalibrationProfileManagerImpl_t2025108506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (BackgroundPlaneAbstractBehaviour_t3732945727), -1, sizeof(BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1590[13] = 
{
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumFramesToUpdateVideoBg_5(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (EyewearUserCalibratorImpl_t1518014586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t3894463544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (RotationalDeviceTrackerImpl_t832065887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (IOSCamRecoveringHelper_t979448318), -1, sizeof(IOSCamRecoveringHelper_t979448318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1595[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (MeshUtils_t2110180948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (ExternalStereoCameraConfiguration_t4187656756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftNearClipPlane_16(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftFarClipPlane_17(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightNearClipPlane_18(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightFarClipPlane_19(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_20(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightVerticalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftProjection_24(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightProjection_25(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftNearClipPlane_26(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftFarClipPlane_27(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightNearClipPlane_28(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightFarClipPlane_29(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftVerticalVirtualFoV_30(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftHorizontalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightVerticalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightHorizontalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetLeftMatrix_34(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetRightMatrix_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (NullHideExcessAreaClipping_t2290611987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (StencilHideExcessAreaClipping_t3377289090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1599[13] = 
{
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalScale_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
