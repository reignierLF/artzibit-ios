﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerlessMenuController/UIItems
struct  UIItems_t3002935515  : public Il2CppObject
{
public:
	// UnityEngine.UI.Button MarkerlessMenuController/UIItems::minusButton
	Button_t2872111280 * ___minusButton_0;
	// UnityEngine.UI.Button MarkerlessMenuController/UIItems::plusButton
	Button_t2872111280 * ___plusButton_1;
	// UnityEngine.UI.Button MarkerlessMenuController/UIItems::closeButton
	Button_t2872111280 * ___closeButton_2;
	// UnityEngine.UI.Button MarkerlessMenuController/UIItems::screenShotButton
	Button_t2872111280 * ___screenShotButton_3;
	// UnityEngine.UI.Button MarkerlessMenuController/UIItems::confirmSizeButton
	Button_t2872111280 * ___confirmSizeButton_4;
	// UnityEngine.UI.Text MarkerlessMenuController/UIItems::distanceText
	Text_t356221433 * ___distanceText_5;
	// UnityEngine.UI.Text MarkerlessMenuController/UIItems::sizeText
	Text_t356221433 * ___sizeText_6;
	// UnityEngine.UI.Text MarkerlessMenuController/UIItems::totalPriceText
	Text_t356221433 * ___totalPriceText_7;
	// UnityEngine.GameObject MarkerlessMenuController/UIItems::detailPanel
	GameObject_t1756533147 * ___detailPanel_8;
	// UnityEngine.GameObject MarkerlessMenuController/UIItems::detectorPanel
	GameObject_t1756533147 * ___detectorPanel_9;

public:
	inline static int32_t get_offset_of_minusButton_0() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___minusButton_0)); }
	inline Button_t2872111280 * get_minusButton_0() const { return ___minusButton_0; }
	inline Button_t2872111280 ** get_address_of_minusButton_0() { return &___minusButton_0; }
	inline void set_minusButton_0(Button_t2872111280 * value)
	{
		___minusButton_0 = value;
		Il2CppCodeGenWriteBarrier(&___minusButton_0, value);
	}

	inline static int32_t get_offset_of_plusButton_1() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___plusButton_1)); }
	inline Button_t2872111280 * get_plusButton_1() const { return ___plusButton_1; }
	inline Button_t2872111280 ** get_address_of_plusButton_1() { return &___plusButton_1; }
	inline void set_plusButton_1(Button_t2872111280 * value)
	{
		___plusButton_1 = value;
		Il2CppCodeGenWriteBarrier(&___plusButton_1, value);
	}

	inline static int32_t get_offset_of_closeButton_2() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___closeButton_2)); }
	inline Button_t2872111280 * get_closeButton_2() const { return ___closeButton_2; }
	inline Button_t2872111280 ** get_address_of_closeButton_2() { return &___closeButton_2; }
	inline void set_closeButton_2(Button_t2872111280 * value)
	{
		___closeButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___closeButton_2, value);
	}

	inline static int32_t get_offset_of_screenShotButton_3() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___screenShotButton_3)); }
	inline Button_t2872111280 * get_screenShotButton_3() const { return ___screenShotButton_3; }
	inline Button_t2872111280 ** get_address_of_screenShotButton_3() { return &___screenShotButton_3; }
	inline void set_screenShotButton_3(Button_t2872111280 * value)
	{
		___screenShotButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___screenShotButton_3, value);
	}

	inline static int32_t get_offset_of_confirmSizeButton_4() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___confirmSizeButton_4)); }
	inline Button_t2872111280 * get_confirmSizeButton_4() const { return ___confirmSizeButton_4; }
	inline Button_t2872111280 ** get_address_of_confirmSizeButton_4() { return &___confirmSizeButton_4; }
	inline void set_confirmSizeButton_4(Button_t2872111280 * value)
	{
		___confirmSizeButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___confirmSizeButton_4, value);
	}

	inline static int32_t get_offset_of_distanceText_5() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___distanceText_5)); }
	inline Text_t356221433 * get_distanceText_5() const { return ___distanceText_5; }
	inline Text_t356221433 ** get_address_of_distanceText_5() { return &___distanceText_5; }
	inline void set_distanceText_5(Text_t356221433 * value)
	{
		___distanceText_5 = value;
		Il2CppCodeGenWriteBarrier(&___distanceText_5, value);
	}

	inline static int32_t get_offset_of_sizeText_6() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___sizeText_6)); }
	inline Text_t356221433 * get_sizeText_6() const { return ___sizeText_6; }
	inline Text_t356221433 ** get_address_of_sizeText_6() { return &___sizeText_6; }
	inline void set_sizeText_6(Text_t356221433 * value)
	{
		___sizeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___sizeText_6, value);
	}

	inline static int32_t get_offset_of_totalPriceText_7() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___totalPriceText_7)); }
	inline Text_t356221433 * get_totalPriceText_7() const { return ___totalPriceText_7; }
	inline Text_t356221433 ** get_address_of_totalPriceText_7() { return &___totalPriceText_7; }
	inline void set_totalPriceText_7(Text_t356221433 * value)
	{
		___totalPriceText_7 = value;
		Il2CppCodeGenWriteBarrier(&___totalPriceText_7, value);
	}

	inline static int32_t get_offset_of_detailPanel_8() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___detailPanel_8)); }
	inline GameObject_t1756533147 * get_detailPanel_8() const { return ___detailPanel_8; }
	inline GameObject_t1756533147 ** get_address_of_detailPanel_8() { return &___detailPanel_8; }
	inline void set_detailPanel_8(GameObject_t1756533147 * value)
	{
		___detailPanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___detailPanel_8, value);
	}

	inline static int32_t get_offset_of_detectorPanel_9() { return static_cast<int32_t>(offsetof(UIItems_t3002935515, ___detectorPanel_9)); }
	inline GameObject_t1756533147 * get_detectorPanel_9() const { return ___detectorPanel_9; }
	inline GameObject_t1756533147 ** get_address_of_detectorPanel_9() { return &___detectorPanel_9; }
	inline void set_detectorPanel_9(GameObject_t1756533147 * value)
	{
		___detectorPanel_9 = value;
		Il2CppCodeGenWriteBarrier(&___detectorPanel_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
