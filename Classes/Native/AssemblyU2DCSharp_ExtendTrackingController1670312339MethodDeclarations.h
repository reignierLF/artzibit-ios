﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExtendTrackingController
struct ExtendTrackingController_t1670312339;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"

// System.Void ExtendTrackingController::.ctor()
extern "C"  void ExtendTrackingController__ctor_m111577496 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExtendTrackingController::Start()
extern "C"  void ExtendTrackingController_Start_m4291075508 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExtendTrackingController::Update()
extern "C"  void ExtendTrackingController_Update_m3705178103 (ExtendTrackingController_t1670312339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExtendTrackingController::lockToggleButtonValueChangeHandler(UnityEngine.UI.Toggle)
extern "C"  void ExtendTrackingController_lockToggleButtonValueChangeHandler_m1729102927 (ExtendTrackingController_t1670312339 * __this, Toggle_t3976754468 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExtendTrackingController::<Start>m__0(System.Boolean)
extern "C"  void ExtendTrackingController_U3CStartU3Em__0_m55630388 (ExtendTrackingController_t1670312339 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
