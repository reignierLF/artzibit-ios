﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DidFinishSplashScreenLoading
struct DidFinishSplashScreenLoading_t2427584111;

#include "codegen/il2cpp-codegen.h"

// System.Void DidFinishSplashScreenLoading::.ctor()
extern "C"  void DidFinishSplashScreenLoading__ctor_m4194989786 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DidFinishSplashScreenLoading::_DidFinishUnitySplashScreenLoading()
extern "C"  void DidFinishSplashScreenLoading__DidFinishUnitySplashScreenLoading_m4151621763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DidFinishSplashScreenLoading::Start()
extern "C"  void DidFinishSplashScreenLoading_Start_m1705684910 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DidFinishSplashScreenLoading::Update()
extern "C"  void DidFinishSplashScreenLoading_Update_m3558713339 (DidFinishSplashScreenLoading_t2427584111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DidFinishSplashScreenLoading::DidFinishUnitySplashScreenLoading()
extern "C"  void DidFinishSplashScreenLoading_DidFinishUnitySplashScreenLoading_m3572516558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
