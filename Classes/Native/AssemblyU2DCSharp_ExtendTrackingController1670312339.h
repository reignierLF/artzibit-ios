﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2654589389;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtendTrackingController
struct  ExtendTrackingController_t1670312339  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle ExtendTrackingController::lockToggle
	Toggle_t3976754468 * ___lockToggle_2;
	// Vuforia.ImageTargetBehaviour ExtendTrackingController::itb
	ImageTargetBehaviour_t2654589389 * ___itb_3;

public:
	inline static int32_t get_offset_of_lockToggle_2() { return static_cast<int32_t>(offsetof(ExtendTrackingController_t1670312339, ___lockToggle_2)); }
	inline Toggle_t3976754468 * get_lockToggle_2() const { return ___lockToggle_2; }
	inline Toggle_t3976754468 ** get_address_of_lockToggle_2() { return &___lockToggle_2; }
	inline void set_lockToggle_2(Toggle_t3976754468 * value)
	{
		___lockToggle_2 = value;
		Il2CppCodeGenWriteBarrier(&___lockToggle_2, value);
	}

	inline static int32_t get_offset_of_itb_3() { return static_cast<int32_t>(offsetof(ExtendTrackingController_t1670312339, ___itb_3)); }
	inline ImageTargetBehaviour_t2654589389 * get_itb_3() const { return ___itb_3; }
	inline ImageTargetBehaviour_t2654589389 ** get_address_of_itb_3() { return &___itb_3; }
	inline void set_itb_3(ImageTargetBehaviour_t2654589389 * value)
	{
		___itb_3 = value;
		Il2CppCodeGenWriteBarrier(&___itb_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
