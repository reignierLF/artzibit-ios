﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "Vuforia_UnityExtensions_iOS_U3CModuleU3E3783534214.h"
#include "Vuforia_UnityExtensions_iOS_Vuforia_VuforiaNativeI1210651633.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_Markerless1009278913.h"
#include "AssemblyU2DCSharp_Markerless_U3CGetCoordinatesU3Ec2481679252.h"
#include "AssemblyU2DCSharp_ArtFrame69822562.h"
#include "AssemblyU2DCSharp_DetectHandler4061016557.h"
#include "AssemblyU2DCSharp_FadeInOut611841665.h"
#include "AssemblyU2DCSharp_GetDistance254146547.h"
#include "AssemblyU2DCSharp_DidFinishSplashScreenLoading2427584111.h"
#include "AssemblyU2DCSharp_MainCameraScene1629247010.h"
#include "AssemblyU2DCSharp_MainSceneController1039965335.h"
#include "AssemblyU2DCSharp_PersistentScript2409504826.h"
#include "AssemblyU2DCSharp_ExtendTrackingController1670312339.h"
#include "AssemblyU2DCSharp_LockAndUnlock2349896792.h"
#include "AssemblyU2DCSharp_MarkerMenuController1479643319.h"
#include "AssemblyU2DCSharp_MarkerMenuController_UIItems2800976608.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture947150707.h"
#include "AssemblyU2DCSharp_MarkerPinchGesture_AccessableVar4249881888.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController3874082612.h"
#include "AssemblyU2DCSharp_MarkerlessMenuController_UIItems3002935515.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture1797694232.h"
#include "AssemblyU2DCSharp_MarkerlessPinchGesture_Accessabl3397361249.h"
#include "AssemblyU2DCSharp_MenuController848154101.h"
#include "AssemblyU2DCSharp_MenuController_UIItems3088753794.h"
#include "AssemblyU2DCSharp_PinchGesture1024774609.h"
#include "AssemblyU2DCSharp_PinchGesture_AccessableVariable686036674.h"
#include "AssemblyU2DCSharp_URLImage3675617922.h"
#include "AssemblyU2DCSharp_URLImage_U3CStartU3Ec__Iterator03672507052.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_WSAUnityPlayer425981959.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuMarkBehaviour2060629989.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaConfiguration3823746026.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaRuntimeInitializa1850075444.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3842535002.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1749519406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1746754562.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_C900829694.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2691167515.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2157404822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3369627127.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2144252492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3675451859.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3634411257.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_2607665220.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3273007553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3398611001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1039424009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Cr69389957.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_4103805620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1952940174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3887193949.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (VuforiaNativeIosWrapper_t1210651633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2016[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Markerless_t1009278913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[10] = 
{
	Markerless_t1009278913::get_offset_of_originalLatitude_2(),
	Markerless_t1009278913::get_offset_of_originalLongitude_3(),
	Markerless_t1009278913::get_offset_of_currentLongitude_4(),
	Markerless_t1009278913::get_offset_of_currentLatitude_5(),
	Markerless_t1009278913::get_offset_of_distanceTextObject_6(),
	Markerless_t1009278913::get_offset_of_distance_7(),
	Markerless_t1009278913::get_offset_of_setOriginalValues_8(),
	Markerless_t1009278913::get_offset_of_targetPosition_9(),
	Markerless_t1009278913::get_offset_of_originalPosition_10(),
	Markerless_t1009278913::get_offset_of_speed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CGetCoordinatesU3Ec__Iterator0_t2481679252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[5] = 
{
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252::get_offset_of_U24this_1(),
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252::get_offset_of_U24current_2(),
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252::get_offset_of_U24disposing_3(),
	U3CGetCoordinatesU3Ec__Iterator0_t2481679252::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (ArtFrame_t69822562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[8] = 
{
	ArtFrame_t69822562::get_offset_of_artFrame_2(),
	ArtFrame_t69822562::get_offset_of_art_3(),
	ArtFrame_t69822562::get_offset_of_framelessHolder_4(),
	ArtFrame_t69822562::get_offset_of_width3DText_5(),
	ArtFrame_t69822562::get_offset_of_height3DText_6(),
	ArtFrame_t69822562::get_offset_of_distance3DText_7(),
	ArtFrame_t69822562::get_offset_of_persistentScript_8(),
	ArtFrame_t69822562::get_offset_of_unitToCM_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (DetectHandler_t4061016557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[7] = 
{
	DetectHandler_t4061016557::get_offset_of_isDetected_2(),
	DetectHandler_t4061016557::get_offset_of_canvas_3(),
	DetectHandler_t4061016557::get_offset_of_menuController_4(),
	DetectHandler_t4061016557::get_offset_of_mTrackableBehaviour_5(),
	DetectHandler_t4061016557::get_offset_of_detectorPanelAnimator_6(),
	DetectHandler_t4061016557::get_offset_of_detailPanelAnimator_7(),
	DetectHandler_t4061016557::get_offset_of_skipAnimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (FadeInOut_t611841665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[5] = 
{
	FadeInOut_t611841665::get_offset_of_fadeTexture_2(),
	FadeInOut_t611841665::get_offset_of_fadeSpeed_3(),
	FadeInOut_t611841665::get_offset_of_drawDepth_4(),
	FadeInOut_t611841665::get_offset_of_alpha_5(),
	FadeInOut_t611841665::get_offset_of_fadeDir_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (GetDistance_t254146547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[4] = 
{
	GetDistance_t254146547::get_offset_of_distanceUnitToCM_2(),
	GetDistance_t254146547::get_offset_of_artFrame_3(),
	GetDistance_t254146547::get_offset_of_distanceInUnit_4(),
	GetDistance_t254146547::get_offset_of_distanceInCM_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (DidFinishSplashScreenLoading_t2427584111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (MainCameraScene_t1629247010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	MainCameraScene_t1629247010::get_offset_of_mCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (MainSceneController_t1039965335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[11] = 
{
	MainSceneController_t1039965335::get_offset_of_closeButton_2(),
	MainSceneController_t1039965335::get_offset_of_markerButton_3(),
	MainSceneController_t1039965335::get_offset_of_markerlessButton_4(),
	MainSceneController_t1039965335::get_offset_of_emailMarkerButton_5(),
	MainSceneController_t1039965335::get_offset_of_artImageUrl_6(),
	MainSceneController_t1039965335::get_offset_of_selectedWidth_7(),
	MainSceneController_t1039965335::get_offset_of_selectedHeight_8(),
	MainSceneController_t1039965335::get_offset_of_selectedPrice_9(),
	MainSceneController_t1039965335::get_offset_of_selectedWidthAmountPerCm_10(),
	MainSceneController_t1039965335::get_offset_of_selectedHeightAmountPerCm_11(),
	MainSceneController_t1039965335::get_offset_of_persistentScript_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (PersistentScript_t2409504826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[6] = 
{
	PersistentScript_t2409504826::get_offset_of_artImageUrl_2(),
	PersistentScript_t2409504826::get_offset_of_selectedWidth_3(),
	PersistentScript_t2409504826::get_offset_of_selectedHeight_4(),
	PersistentScript_t2409504826::get_offset_of_selectedPrice_5(),
	PersistentScript_t2409504826::get_offset_of_selectedWidthAmountPerCm_6(),
	PersistentScript_t2409504826::get_offset_of_selectedHeightAmountPerCm_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (ExtendTrackingController_t1670312339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	ExtendTrackingController_t1670312339::get_offset_of_lockToggle_2(),
	ExtendTrackingController_t1670312339::get_offset_of_itb_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (LockAndUnlock_t2349896792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[3] = 
{
	LockAndUnlock_t2349896792::get_offset_of_script_2(),
	LockAndUnlock_t2349896792::get_offset_of_background_3(),
	LockAndUnlock_t2349896792::get_offset_of_imageScript_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (MarkerMenuController_t1479643319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[6] = 
{
	MarkerMenuController_t1479643319::get_offset_of_uiItems_2(),
	MarkerMenuController_t1479643319::get_offset_of_markerPinchGesture_3(),
	MarkerMenuController_t1479643319::get_offset_of_getDistance_4(),
	MarkerMenuController_t1479643319::get_offset_of_detectHandler_5(),
	MarkerMenuController_t1479643319::get_offset_of_imageTarget_6(),
	MarkerMenuController_t1479643319::get_offset_of_currentDistance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (UIItems_t2800976608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[8] = 
{
	UIItems_t2800976608::get_offset_of_closeButton_0(),
	UIItems_t2800976608::get_offset_of_screenShotButton_1(),
	UIItems_t2800976608::get_offset_of_confirmSizeButton_2(),
	UIItems_t2800976608::get_offset_of_distanceText_3(),
	UIItems_t2800976608::get_offset_of_sizeText_4(),
	UIItems_t2800976608::get_offset_of_totalPriceText_5(),
	UIItems_t2800976608::get_offset_of_detailPanel_6(),
	UIItems_t2800976608::get_offset_of_detectorPanel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (MarkerPinchGesture_t947150707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[11] = 
{
	MarkerPinchGesture_t947150707::get_offset_of_accessableVariable_2(),
	MarkerPinchGesture_t947150707::get_offset_of_markerMenuController_3(),
	MarkerPinchGesture_t947150707::get_offset_of_artFrame_4(),
	MarkerPinchGesture_t947150707::get_offset_of_unitToCM_5(),
	MarkerPinchGesture_t947150707::get_offset_of_orthoZoomSpeed_6(),
	MarkerPinchGesture_t947150707::get_offset_of_widthInCM_7(),
	MarkerPinchGesture_t947150707::get_offset_of_heightInCM_8(),
	MarkerPinchGesture_t947150707::get_offset_of_persistentScript_9(),
	MarkerPinchGesture_t947150707::get_offset_of_widthPriceRatioPerCm_10(),
	MarkerPinchGesture_t947150707::get_offset_of_heightPriceRatioPerCm_11(),
	MarkerPinchGesture_t947150707::get_offset_of_isGetSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (AccessableVariable_t4249881888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[6] = 
{
	AccessableVariable_t4249881888::get_offset_of_artFrameLossyScaleX_0(),
	AccessableVariable_t4249881888::get_offset_of_artFrameLossyScaleY_1(),
	AccessableVariable_t4249881888::get_offset_of_artFrameLossyScaleZ_2(),
	AccessableVariable_t4249881888::get_offset_of_currentWidth_3(),
	AccessableVariable_t4249881888::get_offset_of_currentHeight_4(),
	AccessableVariable_t4249881888::get_offset_of_currentPrice_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (MarkerlessMenuController_t3874082612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[5] = 
{
	MarkerlessMenuController_t3874082612::get_offset_of_uiItems_2(),
	MarkerlessMenuController_t3874082612::get_offset_of_markerlessPinchGesture_3(),
	MarkerlessMenuController_t3874082612::get_offset_of_getDistance_4(),
	MarkerlessMenuController_t3874082612::get_offset_of_imageTarget_5(),
	MarkerlessMenuController_t3874082612::get_offset_of_currentDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (UIItems_t3002935515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[10] = 
{
	UIItems_t3002935515::get_offset_of_minusButton_0(),
	UIItems_t3002935515::get_offset_of_plusButton_1(),
	UIItems_t3002935515::get_offset_of_closeButton_2(),
	UIItems_t3002935515::get_offset_of_screenShotButton_3(),
	UIItems_t3002935515::get_offset_of_confirmSizeButton_4(),
	UIItems_t3002935515::get_offset_of_distanceText_5(),
	UIItems_t3002935515::get_offset_of_sizeText_6(),
	UIItems_t3002935515::get_offset_of_totalPriceText_7(),
	UIItems_t3002935515::get_offset_of_detailPanel_8(),
	UIItems_t3002935515::get_offset_of_detectorPanel_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (MarkerlessPinchGesture_t1797694232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[11] = 
{
	MarkerlessPinchGesture_t1797694232::get_offset_of_accessableVariable_2(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_markerlessMenuController_3(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_artFrame_4(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_unitToCM_5(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_orthoZoomSpeed_6(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_widthInCM_7(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_heightInCM_8(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_persistentScript_9(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_widthPriceRatioPerCm_10(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_heightPriceRatioPerCm_11(),
	MarkerlessPinchGesture_t1797694232::get_offset_of_isGetSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (AccessableVariable_t3397361249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[6] = 
{
	AccessableVariable_t3397361249::get_offset_of_artFrameLossyScaleX_0(),
	AccessableVariable_t3397361249::get_offset_of_artFrameLossyScaleY_1(),
	AccessableVariable_t3397361249::get_offset_of_artFrameLossyScaleZ_2(),
	AccessableVariable_t3397361249::get_offset_of_currentWidth_3(),
	AccessableVariable_t3397361249::get_offset_of_currentHeight_4(),
	AccessableVariable_t3397361249::get_offset_of_currentPrice_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (MenuController_t848154101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	MenuController_t848154101::get_offset_of_uiItems_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (UIItems_t3088753794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[7] = 
{
	UIItems_t3088753794::get_offset_of_closeButton_0(),
	UIItems_t3088753794::get_offset_of_screenShotButton_1(),
	UIItems_t3088753794::get_offset_of_addCartButton_2(),
	UIItems_t3088753794::get_offset_of_sizeText_3(),
	UIItems_t3088753794::get_offset_of_totalPriceText_4(),
	UIItems_t3088753794::get_offset_of_detailPanel_5(),
	UIItems_t3088753794::get_offset_of_detectorPanel_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (PinchGesture_t1024774609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[9] = 
{
	PinchGesture_t1024774609::get_offset_of_accessableVariable_2(),
	PinchGesture_t1024774609::get_offset_of_markerLessMenuController_3(),
	PinchGesture_t1024774609::get_offset_of_artFrame_4(),
	PinchGesture_t1024774609::get_offset_of_unitToCM_5(),
	PinchGesture_t1024774609::get_offset_of_orthoZoomSpeed_6(),
	PinchGesture_t1024774609::get_offset_of_widthInCM_7(),
	PinchGesture_t1024774609::get_offset_of_heightInCM_8(),
	PinchGesture_t1024774609::get_offset_of_widthInInches_9(),
	PinchGesture_t1024774609::get_offset_of_heightInInches_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (AccessableVariable_t686036674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[3] = 
{
	AccessableVariable_t686036674::get_offset_of_artFrameLossyScaleX_0(),
	AccessableVariable_t686036674::get_offset_of_artFrameLossyScaleY_1(),
	AccessableVariable_t686036674::get_offset_of_artFrameLossyScaleZ_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (URLImage_t3675617922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	URLImage_t3675617922::get_offset_of_url_2(),
	URLImage_t3675617922::get_offset_of_persistentScript_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CStartU3Ec__Iterator0_t3672507052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[6] = 
{
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U3CwwwU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U3CimageRendererU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3672507052::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2052[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (WSAUnityPlayer_t425981959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[1] = 
{
	WSAUnityPlayer_t425981959::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2071[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (VuMarkBehaviour_t2060629989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2075[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (VuforiaConfiguration_t3823746026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (VuforiaRuntimeInitialization_t1850075444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (AxisTouchButton_t3842535002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[6] = 
{
	AxisTouchButton_t3842535002::get_offset_of_axisName_2(),
	AxisTouchButton_t3842535002::get_offset_of_axisValue_3(),
	AxisTouchButton_t3842535002::get_offset_of_responseSpeed_4(),
	AxisTouchButton_t3842535002::get_offset_of_returnToCentreSpeed_5(),
	AxisTouchButton_t3842535002::get_offset_of_m_PairedWith_6(),
	AxisTouchButton_t3842535002::get_offset_of_m_Axis_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (ButtonHandler_t1749519406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[1] = 
{
	ButtonHandler_t1749519406::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (CrossPlatformInputManager_t1746754562), -1, sizeof(CrossPlatformInputManager_t1746754562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t1746754562_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (ActiveInputMethod_t900829694)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	ActiveInputMethod_t900829694::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (VirtualAxis_t2691167515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[3] = 
{
	VirtualAxis_t2691167515::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_t2691167515::get_offset_of_m_Value_1(),
	VirtualAxis_t2691167515::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (VirtualButton_t2157404822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[5] = 
{
	VirtualButton_t2157404822::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_t2157404822::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_t2157404822::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_t2157404822::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_t2157404822::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (InputAxisScrollbar_t3369627127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[1] = 
{
	InputAxisScrollbar_t3369627127::get_offset_of_axis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (Joystick_t2144252492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[9] = 
{
	Joystick_t2144252492::get_offset_of_MovementRange_2(),
	Joystick_t2144252492::get_offset_of_axesToUse_3(),
	Joystick_t2144252492::get_offset_of_horizontalAxisName_4(),
	Joystick_t2144252492::get_offset_of_verticalAxisName_5(),
	Joystick_t2144252492::get_offset_of_m_StartPos_6(),
	Joystick_t2144252492::get_offset_of_m_UseX_7(),
	Joystick_t2144252492::get_offset_of_m_UseY_8(),
	Joystick_t2144252492::get_offset_of_m_HorizontalVirtualAxis_9(),
	Joystick_t2144252492::get_offset_of_m_VerticalVirtualAxis_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (AxisOption_t3675451859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2090[4] = 
{
	AxisOption_t3675451859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (MobileControlRig_t3634411257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (MobileInput_t2607665220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (StandaloneInput_t3273007553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (TiltInput_t3398611001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[5] = 
{
	TiltInput_t3398611001::get_offset_of_mapping_2(),
	TiltInput_t3398611001::get_offset_of_tiltAroundAxis_3(),
	TiltInput_t3398611001::get_offset_of_fullTiltAngle_4(),
	TiltInput_t3398611001::get_offset_of_centreAngleOffset_5(),
	TiltInput_t3398611001::get_offset_of_m_SteerAxis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (AxisOptions_t1039424009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[3] = 
{
	AxisOptions_t1039424009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (AxisMapping_t69389957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[2] = 
{
	AxisMapping_t69389957::get_offset_of_type_0(),
	AxisMapping_t69389957::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (MappingType_t4103805620)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	MappingType_t4103805620::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (TouchPad_t1952940174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[18] = 
{
	TouchPad_t1952940174::get_offset_of_axesToUse_2(),
	TouchPad_t1952940174::get_offset_of_controlStyle_3(),
	TouchPad_t1952940174::get_offset_of_horizontalAxisName_4(),
	TouchPad_t1952940174::get_offset_of_verticalAxisName_5(),
	TouchPad_t1952940174::get_offset_of_Xsensitivity_6(),
	TouchPad_t1952940174::get_offset_of_Ysensitivity_7(),
	TouchPad_t1952940174::get_offset_of_m_StartPos_8(),
	TouchPad_t1952940174::get_offset_of_m_PreviousDelta_9(),
	TouchPad_t1952940174::get_offset_of_m_JoytickOutput_10(),
	TouchPad_t1952940174::get_offset_of_m_UseX_11(),
	TouchPad_t1952940174::get_offset_of_m_UseY_12(),
	TouchPad_t1952940174::get_offset_of_m_HorizontalVirtualAxis_13(),
	TouchPad_t1952940174::get_offset_of_m_VerticalVirtualAxis_14(),
	TouchPad_t1952940174::get_offset_of_m_Dragging_15(),
	TouchPad_t1952940174::get_offset_of_m_Id_16(),
	TouchPad_t1952940174::get_offset_of_m_PreviousTouchPos_17(),
	TouchPad_t1952940174::get_offset_of_m_Center_18(),
	TouchPad_t1952940174::get_offset_of_m_Image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (AxisOption_t3887193949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2099[4] = 
{
	AxisOption_t3887193949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
