﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerMenuController/UIItems
struct UIItems_t2800976608;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerMenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m3373216083 (UIItems_t2800976608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
