﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerlessPinchGesture
struct MarkerlessPinchGesture_t1797694232;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerlessPinchGesture::.ctor()
extern "C"  void MarkerlessPinchGesture__ctor_m1782302171 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessPinchGesture::Start()
extern "C"  void MarkerlessPinchGesture_Start_m1053862679 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessPinchGesture::Update()
extern "C"  void MarkerlessPinchGesture_Update_m4203074650 (MarkerlessPinchGesture_t1797694232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
