﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuController
struct MenuController_t848154101;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MenuController::.ctor()
extern "C"  void MenuController__ctor_m1077658770 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::_CloseUnity3dARView()
extern "C"  void MenuController__CloseUnity3dARView_m518168999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::_ScreenShot()
extern "C"  void MenuController__ScreenShot_m3215517515 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Start()
extern "C"  void MenuController_Start_m3141389506 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::Update()
extern "C"  void MenuController_Update_m843216889 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::closeEvent()
extern "C"  void MenuController_closeEvent_m638198012 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::screenShotEvent()
extern "C"  void MenuController_screenShotEvent_m3813898030 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::addCartEvent()
extern "C"  void MenuController_addCartEvent_m402579493 (MenuController_t848154101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MenuController::ReturnToMain(System.String)
extern "C"  void MenuController_ReturnToMain_m2516101376 (MenuController_t848154101 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
