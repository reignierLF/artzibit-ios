﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MenuController/UIItems
struct UIItems_t3088753794;

#include "codegen/il2cpp-codegen.h"

// System.Void MenuController/UIItems::.ctor()
extern "C"  void UIItems__ctor_m4092615153 (UIItems_t3088753794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
