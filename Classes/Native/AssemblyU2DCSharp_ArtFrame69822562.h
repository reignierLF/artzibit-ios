﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// PersistentScript
struct PersistentScript_t2409504826;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArtFrame
struct  ArtFrame_t69822562  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ArtFrame::artFrame
	GameObject_t1756533147 * ___artFrame_2;
	// UnityEngine.GameObject ArtFrame::art
	GameObject_t1756533147 * ___art_3;
	// UnityEngine.GameObject ArtFrame::framelessHolder
	GameObject_t1756533147 * ___framelessHolder_4;
	// UnityEngine.TextMesh ArtFrame::width3DText
	TextMesh_t1641806576 * ___width3DText_5;
	// UnityEngine.TextMesh ArtFrame::height3DText
	TextMesh_t1641806576 * ___height3DText_6;
	// UnityEngine.TextMesh ArtFrame::distance3DText
	TextMesh_t1641806576 * ___distance3DText_7;
	// PersistentScript ArtFrame::persistentScript
	PersistentScript_t2409504826 * ___persistentScript_8;
	// System.Single ArtFrame::unitToCM
	float ___unitToCM_9;

public:
	inline static int32_t get_offset_of_artFrame_2() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___artFrame_2)); }
	inline GameObject_t1756533147 * get_artFrame_2() const { return ___artFrame_2; }
	inline GameObject_t1756533147 ** get_address_of_artFrame_2() { return &___artFrame_2; }
	inline void set_artFrame_2(GameObject_t1756533147 * value)
	{
		___artFrame_2 = value;
		Il2CppCodeGenWriteBarrier(&___artFrame_2, value);
	}

	inline static int32_t get_offset_of_art_3() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___art_3)); }
	inline GameObject_t1756533147 * get_art_3() const { return ___art_3; }
	inline GameObject_t1756533147 ** get_address_of_art_3() { return &___art_3; }
	inline void set_art_3(GameObject_t1756533147 * value)
	{
		___art_3 = value;
		Il2CppCodeGenWriteBarrier(&___art_3, value);
	}

	inline static int32_t get_offset_of_framelessHolder_4() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___framelessHolder_4)); }
	inline GameObject_t1756533147 * get_framelessHolder_4() const { return ___framelessHolder_4; }
	inline GameObject_t1756533147 ** get_address_of_framelessHolder_4() { return &___framelessHolder_4; }
	inline void set_framelessHolder_4(GameObject_t1756533147 * value)
	{
		___framelessHolder_4 = value;
		Il2CppCodeGenWriteBarrier(&___framelessHolder_4, value);
	}

	inline static int32_t get_offset_of_width3DText_5() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___width3DText_5)); }
	inline TextMesh_t1641806576 * get_width3DText_5() const { return ___width3DText_5; }
	inline TextMesh_t1641806576 ** get_address_of_width3DText_5() { return &___width3DText_5; }
	inline void set_width3DText_5(TextMesh_t1641806576 * value)
	{
		___width3DText_5 = value;
		Il2CppCodeGenWriteBarrier(&___width3DText_5, value);
	}

	inline static int32_t get_offset_of_height3DText_6() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___height3DText_6)); }
	inline TextMesh_t1641806576 * get_height3DText_6() const { return ___height3DText_6; }
	inline TextMesh_t1641806576 ** get_address_of_height3DText_6() { return &___height3DText_6; }
	inline void set_height3DText_6(TextMesh_t1641806576 * value)
	{
		___height3DText_6 = value;
		Il2CppCodeGenWriteBarrier(&___height3DText_6, value);
	}

	inline static int32_t get_offset_of_distance3DText_7() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___distance3DText_7)); }
	inline TextMesh_t1641806576 * get_distance3DText_7() const { return ___distance3DText_7; }
	inline TextMesh_t1641806576 ** get_address_of_distance3DText_7() { return &___distance3DText_7; }
	inline void set_distance3DText_7(TextMesh_t1641806576 * value)
	{
		___distance3DText_7 = value;
		Il2CppCodeGenWriteBarrier(&___distance3DText_7, value);
	}

	inline static int32_t get_offset_of_persistentScript_8() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___persistentScript_8)); }
	inline PersistentScript_t2409504826 * get_persistentScript_8() const { return ___persistentScript_8; }
	inline PersistentScript_t2409504826 ** get_address_of_persistentScript_8() { return &___persistentScript_8; }
	inline void set_persistentScript_8(PersistentScript_t2409504826 * value)
	{
		___persistentScript_8 = value;
		Il2CppCodeGenWriteBarrier(&___persistentScript_8, value);
	}

	inline static int32_t get_offset_of_unitToCM_9() { return static_cast<int32_t>(offsetof(ArtFrame_t69822562, ___unitToCM_9)); }
	inline float get_unitToCM_9() const { return ___unitToCM_9; }
	inline float* get_address_of_unitToCM_9() { return &___unitToCM_9; }
	inline void set_unitToCM_9(float value)
	{
		___unitToCM_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
