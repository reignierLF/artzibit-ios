﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ArtFrame
struct ArtFrame_t69822562;

#include "codegen/il2cpp-codegen.h"

// System.Void ArtFrame::.ctor()
extern "C"  void ArtFrame__ctor_m474197265 (ArtFrame_t69822562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArtFrame::Start()
extern "C"  void ArtFrame_Start_m1229393505 (ArtFrame_t69822562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ArtFrame::Update()
extern "C"  void ArtFrame_Update_m3516943232 (ArtFrame_t69822562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
