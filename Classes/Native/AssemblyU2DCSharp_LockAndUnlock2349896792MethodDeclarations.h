﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LockAndUnlock
struct LockAndUnlock_t2349896792;

#include "codegen/il2cpp-codegen.h"

// System.Void LockAndUnlock::.ctor()
extern "C"  void LockAndUnlock__ctor_m1094286887 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockAndUnlock::Start()
extern "C"  void LockAndUnlock_Start_m761137011 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LockAndUnlock::Update()
extern "C"  void LockAndUnlock_Update_m587801242 (LockAndUnlock_t2349896792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
