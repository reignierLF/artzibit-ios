﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerlessPinchGesture/AccessableVariable
struct AccessableVariable_t3397361249;

#include "codegen/il2cpp-codegen.h"

// System.Void MarkerlessPinchGesture/AccessableVariable::.ctor()
extern "C"  void AccessableVariable__ctor_m3410707070 (AccessableVariable_t3397361249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
