﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DetectHandler
struct DetectHandler_t4061016557;

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"

// System.Void DetectHandler::.ctor()
extern "C"  void DetectHandler__ctor_m3751729232 (DetectHandler_t4061016557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectHandler::Start()
extern "C"  void DetectHandler_Start_m1978693000 (DetectHandler_t4061016557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectHandler::Update()
extern "C"  void DetectHandler_Update_m95363405 (DetectHandler_t4061016557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DetectHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void DetectHandler_OnTrackableStateChanged_m2541740733 (DetectHandler_t4061016557 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
