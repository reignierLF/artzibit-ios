﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ArtFrame
struct ArtFrame_t69822562;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetDistance
struct  GetDistance_t254146547  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GetDistance::distanceUnitToCM
	float ___distanceUnitToCM_2;
	// ArtFrame GetDistance::artFrame
	ArtFrame_t69822562 * ___artFrame_3;
	// System.Single GetDistance::distanceInUnit
	float ___distanceInUnit_4;
	// System.Single GetDistance::distanceInCM
	float ___distanceInCM_5;

public:
	inline static int32_t get_offset_of_distanceUnitToCM_2() { return static_cast<int32_t>(offsetof(GetDistance_t254146547, ___distanceUnitToCM_2)); }
	inline float get_distanceUnitToCM_2() const { return ___distanceUnitToCM_2; }
	inline float* get_address_of_distanceUnitToCM_2() { return &___distanceUnitToCM_2; }
	inline void set_distanceUnitToCM_2(float value)
	{
		___distanceUnitToCM_2 = value;
	}

	inline static int32_t get_offset_of_artFrame_3() { return static_cast<int32_t>(offsetof(GetDistance_t254146547, ___artFrame_3)); }
	inline ArtFrame_t69822562 * get_artFrame_3() const { return ___artFrame_3; }
	inline ArtFrame_t69822562 ** get_address_of_artFrame_3() { return &___artFrame_3; }
	inline void set_artFrame_3(ArtFrame_t69822562 * value)
	{
		___artFrame_3 = value;
		Il2CppCodeGenWriteBarrier(&___artFrame_3, value);
	}

	inline static int32_t get_offset_of_distanceInUnit_4() { return static_cast<int32_t>(offsetof(GetDistance_t254146547, ___distanceInUnit_4)); }
	inline float get_distanceInUnit_4() const { return ___distanceInUnit_4; }
	inline float* get_address_of_distanceInUnit_4() { return &___distanceInUnit_4; }
	inline void set_distanceInUnit_4(float value)
	{
		___distanceInUnit_4 = value;
	}

	inline static int32_t get_offset_of_distanceInCM_5() { return static_cast<int32_t>(offsetof(GetDistance_t254146547, ___distanceInCM_5)); }
	inline float get_distanceInCM_5() const { return ___distanceInCM_5; }
	inline float* get_address_of_distanceInCM_5() { return &___distanceInCM_5; }
	inline void set_distanceInCM_5(float value)
	{
		___distanceInCM_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
