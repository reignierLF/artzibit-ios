﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExtendTrackingController
struct ExtendTrackingController_t1670312339;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LockAndUnlock
struct  LockAndUnlock_t2349896792  : public MonoBehaviour_t1158329972
{
public:
	// ExtendTrackingController LockAndUnlock::script
	ExtendTrackingController_t1670312339 * ___script_2;
	// UnityEngine.Transform LockAndUnlock::background
	Transform_t3275118058 * ___background_3;
	// UnityEngine.UI.Image LockAndUnlock::imageScript
	Image_t2042527209 * ___imageScript_4;

public:
	inline static int32_t get_offset_of_script_2() { return static_cast<int32_t>(offsetof(LockAndUnlock_t2349896792, ___script_2)); }
	inline ExtendTrackingController_t1670312339 * get_script_2() const { return ___script_2; }
	inline ExtendTrackingController_t1670312339 ** get_address_of_script_2() { return &___script_2; }
	inline void set_script_2(ExtendTrackingController_t1670312339 * value)
	{
		___script_2 = value;
		Il2CppCodeGenWriteBarrier(&___script_2, value);
	}

	inline static int32_t get_offset_of_background_3() { return static_cast<int32_t>(offsetof(LockAndUnlock_t2349896792, ___background_3)); }
	inline Transform_t3275118058 * get_background_3() const { return ___background_3; }
	inline Transform_t3275118058 ** get_address_of_background_3() { return &___background_3; }
	inline void set_background_3(Transform_t3275118058 * value)
	{
		___background_3 = value;
		Il2CppCodeGenWriteBarrier(&___background_3, value);
	}

	inline static int32_t get_offset_of_imageScript_4() { return static_cast<int32_t>(offsetof(LockAndUnlock_t2349896792, ___imageScript_4)); }
	inline Image_t2042527209 * get_imageScript_4() const { return ___imageScript_4; }
	inline Image_t2042527209 ** get_address_of_imageScript_4() { return &___imageScript_4; }
	inline void set_imageScript_4(Image_t2042527209 * value)
	{
		___imageScript_4 = value;
		Il2CppCodeGenWriteBarrier(&___imageScript_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
