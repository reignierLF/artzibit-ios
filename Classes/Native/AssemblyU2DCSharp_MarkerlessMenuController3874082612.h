﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MarkerlessMenuController/UIItems
struct UIItems_t3002935515;
// MarkerlessPinchGesture
struct MarkerlessPinchGesture_t1797694232;
// GetDistance
struct GetDistance_t254146547;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerlessMenuController
struct  MarkerlessMenuController_t3874082612  : public MonoBehaviour_t1158329972
{
public:
	// MarkerlessMenuController/UIItems MarkerlessMenuController::uiItems
	UIItems_t3002935515 * ___uiItems_2;
	// MarkerlessPinchGesture MarkerlessMenuController::markerlessPinchGesture
	MarkerlessPinchGesture_t1797694232 * ___markerlessPinchGesture_3;
	// GetDistance MarkerlessMenuController::getDistance
	GetDistance_t254146547 * ___getDistance_4;
	// UnityEngine.GameObject MarkerlessMenuController::imageTarget
	GameObject_t1756533147 * ___imageTarget_5;
	// System.Single MarkerlessMenuController::currentDistance
	float ___currentDistance_6;

public:
	inline static int32_t get_offset_of_uiItems_2() { return static_cast<int32_t>(offsetof(MarkerlessMenuController_t3874082612, ___uiItems_2)); }
	inline UIItems_t3002935515 * get_uiItems_2() const { return ___uiItems_2; }
	inline UIItems_t3002935515 ** get_address_of_uiItems_2() { return &___uiItems_2; }
	inline void set_uiItems_2(UIItems_t3002935515 * value)
	{
		___uiItems_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiItems_2, value);
	}

	inline static int32_t get_offset_of_markerlessPinchGesture_3() { return static_cast<int32_t>(offsetof(MarkerlessMenuController_t3874082612, ___markerlessPinchGesture_3)); }
	inline MarkerlessPinchGesture_t1797694232 * get_markerlessPinchGesture_3() const { return ___markerlessPinchGesture_3; }
	inline MarkerlessPinchGesture_t1797694232 ** get_address_of_markerlessPinchGesture_3() { return &___markerlessPinchGesture_3; }
	inline void set_markerlessPinchGesture_3(MarkerlessPinchGesture_t1797694232 * value)
	{
		___markerlessPinchGesture_3 = value;
		Il2CppCodeGenWriteBarrier(&___markerlessPinchGesture_3, value);
	}

	inline static int32_t get_offset_of_getDistance_4() { return static_cast<int32_t>(offsetof(MarkerlessMenuController_t3874082612, ___getDistance_4)); }
	inline GetDistance_t254146547 * get_getDistance_4() const { return ___getDistance_4; }
	inline GetDistance_t254146547 ** get_address_of_getDistance_4() { return &___getDistance_4; }
	inline void set_getDistance_4(GetDistance_t254146547 * value)
	{
		___getDistance_4 = value;
		Il2CppCodeGenWriteBarrier(&___getDistance_4, value);
	}

	inline static int32_t get_offset_of_imageTarget_5() { return static_cast<int32_t>(offsetof(MarkerlessMenuController_t3874082612, ___imageTarget_5)); }
	inline GameObject_t1756533147 * get_imageTarget_5() const { return ___imageTarget_5; }
	inline GameObject_t1756533147 ** get_address_of_imageTarget_5() { return &___imageTarget_5; }
	inline void set_imageTarget_5(GameObject_t1756533147 * value)
	{
		___imageTarget_5 = value;
		Il2CppCodeGenWriteBarrier(&___imageTarget_5, value);
	}

	inline static int32_t get_offset_of_currentDistance_6() { return static_cast<int32_t>(offsetof(MarkerlessMenuController_t3874082612, ___currentDistance_6)); }
	inline float get_currentDistance_6() const { return ___currentDistance_6; }
	inline float* get_address_of_currentDistance_6() { return &___currentDistance_6; }
	inline void set_currentDistance_6(float value)
	{
		___currentDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
