﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MarkerMenuController/UIItems
struct UIItems_t2800976608;
// MarkerPinchGesture
struct MarkerPinchGesture_t947150707;
// GetDistance
struct GetDistance_t254146547;
// DetectHandler
struct DetectHandler_t4061016557;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerMenuController
struct  MarkerMenuController_t1479643319  : public MonoBehaviour_t1158329972
{
public:
	// MarkerMenuController/UIItems MarkerMenuController::uiItems
	UIItems_t2800976608 * ___uiItems_2;
	// MarkerPinchGesture MarkerMenuController::markerPinchGesture
	MarkerPinchGesture_t947150707 * ___markerPinchGesture_3;
	// GetDistance MarkerMenuController::getDistance
	GetDistance_t254146547 * ___getDistance_4;
	// DetectHandler MarkerMenuController::detectHandler
	DetectHandler_t4061016557 * ___detectHandler_5;
	// UnityEngine.GameObject MarkerMenuController::imageTarget
	GameObject_t1756533147 * ___imageTarget_6;
	// System.Single MarkerMenuController::currentDistance
	float ___currentDistance_7;

public:
	inline static int32_t get_offset_of_uiItems_2() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___uiItems_2)); }
	inline UIItems_t2800976608 * get_uiItems_2() const { return ___uiItems_2; }
	inline UIItems_t2800976608 ** get_address_of_uiItems_2() { return &___uiItems_2; }
	inline void set_uiItems_2(UIItems_t2800976608 * value)
	{
		___uiItems_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiItems_2, value);
	}

	inline static int32_t get_offset_of_markerPinchGesture_3() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___markerPinchGesture_3)); }
	inline MarkerPinchGesture_t947150707 * get_markerPinchGesture_3() const { return ___markerPinchGesture_3; }
	inline MarkerPinchGesture_t947150707 ** get_address_of_markerPinchGesture_3() { return &___markerPinchGesture_3; }
	inline void set_markerPinchGesture_3(MarkerPinchGesture_t947150707 * value)
	{
		___markerPinchGesture_3 = value;
		Il2CppCodeGenWriteBarrier(&___markerPinchGesture_3, value);
	}

	inline static int32_t get_offset_of_getDistance_4() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___getDistance_4)); }
	inline GetDistance_t254146547 * get_getDistance_4() const { return ___getDistance_4; }
	inline GetDistance_t254146547 ** get_address_of_getDistance_4() { return &___getDistance_4; }
	inline void set_getDistance_4(GetDistance_t254146547 * value)
	{
		___getDistance_4 = value;
		Il2CppCodeGenWriteBarrier(&___getDistance_4, value);
	}

	inline static int32_t get_offset_of_detectHandler_5() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___detectHandler_5)); }
	inline DetectHandler_t4061016557 * get_detectHandler_5() const { return ___detectHandler_5; }
	inline DetectHandler_t4061016557 ** get_address_of_detectHandler_5() { return &___detectHandler_5; }
	inline void set_detectHandler_5(DetectHandler_t4061016557 * value)
	{
		___detectHandler_5 = value;
		Il2CppCodeGenWriteBarrier(&___detectHandler_5, value);
	}

	inline static int32_t get_offset_of_imageTarget_6() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___imageTarget_6)); }
	inline GameObject_t1756533147 * get_imageTarget_6() const { return ___imageTarget_6; }
	inline GameObject_t1756533147 ** get_address_of_imageTarget_6() { return &___imageTarget_6; }
	inline void set_imageTarget_6(GameObject_t1756533147 * value)
	{
		___imageTarget_6 = value;
		Il2CppCodeGenWriteBarrier(&___imageTarget_6, value);
	}

	inline static int32_t get_offset_of_currentDistance_7() { return static_cast<int32_t>(offsetof(MarkerMenuController_t1479643319, ___currentDistance_7)); }
	inline float get_currentDistance_7() const { return ___currentDistance_7; }
	inline float* get_address_of_currentDistance_7() { return &___currentDistance_7; }
	inline void set_currentDistance_7(float value)
	{
		___currentDistance_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
