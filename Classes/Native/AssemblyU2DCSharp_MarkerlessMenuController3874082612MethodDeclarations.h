﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerlessMenuController
struct MarkerlessMenuController_t3874082612;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MarkerlessMenuController::.ctor()
extern "C"  void MarkerlessMenuController__ctor_m1293276087 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::_CloseUnity3dARView()
extern "C"  void MarkerlessMenuController__CloseUnity3dARView_m2219918690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::_ScreenShot()
extern "C"  void MarkerlessMenuController__ScreenShot_m1921154874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::_ConfirmWidth(System.String)
extern "C"  void MarkerlessMenuController__ConfirmWidth_m1963284504 (Il2CppObject * __this /* static, unused */, String_t* ___confirmWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::_ConfirmHeight(System.String)
extern "C"  void MarkerlessMenuController__ConfirmHeight_m3076499927 (Il2CppObject * __this /* static, unused */, String_t* ___confirmHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::_ConfirmPrice(System.String)
extern "C"  void MarkerlessMenuController__ConfirmPrice_m1664757449 (Il2CppObject * __this /* static, unused */, String_t* ___confirmPrice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::Start()
extern "C"  void MarkerlessMenuController_Start_m642934771 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::Update()
extern "C"  void MarkerlessMenuController_Update_m1687670614 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::decreaseDistanceEvent()
extern "C"  void MarkerlessMenuController_decreaseDistanceEvent_m225527146 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::increaseDistanceEvent()
extern "C"  void MarkerlessMenuController_increaseDistanceEvent_m1525724254 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::closeEvent()
extern "C"  void MarkerlessMenuController_closeEvent_m1344608029 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::screenShotEvent()
extern "C"  void MarkerlessMenuController_screenShotEvent_m260816955 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::confirmSizeEvent()
extern "C"  void MarkerlessMenuController_confirmSizeEvent_m4127143624 (MarkerlessMenuController_t3874082612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerlessMenuController::ReturnToMain(System.String)
extern "C"  void MarkerlessMenuController_ReturnToMain_m157268523 (MarkerlessMenuController_t3874082612 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
