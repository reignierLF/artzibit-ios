﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PinchGesture
struct PinchGesture_t1024774609;

#include "codegen/il2cpp-codegen.h"

// System.Void PinchGesture::.ctor()
extern "C"  void PinchGesture__ctor_m1146794606 (PinchGesture_t1024774609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchGesture::Start()
extern "C"  void PinchGesture_Start_m3873565150 (PinchGesture_t1024774609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PinchGesture::Update()
extern "C"  void PinchGesture_Update_m644332661 (PinchGesture_t1024774609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
