﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PersistentScript
struct PersistentScript_t2409504826;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// URLImage
struct  URLImage_t3675617922  : public MonoBehaviour_t1158329972
{
public:
	// System.String URLImage::url
	String_t* ___url_2;
	// PersistentScript URLImage::persistentScript
	PersistentScript_t2409504826 * ___persistentScript_3;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(URLImage_t3675617922, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_persistentScript_3() { return static_cast<int32_t>(offsetof(URLImage_t3675617922, ___persistentScript_3)); }
	inline PersistentScript_t2409504826 * get_persistentScript_3() const { return ___persistentScript_3; }
	inline PersistentScript_t2409504826 ** get_address_of_persistentScript_3() { return &___persistentScript_3; }
	inline void set_persistentScript_3(PersistentScript_t2409504826 * value)
	{
		___persistentScript_3 = value;
		Il2CppCodeGenWriteBarrier(&___persistentScript_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
