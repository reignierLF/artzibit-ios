﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetDistance
struct GetDistance_t254146547;

#include "codegen/il2cpp-codegen.h"

// System.Void GetDistance::.ctor()
extern "C"  void GetDistance__ctor_m782192962 (GetDistance_t254146547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetDistance::Start()
extern "C"  void GetDistance_Start_m2748077262 (GetDistance_t254146547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetDistance::Update()
extern "C"  void GetDistance_Update_m3767149079 (GetDistance_t254146547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
