﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FadeInOut
struct FadeInOut_t611841665;

#include "codegen/il2cpp-codegen.h"

// System.Void FadeInOut::.ctor()
extern "C"  void FadeInOut__ctor_m3835509650 (FadeInOut_t611841665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeInOut::Start()
extern "C"  void FadeInOut_Start_m4254002546 (FadeInOut_t611841665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeInOut::Update()
extern "C"  void FadeInOut_Update_m3292029453 (FadeInOut_t611841665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeInOut::OnGUI()
extern "C"  void FadeInOut_OnGUI_m798385406 (FadeInOut_t611841665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
