﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Markerless
struct Markerless_t1009278913;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Markerless::.ctor()
extern "C"  void Markerless__ctor_m832057758 (Markerless_t1009278913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Markerless::GetCoordinates()
extern "C"  Il2CppObject * Markerless_GetCoordinates_m2944219429 (Markerless_t1009278913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Markerless::Calc(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Markerless_Calc_m997497509 (Markerless_t1009278913 * __this, float ___lat10, float ___lon11, float ___lat22, float ___lon23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Markerless::Start()
extern "C"  void Markerless_Start_m3555613838 (Markerless_t1009278913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Markerless::Update()
extern "C"  void Markerless_Update_m3633286693 (Markerless_t1009278913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
