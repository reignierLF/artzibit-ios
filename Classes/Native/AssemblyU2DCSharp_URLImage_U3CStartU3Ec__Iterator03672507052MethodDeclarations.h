﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// URLImage/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3672507052;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void URLImage/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3826209459 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean URLImage/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1189823173 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object URLImage/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m847187761 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object URLImage/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2587673849 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void URLImage/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2022703690 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void URLImage/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m601701508 (U3CStartU3Ec__Iterator0_t3672507052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
