﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerPinchGesture/AccessableVariable
struct  AccessableVariable_t4249881888  : public Il2CppObject
{
public:
	// System.Single MarkerPinchGesture/AccessableVariable::artFrameLossyScaleX
	float ___artFrameLossyScaleX_0;
	// System.Single MarkerPinchGesture/AccessableVariable::artFrameLossyScaleY
	float ___artFrameLossyScaleY_1;
	// System.Single MarkerPinchGesture/AccessableVariable::artFrameLossyScaleZ
	float ___artFrameLossyScaleZ_2;
	// System.Int32 MarkerPinchGesture/AccessableVariable::currentWidth
	int32_t ___currentWidth_3;
	// System.Int32 MarkerPinchGesture/AccessableVariable::currentHeight
	int32_t ___currentHeight_4;
	// System.Int32 MarkerPinchGesture/AccessableVariable::currentPrice
	int32_t ___currentPrice_5;

public:
	inline static int32_t get_offset_of_artFrameLossyScaleX_0() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___artFrameLossyScaleX_0)); }
	inline float get_artFrameLossyScaleX_0() const { return ___artFrameLossyScaleX_0; }
	inline float* get_address_of_artFrameLossyScaleX_0() { return &___artFrameLossyScaleX_0; }
	inline void set_artFrameLossyScaleX_0(float value)
	{
		___artFrameLossyScaleX_0 = value;
	}

	inline static int32_t get_offset_of_artFrameLossyScaleY_1() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___artFrameLossyScaleY_1)); }
	inline float get_artFrameLossyScaleY_1() const { return ___artFrameLossyScaleY_1; }
	inline float* get_address_of_artFrameLossyScaleY_1() { return &___artFrameLossyScaleY_1; }
	inline void set_artFrameLossyScaleY_1(float value)
	{
		___artFrameLossyScaleY_1 = value;
	}

	inline static int32_t get_offset_of_artFrameLossyScaleZ_2() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___artFrameLossyScaleZ_2)); }
	inline float get_artFrameLossyScaleZ_2() const { return ___artFrameLossyScaleZ_2; }
	inline float* get_address_of_artFrameLossyScaleZ_2() { return &___artFrameLossyScaleZ_2; }
	inline void set_artFrameLossyScaleZ_2(float value)
	{
		___artFrameLossyScaleZ_2 = value;
	}

	inline static int32_t get_offset_of_currentWidth_3() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___currentWidth_3)); }
	inline int32_t get_currentWidth_3() const { return ___currentWidth_3; }
	inline int32_t* get_address_of_currentWidth_3() { return &___currentWidth_3; }
	inline void set_currentWidth_3(int32_t value)
	{
		___currentWidth_3 = value;
	}

	inline static int32_t get_offset_of_currentHeight_4() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___currentHeight_4)); }
	inline int32_t get_currentHeight_4() const { return ___currentHeight_4; }
	inline int32_t* get_address_of_currentHeight_4() { return &___currentHeight_4; }
	inline void set_currentHeight_4(int32_t value)
	{
		___currentHeight_4 = value;
	}

	inline static int32_t get_offset_of_currentPrice_5() { return static_cast<int32_t>(offsetof(AccessableVariable_t4249881888, ___currentPrice_5)); }
	inline int32_t get_currentPrice_5() const { return ___currentPrice_5; }
	inline int32_t* get_address_of_currentPrice_5() { return &___currentPrice_5; }
	inline void set_currentPrice_5(int32_t value)
	{
		___currentPrice_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
