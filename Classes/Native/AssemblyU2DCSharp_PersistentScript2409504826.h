﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PersistentScript
struct  PersistentScript_t2409504826  : public MonoBehaviour_t1158329972
{
public:
	// System.String PersistentScript::artImageUrl
	String_t* ___artImageUrl_2;
	// System.String PersistentScript::selectedWidth
	String_t* ___selectedWidth_3;
	// System.String PersistentScript::selectedHeight
	String_t* ___selectedHeight_4;
	// System.String PersistentScript::selectedPrice
	String_t* ___selectedPrice_5;
	// System.String PersistentScript::selectedWidthAmountPerCm
	String_t* ___selectedWidthAmountPerCm_6;
	// System.String PersistentScript::selectedHeightAmountPerCm
	String_t* ___selectedHeightAmountPerCm_7;

public:
	inline static int32_t get_offset_of_artImageUrl_2() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___artImageUrl_2)); }
	inline String_t* get_artImageUrl_2() const { return ___artImageUrl_2; }
	inline String_t** get_address_of_artImageUrl_2() { return &___artImageUrl_2; }
	inline void set_artImageUrl_2(String_t* value)
	{
		___artImageUrl_2 = value;
		Il2CppCodeGenWriteBarrier(&___artImageUrl_2, value);
	}

	inline static int32_t get_offset_of_selectedWidth_3() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___selectedWidth_3)); }
	inline String_t* get_selectedWidth_3() const { return ___selectedWidth_3; }
	inline String_t** get_address_of_selectedWidth_3() { return &___selectedWidth_3; }
	inline void set_selectedWidth_3(String_t* value)
	{
		___selectedWidth_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectedWidth_3, value);
	}

	inline static int32_t get_offset_of_selectedHeight_4() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___selectedHeight_4)); }
	inline String_t* get_selectedHeight_4() const { return ___selectedHeight_4; }
	inline String_t** get_address_of_selectedHeight_4() { return &___selectedHeight_4; }
	inline void set_selectedHeight_4(String_t* value)
	{
		___selectedHeight_4 = value;
		Il2CppCodeGenWriteBarrier(&___selectedHeight_4, value);
	}

	inline static int32_t get_offset_of_selectedPrice_5() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___selectedPrice_5)); }
	inline String_t* get_selectedPrice_5() const { return ___selectedPrice_5; }
	inline String_t** get_address_of_selectedPrice_5() { return &___selectedPrice_5; }
	inline void set_selectedPrice_5(String_t* value)
	{
		___selectedPrice_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectedPrice_5, value);
	}

	inline static int32_t get_offset_of_selectedWidthAmountPerCm_6() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___selectedWidthAmountPerCm_6)); }
	inline String_t* get_selectedWidthAmountPerCm_6() const { return ___selectedWidthAmountPerCm_6; }
	inline String_t** get_address_of_selectedWidthAmountPerCm_6() { return &___selectedWidthAmountPerCm_6; }
	inline void set_selectedWidthAmountPerCm_6(String_t* value)
	{
		___selectedWidthAmountPerCm_6 = value;
		Il2CppCodeGenWriteBarrier(&___selectedWidthAmountPerCm_6, value);
	}

	inline static int32_t get_offset_of_selectedHeightAmountPerCm_7() { return static_cast<int32_t>(offsetof(PersistentScript_t2409504826, ___selectedHeightAmountPerCm_7)); }
	inline String_t* get_selectedHeightAmountPerCm_7() const { return ___selectedHeightAmountPerCm_7; }
	inline String_t** get_address_of_selectedHeightAmountPerCm_7() { return &___selectedHeightAmountPerCm_7; }
	inline void set_selectedHeightAmountPerCm_7(String_t* value)
	{
		___selectedHeightAmountPerCm_7 = value;
		Il2CppCodeGenWriteBarrier(&___selectedHeightAmountPerCm_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
