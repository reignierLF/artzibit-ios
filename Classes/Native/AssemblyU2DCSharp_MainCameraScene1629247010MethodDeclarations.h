﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainCameraScene
struct MainCameraScene_t1629247010;

#include "codegen/il2cpp-codegen.h"

// System.Void MainCameraScene::.ctor()
extern "C"  void MainCameraScene__ctor_m3800890799 (MainCameraScene_t1629247010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainCameraScene::Start()
extern "C"  void MainCameraScene_Start_m3008645075 (MainCameraScene_t1629247010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainCameraScene::Update()
extern "C"  void MainCameraScene_Update_m2667073068 (MainCameraScene_t1629247010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
