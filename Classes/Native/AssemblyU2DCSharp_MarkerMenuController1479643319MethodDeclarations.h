﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MarkerMenuController
struct MarkerMenuController_t1479643319;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void MarkerMenuController::.ctor()
extern "C"  void MarkerMenuController__ctor_m115110224 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::_CloseUnity3dARView()
extern "C"  void MarkerMenuController__CloseUnity3dARView_m1469234805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::_ScreenShot()
extern "C"  void MarkerMenuController__ScreenShot_m4181111469 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::_ConfirmWidth(System.String)
extern "C"  void MarkerMenuController__ConfirmWidth_m2587999647 (Il2CppObject * __this /* static, unused */, String_t* ___confirmWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::_ConfirmHeight(System.String)
extern "C"  void MarkerMenuController__ConfirmHeight_m4154330880 (Il2CppObject * __this /* static, unused */, String_t* ___confirmHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::_ConfirmPrice(System.String)
extern "C"  void MarkerMenuController__ConfirmPrice_m2206267518 (Il2CppObject * __this /* static, unused */, String_t* ___confirmPrice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::Start()
extern "C"  void MarkerMenuController_Start_m4009115908 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::Update()
extern "C"  void MarkerMenuController_Update_m3378171543 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::closeEvent()
extern "C"  void MarkerMenuController_closeEvent_m1957353194 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::screenShotEvent()
extern "C"  void MarkerMenuController_screenShotEvent_m538650796 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::confirmSizeEvent()
extern "C"  void MarkerMenuController_confirmSizeEvent_m269386821 (MarkerMenuController_t1479643319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MarkerMenuController::ReturnToMain(System.String)
extern "C"  void MarkerMenuController_ReturnToMain_m4254932418 (MarkerMenuController_t1479643319 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
