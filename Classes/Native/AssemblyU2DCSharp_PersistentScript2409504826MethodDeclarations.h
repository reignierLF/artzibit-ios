﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PersistentScript
struct PersistentScript_t2409504826;

#include "codegen/il2cpp-codegen.h"

// System.Void PersistentScript::.ctor()
extern "C"  void PersistentScript__ctor_m1235758893 (PersistentScript_t2409504826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersistentScript::Awake()
extern "C"  void PersistentScript_Awake_m3640625178 (PersistentScript_t2409504826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersistentScript::Start()
extern "C"  void PersistentScript_Start_m1814869605 (PersistentScript_t2409504826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PersistentScript::Update()
extern "C"  void PersistentScript_Update_m3867800380 (PersistentScript_t2409504826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
