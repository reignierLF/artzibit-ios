﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PinchGesture/AccessableVariable
struct AccessableVariable_t686036674;
// MarkerlessMenuController
struct MarkerlessMenuController_t3874082612;
// ArtFrame
struct ArtFrame_t69822562;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PinchGesture
struct  PinchGesture_t1024774609  : public MonoBehaviour_t1158329972
{
public:
	// PinchGesture/AccessableVariable PinchGesture::accessableVariable
	AccessableVariable_t686036674 * ___accessableVariable_2;
	// MarkerlessMenuController PinchGesture::markerLessMenuController
	MarkerlessMenuController_t3874082612 * ___markerLessMenuController_3;
	// ArtFrame PinchGesture::artFrame
	ArtFrame_t69822562 * ___artFrame_4;
	// System.Single PinchGesture::unitToCM
	float ___unitToCM_5;
	// System.Single PinchGesture::orthoZoomSpeed
	float ___orthoZoomSpeed_6;
	// System.Int32 PinchGesture::widthInCM
	int32_t ___widthInCM_7;
	// System.Int32 PinchGesture::heightInCM
	int32_t ___heightInCM_8;
	// System.Single PinchGesture::widthInInches
	float ___widthInInches_9;
	// System.Single PinchGesture::heightInInches
	float ___heightInInches_10;

public:
	inline static int32_t get_offset_of_accessableVariable_2() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___accessableVariable_2)); }
	inline AccessableVariable_t686036674 * get_accessableVariable_2() const { return ___accessableVariable_2; }
	inline AccessableVariable_t686036674 ** get_address_of_accessableVariable_2() { return &___accessableVariable_2; }
	inline void set_accessableVariable_2(AccessableVariable_t686036674 * value)
	{
		___accessableVariable_2 = value;
		Il2CppCodeGenWriteBarrier(&___accessableVariable_2, value);
	}

	inline static int32_t get_offset_of_markerLessMenuController_3() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___markerLessMenuController_3)); }
	inline MarkerlessMenuController_t3874082612 * get_markerLessMenuController_3() const { return ___markerLessMenuController_3; }
	inline MarkerlessMenuController_t3874082612 ** get_address_of_markerLessMenuController_3() { return &___markerLessMenuController_3; }
	inline void set_markerLessMenuController_3(MarkerlessMenuController_t3874082612 * value)
	{
		___markerLessMenuController_3 = value;
		Il2CppCodeGenWriteBarrier(&___markerLessMenuController_3, value);
	}

	inline static int32_t get_offset_of_artFrame_4() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___artFrame_4)); }
	inline ArtFrame_t69822562 * get_artFrame_4() const { return ___artFrame_4; }
	inline ArtFrame_t69822562 ** get_address_of_artFrame_4() { return &___artFrame_4; }
	inline void set_artFrame_4(ArtFrame_t69822562 * value)
	{
		___artFrame_4 = value;
		Il2CppCodeGenWriteBarrier(&___artFrame_4, value);
	}

	inline static int32_t get_offset_of_unitToCM_5() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___unitToCM_5)); }
	inline float get_unitToCM_5() const { return ___unitToCM_5; }
	inline float* get_address_of_unitToCM_5() { return &___unitToCM_5; }
	inline void set_unitToCM_5(float value)
	{
		___unitToCM_5 = value;
	}

	inline static int32_t get_offset_of_orthoZoomSpeed_6() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___orthoZoomSpeed_6)); }
	inline float get_orthoZoomSpeed_6() const { return ___orthoZoomSpeed_6; }
	inline float* get_address_of_orthoZoomSpeed_6() { return &___orthoZoomSpeed_6; }
	inline void set_orthoZoomSpeed_6(float value)
	{
		___orthoZoomSpeed_6 = value;
	}

	inline static int32_t get_offset_of_widthInCM_7() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___widthInCM_7)); }
	inline int32_t get_widthInCM_7() const { return ___widthInCM_7; }
	inline int32_t* get_address_of_widthInCM_7() { return &___widthInCM_7; }
	inline void set_widthInCM_7(int32_t value)
	{
		___widthInCM_7 = value;
	}

	inline static int32_t get_offset_of_heightInCM_8() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___heightInCM_8)); }
	inline int32_t get_heightInCM_8() const { return ___heightInCM_8; }
	inline int32_t* get_address_of_heightInCM_8() { return &___heightInCM_8; }
	inline void set_heightInCM_8(int32_t value)
	{
		___heightInCM_8 = value;
	}

	inline static int32_t get_offset_of_widthInInches_9() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___widthInInches_9)); }
	inline float get_widthInInches_9() const { return ___widthInInches_9; }
	inline float* get_address_of_widthInInches_9() { return &___widthInInches_9; }
	inline void set_widthInInches_9(float value)
	{
		___widthInInches_9 = value;
	}

	inline static int32_t get_offset_of_heightInInches_10() { return static_cast<int32_t>(offsetof(PinchGesture_t1024774609, ___heightInInches_10)); }
	inline float get_heightInInches_10() const { return ___heightInInches_10; }
	inline float* get_address_of_heightInInches_10() { return &___heightInInches_10; }
	inline void set_heightInInches_10(float value)
	{
		___heightInInches_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
