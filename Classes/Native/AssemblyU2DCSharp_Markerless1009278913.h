﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Markerless
struct  Markerless_t1009278913  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Markerless::originalLatitude
	float ___originalLatitude_2;
	// System.Single Markerless::originalLongitude
	float ___originalLongitude_3;
	// System.Single Markerless::currentLongitude
	float ___currentLongitude_4;
	// System.Single Markerless::currentLatitude
	float ___currentLatitude_5;
	// UnityEngine.GameObject Markerless::distanceTextObject
	GameObject_t1756533147 * ___distanceTextObject_6;
	// System.Double Markerless::distance
	double ___distance_7;
	// System.Boolean Markerless::setOriginalValues
	bool ___setOriginalValues_8;
	// UnityEngine.Vector3 Markerless::targetPosition
	Vector3_t2243707580  ___targetPosition_9;
	// UnityEngine.Vector3 Markerless::originalPosition
	Vector3_t2243707580  ___originalPosition_10;
	// System.Single Markerless::speed
	float ___speed_11;

public:
	inline static int32_t get_offset_of_originalLatitude_2() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___originalLatitude_2)); }
	inline float get_originalLatitude_2() const { return ___originalLatitude_2; }
	inline float* get_address_of_originalLatitude_2() { return &___originalLatitude_2; }
	inline void set_originalLatitude_2(float value)
	{
		___originalLatitude_2 = value;
	}

	inline static int32_t get_offset_of_originalLongitude_3() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___originalLongitude_3)); }
	inline float get_originalLongitude_3() const { return ___originalLongitude_3; }
	inline float* get_address_of_originalLongitude_3() { return &___originalLongitude_3; }
	inline void set_originalLongitude_3(float value)
	{
		___originalLongitude_3 = value;
	}

	inline static int32_t get_offset_of_currentLongitude_4() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___currentLongitude_4)); }
	inline float get_currentLongitude_4() const { return ___currentLongitude_4; }
	inline float* get_address_of_currentLongitude_4() { return &___currentLongitude_4; }
	inline void set_currentLongitude_4(float value)
	{
		___currentLongitude_4 = value;
	}

	inline static int32_t get_offset_of_currentLatitude_5() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___currentLatitude_5)); }
	inline float get_currentLatitude_5() const { return ___currentLatitude_5; }
	inline float* get_address_of_currentLatitude_5() { return &___currentLatitude_5; }
	inline void set_currentLatitude_5(float value)
	{
		___currentLatitude_5 = value;
	}

	inline static int32_t get_offset_of_distanceTextObject_6() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___distanceTextObject_6)); }
	inline GameObject_t1756533147 * get_distanceTextObject_6() const { return ___distanceTextObject_6; }
	inline GameObject_t1756533147 ** get_address_of_distanceTextObject_6() { return &___distanceTextObject_6; }
	inline void set_distanceTextObject_6(GameObject_t1756533147 * value)
	{
		___distanceTextObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___distanceTextObject_6, value);
	}

	inline static int32_t get_offset_of_distance_7() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___distance_7)); }
	inline double get_distance_7() const { return ___distance_7; }
	inline double* get_address_of_distance_7() { return &___distance_7; }
	inline void set_distance_7(double value)
	{
		___distance_7 = value;
	}

	inline static int32_t get_offset_of_setOriginalValues_8() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___setOriginalValues_8)); }
	inline bool get_setOriginalValues_8() const { return ___setOriginalValues_8; }
	inline bool* get_address_of_setOriginalValues_8() { return &___setOriginalValues_8; }
	inline void set_setOriginalValues_8(bool value)
	{
		___setOriginalValues_8 = value;
	}

	inline static int32_t get_offset_of_targetPosition_9() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___targetPosition_9)); }
	inline Vector3_t2243707580  get_targetPosition_9() const { return ___targetPosition_9; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_9() { return &___targetPosition_9; }
	inline void set_targetPosition_9(Vector3_t2243707580  value)
	{
		___targetPosition_9 = value;
	}

	inline static int32_t get_offset_of_originalPosition_10() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___originalPosition_10)); }
	inline Vector3_t2243707580  get_originalPosition_10() const { return ___originalPosition_10; }
	inline Vector3_t2243707580 * get_address_of_originalPosition_10() { return &___originalPosition_10; }
	inline void set_originalPosition_10(Vector3_t2243707580  value)
	{
		___originalPosition_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(Markerless_t1009278913, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
