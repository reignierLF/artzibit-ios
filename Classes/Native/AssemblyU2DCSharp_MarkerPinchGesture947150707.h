﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MarkerPinchGesture/AccessableVariable
struct AccessableVariable_t4249881888;
// MarkerMenuController
struct MarkerMenuController_t1479643319;
// ArtFrame
struct ArtFrame_t69822562;
// PersistentScript
struct PersistentScript_t2409504826;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MarkerPinchGesture
struct  MarkerPinchGesture_t947150707  : public MonoBehaviour_t1158329972
{
public:
	// MarkerPinchGesture/AccessableVariable MarkerPinchGesture::accessableVariable
	AccessableVariable_t4249881888 * ___accessableVariable_2;
	// MarkerMenuController MarkerPinchGesture::markerMenuController
	MarkerMenuController_t1479643319 * ___markerMenuController_3;
	// ArtFrame MarkerPinchGesture::artFrame
	ArtFrame_t69822562 * ___artFrame_4;
	// System.Single MarkerPinchGesture::unitToCM
	float ___unitToCM_5;
	// System.Single MarkerPinchGesture::orthoZoomSpeed
	float ___orthoZoomSpeed_6;
	// System.Int32 MarkerPinchGesture::widthInCM
	int32_t ___widthInCM_7;
	// System.Int32 MarkerPinchGesture::heightInCM
	int32_t ___heightInCM_8;
	// PersistentScript MarkerPinchGesture::persistentScript
	PersistentScript_t2409504826 * ___persistentScript_9;
	// System.Int32 MarkerPinchGesture::widthPriceRatioPerCm
	int32_t ___widthPriceRatioPerCm_10;
	// System.Int32 MarkerPinchGesture::heightPriceRatioPerCm
	int32_t ___heightPriceRatioPerCm_11;
	// System.Boolean MarkerPinchGesture::isGetSize
	bool ___isGetSize_12;

public:
	inline static int32_t get_offset_of_accessableVariable_2() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___accessableVariable_2)); }
	inline AccessableVariable_t4249881888 * get_accessableVariable_2() const { return ___accessableVariable_2; }
	inline AccessableVariable_t4249881888 ** get_address_of_accessableVariable_2() { return &___accessableVariable_2; }
	inline void set_accessableVariable_2(AccessableVariable_t4249881888 * value)
	{
		___accessableVariable_2 = value;
		Il2CppCodeGenWriteBarrier(&___accessableVariable_2, value);
	}

	inline static int32_t get_offset_of_markerMenuController_3() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___markerMenuController_3)); }
	inline MarkerMenuController_t1479643319 * get_markerMenuController_3() const { return ___markerMenuController_3; }
	inline MarkerMenuController_t1479643319 ** get_address_of_markerMenuController_3() { return &___markerMenuController_3; }
	inline void set_markerMenuController_3(MarkerMenuController_t1479643319 * value)
	{
		___markerMenuController_3 = value;
		Il2CppCodeGenWriteBarrier(&___markerMenuController_3, value);
	}

	inline static int32_t get_offset_of_artFrame_4() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___artFrame_4)); }
	inline ArtFrame_t69822562 * get_artFrame_4() const { return ___artFrame_4; }
	inline ArtFrame_t69822562 ** get_address_of_artFrame_4() { return &___artFrame_4; }
	inline void set_artFrame_4(ArtFrame_t69822562 * value)
	{
		___artFrame_4 = value;
		Il2CppCodeGenWriteBarrier(&___artFrame_4, value);
	}

	inline static int32_t get_offset_of_unitToCM_5() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___unitToCM_5)); }
	inline float get_unitToCM_5() const { return ___unitToCM_5; }
	inline float* get_address_of_unitToCM_5() { return &___unitToCM_5; }
	inline void set_unitToCM_5(float value)
	{
		___unitToCM_5 = value;
	}

	inline static int32_t get_offset_of_orthoZoomSpeed_6() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___orthoZoomSpeed_6)); }
	inline float get_orthoZoomSpeed_6() const { return ___orthoZoomSpeed_6; }
	inline float* get_address_of_orthoZoomSpeed_6() { return &___orthoZoomSpeed_6; }
	inline void set_orthoZoomSpeed_6(float value)
	{
		___orthoZoomSpeed_6 = value;
	}

	inline static int32_t get_offset_of_widthInCM_7() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___widthInCM_7)); }
	inline int32_t get_widthInCM_7() const { return ___widthInCM_7; }
	inline int32_t* get_address_of_widthInCM_7() { return &___widthInCM_7; }
	inline void set_widthInCM_7(int32_t value)
	{
		___widthInCM_7 = value;
	}

	inline static int32_t get_offset_of_heightInCM_8() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___heightInCM_8)); }
	inline int32_t get_heightInCM_8() const { return ___heightInCM_8; }
	inline int32_t* get_address_of_heightInCM_8() { return &___heightInCM_8; }
	inline void set_heightInCM_8(int32_t value)
	{
		___heightInCM_8 = value;
	}

	inline static int32_t get_offset_of_persistentScript_9() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___persistentScript_9)); }
	inline PersistentScript_t2409504826 * get_persistentScript_9() const { return ___persistentScript_9; }
	inline PersistentScript_t2409504826 ** get_address_of_persistentScript_9() { return &___persistentScript_9; }
	inline void set_persistentScript_9(PersistentScript_t2409504826 * value)
	{
		___persistentScript_9 = value;
		Il2CppCodeGenWriteBarrier(&___persistentScript_9, value);
	}

	inline static int32_t get_offset_of_widthPriceRatioPerCm_10() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___widthPriceRatioPerCm_10)); }
	inline int32_t get_widthPriceRatioPerCm_10() const { return ___widthPriceRatioPerCm_10; }
	inline int32_t* get_address_of_widthPriceRatioPerCm_10() { return &___widthPriceRatioPerCm_10; }
	inline void set_widthPriceRatioPerCm_10(int32_t value)
	{
		___widthPriceRatioPerCm_10 = value;
	}

	inline static int32_t get_offset_of_heightPriceRatioPerCm_11() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___heightPriceRatioPerCm_11)); }
	inline int32_t get_heightPriceRatioPerCm_11() const { return ___heightPriceRatioPerCm_11; }
	inline int32_t* get_address_of_heightPriceRatioPerCm_11() { return &___heightPriceRatioPerCm_11; }
	inline void set_heightPriceRatioPerCm_11(int32_t value)
	{
		___heightPriceRatioPerCm_11 = value;
	}

	inline static int32_t get_offset_of_isGetSize_12() { return static_cast<int32_t>(offsetof(MarkerPinchGesture_t947150707, ___isGetSize_12)); }
	inline bool get_isGetSize_12() const { return ___isGetSize_12; }
	inline bool* get_address_of_isGetSize_12() { return &___isGetSize_12; }
	inline void set_isGetSize_12(bool value)
	{
		___isGetSize_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
