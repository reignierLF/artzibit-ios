//
//  RegistrationViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 24/5/17.
//
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic, strong) UIView *mainNavigationBarView;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initRegistrationDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;

    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img-bg-2"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight / 3);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    backButton.icon = [UIImage imageNamed:@"left-arrow-b"];
    [_mainNavigationBarView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initRegistrationDetail{
    
    NSArray *iconArray = @[@"name-su",
                           @"user-su",
                           @"mail-su",
                           @"password-su",
                           @"password-su",
                           @"country-su"];
    
    NSArray *placeHolderArray = @[@"Name",
                                  @"Username",
                                  @"E-mail",
                                  @"Password",
                                  @"Confirm Password",
                                  @"Country"];
    
    for (int i = 0; i < 6; i++) {
    
        cUITextField *textField = [[cUITextField alloc] initWithFrame:CGRectMake(70, ((_screenHeight / 3) + 20) + (((_screenHeight / 18) + 10) * i), _screenWidth - 100, _screenHeight / 18)];
        //nameTextField.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
        textField.placeholder = [NSString stringWithFormat:@"%@",[placeHolderArray objectAtIndex:i]];
        textField.tag = i;
        [self.view addSubview:textField];
        
        UIImageView *iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[iconArray objectAtIndex:i]]]];
        iconImageView.frame = CGRectMake(40, textField.frame.origin.y + 10, 20, 20);
        iconImageView.contentMode = UIViewContentModeScaleAspectFill;
        iconImageView.clipsToBounds = YES;
        iconImageView.userInteractionEnabled = NO;
        [self.view addSubview:iconImageView];
        
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(30, textField.frame.origin.y + textField.frame.size.height, textField.frame.size.width + 40, 1)];
        seperator.backgroundColor = [UIColor grayColor];
        [self.view addSubview:seperator];
    }
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpButton.frame = CGRectMake(30, _screenHeight - ((_screenHeight / 16) * 3), _screenWidth - 60, (_screenHeight / 16) + 10);
    signUpButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    signUpButton.layer.cornerRadius = signUpButton.frame.size.height / 2.5;
    signUpButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    signUpButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    signUpButton.layer.shadowOpacity =  0.2;
    signUpButton.layer.shadowRadius =   5.0;
    [signUpButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    [self.view addSubview:signUpButton];
    
    [signUpButton addTarget:self action:@selector(signUp) forControlEvents:UIControlEventTouchUpInside];
    
    /*
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"By signing up you agree to our Terms of Service. View our FAQs here." attributes:nil];
    NSRange linkRange = NSMakeRange(31, 16); // for the word "link" in the string above
    
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor redColor],
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle) };
    [attributedString setAttributes:linkAttributes range:linkRange];
    
    UILabel *tosLabel = [[UILabel alloc] initWithFrame:CGRectMake(signUpButton.frame.origin.x, signUpButton.frame.origin.y + signUpButton.frame.size.height, signUpButton.frame.size.width, (_screenHeight / 16))];
    //tosLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    tosLabel.attributedText = attributedString;
    tosLabel.textAlignment = NSTextAlignmentCenter;
    tosLabel.font = [UIFont systemFontOfSize:10];
    [tosLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self.view addSubview:tosLabel];
     */
    
    DynamicLabel *tosLabel = [[DynamicLabel alloc] initWithFrame:CGRectMake(signUpButton.frame.origin.x, signUpButton.frame.origin.y + signUpButton.frame.size.height + 10, signUpButton.frame.size.width, (_screenHeight / 16))];
    tosLabel.delegate = self;
    //tosLabel.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.2];
    tosLabel.text = @"By signing up you agree to our Terms of Service.\nView our FAQs here.";
    tosLabel.textAlignment = NSTextAlignmentCenter;
    tosLabel.linkArray = @[@"Terms of Service", @"FAQs"];
    tosLabel.textAlignment = NSTextAlignmentCenter;
    tosLabel.numberOfLines = 0;
    tosLabel.lineBreakMode = NSLineBreakByWordWrapping;
    tosLabel.font = [UIFont systemFontOfSize:14];
    [tosLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self.view addSubview:tosLabel];
}

-(void)didSelectAtIndex:(NSInteger)index dynamicLabel:(DynamicLabel *)dynamicLabel{

    if(index == 0){
    
        ToSViewController *tosVC = [[ToSViewController alloc] init];
        tosVC.screenWidth = _screenWidth;
        tosVC.screenHeight = _screenHeight;
        tosVC.navigationBarHeight = _navigationBarHeight;
        tosVC.backgroundColor = _backgroundColor;
        tosVC.navigationBarColor = _navigationBarColor;
        
        [self.navigationController pushViewController:tosVC animated:YES];
    }else{
    
        FAQViewController *faqVC = [[FAQViewController alloc] init];
        faqVC.screenWidth = _screenWidth;
        faqVC.screenHeight = _screenHeight;
        faqVC.navigationBarHeight = _navigationBarHeight;
        faqVC.backgroundColor = _backgroundColor;
        faqVC.navigationBarColor = _navigationBarColor;
        
        [self.navigationController pushViewController:faqVC animated:YES];
    }
}

-(void)signUp{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [_delegate didFinishSignUp:self];
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
