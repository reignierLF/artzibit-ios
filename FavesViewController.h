//
//  FavesViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 30/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIButton.h"
#import "cUIScrollView.h"
#import "ImageLoader.h"

#import "ArtViewController.h"

#import "Api.h"
#import "Favorites.h"

@class FavesViewController;

@protocol FavesDelegate
-(void)disableCompassNavigationFromFaves:(FavesViewController*)faves;
-(void)enableCompassNavigationFromFaves:(FavesViewController*)faves;
-(void)goToMain;
@end

@interface FavesViewController : UIViewController <UIScrollViewDelegate, ArtDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UINavigationController *favesNavigationController;

@property (nonatomic, strong) cUIScrollView *favesScrollView;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) Favorites *favorites;

@property (nonatomic, weak) id <FavesDelegate> delegate;

-(void)reset;

@end
