//
//  cUIButton.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import "cUIButton.h"

@implementation cUIButton

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    float iconWidth = self.frame.size.width - (self.frame.size.width * 0.55);
    float iconHeight = self.frame.size.height - (self.frame.size.height * 0.55);
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width / 2) - (iconWidth / 2), (self.frame.size.height / 2) - (iconHeight / 2), iconWidth, iconHeight)];
    //imageView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    imageView.image = _icon;
    imageView.clipsToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
    
    _label = [[UILabel alloc] initWithFrame:imageView.frame];
    _label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_label];
}

@end
