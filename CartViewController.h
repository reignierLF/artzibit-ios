//
//  CartViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIButton.h"
#import "cUIScrollView.h"

#import "ItemCell.h"

@class CartViewController;

@protocol CartDelegate
-(void)goToMain;
@end

@interface CartViewController : UIViewController <UIScrollViewDelegate, ItemCellDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, weak) id <CartDelegate> delegate;

@end
