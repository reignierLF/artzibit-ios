//
//  FIlterViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 6/6/17.
//
//

#import "FilterViewController.h"

@interface FilterViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;
@property (nonatomic, strong) NSArray *filterTitleArray;
@property (nonatomic, strong) NSMutableArray *buttonArray;
@property (nonatomic, strong) NSArray *optionArray;
@property (nonatomic, strong) NSArray *artistsArray;
@property (nonatomic, strong) NSArray *categoriesArray;
@property (nonatomic, strong) NSArray *stylesArray;
@property (nonatomic) NSInteger currentSelectedFilter;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api filtersIsComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"filters list = %@", jsonDictionary);
            
            _filters = [[Filters alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    [self initButtonList];
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    NSLog(@"failed to load data");
                }
            });
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = _backgroundColor;
    
    _api = [[Api alloc] init];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float directionWidth = 20;
    float directionHeight = directionWidth;
    
    UIImageView *upDirection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"up-Direction-w"]];
    upDirection.frame = CGRectMake((_mainNavigationBarView.frame.size.width / 2) - (directionWidth / 2) , directionHeight / 4, directionWidth, directionHeight);
    upDirection.userInteractionEnabled = NO;
    [_mainNavigationBarView addSubview:upDirection];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Filter";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initButtonList{
    
    _currentSelectedFilter = 0;
    
    _buttonArray = [[NSMutableArray alloc] init];
    _artistsArray = _filters.artists.artistsName;
    _categoriesArray = _filters.categories.categoryName;
    _stylesArray = _filters.styles.styleName;
    
    _filterTitleArray = @[@"Artists", @"Style", @"Category"];
    _optionArray = @[_artistsArray, _stylesArray, _categoriesArray];
    
    [_filters saveCurrentFilteredSelected:@"Artists" index:_artistsArray.count + 1];
    [_filters saveCurrentFilteredSelected:@"Style" index:_stylesArray.count + 1];
    [_filters saveCurrentFilteredSelected:@"Category" index:_categoriesArray.count + 1];
    
    for (int i = 0; i < _filterTitleArray.count; i++) {
    
        _filterButton = [[FilterButton alloc] initWithFrame:CGRectMake(0, _navigationBarHeight + (80 * i), _screenWidth, 80)];
        //_filterButton.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.8];
        _filterButton.delegate = self;
        _filterButton.leftLabel.text = [NSString stringWithFormat:@"%@",[_filterTitleArray objectAtIndex:i]];
        _filterButton.leftLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
        _filterButton.rightLabel.text = @"All";
        _filterButton.rightLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
        _filterButton.tag = i;
        [self.view addSubview:_filterButton];
        
        [_buttonArray addObject:_filterButton];
        
        if(i == (_filterTitleArray.count - 1)){
            _filterButton.seperator.hidden = YES;
        }
    }
}

-(void)didClick:(FilterButton *)filterButton{
    
    _subFilterVC = [[SubFilterViewController alloc] init];
    _subFilterVC.delegate = self;
    _subFilterVC.screenWidth = _screenWidth;
    _subFilterVC.screenHeight = _screenHeight;
    _subFilterVC.navigationBarHeight = _navigationBarHeight;
    _subFilterVC.backgroundColor = _backgroundColor;
    _subFilterVC.navigationBarColor = _navigationBarColor;
    _subFilterVC.title = [_filterTitleArray objectAtIndex:filterButton.tag];
    _subFilterVC.currentSelectedFilter = [_filters loadCurrentFilteredSelected:_subFilterVC.title];
    
    if(filterButton.tag == 0){
    
        _subFilterVC.array = _artistsArray;
    }else if(filterButton.tag == 1){
    
        _subFilterVC.array = _stylesArray;
    }else if(filterButton.tag == 2){
    
        _subFilterVC.array = _categoriesArray;
    }
    
    [_filterNavigationController pushViewController:_subFilterVC animated:YES];

    _currentSelectedFilter = filterButton.tag;
    
    NSLog(@"tag = %ld", (long)filterButton.tag);
}

-(void)didSelectThisOption:(SubFilterViewController *)subFilter index:(int)index{

    FilterButton *newFilterButton = [_buttonArray objectAtIndex:_currentSelectedFilter];
    
    if(index == subFilter.array.count){
    
        newFilterButton.rightLabel.text = @"All";
        index = (int)subFilter.array.count + 1;
    }else{
    
        newFilterButton.rightLabel.text = [NSString stringWithFormat:@"%@",[[_optionArray objectAtIndex:_currentSelectedFilter] objectAtIndex:index]];
    }
    
    [_filters saveCurrentFilteredSelected:[_filterTitleArray objectAtIndex:_currentSelectedFilter] index:index];
    
    NSLog(@"subfilter = %ld", (long)index);
}
@end
