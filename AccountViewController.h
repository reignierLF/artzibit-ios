//
//  AccountViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/5/17.
//
//

#import <UIKit/UIKit.h>
#import "ToSViewController.h"//
#import "FAQViewController.h"

#import "cUIButton.h"

@class AccountViewController;

@protocol AccountDelegate
-(void)goToMain:(AccountViewController*)account;
@end

@interface AccountViewController : UIViewController

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, weak) id <AccountDelegate> delegate;

@end
