//
//  FAQViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 26/5/17.
//
//

#import "FAQViewController.h"

@interface FAQViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;

@property (nonatomic, strong) NSMutableArray *faqButtonArray;
@property (nonatomic, strong) NSMutableArray *faqDotArray;
@property (nonatomic, strong) NSMutableArray *faqLabelArray;

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initFAQScrollView];
    [self initFAQList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    backButton.icon = [UIImage imageNamed:@"left-arrow"];
    [_mainNavigationBarView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    _faqButtonArray = [[NSMutableArray alloc] init];
    _faqDotArray = [[NSMutableArray alloc] init];
    _faqLabelArray = [[NSMutableArray alloc] init];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"FAQs";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initFAQScrollView{

    _faqScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, _screenHeight - _navigationBarHeight)];
    _faqScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _faqScrollView.delegate = self;
    _faqScrollView.directionalLockEnabled = YES;
    _faqScrollView.contentSize = CGSizeMake(_faqScrollView.frame.size.width, 11 * (_faqScrollView.frame.size.height / 5));
    [self.view addSubview:_faqScrollView];
}

-(void)initFAQList{

    for (int i = 0; i < 11; i++) {
    
        cUIButton *faqButton = [cUIButton buttonWithType:UIButtonTypeCustom];
        faqButton.frame = CGRectMake(0, i * (_faqScrollView.frame.size.height / 5), _faqScrollView.frame.size.width, _faqScrollView.frame.size.height / 5);
        faqButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        faqButton.tag = i;
        [_faqScrollView addSubview:faqButton];
        
        faqButton.label.frame = CGRectMake(30, faqButton.label.frame.origin.y, faqButton.frame.size.width - 60, faqButton.label.frame.size.height);
        faqButton.label.text = [NSString stringWithFormat:@"%@", [[self faqQuestions] objectAtIndex:i]];
        faqButton.label.numberOfLines = 0;
        faqButton.label.textAlignment = NSTextAlignmentLeft;
        faqButton.label.lineBreakMode = NSLineBreakByWordWrapping;
        
        [faqButton addTarget:self action:@selector(faqEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        if(i != 0){
        
            UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(30, 1, faqButton.frame.size.width - 60, 1)];
            seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.2];
            [faqButton addSubview:seperator];
        }
        
        UILabel *faqDotLabel = [[UILabel alloc] initWithFrame:CGRectMake(faqButton.label.frame.origin.x, faqButton.frame.origin.y + faqButton.frame.size.height, 20, 0)];
        //faqDotLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
        faqDotLabel.text = @"\n•";
        faqDotLabel.numberOfLines = 0;
        faqDotLabel.textAlignment = NSTextAlignmentCenter;
        [_faqScrollView addSubview:faqDotLabel];
        
        UILabel *faqLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, faqDotLabel.frame.origin.y, faqButton.frame.size.width - 80, 0)];
        //faqLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
        faqLabel.text = [NSString stringWithFormat:@"\n%@\n",[[self faqAnswers] objectAtIndex:i]];
        faqLabel.numberOfLines = 0;
        faqLabel.lineBreakMode = NSLineBreakByWordWrapping;
        //[faqLabel sizeToFit];
        [_faqScrollView addSubview:faqLabel];
        
        [_faqButtonArray addObject:faqButton];
        [_faqDotArray addObject:faqDotLabel];
        [_faqLabelArray addObject:faqLabel];
    }
}

-(void)faqEvent:(UIButton*)sender{
    
    if (sender.selected==YES) {
        [sender setSelected:NO];
        
        __block float revertSize = 0;
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             UILabel *faqDotLabel = [_faqDotArray objectAtIndex:sender.tag];
                             faqDotLabel.frame = CGRectMake(faqDotLabel.frame.origin.x, faqDotLabel.frame.origin.y, faqDotLabel.frame.size.width, 0);
                             
                             UILabel *faqLabel = [_faqLabelArray objectAtIndex:sender.tag];
                             
                             revertSize = faqLabel.frame.size.height;
                             
                             faqLabel.frame = CGRectMake(faqLabel.frame.origin.x, faqLabel.frame.origin.y, faqLabel.frame.size.width, 0);
                             
                             for (int i = (int)sender.tag + 1; i < _faqButtonArray.count; i++) {
                                 
                                 cUIButton *newRefFaqButton = [_faqButtonArray objectAtIndex:i];
                                 newRefFaqButton.frame = CGRectMake(newRefFaqButton.frame.origin.x, newRefFaqButton.frame.origin.y - revertSize, newRefFaqButton.frame.size.width, newRefFaqButton.frame.size.height);
                                 
                                 UILabel *newFaqDotLabel = [_faqDotArray objectAtIndex:i];
                                 newFaqDotLabel.frame = CGRectMake(newFaqDotLabel.frame.origin.x, newRefFaqButton.frame.origin.y + newRefFaqButton.frame.size.height, newFaqDotLabel.frame.size.width, newFaqDotLabel.frame.size.height);
                                 
                                 UILabel *newRefFaqLabel = [_faqLabelArray objectAtIndex:i];
                                 newRefFaqLabel.frame = CGRectMake(newRefFaqLabel.frame.origin.x, newRefFaqButton.frame.origin.y + newRefFaqButton.frame.size.height, newRefFaqLabel.frame.size.width, newRefFaqLabel.frame.size.height);
                             }
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                
                             }
                         }];
    }else{
        [sender setSelected:YES];
        
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             UILabel *faqDotLabel = [_faqDotArray objectAtIndex:sender.tag];
                             faqDotLabel.frame = CGRectMake(faqDotLabel.frame.origin.x, faqDotLabel.frame.origin.y, faqDotLabel.frame.size.width, 40);
                             
                             UILabel *faqLabel = [_faqLabelArray objectAtIndex:sender.tag];
                             [faqLabel sizeToFit];
                             
                             for (int i = (int)sender.tag + 1; i < _faqButtonArray.count; i++) {
                                 
                                 cUIButton *newRefFaqButton = [_faqButtonArray objectAtIndex:i];
                                 newRefFaqButton.frame = CGRectMake(newRefFaqButton.frame.origin.x, newRefFaqButton.frame.origin.y + faqLabel.frame.size.height, newRefFaqButton.frame.size.width, newRefFaqButton.frame.size.height);
                                 
                                 UILabel *newFaqDotLabel = [_faqDotArray objectAtIndex:i];
                                 newFaqDotLabel.frame = CGRectMake(newFaqDotLabel.frame.origin.x, newRefFaqButton.frame.origin.y + newRefFaqButton.frame.size.height, newFaqDotLabel.frame.size.width, newFaqDotLabel.frame.size.height);
                                 
                                 UILabel *newRefFaqLabel = [_faqLabelArray objectAtIndex:i];
                                 newRefFaqLabel.frame = CGRectMake(newRefFaqLabel.frame.origin.x, newRefFaqButton.frame.origin.y + newRefFaqButton.frame.size.height, newRefFaqLabel.frame.size.width, newRefFaqLabel.frame.size.height);
                             }
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                                 if(sender.tag == (_faqButtonArray.count - 1)){
                                     
                                     CGPoint bottomOffset = CGPointMake(0, _faqScrollView.contentSize.height - _faqScrollView.bounds.size.height);
                                     [_faqScrollView setContentOffset:bottomOffset animated:YES];
                                 }
                             }
                         }];
    }
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         UILabel *faqLabel = [_faqLabelArray lastObject];
                         
                         _faqScrollView.contentSize = CGSizeMake(_faqScrollView.frame.size.width, faqLabel.frame.origin.y + faqLabel.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
}

-(NSArray*)faqQuestions{

    NSArray *array = @[
                       @"What’s the difference between Fixed and Custom sizes?",
                       @"How do I make sure my purchase is authentic?",
                       @"Any tips on selecting an art print?",
                       @"Is there a way to visualize art prints on my wall before I buy?",
                       @"I’d like to order an art print but in a size larger than the largest for sale. Is this possible?",
                       @"How can I pay?",
                       @"What do I do if I made a mistake with my personal/shipping information?",
                       @"How long will it take to receive my order?",
                       @"Is it possible to track my package?",
                       @"What should I do if my art print is damaged during shipping?",
                       @"What is the Artzibit return policy?"
                       ];
    
    return array;
}

-(NSArray*)faqAnswers{

    NSArray *array = @[
                       @"Fixed sizes are artworks that are original pieces which only come in the sizes that the artist made them available in.\n\nCustom sizes are artworks that can be printed in a variety of sizes.",
                       @"Each artwork comes with the artist’s signature.\n\nAs another measure of certifying authenticity, Arztibit sends a certificate with every artwork that we sell. Each certificate contains the artist’s signature, an identification number, and the edition number of the Limited, Exclusive, or Open edition series in which the item is sold.",
                       @"Find your ideal artwork by editing the filter settings on your profile. You can narrow down your selection to the style, price and size range that fits your need.",
                       @"Go the the artwork’s individual page and click the AR button right below the preview image.\n\nSelect whether you have the market or not and view the artwork using augmented reality on your wall! You can then scale selected artworks to find the perfect style and fit for your home.\n\nWe recommend downloading the Artzibit marker for the most precise measurements.",
                       @"If your desired artwork is available for custom sizes, you just have to key in your desired size in it’s product page or view it in AR and scale accordingly until it fits your needs.\n\nWe can produce prints up to the largest format size possible depending on the size of our artists’ HD files.",
                       @"Artzibit currently only allows payment through credit cards.",
                       @"You can easily edit your personal details and shipping information on your profile.",
                       @"From placing your order on the to your door, our delivery deadlines can vary from 5 to 15 business days. Please remember that we take the necessary time to carefully arrange delivery or print, and prepare all artworks for shipment. Delivery after shipment may take between 2-5 days.",
                       @"Yes. You’ll automatically be sent a tracking link via e-mail once your order is confirmed and when your package is picked up by our carrier.\n\nYou will also be able to view tracking information in-app. Just go to “My Orders” and click on a specific order number to see the progress of your shipment.",
                       @"Send us a message at contact@artzibit.net along with images of your damaged artwork. We’ll then work together to either replace or refund your artwork.",
                       @"We handle every return on a case-by-case basis. If you are not satisfied with your purchase for any reason, please contact us within 5 business days after you receive your artwork."
                       ];
    
    return array;
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
