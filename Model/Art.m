//
//  Art.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 14/6/17.
//
//

#import "Art.h"

@implementation Art

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _status =       [[_checking checkForNull:[dic valueForKey:@"status"]] boolValue];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            _artId = [_checking checkForNullArray:[dataDic valueForKey:@"id"]];
            _artName = [_checking checkForNullArray:[dataDic valueForKey:@"name"]];
            _artArtistName = [_checking checkForNullArray:[dataDic valueForKey:@"artist_name"]];
            _artImage = [_checking checkForNullArray:[dataDic valueForKey:@"image"]];
            _artArtworkStyleId = [_checking checkForNullArray:[dataDic valueForKey:@"artwork_style_id"]];
            _artSizeId = [_checking checkForNullArray:[dataDic valueForKey:@"size_id"]];
            _artPrice = [_checking checkForNullArray:[dataDic valueForKey:@"price"]];
            _artPriceId = [_checking checkForNullArray:[dataDic valueForKey:@"price_id"]];
            _artPricePerUnitId = [_checking checkForNullArray:[dataDic valueForKey:@"price_per_unit_id"]];
            _artFavoriteId = [_checking checkForNullArray:[dataDic valueForKey:@"favorite_id"]];
            _artFavorited = [_checking checkForNullArray:[dataDic valueForKey:@"favorited"]];
            _artCartId = [_checking checkForNullArray:[dataDic valueForKey:@"cart_id"]];
            _artCartItemId = [_checking checkForNullArray:[dataDic valueForKey:@"cart_item_id"]];
        }
    }
    
    return self;
}

@end
