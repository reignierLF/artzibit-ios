//
//  Favorites.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/6/17.
//
//

#import <Foundation/Foundation.h>
#import "Checking.h"

typedef enum {
    GET,
    POST,
    PUT
} HttpMethod;

@interface Put : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSString *favoriteId;
@property (nonatomic, readonly) NSString *favoriteUserId;
@property (nonatomic, readonly) NSString *favoriteArtworkId;
@property (nonatomic, readonly) BOOL favoriteIsDeleted;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Post : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSString *favoriteId;
@property (nonatomic, readonly) NSString *favoriteUserId;
@property (nonatomic, readonly) NSString *favoriteArtworkId;
@property (nonatomic, readonly) BOOL favoriteIsDeleted;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Get : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSArray *favoriteArtworkId;
@property (nonatomic, readonly) NSArray *favoriteId;
@property (nonatomic, readonly) NSArray *favoriteImage;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Favorites : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) Get *get;
@property (nonatomic, readonly) Post *post;
@property (nonatomic, readonly) Put *put;

-(instancetype)initWithDictionary:(NSDictionary*)dic method:(HttpMethod)method;

@end
