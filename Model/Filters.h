//
//  Filters.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import <Foundation/Foundation.h>
#import "Checking.h"

@interface Artists : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSArray *artistsId;
@property (nonatomic, readonly) NSArray *artistsName;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Categories : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSArray *categoryId;
@property (nonatomic, readonly) NSArray *categoryName;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Styles : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSArray *styleId;
@property (nonatomic, readonly) NSArray *styleName;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Filters : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) Artists *artists;
@property (nonatomic, readonly) Categories *categories;
@property (nonatomic, readonly) Styles *styles;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

-(void)saveCurrentFilteredSelected:(NSString*)filter index:(NSInteger)index;
-(NSInteger)loadCurrentFilteredSelected:(NSString*)filter;
@end
