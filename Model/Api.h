//
//  Api.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 14/6/17.
//
//

#import <Foundation/Foundation.h>

typedef void(^jsonResult)(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse);

@interface Api : NSObject

@property (nonatomic, readonly) NSString *url;

-(void)allArtsByCategory:(int)category
              isComplete:(jsonResult)check;

-(void)filtersIsComplete:(jsonResult)check;

-(void)artDetailsWithId:(NSString*)artId
             isComplete:(jsonResult)check;

-(void)userFavoritesIsComplete:(jsonResult)check;

-(void)createUserFavoriteArtworkWithId:(int)artworkId
                            isComplete:(jsonResult)check;

-(void)updateUserFavoriteArtworkWithFavoriteId:(int)favoriteId
                                       deleted:(BOOL)deleted
                                    isComplete:(jsonResult)check;

-(void)userCartIsComplete:(jsonResult)check;
@end
