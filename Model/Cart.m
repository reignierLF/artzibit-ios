//
//  Cart.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 9/7/17.
//
//

#import "Cart.h"

@implementation Cart

-(instancetype)initWithDictionary:(NSDictionary*)dic method:(HttpMethod)method{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _status =       [[_checking checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message = [_checking checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            switch(method) {
                case GET:
                    
                    _get = [[Get alloc] initWithDictionary:dataDic];
                    break;
                case POST:
                    
                    _post = [[Post alloc] initWithDictionary:dataDic];
                    break;
                case PUT:
                    
                    _put = [[Put alloc] initWithDictionary:dataDic];
                    break;
                default:
                    [NSException raise:NSGenericException format:@"Erro method"];
            }
        }
    }
    
    return self;
}

@end
