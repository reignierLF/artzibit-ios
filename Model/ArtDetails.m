//
//  ArtDetails.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import "ArtDetails.h"

@implementation ArtAvailableSizes

-(id)initWithDictionary:(NSDictionary *)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];

        _size = dic.count;
        
        if(_size != 0){
            
            _label = [_checking checkForNullArray:[dic valueForKey:@"label"]];
            _sizeId = [_checking checkForNullArray:[dic valueForKey:@"size_id"]];
            _width = [_checking checkForNullArray:[dic valueForKey:@"width"]];
            _height = [_checking checkForNullArray:[dic valueForKey:@"height"]];
            _priceId = [_checking checkForNullArray:[dic valueForKey:@"price_id"]];
            _price = [_checking checkForNullArray:[dic valueForKey:@"price"]];
            _widthAmountPerCm = [_checking checkForNullArray:[dic valueForKey:@"width_amount_per_cm"]];
            _heightAmountPerCm = [_checking checkForNullArray:[dic valueForKey:@"height_amount_per_cm"]];
        }else{
        
            _size = 3;
            
            _label = @[@"If you see this message", @"ask Aldrin to add some data", @"Thank you!"];
        }
    }
    
    return self;
}

@end

@implementation ArtDetails

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _status = [[_checking checkForNull:[dic valueForKey:@"status"]] boolValue];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
        
            _artId = [_checking checkForNull:[dataDic valueForKey:@"id"]];
            _artName = [_checking checkForNull:[dataDic valueForKey:@"name"]];
            _artArtistName = [_checking checkForNull:[dataDic valueForKey:@"artist_name"]];
            _artImage = [_checking checkForNull:[dataDic valueForKey:@"image"]];
            _artDescription = [_checking checkForNull:[dataDic valueForKey:@"description"]];
            _artSizeType = [_checking checkForNull:[dataDic valueForKey:@"size_type"]];
            _artSizeId = [_checking checkForNull:[dataDic valueForKey:@"size_id"]];
            _artFeaturedSize = [_checking checkForNull:[dataDic valueForKey:@"featured_size"]];
            _artWidth = [_checking checkForNull:[dataDic valueForKey:@"width"]];
            _artHeight = [_checking checkForNull:[dataDic valueForKey:@"height"]];
            _artPriceId = [_checking checkForNull:[dataDic valueForKey:@"price_id"]];
            _artFeaturedPrice = [_checking checkForNull:[dataDic valueForKey:@"featured_price"]];
            
            _artAvailableSizes = [[ArtAvailableSizes alloc] initWithDictionary:[_checking checkForNullDictionary:[dataDic valueForKey:@"available_sizes"]]];
            
            _artPricePerUnit = [_checking checkForNull:[dataDic valueForKey:@"price_per_unit"]];
            _artFavoriteId = [_checking checkForNull:[dataDic valueForKey:@"favorite_id"]];
            _artFavorited = [_checking checkForNull:[dataDic valueForKey:@"favorited"]];
            _artCartId = [_checking checkForNull:[dataDic valueForKey:@"cart_id"]];
            _artCartItemId = [_checking checkForNull:[dataDic valueForKey:@"cart_item_id"]];
        }
    }
    
    return self;
}

@end
