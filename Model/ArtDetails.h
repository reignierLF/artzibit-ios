//
//  ArtDetails.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import <Foundation/Foundation.h>
#import "Checking.h"

@interface ArtAvailableSizes : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) NSArray *label;
@property (nonatomic, readonly) NSArray *sizeId;
@property (nonatomic, readonly) NSArray *width;
@property (nonatomic, readonly) NSArray *height;
@property (nonatomic, readonly) NSArray *priceId;
@property (nonatomic, readonly) NSArray *price;
@property (nonatomic, readonly) NSArray *widthAmountPerCm;
@property (nonatomic, readonly) NSArray *heightAmountPerCm;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface ArtDetails : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) NSString *artId;
@property (nonatomic, readonly) NSString *artName;
@property (nonatomic, readonly) NSString *artArtistName;
@property (nonatomic, readonly) NSString *artImage;
@property (nonatomic, readonly) NSString *artDescription;
@property (nonatomic, readonly) NSString *artSizeType;
@property (nonatomic, readonly) NSString *artSizeId;
@property (nonatomic, readonly) NSString *artFeaturedSize;
@property (nonatomic, readonly) NSString *artWidth;
@property (nonatomic, readonly) NSString *artHeight;
@property (nonatomic, readonly) NSString *artPriceId;
@property (nonatomic, readonly) NSString *artFeaturedPrice;
@property (nonatomic, strong) ArtAvailableSizes *artAvailableSizes;
@property (nonatomic, readonly) NSString *artPricePerUnit;
@property (nonatomic, readonly) NSString *artFavoriteId;
@property (nonatomic, readonly) NSString *artFavorited;
@property (nonatomic, readonly) NSString *artCartId;
@property (nonatomic, readonly) NSString *artCartItemId;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
