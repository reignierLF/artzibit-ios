//
//  Checking.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import <Foundation/Foundation.h>

@interface Checking : NSObject

-(NSString*)checkForNull:(id)object;

-(NSArray*)checkForNullArray:(id)object;

-(NSDictionary*)checkForNullDictionary:(id)object;

@end
