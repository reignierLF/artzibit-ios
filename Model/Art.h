//
//  Art.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 14/6/17.
//
//

#import <Foundation/Foundation.h>
#import "Checking.h"

@interface Art : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) NSArray *artId;
@property (nonatomic, readonly) NSArray *artName;
@property (nonatomic, readonly) NSArray *artArtistName;
@property (nonatomic, readonly) NSArray *artImage;
@property (nonatomic, readonly) NSArray *artArtworkStyleId;
@property (nonatomic, readonly) NSArray *artSizeId;
@property (nonatomic, readonly) NSArray *artPrice;
@property (nonatomic, readonly) NSArray *artPriceId;
@property (nonatomic, readonly) NSArray *artPricePerUnitId;
@property (nonatomic, readonly) NSArray *artFavoriteId;
@property (nonatomic, readonly) NSArray *artFavorited;
@property (nonatomic, readonly) NSArray *artCartId;
@property (nonatomic, readonly) NSArray *artCartItemId;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end
