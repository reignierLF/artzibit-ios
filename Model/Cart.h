//
//  Cart.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 9/7/17.
//
//

#import <Foundation/Foundation.h>
#import "Checking.h"

typedef enum {
    GET,
    POST,
    PUT
} HttpMethod;

@interface Put : NSObject

@property (nonatomic, strong) Checking *checking;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Post : NSObject

@property (nonatomic, strong) Checking *checking;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Get : NSObject

@property (nonatomic, strong) Checking *checking;

-(instancetype)initWithDictionary:(NSDictionary*)dic;

@end

@interface Cart : NSObject

@property (nonatomic, strong) Checking *checking;

@property (nonatomic, readonly) BOOL status;
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) NSInteger size;

@property (nonatomic, readonly) Get *get;
@property (nonatomic, readonly) Post *post;
@property (nonatomic, readonly) Put *put;

-(instancetype)initWithDictionary:(NSDictionary*)dic method:(HttpMethod)method;

@end
