//
//  Favorites.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/6/17.
//
//

#import "Favorites.h"

@implementation Put

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        if(dic == nil){
            
            NSLog(@"Its either there are no data coming from server or you are using a wrong method");
        }
        
        _checking = [[Checking alloc] init];
        
        _favoriteId = [_checking checkForNull:[dic valueForKey:@"id"]];
        _favoriteUserId = [_checking checkForNull:[dic valueForKey:@"user_id"]];
        _favoriteArtworkId = [_checking checkForNull:[dic valueForKey:@"artwork_id"]];
        _favoriteIsDeleted = [_checking checkForNull:[dic valueForKey:@"deleted"]];
    }
    
    return self;
}

@end

@implementation Post

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        if(dic == nil){
            
            NSLog(@"Its either there are no data coming from server or you are using a wrong method");
        }
        
        _checking = [[Checking alloc] init];
        
        _favoriteId = [_checking checkForNull:[dic valueForKey:@"id"]];
        _favoriteUserId = [_checking checkForNull:[dic valueForKey:@"user_id"]];
        _favoriteArtworkId = [_checking checkForNull:[dic valueForKey:@"artwork_id"]];
        _favoriteIsDeleted = [_checking checkForNull:[dic valueForKey:@"deleted"]];
    }
    
    return self;
}

@end

@implementation Get

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        if(dic == nil){
        
            NSLog(@"Its either there are no data coming from server or you are using a wrong method");
        }
        
        _checking = [[Checking alloc] init];
        
        _favoriteArtworkId = [_checking checkForNullArray:[dic valueForKey:@"artwork_id"]];
        _favoriteId = [_checking checkForNullArray:[dic valueForKey:@"id"]];
        _favoriteImage = [_checking checkForNullArray:[dic valueForKey:@"image"]];
    }
    
    return self;
}

@end

@implementation Favorites

-(instancetype)initWithDictionary:(NSDictionary*)dic method:(HttpMethod)method{

    self = [super init];
    
    if(self){
    
        _checking = [[Checking alloc] init];
        
        _status =       [[_checking checkForNull:[dic valueForKey:@"status"]] boolValue];
        _message = [_checking checkForNull:[dic valueForKey:@"message"]];
        
        NSDictionary *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            switch(method) {
                case GET:
                    
                    _get = [[Get alloc] initWithDictionary:dataDic];
                    break;
                case POST:
                    
                    _post = [[Post alloc] initWithDictionary:dataDic];
                    break;
                case PUT:
                    
                    _put = [[Put alloc] initWithDictionary:dataDic];
                    break;
                default:
                    [NSException raise:NSGenericException format:@"Erro method"];
            }
        }
    }
    
    return self;
}

@end
