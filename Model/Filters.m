//
//  Filters.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import "Filters.h"

@implementation Artists

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];

        _artistsId = [_checking checkForNullArray:[dic valueForKey:@"id"]];
        _artistsName = [_checking checkForNullArray:[dic valueForKey:@"name"]];
    }
    
    return self;
}

@end

@implementation Categories

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _categoryId = [_checking checkForNullArray:[dic valueForKey:@"id"]];
        _categoryName = [_checking checkForNullArray:[dic valueForKey:@"name"]];
    }
    
    return self;
}

@end

@implementation Styles

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _styleId = [_checking checkForNullArray:[dic valueForKey:@"id"]];
        _styleName = [_checking checkForNullArray:[dic valueForKey:@"name"]];
    }
    
    return self;
}

@end

@implementation Filters

-(instancetype)initWithDictionary:(NSDictionary*)dic{
    
    self = [super init];
    
    if(self){
        
        _checking = [[Checking alloc] init];
        
        _status = [[_checking checkForNull:[dic valueForKey:@"status"]] boolValue];
        
        NSArray *dataDic = [dic valueForKey:@"data"];
        
        _size = dataDic.count;
        
        if(_size != 0){
            
            NSDictionary *artistsDic = [dataDic objectAtIndex:0];
            _artists = [[Artists alloc] initWithDictionary:[_checking checkForNullDictionary:[artistsDic valueForKey:@"values"]]];
            
            NSDictionary *categoriesDic = [dataDic objectAtIndex:1];
            _categories = [[Categories alloc] initWithDictionary:[_checking checkForNullDictionary:[categoriesDic valueForKey:@"values"]]];
            
            NSDictionary *styleDic = [dataDic objectAtIndex:2];
            _styles = [[Styles alloc] initWithDictionary:[_checking checkForNullDictionary:[styleDic valueForKey:@"values"]]];
        }
    }
    
    return self;
}

-(void)saveCurrentFilteredSelected:(NSString*)filter index:(NSInteger)index{

    [[NSUserDefaults standardUserDefaults] setInteger:index forKey:filter];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSInteger)loadCurrentFilteredSelected:(NSString*)filter{

    return [[NSUserDefaults standardUserDefaults] integerForKey:filter];
}
@end
