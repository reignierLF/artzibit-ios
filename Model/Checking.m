//
//  Checking.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 15/6/17.
//
//

#import "Checking.h"

@implementation Checking

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
    }
    
    return self;
}

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}

-(NSArray*)checkForNullArray:(id)object{
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [object count]; i++) {
        
        if([object objectAtIndex:i] == (id)[NSNull null] || object == nil){
            
            [array addObject:@""];
        }else{
            
            [array addObject:[object objectAtIndex:i]];
        }
    }
    
    return array;
}

-(NSDictionary*)checkForNullDictionary:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @[];
    }
    
    return object;
}
@end
