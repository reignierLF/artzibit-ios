//
//  Api.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 14/6/17.
//
//

#import "Api.h"

@implementation Api

-(instancetype)init{
    
    self = [super init];
    
    if(self){
        
        _url = @"https://artzibit-dev.herokuapp.com";
        //_url = @"http://192.168.1.2:3000/api/v1/";
    }
    
    return self;
}

-(void)allArtsByCategory:(int)category
              isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"b692b2b7-d2c3-a807-7c6a-eb08b7ebee69" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/artworks?size_option_id=%d",_url,category]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at home all arts : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at home all arts : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)filtersIsComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"d69869e4-9160-b634-0fd8-370d1789fb45" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/filters",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at filters : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at filters : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)artDetailsWithId:(NSString*)artId
             isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"160d1f81-aec2-f189-3fd8-8403d630599f" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/artworks/%@",_url,artId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at art details : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at filters : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)userFavoritesIsComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"cce2fc04-17d5-186c-d701-7c32c9ef5f66" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user_favorites",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at user favorite : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at favorite : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)createUserFavoriteArtworkWithId:(int)artworkId
                            isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"0680fae1-d5c4-48ce-1d66-060d3c647e53" };
    NSDictionary *parameters = @{ @"user_favorite": @{ @"artwork_id": [NSString stringWithFormat:@"%d",artworkId] } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user_favorites",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at create user favorite : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at filters : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)updateUserFavoriteArtworkWithFavoriteId:(int)favoriteId
                                       deleted:(BOOL)deleted
                                    isComplete:(jsonResult)check{

    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"8696d56f-f60f-2deb-27e4-2f7ed02d1750" };
    NSDictionary *parameters = @{ @"user_favorite": @{ @"deleted": @(deleted) } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/user_favorites/%d", _url, favoriteId]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"PUT"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at update user favorite : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at filters : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}

-(void)userCartIsComplete:(jsonResult)check{
    
    NSDictionary *headers = @{ @"x-user-email": @"gravides.dren@gmail.com",
                               @"x-user-token": @"2W6C81JJDCzBbNTZ5bQN",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"e6da9edf-a3d2-8b3b-95e2-4ba52a6c72c1" };
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v1/carts",_url]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        
                                                        if(data == nil){
                                                            
                                                            data = [NSData new];
                                                        }
                                                        
                                                        NSLog(@"Error at user cart : %@", error);
                                                        
                                                        check(NO, data, error, response, nil);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        check(YES, data, error, response, httpResponse);
                                                        
                                                        NSLog(@"Http response at cart : %@", httpResponse);
                                                    }
                                                }];
    [dataTask resume];
}
@end
