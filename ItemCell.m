//
//  ItemCell.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 18/5/17.
//
//

#import "ItemCell.h"
#import "ImageLoader.h"

@interface ItemCell ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) DropDownV2 *quantityButton;

@end

@implementation ItemCell

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    ImageLoader *il = [[ImageLoader alloc] init];
    
    UIImageView *artImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 20, _viewHeight - 40, _viewHeight - 40)];
    artImageView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    artImageView.contentMode = UIViewContentModeScaleAspectFill;
    //eventImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Img0%li",(long)self.tag + 1]];
    artImageView.clipsToBounds = YES;
    artImageView.layer.cornerRadius = artImageView.frame.size.width / 5;
    [artImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addSubview:artImageView];
    
    [il parseImage:artImageView url:_imageUrl errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    float titleArtWidth = _viewWidth - (artImageView.frame.origin.x + artImageView.frame.size.width + 20 + 40);
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(artImageView.frame.origin.x + artImageView.frame.size.width + 20, artImageView.frame.origin.y - 10, titleArtWidth, 40)];
    //priceLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.6];
    priceLabel.text = _price;
    priceLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [priceLabel sizeToFit];
    [self addSubview:priceLabel];
    
    UILabel *titleArtLabel = [[UILabel alloc] initWithFrame:CGRectMake(priceLabel.frame.origin.x, priceLabel.frame.origin.y + priceLabel.frame.size.height, priceLabel.frame.size.width, 30)];
    //titleArtLabel.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.6];
    titleArtLabel.text = _title;
    titleArtLabel.textColor = [UIColor grayColor];
    titleArtLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    //titleArtLabel.font = [UIFont systemFontOfSize:14];
    [titleArtLabel sizeToFit];
    [self addSubview:titleArtLabel];
    
    UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleArtLabel.frame.origin.x, titleArtLabel.frame.origin.y + titleArtLabel.frame.size.height + 10, titleArtWidth, titleArtLabel.frame.size.height)];
    //sizeLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.6];
    sizeLabel.text = _size;
    sizeLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    //sizeLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:sizeLabel];
    
    NSArray *quantityArray = @[@"1", @"2", @"3", @"4"];
    
    _quantityButton = [DropDownV2 buttonWithType:UIButtonTypeCustom];
    _quantityButton.frame = CGRectMake(sizeLabel.frame.origin.x, sizeLabel.frame.origin.y + sizeLabel.frame.size.height, (titleArtWidth / 2) + 20, sizeLabel.frame.size.height);
    _quantityButton.delegate = self;
    _quantityButton.titleArray = quantityArray;
    [_quantityButton setTitle:@"Quantity: 1" forState:UIControlStateNormal];
    [_quantityButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //_quantityButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:16];
    _quantityButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    _quantityButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _quantityButton.tag = 0;
    [self addSubview:_quantityButton];
    
    NSArray *installationArray = @[@"No", @"Yes"];
    
    DropDownV2 *installationButton = [DropDownV2 buttonWithType:UIButtonTypeCustom];
    installationButton.frame = CGRectMake(_quantityButton.frame.origin.x, _quantityButton.frame.origin.y + _quantityButton.frame.size.height + 5, (titleArtWidth / 2) + 20, sizeLabel.frame.size.height);
    installationButton.delegate = self;
    installationButton.titleArray = installationArray;
    [installationButton setTitle:@"Installation: No" forState:UIControlStateNormal];
    [installationButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    installationButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    installationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    installationButton.tag = 1;
    //installationButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:16];
    [self addSubview:installationButton];
    
    //installationButton.scrollView.frame = CGRectMake(installationButton.scrollView.frame.origin.x, installationButton.scrollView.frame.origin.y, 80, installationButton.scrollView.frame.size.height);
    
    UIButton *closebutton = [UIButton buttonWithType:UIButtonTypeCustom];
    closebutton.frame = CGRectMake(_viewWidth - 40, priceLabel.frame.origin.y, 40, 40);
    //closebutton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    [closebutton setTitle:@"X" forState:UIControlStateNormal];
    [closebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    closebutton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    //button.titleLabel.textColor = [UIColor blackColor];
    [self addSubview:closebutton];
    
    [closebutton addTarget:self action:@selector(removeEvent) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.tag != (_maxSize - 1)){
    
        UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(20, _viewHeight - 1, _viewWidth - 40, 1)];
        seperatorView.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
        [self addSubview:seperatorView];
    }
}

-(void)didSelectItem:(DropDownV2 *)dropDown index:(NSInteger)index{
    
    if(dropDown.tag == 0){
    
        [dropDown setTitle:[NSString stringWithFormat:@"Quantity: %@",[dropDown.titleArray objectAtIndex:index]] forState:UIControlStateNormal];
    }else if(dropDown.tag == 1){
    
        [dropDown setTitle:[NSString stringWithFormat:@"Installation: %@",[dropDown.titleArray objectAtIndex:index]] forState:UIControlStateNormal];
    }
}

-(void)removeEvent{
    
    /*
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         self.alpha = 0.0;
                         self.frame = CGRectMake(-self.frame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                             //[self removeFromSuperview];
                         }
                     }];
     */
    [_delegate removeItem:self];
}
@end
