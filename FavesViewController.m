//
//  FavesViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 30/5/17.
//
//

#import "FavesViewController.h"

@interface FavesViewController ()
@property (nonatomic, strong) ImageLoader *il;
@property (nonatomic, strong) UIView *mainNavigationBarView;

@end

@implementation FavesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self loadUserFavorites];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _api = [[Api alloc] init];
    _il = [[ImageLoader alloc] init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *resetButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    resetButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    resetButton.icon = [UIImage imageNamed:@"star"];
    //[_mainNavigationBarView addSubview:resetButton];
    
    [resetButton addTarget:self action:@selector(reset) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *mainButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    mainButton.frame = CGRectMake(_mainNavigationBarView.frame.size.width - width, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    mainButton.icon = [UIImage imageNamed:@"right-arrow"];
    [_mainNavigationBarView addSubview:mainButton];
    
    [mainButton addTarget:self action:@selector(navigateToMain) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Faves";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)loadUserFavorites{

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api userFavoritesIsComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"user favorites = %@", jsonDictionary);
            
            _favorites = [[Favorites alloc] initWithDictionary:jsonDictionary method:GET];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    if(_favorites.status){
                        
                        [self initFAQScrollView];
                        [self initFavesListV2];
                    }else{
                        
                        //NSLog(@"user favorites get false");
                        
                        UIAlertController *alertControllerFaves = [UIAlertController alertControllerWithTitle:@"" message:_favorites.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            
                        }];
                        
                        [alertControllerFaves addAction:close];
                        
                        [self presentViewController:alertControllerFaves animated:YES completion:nil];
                    }
                }else{
                    
                    /*
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    */
                    
                    [self reset];
                    
                    NSLog(@"failed to load data");
                }
            });
        }];
    });
}

-(void)reloadUserFavorites{

    [self loadUserFavorites];
}

-(void)initFAQScrollView{
    
    _favesScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, _screenHeight - _navigationBarHeight)];
    _favesScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    _favesScrollView.delegate = self;
    _favesScrollView.directionalLockEnabled = YES;
    _favesScrollView.contentSize = CGSizeMake(_favesScrollView.frame.size.width, _favorites.size * (_favesScrollView.frame.size.height / 5));
    [self.view addSubview:_favesScrollView];
}

-(void)initFavesListV2{

    __block int size = (int)_favorites.size;
    __block int row = 0;
    __block int type = 0;
    __block int previousType = 0;
    
    while (size > 0) {
        
        if(previousType == type){
            
            type = [self randomNumberBetween:0 maxNumber:4];
        }else{
            
            previousType = type;
        
            [self initRowBoxes:type row:row counter:size block:^(int currentCounter) {
                
                size = currentCounter;
                row++;
            }];
            
            NSLog(@"size = %d", size);
        }
    }
}

-(void)artThumbnailWithView:(UIView*)view index:(int)index{
    
    int counter = index - 1;
    
    if(counter <= 0){
    
        counter = 0;
    }

    UIImageView *artImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    //artImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    artImageView.contentMode = UIViewContentModeScaleAspectFill;
    //eventImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Img0%li",(long)self.tag + 1]];
    artImageView.clipsToBounds = YES;
    artImageView.layer.cornerRadius = view.layer.cornerRadius;
    [artImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [view addSubview:artImageView];
    
    [_il parseImage:artImageView url:[_favorites.get.favoriteImage objectAtIndex:counter] errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    button.frame = artImageView.frame;
    button.tag = counter;
    [button setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [view addSubview:button];
    
    [button addTarget:self action:@selector(didSelectFaveItem:) forControlEvents:UIControlEventTouchUpInside];
}

-(int)init2Boxes:(int)row counter:(int)counter{
    
    NSLog(@"2 boxes");
    
    int loop = 0;
    
    if(counter >= 2){
    
        loop = 2;
    }else if(counter == 1){
    
        loop = 1;
    }

    float width = _screenWidth / 2;
    float height = _screenHeight / 4;
    
    for (int i = 0; i < loop; i++) {
        
        UIView *artView = [[UIView alloc] initWithFrame:CGRectMake((width * i) + 5, (height * row) + 5, width - 10, height - 10)];
        //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
        //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
        //artView.backgroundColor = [UIColor orangeColor];
        //artView.layer.borderColor = [UIColor whiteColor].CGColor;
        //artView.layer.borderWidth = 5;
        artView.layer.cornerRadius = 10;
        [_favesScrollView addSubview:artView];
        
        [self artThumbnailWithView:artView index:counter];
        
        counter--;
    }
    
    return counter;
}

-(int)init3Boxes:(int)row counter:(int)counter{
    
    NSLog(@"3 boxes");
    
    int loop = 0;
    
    if(counter >= 3){
        
        loop = 3;
    }else if(counter == 2){
        
        loop = 2;
    }else if(counter == 1){
        
        loop = 1;
    }

    float width = _screenWidth / 3;
    float height = _screenHeight / 4;
    
    for (int i = 0; i < loop; i++) {
    
        UIView *artView = [[UIView alloc] initWithFrame:CGRectMake((width * i) + 5, (height * row) + 5, width - 10, height - 10)];
        //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
        //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
        //artView.backgroundColor = [UIColor orangeColor];
        //artView.layer.borderColor = [UIColor whiteColor].CGColor;
        //artView.layer.borderWidth = 5;
        artView.layer.cornerRadius = 10;
        [_favesScrollView addSubview:artView];
        
        [self artThumbnailWithView:artView index:counter];
        
        counter--;
    }
    
    return counter;
}

-(int)initLeftLongBox:(int)row counter:(int)counter{
    
    NSLog(@"left long boxes");

    float width = _screenWidth - (_screenWidth / 3);
    float height = _screenHeight / 4;
    
    UIView *longArtView = [[UIView alloc] initWithFrame:CGRectMake(5, (height * row) + 5, width - 10, height - 10)];
    //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
    //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
    //longArtView.backgroundColor = [UIColor orangeColor];
    //longArtView.layer.borderColor = [UIColor whiteColor].CGColor;
    //longArtView.layer.borderWidth = 5;
    longArtView.layer.cornerRadius = 10;
    [_favesScrollView addSubview:longArtView];
    
    [self artThumbnailWithView:longArtView index:counter];
    
    counter--;
    
    if(counter > 0){
    
        UIView *shortArtView = [[UIView alloc] initWithFrame:CGRectMake(width + 5, (height * row) + 5, (_screenWidth / 3) - 10, height - 10)];
        //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
        //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
        //shortArtView.backgroundColor = [UIColor orangeColor];
        //shortArtView.layer.borderColor = [UIColor whiteColor].CGColor;
        //shortArtView.layer.borderWidth = 5;
        shortArtView.layer.cornerRadius = 10;
        [_favesScrollView addSubview:shortArtView];
        
        [self artThumbnailWithView:shortArtView index:counter];
        
        counter--;
    }
    
    return counter;
}

-(int)initRightLongBox:(int)row counter:(int)counter{
    
    NSLog(@"right long boxes");

    float width = _screenWidth - (_screenWidth / 3);
    float height = _screenHeight / 4;
    
    UIView *shortArtView = [[UIView alloc] initWithFrame:CGRectMake(5, (height * row) + 5, (_screenWidth / 3) - 10, height - 10)];
    //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
    //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
    //shortArtView.backgroundColor = [UIColor orangeColor];
    //shortArtView.layer.borderColor = [UIColor whiteColor].CGColor;
    //shortArtView.layer.borderWidth = 5;
    shortArtView.layer.cornerRadius = 10;
    [_favesScrollView addSubview:shortArtView];
    
    [self artThumbnailWithView:shortArtView index:counter];
    
    counter--;
    
    if(counter > 0){
        
        UIView *longArtView = [[UIView alloc] initWithFrame:CGRectMake((_screenWidth / 3) + 5, (height * row) + 5, width - 10, height - 10)];
        //artView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:[self randomNumberBetween:1 maxNumber:9] * 0.1];
        //artView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
        //longArtView.backgroundColor = [UIColor orangeColor];
        //longArtView.layer.borderColor = [UIColor whiteColor].CGColor;
        //longArtView.layer.borderWidth = 5;
        longArtView.layer.cornerRadius = 10;
        [_favesScrollView addSubview:longArtView];
        
        [self artThumbnailWithView:longArtView index:counter];
        
        counter--;
    }
    
    return counter;
}

-(void)initRowBoxes:(int)type row:(int)row counter:(int)counter block:(void(^)(int currentCounter))handler{
    
    int newCounter = 0;

    if(type == 0){
    
        newCounter = [self init2Boxes:row counter:counter];
    }else if(type == 1){
    
        newCounter = [self init3Boxes:row counter:counter];
    }else if(type == 2){
    
        newCounter = [self initLeftLongBox:row counter:counter];
    }else if(type == 3){
    
        newCounter = [self initRightLongBox:row counter:counter];
    }
    
    handler(newCounter);
}

-(int)randomNumberBetween:(int)min maxNumber:(int)max{
    
    return min + arc4random() % (max - min);
}

-(void)didSelectFaveItem:(UIButton*)sender{
    
    NSLog(@"asdasdasdasd");

    ArtViewController *art = [[ArtViewController alloc] init];
    art.delegate = self;
    art.screenWidth = _screenWidth;
    art.screenHeight = _screenHeight;
    art.backgroundColor = _backgroundColor;
    art.artId = [_favorites.get.favoriteArtworkId objectAtIndex:sender.tag];
    //art.artName = [carousel.artName objectAtIndex:sender.tag];
    //art.artArtistName = [carousel.artArtistName objectAtIndex:sender.tag];
    art.artImage = [_favorites.get.favoriteImage objectAtIndex:sender.tag];
    //art.artPrice = [carousel.artPrice objectAtIndex:sender.tag];
    [_favesNavigationController pushViewController:art animated:YES];
    
    [_delegate disableCompassNavigationFromFaves:self];
}

-(void)reset{

    [_favesScrollView removeFromSuperview];
    _favesScrollView = nil;
    
    [self reloadUserFavorites];
}

-(void)navigateToMain{
    
    [_delegate goToMain];
}

-(void)backToHomePage:(ArtViewController *)art{

    [_delegate enableCompassNavigationFromFaves:self];
}

-(void)addToFaveEvent:(ArtViewController *)art{

}

-(void)showUnityAR:(ArtViewController *)art artImageUrl:(NSString *)artImageUrl selectedWidth:(NSString *)selectedWidth selectedHeight:(NSString *)selectedHeight selectedPrice:(NSString *)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm{

}
@end
