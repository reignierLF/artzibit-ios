//
//  DynamicLabel.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 24/5/17.
//
//

#import "DynamicLabel.h"

@implementation DynamicLabel

-(void)didMoveToSuperview{
    
    self.userInteractionEnabled = YES;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.text attributes:nil];
    
    for (int i = 0; i < _linkArray.count; i++) {
        
        NSRange rangeForStyle = [self.text rangeOfString:[NSString stringWithFormat:@"%@",[_linkArray objectAtIndex:i]]];
        
        if (rangeForStyle.location == NSNotFound) {
            
            NSLog(@"The string (testString) does not contain 'how are you doing' as a substring");
        } else {
            
            NSLog(@"Found the range of the substring at (%lu, %lu)", (unsigned long)rangeForStyle.location, rangeForStyle.location + rangeForStyle.length);
            //NSLog(@"range 2 (%lu, %lu)", range2.location, range2.location + range2.length);
        }
        
        NSRange linkRangeForStyle = rangeForStyle; // for the word "link" in the string above
        
        NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [UIColor redColor],
                                          NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                          NSFontAttributeName : self.font};
        
        [attributedString setAttributes:linkAttributes range:linkRangeForStyle];
        
        self.attributedText = attributedString;
    }
    
    NSArray *newLineArray = [self.text componentsSeparatedByString:@"\n"];
    
    NSLog(@"%@",newLineArray);
    
    for (int a = 0; a < newLineArray.count; a++) {
    
        NSMutableAttributedString *attributedStringForButton = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[newLineArray objectAtIndex:a]] attributes:nil];
        
        for (int i = 0; i < _linkArray.count; i++) {
            
            NSRange rangeForButton = [[NSString stringWithFormat:@"%@",[newLineArray objectAtIndex:a]] rangeOfString:[NSString stringWithFormat:@"%@",[_linkArray objectAtIndex:i]]];
            
            if (rangeForButton.location == NSNotFound) {
                
                NSLog(@"The string (testString) does not contain 'how are you doing' as a substring");
            } else {
                
                NSRange linkRangeButton = rangeForButton; // for the word "link" in the string above
                
                //float xpos = [self boundingRectForCharacterRange:linkRange attributeText:attributedString].origin.x + (((self.frame.size.width + self.frame.origin.x) / 2) - ([self boundingRectForCharacterRange:linkRange attributeText:attributedString].size.width / 2));
                float xpos = [self boundingRectForCharacterRange:linkRangeButton attributeText:attributedStringForButton].origin.x;
                float ypos = [self boundingRectForCharacterRange:linkRangeButton attributeText:attributedStringForButton].origin.y;
                float width = [self boundingRectForCharacterRange:linkRangeButton attributeText:attributedStringForButton].size.width + 100;
                float height = self.frame.size.height / newLineArray.count;
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.frame = CGRectMake(((((self.frame.size.width + xpos + width) / 2) - (([self widthOfString:[newLineArray objectAtIndex:a]] + xpos + width) / 2)) + (xpos / 2) + ([self widthOfString:[newLineArray objectAtIndex:a]] / 2)) - self.frame.origin.x, a * height, width, height);
                //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
                button.tag = i;
                [self addSubview:button];
                
                [button addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
                
                //UIView *testView = [[UIView alloc] initWithFrame:CGRectMake(((((self.frame.size.width + xpos + width) / 2) - (([self widthOfString:[newLineArray objectAtIndex:a]] + xpos + width) / 2)) + (xpos / 2) + ([self widthOfString:[newLineArray objectAtIndex:a]] / 2)) - self.frame.origin.x, a * height, width, height)];
                //testView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
                //[self addSubview:testView];
                
                NSLog(@"Found the range of the substring at (%lu, %lu)", (unsigned long)rangeForButton.location, rangeForButton.location + rangeForButton.length);
                //NSLog(@"range 2 (%lu, %lu)", range2.location, range2.location + range2.length);
            }
        }
    }
}

- (CGRect)boundingRectForCharacterRange:(NSRange)range attributeText:(NSMutableAttributedString*)attributeText{
    
    NSLog(@"%f",[[UIApplication sharedApplication] keyWindow].bounds.size.width);
    NSLog(@"%f",[[UIApplication sharedApplication] keyWindow].bounds.size.height);
    
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:attributeText];
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    [textStorage addLayoutManager:layoutManager];
    
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake([self widthOfString:[NSString stringWithFormat:@"%@",attributeText]], (self.bounds.size.height / 2))];
    textContainer.lineFragmentPadding = 0;
    textContainer.lineBreakMode = self.lineBreakMode;
    textContainer.maximumNumberOfLines = self.numberOfLines;
    [layoutManager addTextContainer:textContainer];
    
    NSRange glyphRange;
    
    // Convert the range for glyphs.
    [layoutManager characterRangeForGlyphRange:range actualGlyphRange:&glyphRange];
    
    return [layoutManager boundingRectForGlyphRange:glyphRange inTextContainer:textContainer];
}

- (CGFloat)widthOfString:(NSString *)string{
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:self.font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

-(void)buttonEvent:(UIButton*)sender{

    [_delegate didSelectAtIndex:sender.tag dynamicLabel:self];
    
    NSLog(@"button %ld",(long)sender.tag);
}
@end
