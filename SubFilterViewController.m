//
//  SubFilterViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 7/6/17.
//
//

#import "SubFilterViewController.h"

@interface SubFilterViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;
@property (nonatomic, strong) cUIScrollView *scrollView;

@end

@implementation SubFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initScrollView];
    [self initButtonList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = _backgroundColor;

    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    backButton.icon = [UIImage imageNamed:@"left-arrow"];
    [_mainNavigationBarView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *allButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    allButton.frame = CGRectMake(_mainNavigationBarView.frame.size.width - width, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //allButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    allButton.tag = _array.count;
    [_mainNavigationBarView addSubview:allButton];
    
    allButton.label.text = @"All";
    allButton.label.font = [UIFont fontWithName:@"Futura ICG" size:16];
    allButton.label.textColor = [UIColor whiteColor];
    
    [allButton addTarget:self action:@selector(all) forControlEvents:UIControlEventTouchUpInside];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    _titleLabel.text = self.title;
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [_mainNavigationBarView addSubview:_titleLabel];
}

-(void)initScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _navigationBarHeight, _screenWidth, _screenHeight - _navigationBarHeight)];
    //_scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, 80 * _array.count);
    //_scrollView.showsVerticalScrollIndicator = YES;
    //_scrollView.layer.cornerRadius = 10;
    [self.view addSubview:_scrollView];
}

-(void)initButtonList{
    
    //NSArray *array = @[@"Artists", @"Size", @"Price", @"Categories"];
    
    for (int i = 0; i < _array.count; i++) {
        
        _filterButton = [[FilterButton alloc] initWithFrame:CGRectMake(0, 80 * i, _screenWidth, 80)];
        //_filterButton.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.8];
        _filterButton.delegate = self;
        _filterButton.leftLabel.text = [NSString stringWithFormat:@"%@",[_array objectAtIndex:i]];
        _filterButton.leftLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
        _filterButton.leftLabel.frame = CGRectMake(_filterButton.leftLabel.frame.origin.x, _filterButton.leftLabel.frame.origin.y, _screenWidth, _filterButton.leftLabel.frame.size.height);
        _filterButton.rightLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
        //_filterButton.rightLabel.text = @"All";
        _filterButton.tag = i;
        _filterButton.check.hidden = YES;
        [_scrollView addSubview:_filterButton];
        
        if(i == _currentSelectedFilter){
        
            _filterButton.check.hidden = NO;
        }else if(_currentSelectedFilter == (_array.count + 1)){
        
            _filterButton.check.hidden = NO;
        }
        
        if(i == (_array.count - 1)){
            _filterButton.seperator.hidden = YES;
        }
    };
}

-(void)didClick:(FilterButton *)filterButton{
    
    NSLog(@"tag = %ld", (long)filterButton.tag);
    
    [_delegate didSelectThisOption:self index:(int)filterButton.tag];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)all{

    [_delegate didSelectThisOption:self index:(int)_array.count];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
