//
//  ArtFinderViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import "ArtFinderViewController.h"

@interface ArtFinderViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;

@end

@implementation ArtFinderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = _backgroundColor;
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *mainButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    mainButton.frame = CGRectMake(_mainNavigationBarView.frame.size.width - width, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //mainButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    mainButton.icon = [UIImage imageNamed:@"right-arrow"];
    [_mainNavigationBarView addSubview:mainButton];
    
    [mainButton addTarget:self action:@selector(navigateToMain) forControlEvents:UIControlEventTouchUpInside];
}

-(void)navigateToMain{

    [_delegate goToMain];
}
@end
