//
//  RegistrationViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 24/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUITextField.h"
#import "DynamicLabel.h"

#import "AccountViewController.h"
#import "ToSViewController.h"
#import "FAQViewController.h"

@class RegistrationViewController;

@protocol RegistrationDelegate
-(void)didFinishSignUp:(RegistrationViewController*)registration;
@end

@interface RegistrationViewController : UIViewController <DynamicLabelDelegate>

@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) AccountViewController *accountVC;

@property (nonatomic, weak) id <RegistrationDelegate> delegate;

@end
