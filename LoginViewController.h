//
//  LoginViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/5/17.
//
//

#import <UIKit/UIKit.h>
#import "RegistrationViewController.h"
#import "cUIButton.h"
#import "cUIScrollView.h"
#import "cUITextField.h"

#import "AccountViewController.h"

@class LoginViewController;

@protocol LoginDelegate
-(void)goToMainFromLogin:(LoginViewController*)login;
@end

@interface LoginViewController : UIViewController <UIScrollViewDelegate, RegistrationDelegate, AccountDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UINavigationController *accountNavigationController;
@property (nonatomic, strong) AccountViewController *accountVC;
@property (nonatomic, strong) RegistrationViewController *registrationVC;

@property (nonatomic, weak) id <LoginDelegate> delegate;

@end
