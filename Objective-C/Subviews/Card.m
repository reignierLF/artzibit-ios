//
//  Card.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 20/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Card.h"
#import "ImageLoader.h"

@interface Card ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@end

@implementation Card

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
    }
    
    return self;
}


-(void)didMoveToSuperview{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    UIImageView *artImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    //artImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    artImageView.contentMode = UIViewContentModeScaleAspectFill;
    //eventImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Img0%li",(long)self.tag + 1]];
    artImageView.clipsToBounds = YES;
    artImageView.layer.cornerRadius = self.layer.cornerRadius;
    [artImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addSubview:artImageView];
    
    [il parseImage:artImageView url:_artImage errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    _detailView = [[UIView alloc] initWithFrame:CGRectMake(0, _viewHeight + 20, _viewWidth, (_viewWidth / 2) - 40)];
    //_detailView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1.0];
    [self addSubview:_detailView];
    
    UILabel *artTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, _detailView.frame.size.width - 40, _detailView.frame.size.height / 2)];
    //artTitleLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //artTitleLabel.text = @"Art Title";
    artTitleLabel.text = _artName;
    artTitleLabel.textAlignment = NSTextAlignmentCenter;
    artTitleLabel.numberOfLines = 0;
    artTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    artTitleLabel.font = [UIFont fontWithName:@"Futura ICG" size:20];
    [artTitleLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:artTitleLabel];
    
    float height = artTitleLabel.frame.size.height / 2;
    
    UILabel *artistNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(artTitleLabel.frame.origin.x, artTitleLabel.frame.size.height, artTitleLabel.frame.size.width, height)];
    //artistNameLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //artistNameLabel.text = @"Artist Name";
    artistNameLabel.text = _artArtisName;
    artistNameLabel.textAlignment = NSTextAlignmentCenter;
    artistNameLabel.textColor = [UIColor grayColor];
    artistNameLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [artistNameLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_detailView addSubview:artistNameLabel];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(artistNameLabel.frame.origin.x, artistNameLabel.frame.origin.y + artistNameLabel.frame.size.height, artistNameLabel.frame.size.width, artistNameLabel.frame.size.height)];
    //priceLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    //priceLabel.text = @"$9999.99";
    priceLabel.text = [NSString stringWithFormat:@"$%@",_artPrice];
    priceLabel.textAlignment = NSTextAlignmentCenter;
    priceLabel.font = [UIFont fontWithName:@"Sk-Modernist" size:10];
    [priceLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    //[_detailView addSubview:priceLabel];
    
    UILabel *cardNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 40, 20)];
    //cardNumberLabel.backgroundColor = [UIColor redColor];
    cardNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)self.tag];
    cardNumberLabel.textAlignment = NSTextAlignmentCenter;
    cardNumberLabel.textColor = [UIColor whiteColor];
    cardNumberLabel.font = [UIFont fontWithName:@"Sk-Modernist" size:16];
    [self addSubview:cardNumberLabel];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    //button.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    button.frame = CGRectMake(0, 0, _viewWidth, _viewHeight);
    button.tag = self.tag;
    [button setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [self addSubview:button];
    
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)buttonPressed:(UIButton*)sender{

    [_delegate didClickCard:self sender:sender];
}

@end
