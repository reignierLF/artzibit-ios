//
//  Carousel.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Carousel.h"
#import "cUIScrollView.h"
#import "Card.h"

@interface Carousel () <UIScrollViewDelegate, CardDelegate>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;
@property (nonatomic) NSInteger carouselSize;
@property (nonatomic) NSInteger carouselCounter;

@property (nonatomic, strong) cUIScrollView *carouselScrollView;

@property (nonatomic, strong) NSMutableArray *cardsArray;

@property (nonatomic) NSInteger previousIndex;
@property (nonatomic) NSInteger currentIndex;
@property (nonatomic) NSInteger nextIndex;

@property (nonatomic) float offsetFactor;

@property (nonatomic) float originalWidth;
@property (nonatomic) float originalHeight;

@property (nonatomic) UIView *carouselPageIndicator;
@property (nonatomic) UIView *tempLeftIndicator;
@property (nonatomic) UIView *tempRightIndicator;
@property (nonatomic) float carouselCurrentPos;

@end

@implementation Carousel

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if(self){
    
        _viewWidth = frame.size.width;
        _viewHeight = frame.size.height;
        
        _threshold = 50;
        _shadowIntensity = 0.3;
        _carouselIndex = 0;
        
        _cardsArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(void)didMoveToSuperview{

    if(_size != 0){
        
        [self initCarousel];
        //[self initCarouselIndicator];
    }else{
    
        NSLog(@"no data found in dictionary or array");
    }
}

-(void)initCarousel{
    
    //_carouselSize = 5;
    _carouselSize = _size;
    
    int carouselLooper = 0;
    
    if(_carouselSize <= 1){
    
        carouselLooper = 0;
    }else{
        
        carouselLooper = 4;
    }
    
    _originalWidth = _viewWidth - (_viewWidth / 5);
    _originalHeight = _originalWidth;
    
    _carouselScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(((_viewWidth / 2) - (_originalWidth / 2)) - 10, 0, _originalWidth + 20, _originalHeight)];
    //_carouselScrollView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.2];
    _carouselScrollView.delegate = self;
    _carouselScrollView.bounces = NO;
    _carouselScrollView.pagingEnabled = YES;
    _carouselScrollView.contentSize = CGSizeMake(_carouselScrollView.frame.size.width  * (_carouselSize + carouselLooper), _carouselScrollView.frame.size.height);
    _carouselScrollView.showsHorizontalScrollIndicator = NO;
    _carouselScrollView.clipsToBounds = NO;
    [self addSubview:_carouselScrollView];
    
    [_carouselScrollView scrollRectToVisible:CGRectMake(_carouselScrollView.frame.size.width * (carouselLooper / 2), _carouselScrollView.frame.origin.y, _carouselScrollView.frame.size.width, _carouselScrollView.frame.size.height) animated:NO];
    
    UIView *shadowLeftPanelView = [[UIView alloc] initWithFrame:CGRectMake(0, _carouselScrollView.frame.origin.y + (_threshold / 2), _viewWidth / 4, _carouselScrollView.frame.size.height - _threshold)];
    shadowLeftPanelView.userInteractionEnabled = NO;
    
    CAGradientLayer *leftShadowGradient = [CAGradientLayer layer];
    leftShadowGradient.frame = shadowLeftPanelView.bounds;
    leftShadowGradient.colors = [NSArray arrayWithObjects:(id)[[[UIColor blackColor] colorWithAlphaComponent:_shadowIntensity] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    leftShadowGradient.startPoint = CGPointMake(0.0, 0.5);
    leftShadowGradient.endPoint = CGPointMake(1.0, 0.5);
    [shadowLeftPanelView.layer insertSublayer:leftShadowGradient atIndex:0];
    
    //[self addSubview:shadowLeftPanelView];
    
    UIView *shadowRightView = [[UIView alloc] initWithFrame:CGRectMake(_viewWidth - shadowLeftPanelView.frame.size.width, _carouselScrollView.frame.origin.y, shadowLeftPanelView.frame.size.width, shadowLeftPanelView.frame.size.height)];
    shadowRightView.userInteractionEnabled = NO;
    
    CAGradientLayer *rightShadowGradient = [CAGradientLayer layer];
    rightShadowGradient.frame = shadowRightView.bounds;
    rightShadowGradient.colors = [NSArray arrayWithObjects:(id)[[[UIColor blackColor] colorWithAlphaComponent:_shadowIntensity] CGColor], (id)[[UIColor clearColor] CGColor], nil];
    rightShadowGradient.startPoint = CGPointMake(1.0, 0.5);
    rightShadowGradient.endPoint = CGPointMake(0.0, 0.5);
    [shadowRightView.layer insertSublayer:rightShadowGradient atIndex:0];
    
    //[self addSubview:shadowRightView];
    
    NSInteger infiniteLoop = 0;
    
    float cardHeight = _originalHeight;
    float cardYPos = 0;
    float cardXPos = 10;
    
    _carouselCounter = infiniteLoop;
    
    for (int i = 0; i < _carouselSize + carouselLooper; i++) {
        
        if(_carouselSize > 1){
            
            if(i == 0){
                
                infiniteLoop = _carouselSize - 2;
                
                NSLog(@"1st object in index = %ld", (long)infiniteLoop);
            }else if(i == 1){
                
                infiniteLoop = _carouselSize - 1;
                
                NSLog(@"2nd object in index = %ld", (long)infiniteLoop);
            }else if(i == _carouselSize + 2){
                
                infiniteLoop = 0;
                
                NSLog(@"2nd to the last object = %ld", (long)infiniteLoop);
            }else if(i == _carouselSize + 3){
                
                infiniteLoop = 1;
                
                NSLog(@"last object = %ld", (long)infiniteLoop);
            }else{
                
                infiniteLoop = i - 2;
                
                NSLog(@"original object in order = %li", (long)infiniteLoop);
            }
            
            if(i != 2){
                
                cardHeight = _originalHeight - _threshold;
                cardYPos = _threshold / 2;
            }else{
                
                cardHeight = _originalHeight;
                cardYPos = 0;
            }
            
            cardXPos = ((_originalWidth + 10) * i) + ((i * 10) + 10);
        }
        
        //NSLog(@"infinite %ld",(long)infiniteLoop);
        
        Card *card =                [[Card alloc] initWithFrame:CGRectMake(cardXPos, cardYPos, _originalWidth , cardHeight)];
        card.backgroundColor =      [UIColor colorWithRed:254.0f/255.0f green:104.0f/255.0f blue:129.0f/255.0f alpha:1.0];
        card.delegate =             self;
        card.layer.cornerRadius =   5;
        card.layer.shadowColor =    [UIColor blackColor].CGColor;
        card.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
        card.layer.shadowOpacity =  0.35;
        card.layer.shadowRadius =   5.0;
        card.alpha =                0.0;
        card.tag =                  infiniteLoop;
        card.artId =                [self checkForNull:[_artId objectAtIndex:infiniteLoop]];
        card.artName =              [self checkForNull:[_artName objectAtIndex:infiniteLoop]];
        card.artArtisName =         [self checkForNull:[_artArtistName objectAtIndex:infiniteLoop]];
        card.artImage =             [self checkForNull:[_artImage objectAtIndex:infiniteLoop]];
        card.artPrice =             [self checkForNull:[_artPrice objectAtIndex:infiniteLoop]];
        //card.eventId =              [self checkForNull:[_allEventId objectAtIndex:infiniteLoop]];
        //card.imageUrl =             [self checkForNull:[_allEventImageUrl objectAtIndex:infiniteLoop]];
        //card.location =             [self checkForNull:[_allAddress objectAtIndex:infiniteLoop]];
        //card.date =                 [self checkForNull:[_allDateTime objectAtIndex:infiniteLoop]];
        [_carouselScrollView addSubview:card];
        
        if(i <= 6){
            
            [UIView animateWithDuration: 0.5
                                  delay: (i + 1) * 0.15
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations: ^{
                                 
                                 card.alpha = 1.0;
                             }completion:nil];
        }else{
        
            card.alpha = 1.0;
        }
        
        [_cardsArray addObject:card];
        
        //NSLog(@"all tags : %li",(long)_carouselCounter);
    }
    
    NSLog(@"Total of all cards : %lu", (unsigned long)_cardsArray.count);
    
    _previousIndex = _carouselCounter + 1;
    _currentIndex = _carouselCounter + 2;
    _nextIndex = _carouselCounter + 3;
    
    //NSLog(@"previous > onStart: %ld",(long)_previousIndex);
    //NSLog(@"current > onStart: %ld",(long)_currentIndex);
    //NSLog(@"next > onStart: %ld", (long)_nextIndex);
}

-(void)initCarouselIndicator{

    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(_carouselScrollView.frame.origin.x,_carouselScrollView.frame.origin.y + _carouselScrollView.frame.size.height, _carouselScrollView.frame.size.width, _viewHeight - (_carouselScrollView.frame.origin.y + _carouselScrollView.frame.size.height))];
    //containerView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    [self addSubview:containerView];
    
    UIView *lineIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(0, containerView.frame.size.height - (containerView.frame.size.height / 3), containerView.frame.size.width, 3)];
    lineIndicatorView.backgroundColor = [UIColor darkGrayColor];
    lineIndicatorView.layer.cornerRadius = 3;
    lineIndicatorView.clipsToBounds = YES;
    [containerView addSubview:lineIndicatorView];
    
    _carouselPageIndicator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, lineIndicatorView.frame.size.width / _carouselSize, lineIndicatorView.frame.size.height)];
    _carouselPageIndicator.backgroundColor = [UIColor orangeColor];
    [lineIndicatorView addSubview:_carouselPageIndicator];
    
    _carouselCurrentPos = _carouselPageIndicator.frame.origin.x;
    
    _tempLeftIndicator = [[UIView alloc] initWithFrame:CGRectMake(-_carouselPageIndicator.frame.size.width, _carouselPageIndicator.frame.origin.y, _carouselPageIndicator.frame.size.width, _carouselPageIndicator.frame.size.height)];
    _tempLeftIndicator.backgroundColor = _carouselPageIndicator.backgroundColor;
    [lineIndicatorView addSubview:_tempLeftIndicator];
    
    _tempRightIndicator = [[UIView alloc] initWithFrame:CGRectMake(_tempLeftIndicator.frame.size.width * _carouselSize, _tempLeftIndicator.frame.origin.y, _tempLeftIndicator.frame.size.width, _tempLeftIndicator.frame.size.height)];
    _tempRightIndicator.backgroundColor = _tempLeftIndicator.backgroundColor;
    [lineIndicatorView addSubview:_tempRightIndicator];
}

#pragma mark Events

-(NSString*)checkForNull:(id)object{
    
    if(object == (id)[NSNull null] || object == nil){
        
        object = @"";
    }
    
    return object;
}

#pragma mark Delegates

-(void)didClickCard:(Card*)card sender:(UIButton*)sender{
    
    [_delegate didChooseEventCard:self sender:sender];
}

-(void)scrollViewDidEndDecelerating:(cUIScrollView *)scrollView{
    
    NSLog(@"current index stop: %ld", (long)_currentIndex - 2);
    
    _carouselIndex = _currentIndex - 2;
}

-(void)scrollViewDidScroll:(cUIScrollView *)scrollView{
    
    _offsetFactor = scrollView.contentOffset.x - (scrollView.frame.size.width * _currentIndex);
    
    //NSLog(@"_offsetFactor %f",_offsetFactor);
    
    float offsetPercentage = fabs((100 * _offsetFactor) / scrollView.frame.size.width);
    float indicatorOffsetPercentage = ((100 * _offsetFactor) / scrollView.frame.size.width);
    
    NSLog(@"%f",offsetPercentage);
    NSLog(@"%f",indicatorOffsetPercentage);
    
    for (int i = 0; i < _cardsArray.count; i++) {
        
        Card *card = [_cardsArray objectAtIndex:i];
        
        card.frame = CGRectMake(card.frame.origin.x, _threshold / 2, _originalWidth, _originalHeight - _threshold);
            
        if(i == _previousIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y - fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, card.frame.size.height + fabs((offsetPercentage / 100) * _threshold));
        }
        
        if(i == _currentIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, _originalHeight - fabs((offsetPercentage / 100) * _threshold));
        }
        
        if(i == _nextIndex){
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y - fabs((offsetPercentage / 100) * (_threshold / 2)), card.frame.size.width, card.frame.size.height + fabs((offsetPercentage / 100) * _threshold));
        }
        
        card.detailView.frame = CGRectMake(card.detailView.frame.origin.x, card.frame.size.height + 20, card.frame.size.width, card.detailView.frame.size.height);
    }
    
    _carouselCounter = (int)(scrollView.contentOffset.x) / (int)(scrollView.frame.size.width);
    
    NSLog(@"_carouselCounter = %ld",(long)_carouselCounter);
    
    NSLog(@">>>%f",_carouselPageIndicator.frame.size.width);
    NSLog(@">>>%f",_carouselPageIndicator.frame.origin.x);
    NSLog(@"current index card : %li",(long)_currentIndex - 2);
    
    NSLog(@"ci %f", (_carouselPageIndicator.frame.size.width * (_currentIndex - 2)) + (indicatorOffsetPercentage / 100));
    
    _carouselPageIndicator.frame = CGRectMake((_carouselPageIndicator.frame.size.width * (_currentIndex - 2)) + (indicatorOffsetPercentage / 100), _carouselPageIndicator.frame.origin.y, _carouselPageIndicator.frame.size.width, _carouselPageIndicator.frame.size.height);
    
    _offsetFactor = 0;
    
    NSLog(@"%i == %i",(int)(scrollView.contentOffset.x), (int)(scrollView.frame.size.width));
    
    if((int)(_carouselScrollView.contentOffset.x) >= (int)(_carouselScrollView.contentSize.width - (_carouselScrollView.frame.size.width * 2))){
        
        NSLog(@"back to beginning");
        
        _previousIndex = 1;
        _currentIndex = 2;
        _nextIndex = 3;
        
        [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.size.width * 2, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
        
        [self resetSizesOfCards];
    }else if((int)(scrollView.contentOffset.x) <= (int)(scrollView.frame.size.width)){
        
        NSLog(@"go to end");
        
        _previousIndex = _cardsArray.count - 4;
        _currentIndex = _cardsArray.count - 3;
        _nextIndex = _cardsArray.count - 2;
        
        [scrollView scrollRectToVisible:CGRectMake(scrollView.frame.size.width * (_carouselSize + 1), scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
        
        [self resetSizesOfCards];
    }else{
        
        if((_carouselCounter - 1) < 0){
            
            _previousIndex = _cardsArray.count - 4;
        }else{
            
            _previousIndex = _carouselCounter - 1;
        }
        
        _currentIndex = _carouselCounter;
        
        if((_carouselCounter + 2) == _cardsArray.count){
            
            _nextIndex = 2;
        }else{
            
            _nextIndex = _carouselCounter + 1;
        }
        
        NSLog(@"normal scrolling");
        
        NSLog(@"current index scrolling: %ld", (long)_currentIndex);
    }
}

-(void)resetSizesOfCards{

    for (int i = 0; i < _cardsArray.count; i++) {
        
        Card *card = [_cardsArray objectAtIndex:i];
        
        card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, _originalWidth, _originalHeight - _threshold);

        if(i == _currentIndex){
            
            card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, _originalHeight);
        }
        
        card.detailView.frame = CGRectMake(card.detailView.frame.origin.x, card.frame.size.height + 20, card.frame.size.width, card.detailView.frame.size.height);
    }
    
    NSLog(@"all cards resize to default");
}
@end
