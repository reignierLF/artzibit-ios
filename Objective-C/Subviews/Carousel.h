//
//  Carousel.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 19/12/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Carousel;

@protocol CarouselDelegate

-(void)didChooseEventCard:(Carousel*)carousel sender:(UIButton*)sender;

@end

@interface Carousel : UIView

@property (nonatomic) float threshold;
@property (nonatomic) float shadowIntensity;

@property (nonatomic) NSInteger size;
@property (nonatomic) NSInteger carouselIndex;

@property (nonatomic, strong) NSArray *artId;
@property (nonatomic, strong) NSArray *artName;
@property (nonatomic, strong) NSArray *artArtistName;
@property (nonatomic, strong) NSArray *artImage;
@property (nonatomic, strong) NSArray *artArtworkStyleId;
@property (nonatomic, strong) NSArray *artSizeId;
@property (nonatomic, strong) NSArray *artPrice;
@property (nonatomic, strong) NSArray *artPriceId;
@property (nonatomic, strong) NSArray *artPricePerUnitId;
@property (nonatomic, strong) NSArray *artFavoriteId;
@property (nonatomic, strong) NSArray *artFavorited;
@property (nonatomic, strong) NSArray *artCartId;
@property (nonatomic, strong) NSArray *artCartItemId;

@property (nonatomic, weak) id <CarouselDelegate> delegate;

@end
