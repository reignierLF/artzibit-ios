//
//  PromoCode.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 9/7/17.
//
//

#import "PromoCode.h"

@interface PromoCode ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIView *backgroundView;

@end

@implementation PromoCode

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        self.backgroundColor = [UIColor whiteColor];
        
        _window = [UIApplication sharedApplication].keyWindow;
        
        _viewWidth = _window.frame.size.width;
        _viewHeight = _window.frame.size.height;
    }
    
    return self;
}

-(void)didMoveToSuperview{

    
}
@end
