//
//  ArtViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 21/6/17.
//
//

#import "ArtViewController.h"

@interface ArtViewController ()

@property (nonatomic, strong) cUIScrollView *scrollView;

@property(nonatomic, strong) UIImageView *artBlurImageView;
@property(nonatomic, strong) UIImageView *artImageView;

@property (nonatomic, strong) UIView *buttonvView;
@property (nonatomic, strong) UIView *detailView;
@property (nonatomic, strong) UILabel *quantityCounterLabel;

@property (nonatomic) BOOL isInstallation;

@property (nonatomic, strong) NSString *artImageUrl;
@property (nonatomic, strong) NSString *selectedWidth;
@property (nonatomic, strong) NSString *selectedHeight;
@property (nonatomic, strong) NSString *selectedPrice;
@property (nonatomic, strong) NSString *selectedWidthAmountPerCm;
@property (nonatomic, strong) NSString *selectedHeightAmountPerCm;

@end

@implementation ArtViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initScrollView];
    [self initArtImage];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api artDetailsWithId:_artId isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"art details = %@", jsonDictionary);
            
            _artDetails = [[ArtDetails alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    [self initButtons];
                    [self initDetails];
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    NSLog(@"failed to load data");
                }
            });
        }];
    });
    
    [self initBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _api = [[Api alloc] init];
    
    _isInstallation = NO;
}

-(void)initScrollView{
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _scrollView.backgroundColor = _backgroundColor;
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _scrollView.frame.size.height * 2);
    //_scrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_scrollView];
}

-(void)initArtImage{
    
    ImageLoader *il = [[ImageLoader alloc] init];
    
    _artBlurImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight / 2)];
    //_artBlurImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    _artBlurImageView.contentMode = UIViewContentModeScaleAspectFill;
    //_artBlurImageView.image = [UIImage imageNamed:@"checker"];
    _artBlurImageView.clipsToBounds = YES;
    [_scrollView addSubview:_artBlurImageView];
    
    [il parseImage:_artBlurImageView url:_artImage errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = _artBlurImageView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [_scrollView addSubview:blurEffectView];
    } else {
        _artBlurImageView.backgroundColor = [UIColor blackColor];
    }
    
    float width = _artBlurImageView.frame.size.width - ((_artBlurImageView.frame.size.width / 10) * 2);
    float height = _artBlurImageView.frame.size.height - ((_artBlurImageView.frame.size.height / 10) * 2);
    
    _artImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_artBlurImageView.frame.size.width / 10, _artBlurImageView.frame.size.height / 10, width, height)];
    //_artBlurImageView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.1];
    _artImageView.contentMode = UIViewContentModeScaleAspectFill;
    //_artImageView.image = [UIImage imageNamed:@"checker"];
    _artImageView.clipsToBounds = YES;
    _artImageView.layer.shadowColor =    [UIColor blackColor].CGColor;
    _artImageView.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    _artImageView.layer.shadowOpacity =  0.2;
    _artImageView.layer.shadowRadius =   5.0;
    [_scrollView addSubview:_artImageView];
    
    [il parseImage:_artImageView url:_artImage errorImageName:@"ConnectionLost" style:UIActivityIndicatorViewStyleWhiteLarge];
}

-(void)initButtons{
    
    _buttonvView = [[UIView alloc] initWithFrame:CGRectMake(0, _artBlurImageView.frame.origin.y + _artBlurImageView.frame.size.height, _screenWidth, 100)];
    //view.backgroundColor = [UIColor greenColor];
    [_scrollView addSubview:_buttonvView];
    
    float height = _buttonvView.frame.size.height - (_buttonvView.frame.size.height / 4);
    float columnWidth = _buttonvView.frame.size.width / 3;
    int divider = 8;
    
    cUIButton *faveButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    faveButton.frame = CGRectMake(((columnWidth / 2) - (height / 2)) + (columnWidth / divider), ((_buttonvView.frame.size.height + 50) / 2) - (height / 2), height, height);
    faveButton.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:206.0f/255.0f blue:85.0f/255.0f alpha:1.0];
    faveButton.layer.cornerRadius = faveButton.frame.size.width / 2;
    faveButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    faveButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    faveButton.layer.shadowOpacity =  0.2;
    faveButton.layer.shadowRadius =   5.0;
    faveButton.icon = [UIImage imageNamed:@"star"];
    [_buttonvView addSubview:faveButton];
    
    [faveButton addTarget:self action:@selector(addToFaveEvent) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *arButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    arButton.backgroundColor = [UIColor redColor];
    arButton.frame = CGRectMake((faveButton.frame.origin.x + columnWidth) - (columnWidth / divider), faveButton.frame.origin.y, faveButton.frame.size.width, faveButton.frame.size.height);
    arButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    arButton.layer.cornerRadius = arButton.frame.size.width / 2;
    arButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    arButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    arButton.layer.shadowOpacity =  0.2;
    arButton.layer.shadowRadius =   5.0;
    [_buttonvView addSubview:arButton];
    
    arButton.label.text = @"AR";
    arButton.label.textColor = [UIColor whiteColor];
    arButton.label.font = [UIFont systemFontOfSize:26];
    
    [arButton addTarget:self action:@selector(showUnityAR) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *addToCartButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    addToCartButton.frame = CGRectMake((faveButton.frame.origin.x + (columnWidth * 2)) - ((columnWidth / divider) * 2), arButton.frame.origin.y, arButton.frame.size.width, arButton.frame.size.height);
    addToCartButton.backgroundColor = [UIColor colorWithRed:80.0f/255.0f green:193.0f/255.0f blue:233.0f/255.0f alpha:1.0];
    addToCartButton.layer.cornerRadius = addToCartButton.frame.size.width / 2;
    addToCartButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    addToCartButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    addToCartButton.layer.shadowOpacity =  0.2;
    addToCartButton.layer.shadowRadius =   5.0;
    addToCartButton.icon = [UIImage imageNamed:@"cart"];
    [_buttonvView addSubview:addToCartButton];
    
    [addToCartButton addTarget:self action:@selector(addToCartEvent) forControlEvents:UIControlEventTouchUpInside];
    
    _buttonvView.frame = CGRectMake(_buttonvView.frame.origin.x, _buttonvView.frame.origin.y, _buttonvView.frame.size.width, _buttonvView.frame.size.height + 50);
    
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, _buttonvView.frame.size.height - 5, _screenWidth, 5)];
    seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
    [_buttonvView addSubview:seperator];
}

-(void)initDetails{
    
    _artImageUrl = _artDetails.artImage;
    _selectedWidth = [_artDetails.artAvailableSizes.width objectAtIndex:0];
    _selectedHeight = [_artDetails.artAvailableSizes.height objectAtIndex:0];
    _selectedPrice = [_artDetails.artAvailableSizes.price objectAtIndex:0];
    _selectedWidthAmountPerCm = [_artDetails.artAvailableSizes.widthAmountPerCm objectAtIndex:0];
    _selectedHeightAmountPerCm = [_artDetails.artAvailableSizes.heightAmountPerCm objectAtIndex:0];
    
    _detailView = [[UIView alloc] initWithFrame:CGRectMake(0, _buttonvView.frame.origin.y + _buttonvView.frame.size.height, _screenWidth, 300)];
    //_detailView.backgroundColor = [UIColor greenColor];
    [_scrollView addSubview:_detailView];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, _detailView.frame.size.width - 60, 30)];
    //priceLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    //priceLabel.text = @"$9999.00";
    priceLabel.text = [NSString stringWithFormat:@"$%@",_selectedPrice];
    priceLabel.font = [UIFont fontWithName:@"Futura ICG" size:22];
    //priceLabel.font = [UIFont systemFontOfSize:20];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    [_detailView addSubview:priceLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(priceLabel.frame.origin.x, priceLabel.frame.origin.y + priceLabel.frame.size.height, _detailView.frame.size.width - (priceLabel.frame.origin.x * 2), priceLabel.frame.size.height)];
    //titleLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    //titleLabel.text = @"Title";
    titleLabel.text = _artName;
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //titleLabel.font = [UIFont systemFontOfSize:20];
    titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleLabel sizeToFit];
    [_detailView addSubview:titleLabel];
    
    UILabel *artistLabel = [[UILabel alloc] initWithFrame:CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y + titleLabel.frame.size.height, titleLabel.frame.size.width, titleLabel.frame.size.height)];
    //artistLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    //artistLabel.text = @"Artist";
    artistLabel.text = _artArtistName;
    artistLabel.textColor = [UIColor grayColor];
    //artistLabel.font = [UIFont systemFontOfSize:20];
    artistLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    artistLabel.textAlignment = NSTextAlignmentLeft;
    [artistLabel sizeToFit];
    [_detailView addSubview:artistLabel];
    
    UIView *seperator1 = [[UIView alloc] initWithFrame:CGRectMake(priceLabel.frame.origin.x, artistLabel.frame.origin.y + artistLabel.frame.size.height + 15, priceLabel.frame.size.width, 2)];
    seperator1.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
    [_detailView addSubview:seperator1];
    
    UILabel *availableSizesLabel = [[UILabel alloc] initWithFrame:CGRectMake(artistLabel.frame.origin.x, artistLabel.frame.origin.y + artistLabel.frame.size.height + 30, priceLabel.frame.size.width, 20)];
    //aspectRationLabel.backgroundColor = [UIColor greenColor];
    availableSizesLabel.text = @"View available sizes:";
    //availableSizesLabel.font = [UIFont systemFontOfSize:18];
    availableSizesLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_detailView addSubview:availableSizesLabel];
    
    /*
    UIButton *sizesButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sizesButton.frame = CGRectMake(availableSizesLabel.frame.origin.x, availableSizesLabel.frame.origin.y + availableSizesLabel.frame.size.height + 10, priceLabel.frame.size.width, 50);
    sizesButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    sizesButton.layer.cornerRadius = sizesButton.frame.size.height / 2.5;
    sizesButton.layer.borderColor = [UIColor blackColor].CGColor;
    sizesButton.layer.borderWidth = 1;
    //sizesButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    //sizesButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    //sizesButton.layer.shadowOpacity =  0.2;
    //sizesButton.layer.shadowRadius =   5.0;
    [sizesButton setTitle:@"Small: 30cm x 30cm" forState:UIControlStateNormal];
    [_detailView addSubview:sizesButton];
    */
    
    /*
     _filterButton = [DropDown buttonWithType:UIButtonTypeCustom];
     _filterButton.frame = CGRectMake((_mainNavigationBarView.frame.size.width / 2) - (_mainNavigationBarView.frame.size.width / 4), (_mainNavigationBarView.frame.size.height / 2) - (height / 2), _mainNavigationBarView.frame.size.width / 2, height);
     _filterButton.delegate = self;
     //filterButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
     //filterButton.icon = [UIImage imageNamed:@"art-finder"];
     _filterButton.titleArray = titleArray;
     [_filterButton setTitle:@"Fixed" forState:UIControlStateNormal];
     _filterButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:20];
     [_mainNavigationBarView addSubview:_filterButton];
     
     //[filterButton addTarget:self action:@selector(showFilter) forControlEvents:UIControlEventTouchUpInside];
     */
    DropDownV2 *sizesButton = [DropDownV2 buttonWithType:UIButtonTypeCustom];
    sizesButton.frame = CGRectMake(availableSizesLabel.frame.origin.x, availableSizesLabel.frame.origin.y + availableSizesLabel.frame.size.height + 10, priceLabel.frame.size.width, 40);
    sizesButton.delegate = self;
    //filterButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    //filterButton.icon = [UIImage imageNamed:@"art-finder"];
    sizesButton.layer.cornerRadius = sizesButton.frame.size.height / 2.5;
    sizesButton.layer.borderColor = [UIColor blackColor].CGColor;
    sizesButton.layer.borderWidth = 1;
    sizesButton.titleArray = _artDetails.artAvailableSizes.label;
    sizesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    sizesButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [sizesButton setTitle:[NSString stringWithFormat:@"%@",[_artDetails.artAvailableSizes.label objectAtIndex:0]] forState:UIControlStateNormal];
    [sizesButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //sizesButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:20];
    sizesButton.titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_detailView addSubview:sizesButton];
    
    UIImageView *downDirection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down-Direction"]];
    downDirection.frame = CGRectMake(sizesButton.frame.size.width - (sizesButton.frame.size.height - 10), (sizesButton.frame.size.height / 2) - 5, 10, 10);
    downDirection.userInteractionEnabled = NO;
    [sizesButton addSubview:downDirection];
    
    UILabel *installationLabel = [[UILabel alloc] initWithFrame:CGRectMake(sizesButton.frame.origin.x, sizesButton.frame.origin.y + sizesButton.frame.size.height + 20, availableSizesLabel.frame.size.width, availableSizesLabel.frame.size.height)];
    //aspectRationLabel.backgroundColor = [UIColor greenColor];
    installationLabel.text = @"Install this artwork?";
    installationLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_detailView addSubview:installationLabel];
    
    UILabel *installationDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(installationLabel.frame.origin.x, installationLabel.frame.origin.y + installationLabel.frame.size.height, availableSizesLabel.frame.size.width, availableSizesLabel.frame.size.height)];
    //aspectRationLabel.backgroundColor = [UIColor greenColor];
    installationDescriptionLabel.text = @"*Installation will incur additional fees.";
    installationDescriptionLabel.font = [UIFont fontWithName:@"Futura ICG" size:10];
    [installationDescriptionLabel sizeToFit];
    [_detailView addSubview:installationDescriptionLabel];
    
    /**** toggle button ****/
    
    NSArray *installationOptionArray = [NSArray arrayWithObjects: @"No", @"Yes", nil];
    UISegmentedControl *installationSegmentedControl = [[UISegmentedControl alloc] initWithItems:installationOptionArray];
    installationSegmentedControl.frame = CGRectMake((sizesButton.frame.origin.x + sizesButton.frame.size.width) - 90, installationLabel.frame.origin.y, 90, 30);
    [installationSegmentedControl addTarget:self action:@selector(installationSegment:) forControlEvents: UIControlEventValueChanged];
    installationSegmentedControl.selectedSegmentIndex = 0;
    [_detailView addSubview:installationSegmentedControl];
    
    /*
    UIButton *installationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    installationButton.frame = CGRectMake(installationLabel.frame.origin.x, installationLabel.frame.origin.y + installationLabel.frame.size.height + 10, sizesButton.frame.size.width, sizesButton.frame.size.height);
    installationButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    installationButton.layer.cornerRadius = sizesButton.frame.size.height / 2.5;
    installationButton.layer.borderColor = [UIColor blackColor].CGColor;
    installationButton.layer.borderWidth = 1;
    //sizesButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    //sizesButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    //sizesButton.layer.shadowOpacity =  0.2;
    //sizesButton.layer.shadowRadius =   5.0;
    [installationButton setTitle:@"Select An Installation Option" forState:UIControlStateNormal];
    [_detailView addSubview:installationButton];
    
    //[sizesButton addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    */
    
    /*
    NSArray *installationArray = @[@"No, I don't want this installed.", @"Yes, I would like this installed."];
    
    DropDownV2 *installationButton = [DropDownV2 buttonWithType:UIButtonTypeCustom];
    installationButton.frame = CGRectMake(installationLabel.frame.origin.x, installationLabel.frame.origin.y + installationLabel.frame.size.height + 10, sizesButton.frame.size.width, sizesButton.frame.size.height);
    installationButton.delegate = self;
    //filterButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    //filterButton.icon = [UIImage imageNamed:@"art-finder"];
    installationButton.layer.cornerRadius = sizesButton.frame.size.height / 2.5;
    installationButton.layer.borderColor = [UIColor blackColor].CGColor;
    installationButton.layer.borderWidth = 1;
    installationButton.titleArray = installationArray;
    installationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    installationButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [installationButton setTitle:[NSString stringWithFormat:@"%@",[installationArray objectAtIndex:0]] forState:UIControlStateNormal];
    [installationButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    installationButton.titleLabel.font = [UIFont fontWithName:@"YOUR FONTNAME" size:20];
    [_detailView addSubview:installationButton];
    */
    UILabel *quantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(installationLabel.frame.origin.x, installationLabel.frame.origin.y + installationLabel.frame.size.height + 40, availableSizesLabel.frame.size.width, availableSizesLabel.frame.size.height)];
    //quantityLabel.backgroundColor = [UIColor greenColor];
    quantityLabel.text = @"Quantity";
    quantityLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    [_detailView addSubview:quantityLabel];
    
    UIButton *increaseButton = [UIButton buttonWithType:UIButtonTypeSystem];
    increaseButton.frame = CGRectMake(sizesButton.frame.origin.x + sizesButton.frame.size.width - 30, quantityLabel.frame.origin.y, 30, 30);
    increaseButton.backgroundColor = [UIColor whiteColor];
    increaseButton.layer.borderColor = [UIColor grayColor].CGColor;
    increaseButton.layer.borderWidth = 1;
    increaseButton.tag = 1;
    [increaseButton setTitle:@"+" forState:UIControlStateNormal];
    [increaseButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_detailView addSubview:increaseButton];
    
    [increaseButton addTarget:self action:@selector(quantityCounterEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    _quantityCounterLabel = [[UILabel alloc] initWithFrame:CGRectMake(increaseButton.frame.origin.x - increaseButton.frame.size.width, increaseButton.frame.origin.y, increaseButton.frame.size.width, increaseButton.frame.size.height)];
    //quantityLabel.backgroundColor = [UIColor greenColor];
    _quantityCounterLabel.text = @"1";
    _quantityCounterLabel.font = [UIFont fontWithName:@"Futura ICG" size:16];
    _quantityCounterLabel.textAlignment = NSTextAlignmentCenter;
    [_detailView addSubview:_quantityCounterLabel];
    
    UIButton *decreaseButton = [UIButton buttonWithType:UIButtonTypeSystem];
    decreaseButton.frame = CGRectMake(_quantityCounterLabel.frame.origin.x - _quantityCounterLabel.frame.size.width, _quantityCounterLabel.frame.origin.y, _quantityCounterLabel.frame.size.width, _quantityCounterLabel.frame.size.height);
    decreaseButton.backgroundColor = [UIColor whiteColor];
    decreaseButton.layer.borderColor = [UIColor grayColor].CGColor;
    decreaseButton.layer.borderWidth = 1;
    decreaseButton.tag = 0;
    [decreaseButton setTitle:@"-" forState:UIControlStateNormal];
    [decreaseButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [_detailView addSubview:decreaseButton];
    
    [decreaseButton addTarget:self action:@selector(quantityCounterEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(quantityLabel.frame.origin.x, quantityLabel.frame.origin.y + quantityLabel.frame.size.height + 30, quantityLabel.frame.size.width, 2)];
    seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
    [_detailView addSubview:seperator];
    
    UILabel *descriptionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(seperator.frame.origin.x, seperator.frame.origin.y + seperator.frame.size.height + 15, seperator.frame.size.width, 200)];
    //descriptionsLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.5];
    descriptionsLabel.text = _artDetails.artDescription;
    descriptionsLabel.font = [UIFont fontWithName:@"Futura ICG" size:14];
    descriptionsLabel.numberOfLines = 0;
    descriptionsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [descriptionsLabel sizeToFit];
    [_detailView addSubview:descriptionsLabel];
    
    _detailView.frame = CGRectMake(_detailView.frame.origin.x, _detailView.frame.origin.y, _detailView.frame.size.width, descriptionsLabel.frame.origin.y + descriptionsLabel.frame.size.height + 50);
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _detailView.frame.origin.y + _detailView.frame.size.height);
}

-(void)initBackButton{
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 80, 40)];
    backView.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    [_scrollView addSubview:backView];
    
    UIBezierPath *cornerMaskPath = [UIBezierPath bezierPathWithRoundedRect:backView.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(backView.frame.size.height / 2, backView.frame.size.height / 2)];
    
    CAShapeLayer *cornerMaskLayer = [[CAShapeLayer alloc] init];
    cornerMaskLayer.frame = backView.bounds;
    cornerMaskLayer.path  = cornerMaskPath.CGPath;
    backView.layer.mask = cornerMaskLayer;
    
    cUIButton *backButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(10, -10, backView.frame.size.width - 20, backView.frame.size.height + 20);
    backButton.icon = [UIImage imageNamed:@"left-arrow"];
    backButton.clipsToBounds = YES;
    [backView addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
}

-(void)showUnityAR{
    
    [_delegate showUnityAR:self artImageUrl:_artImageUrl selectedWidth:_selectedWidth selectedHeight:_selectedHeight selectedPrice:_selectedPrice selectedWidthAmountPerCm:_selectedWidthAmountPerCm selectedHeightAmountPerCm:_selectedHeightAmountPerCm];
}

-(void)addToFaveEvent{

    [_delegate addToFaveEvent:self];
}

-(void)addToCartEvent{

}

-(void)didSelectItem:(DropDownV2 *)dropDown index:(NSInteger)index{
    
    _selectedWidth = [_artDetails.artAvailableSizes.width objectAtIndex:index];
    _selectedHeight = [_artDetails.artAvailableSizes.height objectAtIndex:index];
    _selectedPrice = [_artDetails.artAvailableSizes.price objectAtIndex:index];
    _selectedWidthAmountPerCm = [_artDetails.artAvailableSizes.widthAmountPerCm objectAtIndex:index];
    _selectedHeightAmountPerCm = [_artDetails.artAvailableSizes.heightAmountPerCm objectAtIndex:index];
    
    [dropDown setTitle:[NSString stringWithFormat:@"%@",[dropDown.titleArray objectAtIndex:index]] forState:UIControlStateNormal];
}

- (void)installationSegment:(UISegmentedControl *)segment{
    switch (segment.selectedSegmentIndex) {
        case 0:
            
            _isInstallation = NO;
            break;
        case 1:
            
            _isInstallation = YES;
            break;
        default:
            break;
    }
}

-(void)back{
    
    [_delegate backToHomePage:self];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
