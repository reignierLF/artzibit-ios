//
//  CompassViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 12/5/17.
//
//

#import "CompassViewController.h"

@interface CompassViewController ()

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) cUIScrollView *horizontalPager;
@property (nonatomic, strong) cUIScrollView *verticalPager;
@property (nonatomic, strong) UIView *northPage;
@property (nonatomic, strong) UIView *southPage;
@property (nonatomic, strong) UIView *eastPage;
@property (nonatomic, strong) UIView *westPage;
@property (nonatomic, strong) UIView *centerPage;

@end

@implementation CompassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initCompassNavigationPager];
    [self initPageViewControllers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _navigationBarHeight = 65;
    _navigationBarColor = [UIColor colorWithRed:72.0f/255.0f green:207.0f/255.0f blue:174.0f/255.0f alpha:1.0];
    _backgroundColor = [UIColor colorWithRed:246.0f/255.0f green:247.0f/255.0f blue:251.0f/255.0f alpha:1.0];
    
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height;

    self.view.backgroundColor = [UIColor whiteColor];
    
    //_api = [[Api alloc] init];
    //_uc = [[UserCredentials alloc] init];
    
    //_ls = [[LoadingScreen alloc] init];
}

-(void)initCompassNavigationPager{
    
    _horizontalPager = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    _horizontalPager.backgroundColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
    _horizontalPager.delegate = self;
    _horizontalPager.bounces = NO;
    _horizontalPager.scrollEnabled = YES;
    _horizontalPager.pagingEnabled = YES;
    _horizontalPager.directionalLockEnabled = YES;
    _horizontalPager.contentSize = CGSizeMake(_horizontalPager.frame.size.width * 3, _horizontalPager.frame.size.height);
    _horizontalPager.showsHorizontalScrollIndicator = NO;
    //_horizontalPager.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_horizontalPager];
    
    [_horizontalPager scrollRectToVisible:CGRectMake(_horizontalPager.frame.size.width, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:NO];
    
    _verticalPager = [[cUIScrollView alloc] initWithFrame:CGRectMake(_horizontalPager.frame.size.width, 0, _screenWidth, _screenHeight)];
    _verticalPager.backgroundColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
    _verticalPager.delegate = self;
    _verticalPager.bounces = NO;
    _verticalPager.pagingEnabled = YES;
    _verticalPager.directionalLockEnabled = YES;
    _verticalPager.contentSize = CGSizeMake(_verticalPager.frame.size.width, _verticalPager.frame.size.height * 3);
    //_verticalPager.showsHorizontalScrollIndicator = YES;
    _verticalPager.showsVerticalScrollIndicator = NO;
    [_horizontalPager addSubview:_verticalPager];
    
    [_verticalPager scrollRectToVisible:CGRectMake(0, _verticalPager.frame.size.height, _verticalPager.frame.size.width, _verticalPager.frame.size.height) animated:NO];
    
    float width = _verticalPager.frame.size.width;
    float height = _verticalPager.frame.size.height;
    
    _northPage = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    _northPage.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.8];
    [_verticalPager addSubview:_northPage];
    
    _southPage = [[UIView alloc] initWithFrame:CGRectMake(0, height * 2, width, height)];
    _southPage.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.8];
    [_verticalPager addSubview:_southPage];
    
    _eastPage = [[UIView alloc] initWithFrame:CGRectMake(width * 2, 0, width, height)];
    _eastPage.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.8];
    [_horizontalPager addSubview:_eastPage];
    
    _westPage = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    _westPage.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.8];
    [_horizontalPager addSubview:_westPage];
    
    _centerPage = [[UIView alloc] initWithFrame:CGRectMake(0, height, width, height)];
    _centerPage.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.8];
    [_verticalPager addSubview:_centerPage];
}

-(void)initPageViewControllers{

    _main = [[MainViewController alloc] init];
    _main.delegate = self;
    _main.screenWidth = _screenWidth;
    _main.screenHeight = _screenHeight;
    _main.navigationBarHeight = _navigationBarHeight;
    _main.navigationBarColor = _navigationBarColor;
    _main.backgroundColor = _backgroundColor;
    //[_centerPage addSubview:_main.view];
    
    UINavigationController *mainNavigationController = [[UINavigationController alloc] initWithRootViewController:_main];
    mainNavigationController.view.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    mainNavigationController.navigationBarHidden = YES;
    //[accountNavigationController.view addSubview:_login.view];
    [_centerPage addSubview:mainNavigationController.view];
    
    _main.mainNavigationController = mainNavigationController;
    
    _faves = [[FavesViewController alloc] init];
    _faves.delegate = self;
    _faves.screenWidth = _screenWidth;
    _faves.screenHeight = _screenHeight;
    _faves.navigationBarHeight = _navigationBarHeight;
    _faves.navigationBarColor = _navigationBarColor;
    _faves.backgroundColor = _backgroundColor;
    //[_westPage addSubview:_faves.view];
    
    UINavigationController *favesNavigationController = [[UINavigationController alloc] initWithRootViewController:_faves];
    favesNavigationController.view.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    favesNavigationController.navigationBarHidden = YES;
    //[accountNavigationController.view addSubview:_login.view];
    [_westPage addSubview:favesNavigationController.view];
    
    _faves.favesNavigationController = favesNavigationController;
    
    /*
     * ART FINDER DISABLED
     
    _artFinder = [[ArtFinderViewController alloc] init];
    _artFinder.delegate = self;
    _artFinder.screenWidth = _screenWidth;
    _artFinder.screenHeight = _screenHeight;
    _artFinder.navigationBarHeight = _navigationBarHeight;
    _artFinder.navigationBarColor = _navigationBarColor;
    _artFinder.backgroundColor = _backgroundColor;
    [_westPage addSubview:_artFinder.view];
    */
    _cart = [[CartViewController alloc] init];
    _cart.delegate = self;
    _cart.screenWidth = _screenWidth;
    _cart.screenHeight = _screenHeight;
    _cart.navigationBarHeight = _navigationBarHeight;
    _cart.navigationBarColor = _navigationBarColor;
    _cart.backgroundColor = _backgroundColor;
    [_eastPage addSubview:_cart.view];
    
    _login = [[LoginViewController alloc] init];
    _login.delegate = self;
    _login.screenWidth = _screenWidth;
    _login.screenHeight = _screenHeight;
    _login.navigationBarHeight = _navigationBarHeight;
    _login.navigationBarColor = _navigationBarColor;
    _login.backgroundColor = _backgroundColor;
    //[_northPage addSubview:_login.view];
    
    //_account = [[AccountViewController alloc] init];
    //_account.delegate = self;
    //_account.screenWidth = _screenWidth;
    //_account.screenHeight = _screenHeight;
    //_account.navigationBarHeight = _navigationBarHeight;
    //_account.navigationBarColor = _navigationBarColor;
    //_account.backgroundColor = _backgroundColor;
    //[_northPage addSubview:_login.view];
    
    UINavigationController *accountNavigationController = [[UINavigationController alloc] initWithRootViewController:_login];
    accountNavigationController.view.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    accountNavigationController.navigationBarHidden = YES;
    //[accountNavigationController.view addSubview:_login.view];
    [_northPage addSubview:accountNavigationController.view];
    
    _login.accountNavigationController = accountNavigationController;
    
    _filter = [[FilterViewController alloc] init];
    //_filter.delegate = self;
    _filter.screenWidth = _screenWidth;
    _filter.screenHeight = _screenHeight;
    _filter.navigationBarHeight = _navigationBarHeight;
    _filter.navigationBarColor = _navigationBarColor;
    _filter.backgroundColor = _backgroundColor;
    
    UINavigationController *filterNavigationController = [[UINavigationController alloc] initWithRootViewController:_filter];
    filterNavigationController.view.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    filterNavigationController.navigationBarHidden = YES;
    //[filterNavigationController.view addSubview:_filter.view];
    [_southPage addSubview:filterNavigationController.view];
    
    _filter.filterNavigationController = filterNavigationController;
}

#pragma mark Events

-(void)horizontalPagerEvent:(UIButton*)sender{
    
    if(sender.tag == 1){
        
        [_horizontalPager scrollRectToVisible:CGRectMake(0, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
    }else if(sender.tag == 2){
        
        [_horizontalPager scrollRectToVisible:CGRectMake(_horizontalPager.frame.size.width * 2, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
    }else if(sender.tag == 0){
        
        [_horizontalPager scrollRectToVisible:CGRectMake(_horizontalPager.frame.size.width, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
    }
}

#pragma mark Delegates

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    if(_verticalPager.contentOffset.y < scrollView.frame.size.height){
    
        _horizontalPager.scrollEnabled = NO;
        NSLog(@"north page disable side scroll");
    }else if(_verticalPager.contentOffset.y > scrollView.frame.size.height){
    
        _horizontalPager.scrollEnabled = NO;
        NSLog(@"south page disable side scroll");
    }else{
    
        _horizontalPager.scrollEnabled = YES;
        NSLog(@"center page");
    }
}

-(void)goToFaves:(MainViewController *)main{

    [_horizontalPager scrollRectToVisible:CGRectMake(0, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
}

-(void)gotToCart:(MainViewController *)main{

    [_horizontalPager scrollRectToVisible:CGRectMake(_horizontalPager.frame.size.width * 2, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
}

-(void)goToMain{

    [_horizontalPager scrollRectToVisible:CGRectMake(_horizontalPager.frame.size.width, 0, _horizontalPager.frame.size.width, _horizontalPager.frame.size.height) animated:YES];
}

-(void)goToMain:(AccountViewController *)account{

    NSLog(@"ok from compass (account)");
    [_verticalPager scrollRectToVisible:CGRectMake(0, _verticalPager.frame.size.height, _verticalPager.frame.size.width, _verticalPager.frame.size.height) animated:YES];
}

-(void)goToMainFromLogin:(LoginViewController *)login{

    NSLog(@"ok from compass (login)");
    [_verticalPager scrollRectToVisible:CGRectMake(0, _verticalPager.frame.size.height, _verticalPager.frame.size.width, _verticalPager.frame.size.height) animated:YES];
}

-(void)popARView:(MainViewController *)main artImageUrl:(NSString *)artImageUrl selectedWidth:(NSString *)selectedWidth selectedHeight:(NSString *)selectedHeight selectedPrice:(NSString *)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm{

    NSLog(@"Show Unity3d View");
    
    [_delegate popUnityAR:self artImageUrl:artImageUrl selectedWidth:selectedWidth selectedHeight:selectedHeight selectedPrice:selectedPrice selectedWidthAmountPerCm:selectedWidthAmountPerCm selectedHeightAmountPerCm:selectedHeightAmountPerCm];
    
    _artImageUrl = artImageUrl;
    _selectedWidth = selectedWidth;
    _selectedHeight = selectedHeight;
    _selectedPrice = selectedPrice;
    _selectedWidthAmountPerCm = selectedWidthAmountPerCm;
    _selectedHeightAmountPerCm = selectedHeightAmountPerCm;
    
    [UIView animateWithDuration: 0.35
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         _unityView.frame = CGRectMake(0,
                                                       0,
                                                       _unityView.frame.size.width,
                                                       _unityView.frame.size.height);
                     }completion:nil];
}

-(void)enableCompassNavigationFromMain:(MainViewController *)main{

    _horizontalPager.scrollEnabled = YES;
    _verticalPager.scrollEnabled = YES;
}

-(void)disableCompassNavigationFromMain:(MainViewController *)main{

    _horizontalPager.scrollEnabled = NO;
    _verticalPager.scrollEnabled = NO;
}

-(void)createdNewFavoriteId:(MainViewController *)main{

    [_faves reset];
}

-(void)updatedFavorited:(MainViewController *)main{

    [_faves reset];
}

-(void)enableCompassNavigationFromFaves:(FavesViewController *)faves{

    _horizontalPager.scrollEnabled = YES;
    _verticalPager.scrollEnabled = YES;
}

-(void)disableCompassNavigationFromFaves:(FavesViewController *)faves{

    _horizontalPager.scrollEnabled = NO;
    _verticalPager.scrollEnabled = NO;
}
@end
