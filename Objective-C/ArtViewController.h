//
//  ArtViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 21/6/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIScrollView.h"
#import "cUIButton.h"
#import "cUITextField.h"
#import "ImageLoader.h"
#import "DropDownV2.h"

#import "Api.h"
#import "ArtDetails.h"

@class ArtViewController;

@protocol ArtDelegate
-(void)showUnityAR:(ArtViewController*)art artImageUrl:(NSString*)artImageUrl selectedWidth:(NSString*)selectedWidth selectedHeight:(NSString*)selectedHeight selectedPrice:(NSString*)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm;
-(void)addToFaveEvent:(ArtViewController*)art;
-(void)backToHomePage:(ArtViewController*)art;
@end

@interface ArtViewController : UIViewController <UIScrollViewDelegate, DropDownV2Delegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) NSString *artId;
@property (nonatomic, strong) NSString *artName;
@property (nonatomic, strong) NSString *artArtistName;
@property (nonatomic, strong) NSString *artImage;
//@property (nonatomic, strong) NSString *artPrice;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) ArtDetails *artDetails;

@property (nonatomic, weak) id <ArtDelegate> delegate;

@end
