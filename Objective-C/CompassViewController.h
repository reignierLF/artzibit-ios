//
//  CompassViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 12/5/17.
//
//

#import <UIKit/UIKit.h>

#import "cUIScrollView.h"

#import "MainViewController.h"
#import "FilterViewController.h"
#import "ArtFinderViewController.h"
#import "CartViewController.h"
#import "LoginViewController.h"
//#import "AccountViewController.h"
#import "FavesViewController.h"

@class CompassViewController;

@protocol CompassDelegate
-(void)popUnityAR:(CompassViewController*)compass artImageUrl:(NSString *)artImageUrl selectedWidth:(NSString *)selectedWidth selectedHeight:(NSString *)selectedHeight selectedPrice:(NSString *)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm;
@end

@interface CompassViewController : UIViewController <UIScrollViewDelegate, MainDelegate, FavesDelegate, ArtFinderDelegate, CartDelegate, LoginDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIView *rootView;
@property (nonatomic, strong) UIView *unityView;

@property (nonatomic, strong) NSString *artImageUrl;
@property (nonatomic, strong) NSString *selectedWidth;
@property (nonatomic, strong) NSString *selectedHeight;
@property (nonatomic, strong) NSString *selectedPrice;
@property (nonatomic, strong) NSString *selectedWidthAmountPerCm;
@property (nonatomic, strong) NSString *selectedHeightAmountPerCm;

@property (nonatomic, strong) MainViewController *main;
@property (nonatomic, strong) FilterViewController *filter;
@property (nonatomic, strong) ArtFinderViewController *artFinder;
@property (nonatomic, strong) CartViewController *cart;
@property (nonatomic, strong) LoginViewController *login;
//@property (nonatomic, strong) AccountViewController *account;
@property (nonatomic, strong) FavesViewController *faves;

@property (nonatomic, weak) id <CompassDelegate> delegate;

@end
