//
//  MainViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 10/5/17.
//
//

#import "MainViewController.h"
#import "Art.h"

@interface MainViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;
@property (nonatomic) BOOL *isFixed;
@property (nonatomic, strong) Carousel *carousel;
@property (nonatomic, strong) DropDown *filterButton;
@property (nonatomic, strong) DropDownV2 *filterButtonV2;
@property (nonatomic, strong) LoadingScreen *ls;

@property (nonatomic, strong) Art *art;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initCarousel];
    [self initButtomBarButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    _api = [[Api alloc] init];
    _ls = [[LoadingScreen alloc] init];
    
    self.view.backgroundColor = _backgroundColor;

    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    float height = _mainNavigationBarView.frame.size.height - 10;
    float width = height;
    
    cUIButton *artFinderButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    artFinderButton.frame = CGRectMake(0, (_mainNavigationBarView.frame.size.height / 2) - (height / 2), width, height);
    //artFinderButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    artFinderButton.icon = [UIImage imageNamed:@"star"];
    [_mainNavigationBarView addSubview:artFinderButton];
    
    [artFinderButton addTarget:self action:@selector(navigateToFaves) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *cartButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    cartButton.frame = CGRectMake(_mainNavigationBarView.frame.size.width - width, artFinderButton.frame.origin.y, width, height);
    //cartButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    cartButton.icon = [UIImage imageNamed:@"cart"];
    [_mainNavigationBarView addSubview:cartButton];
    
    [cartButton addTarget:self action:@selector(navigateToCart) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Artzibit";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"Futura ICG" size:22];
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initCarousel{
    
    [_ls loadingScreen:^{
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            [_api allArtsByCategory:1 isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"all art = %@", jsonDictionary);
                
                _art = [[Art alloc] initWithDictionary:jsonDictionary];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(_art.status){
                            
                            _carousel = [[Carousel alloc] initWithFrame:CGRectMake(0, _navigationBarHeight + 20, _screenWidth, _screenWidth)];
                            _carousel.delegate = self;
                            _carousel.size = _art.size;
                            _carousel.artId = _art.artId;
                            _carousel.artName = _art.artName;
                            _carousel.artArtistName = _art.artArtistName;
                            _carousel.artImage = _art.artImage;
                            _carousel.artPrice =_art.artPrice;
                            //carousel.allAddress = _ae.address;
                            //carousel.allEventImageUrl = _ae.eventImageUrl;
                            //carousel.allEventId = _ae.eventId;
                            //carousel.allDateTime = _ae.dateTime;
                            [self.view addSubview:_carousel];
                        }else{
                            
                            NSLog(@"art status false");
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* retry = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            
                            [self initCarousel];
                        }];
                        [alertController addAction:retry];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                        NSLog(@"failed to load data");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)initButtomBarButtons{

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, (_navigationBarHeight * 2) + 20 + _screenWidth, _screenWidth, (_screenWidth / 2) - _navigationBarHeight)];
    //view.backgroundColor = [UIColor greenColor];
    [self.view addSubview:view];
    
    float height = view.frame.size.height - (view.frame.size.height / 4);
    float columnWidth = view.frame.size.width / 3;
    int divider = 8;
    
    cUIButton *faveButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    faveButton.frame = CGRectMake(((columnWidth / 2) - (height / 2)) + (columnWidth / divider), (view.frame.size.height / 2) - (height / 2), height, height);
    faveButton.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:206.0f/255.0f blue:85.0f/255.0f alpha:1.0];
    faveButton.layer.cornerRadius = faveButton.frame.size.width / 2;
    faveButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    faveButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    faveButton.layer.shadowOpacity =  0.2;
    faveButton.layer.shadowRadius =   5.0;
    faveButton.icon = [UIImage imageNamed:@"star"];
    [view addSubview:faveButton];
    
    [faveButton addTarget:self action:@selector(addToFaveEvent) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *arButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    arButton.frame = CGRectMake((faveButton.frame.origin.x + columnWidth) - (columnWidth / divider), faveButton.frame.origin.y, faveButton.frame.size.width, faveButton.frame.size.height);
    arButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    arButton.layer.cornerRadius = arButton.frame.size.width / 2;
    arButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    arButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    arButton.layer.shadowOpacity =  0.2;
    arButton.layer.shadowRadius =   5.0;
    [view addSubview:arButton];
    
    arButton.label.text = @"AR";
    arButton.label.textColor = [UIColor whiteColor];
    arButton.label.font = [UIFont systemFontOfSize:26];
    
    [arButton addTarget:self action:@selector(popARView) forControlEvents:UIControlEventTouchUpInside];
    
    cUIButton *addToCartButton = [cUIButton buttonWithType:UIButtonTypeCustom];
    addToCartButton.frame = CGRectMake((faveButton.frame.origin.x + (columnWidth * 2)) - ((columnWidth / divider) * 2), arButton.frame.origin.y, arButton.frame.size.width, arButton.frame.size.height);
    addToCartButton.backgroundColor = [UIColor colorWithRed:80.0f/255.0f green:193.0f/255.0f blue:233.0f/255.0f alpha:1.0];
    addToCartButton.layer.cornerRadius = addToCartButton.frame.size.width / 2;
    addToCartButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    addToCartButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    addToCartButton.layer.shadowOpacity =  0.2;
    addToCartButton.layer.shadowRadius =   5.0;
    addToCartButton.icon = [UIImage imageNamed:@"cart"];
    [view addSubview:addToCartButton];
    
    [addToCartButton addTarget:self action:@selector(addToCartEvent) forControlEvents:UIControlEventTouchUpInside];
    
    float directionWidth = 20;
    float directionHeight = directionWidth;
    
    UIImageView *downDirection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down-Direction"]];
    downDirection.frame = CGRectMake((_screenWidth / 2) - (directionWidth / 2) , view.frame.size.height - (directionHeight / 4), directionWidth, directionHeight);
    downDirection.userInteractionEnabled = NO;
    [view addSubview:downDirection];
}

#pragma mark Events

-(void)navigateToFaves{
    
    [_delegate goToFaves:self];
    
    NSLog(@"navigate to art finder");
}

-(void)navigateToCart{
    
    [_delegate gotToCart:self];
    
    NSLog(@"navigate to cart");
}

-(void)addToCartEvent{

    ArtViewController *art = [[ArtViewController alloc] init];
    art.delegate = self;
    art.screenWidth = _screenWidth;
    art.screenHeight = _screenHeight;
    art.backgroundColor = _backgroundColor;
    art.artId = [_carousel.artId objectAtIndex:_carousel.carouselIndex];
    art.artName = [_carousel.artName objectAtIndex:_carousel.carouselIndex];
    art.artArtistName = [_carousel.artArtistName objectAtIndex:_carousel.carouselIndex];
    art.artImage = [_carousel.artImage objectAtIndex:_carousel.carouselIndex];
    [_mainNavigationController pushViewController:art animated:YES];
    
    [_delegate disableCompassNavigationFromMain:self];
    NSLog(@"Add to cart");
}

-(void)popARView{
    
    NSLog(@"Pop AR View");
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api artDetailsWithId:[_art.artId objectAtIndex:_carousel.carouselIndex] isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"art details = %@", jsonDictionary);
            
            ArtDetails *artDetails = [[ArtDetails alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    [_delegate popARView:self artImageUrl:[NSString stringWithFormat:@"%@",artDetails.artImage] selectedWidth:[NSString stringWithFormat:@"%@",[artDetails.artAvailableSizes.width objectAtIndex:0]] selectedHeight:[NSString stringWithFormat:@"%@",[artDetails.artAvailableSizes.height objectAtIndex:0]] selectedPrice:[NSString stringWithFormat:@"%@",[artDetails.artAvailableSizes.price objectAtIndex:0]] selectedWidthAmountPerCm:[NSString stringWithFormat:@"%@",[artDetails.artAvailableSizes.widthAmountPerCm objectAtIndex:0]] selectedHeightAmountPerCm:[NSString stringWithFormat:@"%@",[artDetails.artAvailableSizes.heightAmountPerCm objectAtIndex:0]]];
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    NSLog(@"failed to load data");
                }
            });
        }];
    });

    //[_delegate popARView:self];
}

-(void)addToFaveEvent{
    
    [_ls loadingScreen:^{
        
        NSLog(@"Add to faves");
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            int index = [[_art.artId objectAtIndex:_carousel.carouselIndex] intValue];
            
            NSLog(@"art ID = %d", index);
            
            [_api createUserFavoriteArtworkWithId:index isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"create favorite id = %@", jsonDictionary);
                
                _favorites = [[Favorites alloc] initWithDictionary:jsonDictionary method:POST];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(_favorites.status){
                            
                            [self refreshCarousel];
                            [_delegate createdNewFavoriteId:self];
                            
                            NSLog(@"created new favorite id");
                        }else{
                            
                            [self updateToFaveEvent];
                            
                            NSLog(@"message from post : %@",_favorites.message);
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        NSLog(@"failed to load data");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

-(void)updateToFaveEvent{

    [_ls loadingScreen:^{
        
        NSLog(@"Update to faves");
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            
            int index = [[_art.artFavoriteId objectAtIndex:_carousel.carouselIndex] intValue];
            BOOL isDeleted = [[_art.artFavorited objectAtIndex:_carousel.carouselIndex] boolValue];
            
            NSLog(@"favorite id = %d", index);
            NSLog(@"Bool = %d", isDeleted);
            
            [_api updateUserFavoriteArtworkWithFavoriteId:index deleted:isDeleted isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
                
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
                
                NSLog(@"put favorite id = %@", jsonDictionary);
                
                _favorites = [[Favorites alloc] initWithDictionary:jsonDictionary method:PUT];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if(isComplete){
                        
                        if(_favorites.status){
                            
                            [self refreshCarousel];
                            [_delegate updatedFavorited:self];
                            
                            NSLog(@"put favorite id");
                        }else{
                            
                            NSLog(@"message from put : %@",_favorites.message);
                        }
                    }else{
                        
                        NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                        
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:close];
                        
                        NSLog(@"failed to load data");
                    }
                    
                    [_ls finishLoading];
                });
            }];
        });
    }];
}

#pragma mark Delegates

-(void)didChooseEventCard:(Carousel *)carousel sender:(UIButton *)sender{
    
    NSLog(@"Button tag : %ld", (long)sender.tag);
    
    ArtViewController *art = [[ArtViewController alloc] init];
    art.delegate = self;
    art.screenWidth = _screenWidth;
    art.screenHeight = _screenHeight;
    art.backgroundColor = _backgroundColor;
    art.artId = [carousel.artId objectAtIndex:sender.tag];
    art.artName = [carousel.artName objectAtIndex:sender.tag];
    art.artArtistName = [carousel.artArtistName objectAtIndex:sender.tag];
    art.artImage = [carousel.artImage objectAtIndex:sender.tag];
    [_mainNavigationController pushViewController:art animated:YES];
    
    [_delegate disableCompassNavigationFromMain:self];
}

-(void)showUnityAR:(ArtViewController *)art artImageUrl:(NSString *)artImageUrl selectedWidth:(NSString *)selectedWidth selectedHeight:(NSString *)selectedHeight selectedPrice:(NSString *)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm{

    [_delegate popARView:self artImageUrl:[NSString stringWithFormat:@"%@",artImageUrl] selectedWidth:[NSString stringWithFormat:@"%@",selectedWidth] selectedHeight:[NSString stringWithFormat:@"%@",selectedHeight] selectedPrice:[NSString stringWithFormat:@"%@",selectedPrice] selectedWidthAmountPerCm:[NSString stringWithFormat:@"%@",selectedWidthAmountPerCm] selectedHeightAmountPerCm:[NSString stringWithFormat:@"%@",selectedHeightAmountPerCm]];
}

-(void)addToFaveEvent:(ArtViewController *)art{

    [self addToFaveEvent];
}

-(void)refreshCarousel{

    [_carousel removeFromSuperview];
    _carousel = nil;
    
    [self initCarousel];
}

-(void)didSelectItem:(DropDownV2 *)dropDown index:(NSInteger)index{
    
    dropDown.userInteractionEnabled = NO;
    
    [_carousel removeFromSuperview];
    _carousel = nil;

    [dropDown setTitle:[NSString stringWithFormat:@"%@",[dropDown.titleArray objectAtIndex:index]] forState:UIControlStateNormal];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        [_api allArtsByCategory:(int)index + 1 isComplete:^(BOOL isComplete, NSData *data, NSError *error, NSURLResponse *response, NSHTTPURLResponse *httpResponse) {
            
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error: nil];
            
            NSLog(@"all art = %@", jsonDictionary);
            
            _art = [[Art alloc] initWithDictionary:jsonDictionary];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(isComplete){
                    
                    if(_art.status){
                        
                        if(index == 0){
                        
                            _isFixed = YES;
                        }else{
                        
                            _isFixed = NO;
                        }
                        
                        [self initCarousel];
                    }else{
                        
                        NSLog(@"art status false");
                    }
                }else{
                    
                    NSString *errorMessage = [NSString stringWithFormat:@"%@\nError code : %ld",error.localizedDescription,(long)error.code];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* close = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:close];
                    
                    NSLog(@"failed to load data");
                }
                
                dropDown.userInteractionEnabled = YES;
            });
        }];
    });
}

-(void)backToHomePage:(ArtViewController *)art{

    [_delegate enableCompassNavigationFromMain:self];
}
@end
