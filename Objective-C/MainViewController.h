//
//  MainViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 10/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIButton.h"
#import "DropDown.h"
#import "DropDownV2.h"
#import "LoadingScreen.h"

#import "Carousel.h"

#import "ArtViewController.h"

#import "Api.h"
#import "Favorites.h"
#import "ArtDetails.h"

@class MainViewController;

@protocol MainDelegate
-(void)disableCompassNavigationFromMain:(MainViewController*)main;
-(void)enableCompassNavigationFromMain:(MainViewController*)main;
-(void)goToFaves:(MainViewController*)main;
-(void)gotToCart:(MainViewController*)main;
-(void)popARView:(MainViewController*)main artImageUrl:(NSString *)artImageUrl selectedWidth:(NSString *)selectedWidth selectedHeight:(NSString *)selectedHeight selectedPrice:(NSString *)selectedPrice selectedWidthAmountPerCm:(NSString *)selectedWidthAmountPerCm selectedHeightAmountPerCm:(NSString *)selectedHeightAmountPerCm;
-(void)createdNewFavoriteId:(MainViewController*)main;
-(void)updatedFavorited:(MainViewController*)main;
@end

@interface MainViewController : UIViewController <CarouselDelegate, DropDownDelegate, DropDownV2Delegate, ArtDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UINavigationController *mainNavigationController;

@property (nonatomic, strong) Api *api;
@property (nonatomic, strong) Favorites *favorites;

@property (nonatomic, weak) id <MainDelegate> delegate;

@end
