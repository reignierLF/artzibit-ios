//
//  DropDownV2.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/6/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIScrollView.h"

@class DropDownV2;

@protocol DropDownV2Delegate

-(void)didSelectItem:(DropDownV2*)dropDown index:(NSInteger)index;

@end

@interface DropDownV2 : UIButton <UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (nonatomic, readonly) UIWindow *window;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) UIImageView *dropDownIndicator;
@property (nonatomic, strong) cUIScrollView *scrollView;
@property (nonatomic, weak) id <DropDownV2Delegate> delegate;

@end
