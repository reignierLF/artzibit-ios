//
//  ParallaxEffect.h
//  TheInsiders
//
//  Created by LF-Mac-Air on 12/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ParallaxEffect : NSObject

-(void)parallax:(UIImageView*)imageView intensity:(NSInteger)intensity;

@end
