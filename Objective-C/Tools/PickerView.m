//
//  PickerView.m
//  TheInsiders
//
//  Created by LF-Mac-Air on 5/1/17.
//  Copyright © 2017 LF-Mac-Air. All rights reserved.
//

#import "PickerView.h"

@interface PickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIPickerView *salutationPickerView;
@property (nonatomic, strong) UIView *pickerTabView;

@end

@implementation PickerView

-(instancetype)init{

    self = [super init];
    
    if(self){
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
        _viewWidth = window.frame.size.width;
        _viewHeight = window.frame.size.height;
        
        self.frame = CGRectMake(0, 0, _viewWidth, _viewHeight);
        self.userInteractionEnabled = NO;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
        
        _dataArray = @[@"Mr.", @"Mrs.", @"Ms."];
        
        _salutationPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, _viewHeight - 30, _viewWidth, 140)];
        _salutationPickerView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f
                                                               green:245.0f/255.0f
                                                                blue:245.0f/255.0f
                                                               alpha:1.0f];
        _salutationPickerView.dataSource = self;
        _salutationPickerView.delegate = self;
        _salutationPickerView.showsSelectionIndicator = YES;
        [_salutationPickerView selectRow:0 inComponent:0 animated:YES];
        [self addSubview:_salutationPickerView];
        
        _pickerTabView = [[UIView alloc] initWithFrame:CGRectMake(0, _salutationPickerView.frame.origin.y - 30, _salutationPickerView.frame.size.width, 30)];
        _pickerTabView.backgroundColor = _salutationPickerView.backgroundColor;
        [self addSubview:_pickerTabView];
        
        UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeSystem];
        doneButton.frame = CGRectMake(_pickerTabView.frame.size.width - 70, 0, 70, _pickerTabView.frame.size.height);
        //doneButton.backgroundColor = [UIColor blueColor];
        [doneButton setTitle:@"Done" forState:UIControlStateNormal];
        [_pickerTabView addSubview:doneButton];
        
        [doneButton addTarget:self action:@selector(doneEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

-(void)didMoveToSuperview{

}

-(void)doneEvent{

    self.userInteractionEnabled = NO;
    
    [UIView animateWithDuration: 0.25
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
                         
                         _salutationPickerView.frame = CGRectMake(_salutationPickerView.frame.origin.x, _viewHeight + 30, _salutationPickerView.frame.size.width, _salutationPickerView.frame.size.height);
                         
                         _pickerTabView.frame = CGRectMake(_pickerTabView.frame.origin.x, _salutationPickerView.frame.origin.y - _pickerTabView.frame.size.height, _pickerTabView.frame.size.width, _pickerTabView.frame.size.height);
                     }completion:nil];
}

-(void)showPicker{
    
    self.userInteractionEnabled = YES;
    
    [UIView animateWithDuration: 0.25
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations: ^{
                         
                         self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
                         
                         _salutationPickerView.frame = CGRectMake(_salutationPickerView.frame.origin.x, _viewHeight - 140, _salutationPickerView.frame.size.width, _salutationPickerView.frame.size.height);
                         
                         _pickerTabView.frame = CGRectMake(_pickerTabView.frame.origin.x, _salutationPickerView.frame.origin.y - _pickerTabView.frame.size.height, _pickerTabView.frame.size.width, _pickerTabView.frame.size.height);
                     }completion:nil];
}

// Number of components.
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [_dataArray count];
}

// Display each row's data.
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [_dataArray objectAtIndex: row];
}

// Do something with the selected row.
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    [_delegate pickerViewDidSelectRow:self salutation:[_dataArray objectAtIndex: row]];
    
    NSLog(@"You selected this: %@", [_dataArray objectAtIndex: row]);
}
@end
