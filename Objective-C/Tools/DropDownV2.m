//
//  DropDownV2.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/6/17.
//
//

#import "DropDownV2.h"

@interface DropDownV2 ()

@property (nonatomic) float viewWidth;
@property (nonatomic) float viewHeight;

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIImage *arrowDownImage;

@property (nonatomic, strong) NSMutableArray *buttonArray;
@property (nonatomic, strong) UIView *arrowUp;

@property (nonatomic) float titleWidth;
@property (nonatomic) CGPoint touchButtonPoint;

@property (nonatomic, strong) CAShapeLayer *mask;

@end

@implementation DropDownV2

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
    }
    
    return self;
}

-(void)didMoveToSuperview{
    
    _buttonArray = [[NSMutableArray alloc] init];
    
    _window = [UIApplication sharedApplication].keyWindow;
    
    _viewWidth = _window.frame.size.width;
    _viewHeight = _window.frame.size.height;
    
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _viewWidth, _viewHeight)];
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _backgroundView.alpha = 0.0;
    _backgroundView.userInteractionEnabled = NO;
    [self addSubview:_backgroundView];
    
    UIButton *closePopUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closePopUpButton.frame = CGRectMake(0, 0, _backgroundView.frame.size.width, _backgroundView.frame.size.height);
    closePopUpButton.tag = -1;
    //button.titleLabel.textColor = [UIColor blackColor];
    [_backgroundView addSubview:closePopUpButton];
    
    [closePopUpButton addTarget:self action:@selector(pressedButton:) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _titleArray.count * 50);
    _scrollView.showsVerticalScrollIndicator = YES;
    _scrollView.layer.cornerRadius = 10;
    [_backgroundView addSubview:_scrollView];
    
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){10, 0}];
    [path addLineToPoint:(CGPoint){20, 20}];
    [path addLineToPoint:(CGPoint){0, 20}];
    [path addLineToPoint:(CGPoint){10, 0}];
    
    _mask = [CAShapeLayer new];
    _mask.path = path.CGPath;
    
    for (int i = 0; i < _titleArray.count; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 50 * i, _scrollView.frame.size.width, 50);
        button.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
        button.layer.cornerRadius = self.frame.size.height / 8;
        [button setTitle:[_titleArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = self.titleLabel.font;
        button.tag = i;
        //button.titleLabel.textColor = [UIColor blackColor];
        [_scrollView addSubview:button];
        
        [button addTarget:self action:@selector(pressedButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_buttonArray addObject:button];
        
        if(i != _titleArray.count){
            
            UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(10, button.frame.size.height - 1, button.frame.size.width - 20, 1)];
            seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
            [button addSubview:seperator];
        }
    }
    
    float directionWidth = 10;
    float directionHeight = directionWidth;
    _titleWidth = [self widthOfString:@"Fixed"];

    _arrowDownImage = [UIImage imageNamed:@"down-Direction-w"];
    
    _dropDownIndicator = [[UIImageView alloc] initWithImage:_arrowDownImage];
    _dropDownIndicator.frame = CGRectMake((self.frame.size.width / 2) + _titleWidth, (self.frame.size.height / 2) - (directionHeight / 2), directionWidth, directionHeight);
    _dropDownIndicator.userInteractionEnabled = NO;
    [self addSubview:_dropDownIndicator];
    
    //_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    //_label.backgroundColor = [UIColor orangeColor];
    //_label.textAlignment = NSTextAlignmentCenter;
    //[self addSubview:_label];
    
    UITapGestureRecognizer *oneFingerTwoTaps = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressedButton:)];
    oneFingerTwoTaps.delegate=self;
    oneFingerTwoTaps.delaysTouchesEnded = YES;
    oneFingerTwoTaps.delaysTouchesBegan = YES;
    // Set required taps and number of touches
    [oneFingerTwoTaps setNumberOfTapsRequired:1];
    //[oneFingerTwoTaps setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:oneFingerTwoTaps];
}

- (CGFloat)widthOfString:(NSString *)string{
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    _backgroundView.userInteractionEnabled = YES;
    
    [self startAnimating];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    [[[[UIApplication sharedApplication] windows] lastObject] addSubview:_backgroundView];
    
    CGPoint point = [touch locationInView:touch.view];
    CGPoint pointOnScreen = [touch.view convertPoint:point toView:_backgroundView];
    _touchButtonPoint = CGPointMake(pointOnScreen.x - point.x, pointOnScreen.y - point.y);
    NSLog(@"Point: %f, %f",_touchButtonPoint.x, _touchButtonPoint.y);
    NSLog(@"Touch");
    
    return NO;
}

-(void)startAnimating{
    
    float scrollHeight = _titleArray.count * 50;
    
    if(scrollHeight > ((_viewHeight / 6) * 3)){
    
        scrollHeight = ((_viewHeight / 6) * 3);
    }
    
    _arrowUp = [[UIView alloc] initWithFrame:CGRectMake((_touchButtonPoint.x + (self.frame.size.width / 2)) - 10, 0, 20, 20)];
    _arrowUp.backgroundColor = [UIColor whiteColor];
    _arrowUp.layer.mask = _mask;
    [_backgroundView addSubview:_arrowUp];
    
    if(_touchButtonPoint.y > (_backgroundView.frame.size.height / 2)){
        
        _scrollView.frame = CGRectMake((_touchButtonPoint.x + (self.frame.size.width / 2)) - (_scrollView.frame.size.width / 2), _touchButtonPoint.y - 5, _scrollView.frame.size.width, 0);
        
        _arrowUp.frame = CGRectMake(_arrowUp.frame.origin.x, _scrollView.frame.origin.y - 7, _arrowUp.frame.size.width, _arrowUp.frame.size.height);
        _arrowUp.transform = CGAffineTransformMakeRotation(M_PI);
    }else{
        
        _scrollView.frame = CGRectMake((_touchButtonPoint.x + (self.frame.size.width / 2)) - (_scrollView.frame.size.width / 2), _touchButtonPoint.y + self.frame.size.height + 5, _scrollView.frame.size.width, 0);
        
        _arrowUp.frame = CGRectMake(_arrowUp.frame.origin.x, _scrollView.frame.origin.y - 15, _arrowUp.frame.size.width, _arrowUp.frame.size.height);
    }
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         _backgroundView.alpha = 1.0;
                         _arrowUp.alpha = 1.0;
                         
                         if(_touchButtonPoint.y > (_backgroundView.frame.size.height / 2)){
                        
                             _scrollView.frame = CGRectMake((_touchButtonPoint.x + (self.frame.size.width / 2)) - (_scrollView.frame.size.width / 2), _touchButtonPoint.y - (5 + scrollHeight), _scrollView.frame.size.width, scrollHeight);
                         }else{
                         
                             _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, scrollHeight);
                         }
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
}

-(void)pressedButton:(UIButton*)sender{
    
    //[self setTitle:[_titleArray objectAtIndex:sender.tag] forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         _backgroundView.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                             _backgroundView.userInteractionEnabled = NO;
                             
                             [_arrowUp removeFromSuperview];
                             _arrowUp = nil;
                         }
                     }];
    
    if(sender.tag >= 0){
    
        [_delegate didSelectItem:self index:sender.tag];
        
        _titleWidth = [self widthOfString:self.titleLabel.text];
        _dropDownIndicator.frame = CGRectMake((self.frame.size.width / 2) + (_titleWidth / 2) + 5, _dropDownIndicator.frame.origin.y, _dropDownIndicator.frame.size.width, _dropDownIndicator.frame.size.height);
    }
}
@end
