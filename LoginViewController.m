//
//  LoginViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/5/17.
//
//

#import "LoginViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) cUIScrollView *loginScrollView;

@property (nonatomic, strong) UIView *signInView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initLoginView];
    [self initLoginButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{
    
    self.view.backgroundColor = _backgroundColor;
    
    //_accountNavigationController = [[UINavigationController alloc] initWithRootViewController:self];
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img-bg"]];
    backgroundImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds = YES;
    [self.view addSubview:backgroundImageView];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Artzibit Logo"]];
    logoImageView.frame = CGRectMake((_screenWidth / 2) - ((_screenWidth - 150) / 2), (_screenHeight / 4) - ((_screenWidth - 150) / 4), _screenWidth - 150, _screenWidth - 150);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    logoImageView.contentMode = UIViewContentModeScaleAspectFill;
    logoImageView.clipsToBounds = NO;
    [self.view addSubview:logoImageView];
}

-(void)initLoginButtons{
    
    UIView *loginView = [[UIView alloc] initWithFrame:CGRectMake(0, _loginScrollView.frame.size.height / 4, _loginScrollView.frame.size.width, _loginScrollView.frame.size.height - (_loginScrollView.frame.size.height / 4))];
    //loginView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    [_loginScrollView addSubview:loginView];
    
    UIButton *facebookLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    facebookLoginButton.frame = CGRectMake(30, 30, _screenWidth - 60, 60);
    facebookLoginButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.8];
    facebookLoginButton.layer.cornerRadius = facebookLoginButton.frame.size.height / 2.5;
    facebookLoginButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    facebookLoginButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    facebookLoginButton.layer.shadowOpacity =  0.2;
    facebookLoginButton.layer.shadowRadius =   5.0;
    [facebookLoginButton setTitle:@"SIGN IN WITH FACEBOOK" forState:UIControlStateNormal];
    [loginView addSubview:facebookLoginButton];
    
    [facebookLoginButton addTarget:self action:@selector(showFacebookLogin) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.frame = CGRectMake(facebookLoginButton.frame.origin.x, facebookLoginButton.frame.origin.y + facebookLoginButton.frame.size.height + 10, facebookLoginButton.frame.size.width, facebookLoginButton.frame.size.height);
    loginButton.backgroundColor = [UIColor whiteColor];
    loginButton.layer.cornerRadius = loginButton.frame.size.height / 2.5;
    loginButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    loginButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    loginButton.layer.shadowOpacity =  0.2;
    loginButton.layer.shadowRadius =   5.0;
    [loginButton setTitle:@"SIGN IN WITH EMAIL" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [loginView addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(showLogin) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpButton.frame = CGRectMake(loginButton.frame.origin.x, loginView.frame.size.height - 50, loginButton.frame.size.width, 40);
    //signUpButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"Don't have an account? Sign up"];
    
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    signUpButton.titleLabel.textColor = [UIColor blackColor];
    [signUpButton setAttributedTitle:titleString forState:UIControlStateNormal];
    [loginView addSubview:signUpButton];
    
    [signUpButton addTarget:self action:@selector(signUp) forControlEvents:UIControlEventTouchUpInside];
}

-(void)initLoginView{

    _loginScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _screenHeight - (_screenHeight / 2), _screenWidth, _screenHeight / 2)];
    //_loginScrollView.backgroundColor = [UIColor whiteColor];
    _loginScrollView.delegate = self;
    _loginScrollView.bounces = NO;
    _loginScrollView.scrollEnabled = NO;
    _loginScrollView.pagingEnabled = YES;
    _loginScrollView.userInteractionEnabled = YES;
    _loginScrollView.directionalLockEnabled = YES;
    _loginScrollView.contentSize = CGSizeMake(_loginScrollView.frame.size.width * 2, _loginScrollView.frame.size.height);
    //_loginScrollView.showsHorizontalScrollIndicator = YES;
    //_loginScrollView.showsVerticalScrollIndicator = YES;
    [self.view addSubview:_loginScrollView];
    
    _signInView = [[UIView alloc] initWithFrame:CGRectMake(_loginScrollView.frame.size.width, _loginScrollView.frame.size.height / 4, _loginScrollView.frame.size.width, _loginScrollView.frame.size.height - (_loginScrollView.frame.size.height / 4))];
    _signInView.backgroundColor = [UIColor whiteColor];
    _signInView.alpha = 0.0;
    [_loginScrollView addSubview:_signInView];
    
    cUITextField *emailTextField = [[cUITextField alloc] initWithFrame:CGRectMake(70, 30, _signInView.frame.size.width - 100, _signInView.frame.size.height / 6)];
    //emailTextField.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
    emailTextField.placeholder = @"E-mail";
    [_signInView addSubview:emailTextField];
    
    UIImageView *emailIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email"]];
    emailIconImageView.frame = CGRectMake(40, emailTextField.frame.origin.y + 10, 20, 20);
    emailIconImageView.contentMode = UIViewContentModeScaleAspectFill;
    emailIconImageView.clipsToBounds = YES;
    emailIconImageView.userInteractionEnabled = NO;
    [_signInView addSubview:emailIconImageView];
    
    UIView *emailSeperator = [[UIView alloc] initWithFrame:CGRectMake(30, emailTextField.frame.origin.y + emailTextField.frame.size.height, emailTextField.frame.size.width + 40, 1)];
    emailSeperator.backgroundColor = [UIColor grayColor];
    [_signInView addSubview:emailSeperator];
    
    cUITextField *passwordTextField = [[cUITextField alloc] initWithFrame:CGRectMake(emailTextField.frame.origin.x, emailTextField.frame.origin.y + emailTextField.frame.size.height + 10, emailTextField.frame.size.width, emailTextField.frame.size.height)];
    //passwordTextField.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5];
    passwordTextField.placeholder = @"Password";
    [_signInView addSubview:passwordTextField];
    
    UIImageView *passwordIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password"]];
    passwordIconImageView.frame = CGRectMake(40, passwordTextField.frame.origin.y + 10, 20, 20);
    passwordIconImageView.contentMode = UIViewContentModeScaleAspectFill;
    passwordIconImageView.clipsToBounds = YES;
    passwordIconImageView.userInteractionEnabled = NO;
    [_signInView addSubview:passwordIconImageView];
    
    UIView *passwordSeperator = [[UIView alloc] initWithFrame:CGRectMake(30, passwordTextField.frame.origin.y + passwordTextField.frame.size.height, emailSeperator.frame.size.width, emailSeperator.frame.size.height)];
    passwordSeperator.backgroundColor = [UIColor grayColor];
    [_signInView addSubview:passwordSeperator];
    
    UIButton *signInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    signInButton.frame = CGRectMake(30, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 30, passwordTextField.frame.size.width + 40, 50);
    signInButton.backgroundColor = [UIColor colorWithRed:237.0f/255.0f green:85.0f/255.0f blue:100.0f/255.0f alpha:1.0];
    signInButton.layer.cornerRadius = signInButton.frame.size.height / 2.5;
    signInButton.layer.shadowColor =    [UIColor blackColor].CGColor;
    signInButton.layer.shadowOffset =   CGSizeMake(0.5, 4.0);
    signInButton.layer.shadowOpacity =  0.2;
    signInButton.layer.shadowRadius =   5.0;
    [signInButton setTitle:@"SIGN IN" forState:UIControlStateNormal];
    [_signInView addSubview:signInButton];
    
    [signInButton addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    
    //UITapGestureRecognizer *hideLoginGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissLoginView)];
    //hideLoginGesture.numberOfTapsRequired = 1;
    //[self.view addGestureRecognizer:hideLoginGesture];
}

-(void)showFacebookLogin{
    
    NSLog(@"facebook login");
}

-(void)showLogin{

    [UIView animateWithDuration:0.8
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         _signInView.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
    
    _loginScrollView.scrollEnabled = YES;
    [_loginScrollView scrollRectToVisible:CGRectMake(_loginScrollView.frame.size.width, 0, _loginScrollView.frame.size.width, _loginScrollView.frame.size.height) animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSLog(@"%f = %f",_loginScrollView.contentOffset.y, _loginScrollView.contentOffset.y);

    if(_loginScrollView.contentOffset.x < (_loginScrollView.contentSize.width / 4)){
    
        _signInView.alpha = 0.0;
        _loginScrollView.scrollEnabled = NO;
        NSLog(@"login view dismissed");
    }else{
    
        _loginScrollView.scrollEnabled = YES;
        NSLog(@"login view still shown");
    }
}

-(void)signIn{

    _accountVC = [[AccountViewController alloc] init];
    _accountVC.delegate = self;
    _accountVC.screenWidth = _screenWidth;
    _accountVC.screenHeight = _screenHeight;
    _accountVC.navigationBarHeight = _navigationBarHeight;
    _accountVC.backgroundColor = _backgroundColor;
    _accountVC.navigationBarColor = _navigationBarColor;
    
    [_accountNavigationController pushViewController:_accountVC animated:YES];
}

-(void)signUp{

    _registrationVC = [[RegistrationViewController alloc] init];
    _registrationVC.delegate = self;
    _registrationVC.navigationBarHeight = _navigationBarHeight;
    _registrationVC.backgroundColor = _backgroundColor;
    _registrationVC.navigationBarColor = _navigationBarColor;
    
    [_accountNavigationController pushViewController:_registrationVC animated:YES];
}

-(void)didFinishSignUp:(RegistrationViewController *)registration{

    [self signIn];
}

-(void)goToMain:(AccountViewController *)account{

    NSLog(@"ok from login");
    [_delegate goToMainFromLogin:self];
}
@end
