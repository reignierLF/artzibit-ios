//
//  DropDown.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 17/5/17.
//
//

#import <UIKit/UIKit.h>

@class DropDown;

@protocol DropDownDelegate

-(void)didSelectItem:(DropDown*)dropDown index:(NSInteger)index;

@end

@interface DropDown : UIButton <UIGestureRecognizerDelegate>

@property (nonatomic, readonly) UIWindow *window;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, weak) id <DropDownDelegate> delegate;

@end
