//
//  ArtFinderViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIButton.h"

@class ArtFinderViewController;

@protocol ArtFinderDelegate
-(void)goToMain;
@end

@interface ArtFinderViewController : UIViewController

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, weak) id <ArtFinderDelegate> delegate;

@end
