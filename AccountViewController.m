//
//  AccountViewController.m
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 19/5/17.
//
//

#import "AccountViewController.h"

@interface AccountViewController ()

@property (nonatomic, strong) UIView *mainNavigationBarView;
@property (nonatomic, strong) UIView *topHeaderView;

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initialize];
    [self initTopHeader];
    [self initOptionList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialize{

    self.view.backgroundColor = [UIColor whiteColor];
    
    _mainNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, _navigationBarHeight)];
    _mainNavigationBarView.backgroundColor = _navigationBarColor;
    [self.view addSubview:_mainNavigationBarView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_screenWidth / 4, 10, _screenWidth / 2, _navigationBarHeight - 10)];
    //_titleLabel.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.6];
    titleLabel.text = @"Account";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [_mainNavigationBarView addSubview:titleLabel];
}

-(void)initTopHeader{
    
    _topHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, _mainNavigationBarView.frame.origin.y + _mainNavigationBarView.frame.size.height, _screenWidth, _screenHeight / 3)];
    //_topHeaderView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.2];
    [self.view addSubview:_topHeaderView];
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img-bg-2"]];
    profileImageView.frame = CGRectMake((_screenWidth / 2) - (_screenWidth / 8), 20, _screenWidth / 4, _screenWidth / 4);
    //_welcomeImageView.backgroundColor = [UIColor orangeColor];
    profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2;
    profileImageView.clipsToBounds = YES;
    
    [_topHeaderView addSubview:profileImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, profileImageView.frame.origin.y + profileImageView.frame.size.height + 10, (_screenWidth - 60), (_topHeaderView.frame.size.height - (profileImageView.frame.origin.y + profileImageView.frame.size.height)) / 2)];
    //nameLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
    nameLabel.text = @"My Name is asdasd";
    //artTitleLabel.text = _location;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont systemFontOfSize:30];
    [nameLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_topHeaderView addSubview:nameLabel];
    
    UILabel *secondaryNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(nameLabel.frame.origin.x, nameLabel.frame.origin.y + nameLabel.frame.size.height, nameLabel.frame.size.width, nameLabel.frame.size.height / 2)];
    //secondaryNameLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
    secondaryNameLabel.text = @"@aaassssddddhchajha";
    //artTitleLabel.text = _location;
    secondaryNameLabel.textAlignment = NSTextAlignmentCenter;
    secondaryNameLabel.font = [UIFont fontWithName:@"Sk-Modernist" size:14];
    [secondaryNameLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [_topHeaderView addSubview:secondaryNameLabel];
}

-(void)initOptionList{

    UIView *listView = [[UIView alloc] initWithFrame:CGRectMake(0, _topHeaderView.frame.origin.y + _topHeaderView.frame.size.height, _screenWidth, _screenHeight - (_topHeaderView.frame.origin.y + _topHeaderView.frame.size.height))];
    //listView.backgroundColor = [[UIColor brownColor] colorWithAlphaComponent:0.2];
    [self.view addSubview:listView];
    
    NSArray *listArray = @[@"Edit Account", @"My Orders", @"Payment Methods", @"Term of Service", @"FAQs", @"Logout", @""];
    
    for (int i = 0; i < listArray.count; i++) {
    
        cUIButton *optionButton = [cUIButton buttonWithType:UIButtonTypeCustom];
        optionButton.frame = CGRectMake(0, i * (listView.frame.size.height / listArray.count), _screenWidth, listView.frame.size.height / listArray.count);
        //optionButton.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.1];
        optionButton.tag = i;
        [listView addSubview:optionButton];
        
        optionButton.label.text = [NSString stringWithFormat:@"%@",[listArray objectAtIndex:i]];
        optionButton.label.textAlignment = NSTextAlignmentLeft;
        optionButton.label.frame = CGRectMake(optionButton.label.frame.size.width / 3, optionButton.label.frame.origin.y, optionButton.label.frame.size.width, optionButton.label.frame.size.height);
        
        [optionButton addTarget:self action:@selector(optionButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        if(i != (listArray.count - 2)){
            
            UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(30, optionButton.frame.origin.y + optionButton.frame.size.height, optionButton.frame.size.width - 60, 1)];
            seperator.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.2];
            [listView addSubview:seperator];
        }
    }
    
    float directionWidth = 20;
    float directionHeight = directionWidth;
    
    UIImageView *downDirection = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down-Direction"]];
    downDirection.frame = CGRectMake((_screenWidth / 2) - (directionWidth / 2) , (listView.frame.origin.y + listView.frame.size.height) - ((directionHeight / 4) + 20), directionWidth, directionHeight);
    downDirection.userInteractionEnabled = NO;
    [self.view addSubview:downDirection];
}

-(void)optionButtonEvent:(UIButton*)sender{

    NSLog(@"sender tag : %ld",(long)sender.tag);
    
    if(sender.tag == 0){ // Edit Account
    
    }else if(sender.tag == 1){ // My Orders
    
    }else if(sender.tag == 2){ // Payment Methods
    
    }else if(sender.tag == 3){ // Term of Service
    
        ToSViewController *tosVC = [[ToSViewController alloc] init];
        tosVC.screenWidth = _screenWidth;
        tosVC.screenHeight = _screenHeight;
        tosVC.navigationBarHeight = _navigationBarHeight;
        tosVC.backgroundColor = _backgroundColor;
        tosVC.navigationBarColor = _navigationBarColor;
        
        [self.navigationController pushViewController:tosVC animated:YES];
    }else if(sender.tag == 4){ // FAQ
        
        FAQViewController *faqVC = [[FAQViewController alloc] init];
        faqVC.screenWidth = _screenWidth;
        faqVC.screenHeight = _screenHeight;
        faqVC.navigationBarHeight = _navigationBarHeight;
        faqVC.backgroundColor = _backgroundColor;
        faqVC.navigationBarColor = _navigationBarColor;
        
        [self.navigationController pushViewController:faqVC animated:YES];
    }else if(sender.tag == 5){ // Logout
    
        [self.navigationController popViewControllerAnimated:YES];
    }else if(sender.tag == 6){
    
        [_delegate goToMain:self];
    }
}
@end
