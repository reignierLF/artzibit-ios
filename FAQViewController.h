//
//  FAQViewController.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 26/5/17.
//
//

#import <UIKit/UIKit.h>
#import "cUIScrollView.h"
#import "cUIButton.h"

@interface FAQViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;
@property (nonatomic) float navigationBarHeight;
@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) cUIScrollView *faqScrollView;

@end
