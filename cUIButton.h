//
//  cUIButton.h
//  Unity-iPhone
//
//  Created by LF-Mac-Air on 16/5/17.
//
//

#import <UIKit/UIKit.h>

@interface cUIButton : UIButton

@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) UILabel *label;

@end
